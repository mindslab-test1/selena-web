package ai.mindslab.engedu.adminweb.common.interceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Component
public class LoginInterceptor implements HandlerInterceptor {
   
   private static final Logger logger = LoggerFactory.getLogger(LoginInterceptor.class);      

   @Override   
   public boolean preHandle( HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
      //로그인 정보 확인 및 처리
      //1. 세션 정보 확인
      if(request.getSession().getAttribute("userInfo") == null){
         logger.info( "go login page");
         response.sendRedirect("/admin/login/main");
      }else{
         HttpSession session = request.getSession();
         session.setMaxInactiveInterval(60*30); // 동작시마다 30분씩 다시 리미트타임..늘려줌...
      }
      
      return true;   
   }

   @Override   public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,                          
                                      ModelAndView modelAndView) throws Exception {
      // TODO Auto-generated method stub   
   }

   @Override   public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
         throws Exception {
      // TODO Auto-generated method stub   }

   }
}