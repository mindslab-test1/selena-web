package ai.mindslab.engedu.adminweb.config;

import ai.mindslab.engedu.adminweb.common.view.DownloadView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import ai.mindslab.engedu.adminweb.common.interceptor.LoginInterceptor;

@Configuration
public class ServletConTextConfig  extends  WebMvcConfigurerAdapter {
	
	@Autowired
	LoginInterceptor lginInterceptor;

	@Bean
	public ViewResolver getViewResolver() {
		InternalResourceViewResolver resolver = new InternalResourceViewResolver();
		resolver.setPrefix("/WEB-INF/view/");
		resolver.setSuffix(".jsp");
		return resolver;
	}

	@Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
    }

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		// TODO Auto-generated method stub
		registry.addInterceptor(lginInterceptor)
				.addPathPatterns("/**")
				.addPathPatterns("/view/**")
				.excludePathPatterns("/login/**/");
	}


	@Bean
	public MappingJackson2JsonView jsonView(){
		return new MappingJackson2JsonView();
	}

}
