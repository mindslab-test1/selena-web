package ai.mindslab.engedu.adminweb.controller.basic;


import ai.mindslab.engedu.admin.login.dao.data.UserVO;
import ai.mindslab.engedu.admin.user.dao.data.UserAuthVO;
import ai.mindslab.engedu.admin.user.dao.data.UserRoleVO;
import ai.mindslab.engedu.admin.user.service.UserAuthService;
import ai.mindslab.engedu.adminweb.common.rsa.ZRsaSecurity;
import ai.mindslab.engedu.adminweb.controller.core.CommonController;
import ai.mindslab.engedu.bqa.commons.data.BaseResponse;
import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.security.PrivateKey;
import java.util.*;

@Slf4j
@Controller
@RequestMapping("/view/user/auth")
public class UserAuthController extends CommonController {

    @Autowired
    UserAuthService service;

    @Autowired
    BCryptPasswordEncoder passwordEncoder;

    @RequestMapping("/list")
    public ModelAndView userAuthList(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        view.setViewName("user/user_auth");
        return view;

    }


    @RequestMapping("/userDataList")
    public ModelAndView getKoenDicList(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        Map<String, Object> param = reqToHash(req);

        int rows = 0;
        int page = 0;
        int total_pages = 0;
        int startNum = 0;

        if (param.containsKey("rows") && param.get("rows") != null) {
            rows = Integer.parseInt(param.get("rows").toString());
        } else {
            rows = 30;
        }

        if (param.containsKey("page") && param.get("page") != null) {
            page = Integer.parseInt(param.get("page").toString());
        } else {
            page = 1;
        }

        if(param.containsKey("sidx") && param.get("sidx").equals("endId")){
            param.put("sidx", "end_id");
        }

        startNum = rows * (page - 1);

        int totalCount = service.selectUserAuthCount(param);

        param.put("start", startNum);
        param.put("rows", rows);

        //게시판 리스트 가져오기
        List<UserAuthVO> list = service.selectUserAuthInfo(param);


        if (list.size() > 0) {
            total_pages = (int) Math.ceil((double) totalCount / rows);
        } else {
            total_pages = 0;
        }

        view.addObject("total", total_pages);    // the total pages of the query
        view.addObject("records", totalCount);     // the total records from the query
        view.addObject("rows", list);

        view.setViewName("jsonView");

        return view;
    }


     @RequestMapping("/getUserRole")
     public ModelAndView getUserRole(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
       ModelAndView view = new ModelAndView();
       List<UserRoleVO> userRole = service.getUserRole();

       view.addObject("roleList", userRole);
       view.setViewName("jsonView");

        return view;

     }


     @RequestMapping("/selectUserAuth")
    public ModelAndView selectUserAuth(HttpServletRequest req, HttpServletResponse res) throws EngEduException {

        ModelAndView view = new ModelAndView();
        Map<String, Object> param = reqToHash(req);

        UserAuthVO userInfo = service.selectUserAuth(param);

        view.addObject("userInfo", userInfo);
        view.setViewName("jsonView");

        return view;
    }

    @RequestMapping("getRSAKeyValue")
    public ModelAndView getRSAKeyValue(HttpServletRequest req, HttpServletResponse res) throws EngEduException {

        ModelAndView view = new ModelAndView();
        view.setViewName("jsonView");

        try {

            ZRsaSecurity zSecurity = new ZRsaSecurity();
            PrivateKey privateKey = zSecurity.getPrivateKey();

            HttpSession session = req.getSession();

            if(session.getAttribute("_rsaPrivateKey_") !=null) {
               session.removeAttribute("_rsaPrivateKey_");
            }

            session.setAttribute("_rsaPrivateKey_", privateKey);

            String publicKeyModulus = zSecurity.getRsaPublicKeyModulus();
            String publicKeyExponent = zSecurity.getRsaPublicKeyExponent();

            view.addObject("publicKeyModulus", publicKeyModulus);
            view.addObject("publicKeyExponent", publicKeyExponent);
        }catch (Exception e){

        }

         return view;
    }

     @RequestMapping("/updateUserAuth")
    public ModelAndView updateUserAuth(HttpServletRequest req, HttpServletResponse res) throws Exception {

        Map<String, Object> param = reqToHash(req);

        ModelAndView view = new ModelAndView();
        HttpSession session = req.getSession();


        UserVO vo = this.getUserInfo(req);

        Set<String> keys = param.keySet();


        String id = param.get("userId").toString();
        String key = param.containsKey("userKey") == true ? param.get("userKey").toString() : null;

        ZRsaSecurity rsa  = new ZRsaSecurity();

        PrivateKey privateKey = (PrivateKey) session.getAttribute("_rsaPrivateKey_");

        session.removeAttribute("_rsaPrivateKey_"); // 키의 재사용을 막는다. 항상 새로운 키를 받도록 강제.

        String userId = rsa.decryptRSA(privateKey, id);
        String userKey = null;


        if(key !=null && key.trim().length() > 0){
            userKey = rsa.decryptRSA(privateKey, key);
            userKey = passwordEncoder.encode(userKey);

            param.put("userKey", userKey);
        }else{
            param.put("userKey", "");
        }

        param.put("userId", userId);
        param.put("updatorId", vo.getUserId());

        BaseResponse<Object> resp = service.updateUserAuth(param);

        view.addObject("result", resp);
        view.setViewName("jsonView");

        return view;

    }

     @RequestMapping("/addUserAuth")
    public ModelAndView addUserAuth(HttpServletRequest req, HttpServletResponse res) throws Exception {

         Map<String, Object> param = reqToHash(req);

        ModelAndView view = new ModelAndView();
        HttpSession session = req.getSession();

        UserVO vo = this.getUserInfo(req);


        String id = param.get("userId").toString();
        String key = param.get("userKey").toString();

        ZRsaSecurity rsa  = new ZRsaSecurity();

        PrivateKey privateKey = (PrivateKey) session.getAttribute("_rsaPrivateKey_");
        session.removeAttribute("_rsaPrivateKey_"); // 키의 재사용을 막는다. 항상 새로운 키를 받도록 강제.

         String userId = rsa.decryptRSA(privateKey, id);
        String userKey = passwordEncoder.encode(rsa.decryptRSA(privateKey, key));

        param.put("userId", userId);
        param.put("userKey", userKey);
        param.put("creatorId", vo.getUserId());
        param.put("updatorId", vo.getUserId());

        int result = service.checkExistUser(param);
         BaseResponse<Object> resp =null;
        if(result ==0){
            resp = service.addUserAuth(param);
        }else{
            resp = new BaseResponse<>();
            resp.setCode(300);
            resp.setMsg("해당 아이디를 가진 사용자가 이미 존재합니다.");
        }


        view.addObject("result", resp);
        view.setViewName("jsonView");

        return view;

     }



     @RequestMapping("/deleteUserAuth")
     public ModelAndView deleteEndToEndAdmin(HttpServletRequest req, HttpServletResponse res) throws EngEduException {

        ModelAndView view = new ModelAndView();
        Map<String, Object> param = reqToHash(req);
        UserVO vo = this.getUserInfo(req);

        BaseResponse<Object> resp = service.deleteUserAuth(param);
        view.addObject("result", resp);
        view.setViewName("jsonView");

        return view;
    }

}
