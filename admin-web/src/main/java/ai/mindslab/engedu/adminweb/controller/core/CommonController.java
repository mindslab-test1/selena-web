package ai.mindslab.engedu.adminweb.controller.core;

import ai.mindslab.engedu.admin.login.dao.data.UserVO;
import ai.mindslab.engedu.adminweb.controller.LoginController;
import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

public class CommonController {

	private static Logger logger  = LoggerFactory.getLogger(LoginController.class);
	private boolean isDebugLevle = logger.isDebugEnabled();
	
	public Map<String, Object> reqToHash(HttpServletRequest request) throws EngEduException{
		Map<String, Object> parameterMap = new HashMap<>();
		Enumeration<String> enums = request.getParameterNames();

		try {

			while(enums.hasMoreElements()){
				String paramName = (String)enums.nextElement();
				String[] parameters = request.getParameterValues(paramName);
				// Parameter가 배열일 경우
				if(parameters.length > 1){
				parameterMap.put(paramName, parameters);
				// Parameter가 배열이 아닌 경우
				}else{
				parameterMap.put(paramName, parameters[0]);
				}
			}

		}catch (Exception e){
			throw new EngEduException(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
		}
		return parameterMap;
	}


	public UserVO getUserInfo(HttpServletRequest request) throws EngEduException{

		HttpSession session = request.getSession();
		UserVO vo   =null;

		try {
			if (session.getAttribute("userInfo") != null) {

				vo = (UserVO)session.getAttribute("userInfo");
			}
		}catch (Exception e){
			throw new EngEduException(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
		}

		return vo;
	}

}
