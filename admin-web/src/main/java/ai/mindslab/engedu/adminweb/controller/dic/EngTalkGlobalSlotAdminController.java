package ai.mindslab.engedu.adminweb.controller.dic;


import ai.mindslab.engedu.admin.engtalk.dao.data.question.EngTalkAdminQuestionSVO;
import ai.mindslab.engedu.admin.engtalk.dao.data.slot.EngTalkGlobalSlotAdminVO;
import ai.mindslab.engedu.admin.engtalk.dao.data.slot.EngTalkLocalSlotAdminVO;
import ai.mindslab.engedu.admin.engtalk.service.EngFreeTalkGloballSlotAdminService;
import ai.mindslab.engedu.admin.engtalk.service.excel.EngFreeTalkGlobalSlotExcel;
import ai.mindslab.engedu.admin.login.dao.data.UserVO;
import ai.mindslab.engedu.adminweb.common.view.DownloadView;
import ai.mindslab.engedu.adminweb.common.view.ExcelView;
import ai.mindslab.engedu.adminweb.controller.core.CommonController;
import ai.mindslab.engedu.bqa.commons.data.BaseResponse;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Slf4j
@Controller
@RequestMapping("/view/engtalk/global/slot")
public class EngTalkGlobalSlotAdminController extends CommonController {

    private static Logger logger  = LoggerFactory.getLogger(EngTalkGlobalSlotAdminController.class);

    @Autowired
    EngFreeTalkGloballSlotAdminService service;

    @Autowired
    EngFreeTalkGlobalSlotExcel engFreeTalkGlobalSlotExcel;

    @Autowired
    DownloadView downloadView;

    @RequestMapping("/list")
    public ModelAndView mathDicView(HttpServletRequest req, HttpServletResponse res) {
        ModelAndView view = new ModelAndView();
        view.setViewName("engtalk/eng_talk_gslot");
        return view;

    }

    @RequestMapping("/diclist")
    public ModelAndView getEngTalkAnswerList(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        Map<String, Object> param = reqToHash(req);

        int rows = 0;
        int page = 0;
        int total_pages = 0;
        int startNum = 0;

        if (param.containsKey("rows") && param.get("rows") != null) {
            rows = Integer.parseInt(param.get("rows").toString());
        } else {
            rows = 30;
        }

        if (param.containsKey("page") && param.get("page") != null) {
            page = Integer.parseInt(param.get("page").toString());
        } else {
            page = 1;
        }

        if(param.containsKey("sidx") && param.get("sidx").equals("endId")){
            param.put("sidx", "end_id");
        }

        startNum = rows * (page - 1);

        int totalCount = service.engTalkGlobalSlotAdminCount(param);

        param.put("start", startNum);
        param.put("rows", rows);

        //게시판 리스트 가져오기
        List<EngTalkGlobalSlotAdminVO> list = service.engTalkGlobalSlotAdminList(param);


        if (list.size() > 0) {
            total_pages = (int) Math.ceil((double) totalCount / rows);
        } else {
            total_pages = 0;
        }

        view.addObject("total", total_pages);    // the total pages of the query
        view.addObject("records", totalCount);     // the total records from the query
        view.addObject("rows", list);

        view.setViewName("jsonView");

        return view;
    }

    @RequestMapping("/selectDetail")
    public ModelAndView selectDetail(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();

        Map<String, Object> param = reqToHash(req);

        EngTalkGlobalSlotAdminVO vo = service.engTalkGlobalSlotAdmin(param);
        view.addObject("data", vo);
        view.setViewName("jsonView");
        return view;
    }

    @RequestMapping("/updateEngTalkSlotAdmin")
    public ModelAndView updateEngTalkSlotAdmin(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        Map<String, Object> param = reqToHash(req);

        UserVO vo = this.getUserInfo(req);

        param.put("user_id", vo.getUserId());

        BaseResponse<Object> resp = service.updateEngTalkSlotAdmin(param);
        view.addObject("result", resp);
        view.setViewName("jsonView");

        return view;

    }

    @RequestMapping("/deleteEngTalkSlotAdmin")
    public ModelAndView deleteEngTalkSlotAdmin(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        Map<String, Object> param = reqToHash(req);

        UserVO vo = this.getUserInfo(req);
        BaseResponse<Object> resp = service.deleteEngTalkSlotAdmin(param);
        view.addObject("result", resp);
        view.setViewName("jsonView");

        return view;

    }


    @RequestMapping(value = "/downSample")
    public ModelAndView downSample(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        Map<String, Object> param = reqToHash(req);

        String fileUrl  = req.getSession().getServletContext().getRealPath("resources/sample/engTalkLocal_global_sample.xlsx");

        logger.info("down sample url : " + fileUrl);

        File file = new File(fileUrl);

        view.addObject("downloadFile", file);
        view.setView(downloadView);

        return view;
    }


    @RequestMapping(value = "/excelDown")
    public ModelAndView excelDown(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        Map<String, Object> param = reqToHash(req);
        param.put("searchType",param.get("excelDownSearchType"));
        param.put("searchText",param.get("excelDownSearchText"));

        String date = new SimpleDateFormat("yyyymmddHHmmss", Locale.KOREA).format(new Date());
        String fileName = "engTalkGlobalSlot_"+ date +".xlsx";
        String title ="영어대화 공통 슬롯관리";

        engFreeTalkGlobalSlotExcel.setTitle(title);
        Workbook book = engFreeTalkGlobalSlotExcel.createExcel(param);


        view.setView(new ExcelView());
        view.addObject("workBook", book);
        view.addObject("fileName", fileName);
        view.addObject("title", title);

        return view;
    }

    @RequestMapping(value="/insertData")
    public ModelAndView uploadExcelData(HttpServletRequest req,
                                        @RequestParam("file") MultipartFile uploadfile)throws Exception {
        ModelAndView view = new ModelAndView();
        UserVO vo = getUserInfo(req);

        EngTalkAdminQuestionSVO svo = new EngTalkAdminQuestionSVO();

        BaseResponse<Object> resp = service.uploadExcelData(vo, uploadfile);
        view.addObject("result", resp);
        view.setViewName("jsonView");

        return view;

    }

}
