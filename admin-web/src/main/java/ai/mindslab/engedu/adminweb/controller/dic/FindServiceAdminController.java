package ai.mindslab.engedu.adminweb.controller.dic;

import ai.mindslab.engedu.admin.findservice.dao.data.FindRegexVO;
import ai.mindslab.engedu.admin.findservice.service.FindServiceAdminService;
import ai.mindslab.engedu.admin.findservice.service.FindServiceExcel;
import ai.mindslab.engedu.admin.login.dao.data.UserVO;
import ai.mindslab.engedu.admin.servicement.dao.data.SelectOptVO;
import ai.mindslab.engedu.adminweb.common.view.DownloadView;
import ai.mindslab.engedu.adminweb.common.view.ExcelView;
import ai.mindslab.engedu.adminweb.controller.game.GameMentAdminController;
import ai.mindslab.engedu.adminweb.controller.core.CommonController;
import ai.mindslab.engedu.bqa.commons.data.BaseResponse;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import org.apache.poi.ss.usermodel.Workbook;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Controller
@RequestMapping("/view/find/regex")
public class FindServiceAdminController extends CommonController {

    private static Logger logger  = LoggerFactory.getLogger(GameMentAdminController.class);

    @Autowired
    FindServiceAdminService service;

    @Autowired
    FindServiceExcel findServiceExcel;

    @Autowired
    DownloadView downloadView;




    @RequestMapping("/list")
    public ModelAndView getRegexView(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        Map<String, Object> param = reqToHash(req);
        List<SelectOptVO> opt = service.selectTypeList(param);

        JSONArray array =  new JSONArray();

        for(SelectOptVO vo : opt){
            JSONObject obj = new JSONObject(vo);
            array.put(obj);
        }

        view.addObject("opts", array.toString());

        view.setViewName("regex/regex_admin");
        return view;

    }

    @RequestMapping("/diclist")
    public ModelAndView getRegexDicList(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        Map<String, Object> param = reqToHash(req);

        int rows = 0;
        int page = 0;
        int total_pages = 0;
        int startNum = 0;

        if (param.containsKey("rows") && param.get("rows") != null) {
            rows = Integer.parseInt(param.get("rows").toString());
        } else {
            rows = 30;
        }

        if (param.containsKey("page") && param.get("page") != null) {
            page = Integer.parseInt(param.get("page").toString());
        } else {
            page = 1;
        }

        if(param.containsKey("sidx") && param.get("sidx").equals("mathId")){
            param.put("sidx", "math_id");
        }

        startNum = rows * (page - 1);

        int totalCount = service.getFindServiceAdminCount(param);

        param.put("start", startNum);
        param.put("rows", rows);

        //게시판 리스트 가져오기
        List<FindRegexVO> list = service.getFindServiceAdminList(param);


        if (list.size() > 0) {
            total_pages = (int) Math.ceil((double) totalCount / rows);
        } else {
            total_pages = 0;
        }

        view.addObject("total", total_pages);    // the total pages of the query
        view.addObject("records", totalCount);     // the total records from the query
        view.addObject("rows", list);

        view.setViewName("jsonView");

        return view;
    }


    @RequestMapping("/selectDetail")
    public ModelAndView selectDetail(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();

        Map<String, Object> param = reqToHash(req);
        FindRegexVO vo = service.getFindServiceAdmin(param);
        view.addObject("data", vo);
        view.setViewName("jsonView");

        return view;

    }


    @RequestMapping("/updateFindServiceAdmin")
    public ModelAndView updateFindServiceAdmin(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        Map<String, Object> param = reqToHash(req);

        UserVO vo = this.getUserInfo(req);

        param.put("user_id", vo.getUserId());

        BaseResponse<Object> resp = service.updateFindServiceAdmin(param);
        view.addObject("result", resp);
        view.setViewName("jsonView");

        return view;

    }



    @RequestMapping("/deleteFindServiceAdmin")
    public ModelAndView deleteFindServiceAdmin(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        Map<String, Object> param = reqToHash(req);
        UserVO vo = this.getUserInfo(req);

        BaseResponse<Object> resp = service.deleteFindServiceAdmin(param);
        view.addObject("result", resp);
        view.setViewName("jsonView");

        return view;
    }


    @RequestMapping(value = "/downSample")
	public ModelAndView downSample(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        Map<String, Object> param = reqToHash(req);

        String fileUrl = req.getSession().getServletContext().getRealPath("resources/sample/findServiceRegex_sample.xlsx");

        logger.info("down sample url : " + fileUrl);

        File file = new File(fileUrl);

        view.addObject("downloadFile", file);
        view.setView(downloadView);

        return view;
    }


    @RequestMapping(value = "/excelDown")
    public ModelAndView excelDown(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        Map<String, Object> param = reqToHash(req);
        param.put("searchType",param.get("excelDownSearchType"));
        param.put("searchText",param.get("excelDownSearchText"));
        String date = new SimpleDateFormat("yyyymmddHHmmss", Locale.KOREA).format(new Date());
        String fileName = "findServiceRegex_"+ date +".xlsx";
        String title ="진입발화";

        findServiceExcel.setTitle(title);
        Workbook book = findServiceExcel.createExcel(param);


        view.setView(new ExcelView());
        view.addObject("workBook", book);
        view.addObject("fileName", fileName);
        view.addObject("title", title);

        return view;
    }


    @RequestMapping(value="/insertData")
    public ModelAndView uploadExcelData(HttpServletRequest req,
                                   @RequestParam("file") MultipartFile uploadfile)throws Exception {
        ModelAndView view = new ModelAndView();
        UserVO vo = getUserInfo(req);
        BaseResponse<Object> resp = service.uploadExcelData(vo, uploadfile);
        view.addObject("result", resp);
        view.setViewName("jsonView");

        return view;

    }


}
