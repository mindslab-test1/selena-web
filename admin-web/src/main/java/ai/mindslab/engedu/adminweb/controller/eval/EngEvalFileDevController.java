package ai.mindslab.engedu.adminweb.controller.eval;

import ai.mindslab.engedu.admin.eval.english.dao.data.EngEvalAdminVO;
import ai.mindslab.engedu.admin.eval.english.dao.data.EngEvalFileExtVO;
import ai.mindslab.engedu.admin.eval.english.service.EnglishEvalAdminService;
import ai.mindslab.engedu.adminweb.controller.core.CommonController;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import ai.mindslab.engedu.common.utils.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Controller
@Slf4j
@RequestMapping("view/eng/eval/file")
public class EngEvalFileDevController extends CommonController {

    @Value("${brain.evaluation.eng.stt.rec.dir}")
    private String sttRecDir;

    @Value("${client.server.domain}")
    private String recordDomain;

    @Value("${brain.evaluation.eng.stt.rec.dir}")
    private String recordDetailDomain;

    @Autowired
    EnglishEvalAdminService service;

    @RequestMapping("/list")
    public ModelAndView userAuthList(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        String url  = recordDomain + recordDetailDomain;
        view.addObject("sttUrl", url);
        view.setViewName("eval/eng_eval_audio_download");
        return view;

    }


    @RequestMapping("/englist")
    public ModelAndView englist(HttpServletRequest req, HttpServletResponse res) throws EngEduException {

        ModelAndView view = new ModelAndView();
        Map<String, Object> param = reqToHash(req);

        boolean isEvalIdSearch = true;

        if(StringUtils.isEmpty(String.valueOf(param.get("evalSearchText")))){
            isEvalIdSearch = false;
        }
        if(param.get("evalSearchText") == null){
            isEvalIdSearch = false;
        }
        if(param.get("evalSearchText") == ""){
            isEvalIdSearch = false;
        }

        ArrayList<Integer> evalIdList = new ArrayList<>();
        if(isEvalIdSearch){

            String[] a = String.valueOf(param.get("evalSearchText")).split(",");
            Arrays.stream(a).forEach(
                    b-> evalIdList.add( Integer.valueOf(b) )
            );
        }

        param.put("evalIdList",evalIdList);

        int rows = 0;
        int page = 0;
        int total_pages = 0;
        int startNum = 0;

        if (param.containsKey("rows") && param.get("rows") != null) {
            rows = Integer.parseInt(param.get("rows").toString());
        } else {
            rows = 30;
        }

        if (param.containsKey("page") && param.get("page") != null) {
            page = Integer.parseInt(param.get("page").toString());
        } else {
            page = 1;
        }

        if(param.containsKey("sidx") && param.get("sidx").equals("endId")){
            param.put("sidx", "end_id");
        }

        startNum = rows * (page - 1);

        int totalCount = service.engEvalAdminCount(param);

        param.put("start", startNum);
        param.put("rows", rows);

        //게시판 리스트 가져오기
        List<EngEvalAdminVO> list = service.engEvalAudioFileList(param);


        if (list.size() > 0) {
            total_pages = (int) Math.ceil((double) totalCount / rows);
        } else {
            total_pages = 0;
        }

        view.addObject("total", total_pages);    // the total pages of the query
        view.addObject("records", totalCount);     // the total records from the query
        view.addObject("rows", list);

        view.setViewName("jsonView");

        return view;
    }




    @RequestMapping("/ext")
    public String engEvalFileExt(HttpServletRequest req, HttpServletResponse res) throws  Exception{

        Map<String, Object> param = reqToHash(req);
        param.put("sttRecDir",sttRecDir);
        param.put("inputText",param.get("inputText"));
        param.put("answerText",param.get("answerText"));
        param.put("grammarScore",param.get("grammarScore"));
        param.put("pronounceScore",param.get("pronounceScore"));

        List<EngEvalFileExtVO> list = service.engEvalFileExt(param);


        String result = evalFileZipProcess(list,req,res);

        return result;


    }

    @RequestMapping("/ext2")
    public String engEvalFileExt2(HttpServletRequest req, HttpServletResponse res) throws  Exception
    {

        ModelAndView view = new ModelAndView();

        Map<String, Object> param = reqToHash(req);
        param.put("evalSearchType",param.get("audioEvalSearchType"));
        param.put("evalSearchText",param.get("audioEvalSearchText"));
        param.put("userIdSearchType",param.get("audioUserIdSearchType"));
        param.put("userId",param.get("audioUserId"));
        param.put("grammarScoreSearchType",param.get("audioGrammarScoreSearchType"));
        param.put("grammarScore",param.get("audioGrammarScore"));
        param.put("pronounceScoreSearchType",param.get("audioPronounceScoreSearchType"));
        param.put("pronounceScore",param.get("audioPronounceScore"));
        param.put("userTextSearchType",param.get("audioUserTextSearchType"));
        param.put("userText",param.get("audioUserText"));
        param.put("answerTextSearchType",param.get("audioAnswerTextSearchType"));
        param.put("answerText",param.get("audioAnswerText"));
        param.put("startDate",param.get("audioStartDate"));
        param.put("endDate",param.get("audioEndDate"));

        boolean isEvalIdSearch = true;

        if(StringUtils.isEmpty(String.valueOf(param.get("evalSearchText")))){
            isEvalIdSearch = false;
        }
        if(param.get("evalSearchText") == null){
            isEvalIdSearch = false;
        }
        if(param.get("evalSearchText") == ""){
            isEvalIdSearch = false;
        }

        ArrayList<Integer> evalIdList = new ArrayList<>();
        if(isEvalIdSearch){

            String[] a = String.valueOf(param.get("evalSearchText")).split(",");
            Arrays.stream(a).forEach(
                    b-> evalIdList.add( Integer.valueOf(b) )
            );
        }

        param.put("evalIdList",evalIdList);

        param.put("sttRecDir",sttRecDir);
        List<EngEvalFileExtVO> list =  service.engEvalFileExtFilter(param);

        String result = evalFileZipProcess(list,req,res);

        return result;

    }

    private String evalFileZipProcess(List<EngEvalFileExtVO> list,HttpServletRequest request,HttpServletResponse response) {

        String currentTime = new DateUtil().getCurrentTime(DateUtil.TO_SECOND);
        if (request.getHeader("User-Agent").indexOf("MSIE 5.5") > -1) {
            response.setHeader("Content-Disposition", "filename=" + currentTime + ".zip" + ";");
        } else {
            response.setHeader("Content-Disposition", "attachment; filename=" + currentTime + ".zip" + ";");
        }
        response.setHeader("Content-Transfer-Encoding", "binary");

        byte[] buf = new byte[4096];

        try {
            OutputStream os = response.getOutputStream();
            ZipOutputStream out = new ZipOutputStream(os);

            for (int i=0; i<list.size(); i++) {

                for(int j=0; j<2; j++){

                    String filePath = list.get(i).getFileFullPath();
                    String tempVal;
                    if(j==0){
                        tempVal = list.get(i).getFileTextName();
                    }else{
                        tempVal = list.get(i).getFileWaveName();
                    }

                    FileInputStream in = new FileInputStream(filePath+tempVal);
                    Path p = Paths.get(tempVal);
                    String fileName = p.getFileName().toString();
                    String evalId = list.get(i).getEvalId();
                    String userId = list.get(i).getUserId();

                    ZipEntry ze = new ZipEntry(evalId +"&"+userId + "&" + fileName);
                    out.putNextEntry(ze);

                    int len;
                    while ((len = in.read(buf)) > 0) {
                        out.write(buf, 0, len);
                    }

                    out.closeEntry();
                    in.close();

                }

            }

            out.close();
        } catch (Exception e){
            e.printStackTrace();
            log.error("error :{} "+ e.getMessage());

        }

        return "success";
    }
}
