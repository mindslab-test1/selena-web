package ai.mindslab.engedu.adminweb.controller.eval;

import ai.mindslab.engedu.admin.eval.english.dao.data.EngEvalAdminVO;
import ai.mindslab.engedu.admin.eval.english.service.EnglishEvalAdminService;
import ai.mindslab.engedu.admin.eval.english.service.EnglishEvalExcel;
import ai.mindslab.engedu.admin.eval.korean.dao.data.KoreanEvalAdminVO;
import ai.mindslab.engedu.admin.eval.korean.service.KoreanEvalAdminService;
import ai.mindslab.engedu.admin.eval.korean.service.KoreanEvalExcel;
import ai.mindslab.engedu.adminweb.common.view.ExcelView;
import ai.mindslab.engedu.adminweb.controller.core.CommonController;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Slf4j
@Controller
@RequestMapping("view/eval/kor/monitor")
public class KorEvalMonitorController extends CommonController {

    @Autowired
    KoreanEvalAdminService service;

    @Autowired
    KoreanEvalExcel excel;

    @Value("${client.server.domain}")
    private String recordDomain;

     @Value("${brain.evaluation.kor.stt.rec.dir}")
     private String recordDetailDomain;


    @RequestMapping("/list")
    public ModelAndView korEvalmonitorView(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        String url  = recordDomain + recordDetailDomain;
        view.addObject("sttUrl", url);
        view.setViewName("eval/kor_monitor");
        return view;

    }


      @RequestMapping("/korlist")
    public ModelAndView codeList(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        Map<String, Object> param = reqToHash(req);

        int rows = 0;
        int page = 0;
        int total_pages = 0;
        int startNum = 0;

        if (param.containsKey("rows") && param.get("rows") != null) {
            rows = Integer.parseInt(param.get("rows").toString());
        } else {
            rows = 30;
        }

        if (param.containsKey("page") && param.get("page") != null) {
            page = Integer.parseInt(param.get("page").toString());
        } else {
            page = 1;
        }

        if(param.containsKey("sidx") && param.get("sidx").equals("endId")){
            param.put("sidx", "end_id");
        }

        startNum = rows * (page - 1);

        int totalCount = service.korEvalAdminCount(param);

        param.put("start", startNum);
        param.put("rows", rows);

        //게시판 리스트 가져오기
        List<KoreanEvalAdminVO> list = service.korEvalAdminList(param);


        if (list.size() > 0) {
            total_pages = (int) Math.ceil((double) totalCount / rows);
        } else {
            total_pages = 0;
        }

        view.addObject("total", total_pages);    // the total pages of the query
        view.addObject("records", totalCount);     // the total records from the query
        view.addObject("rows", list);

        view.setViewName("jsonView");

        return view;
    }


    @RequestMapping("/korEvalAdmin")
    public ModelAndView engEvalAdmin(HttpServletRequest req, HttpServletResponse res) throws EngEduException {

        ModelAndView view = new ModelAndView();
        Map<String, Object> param = reqToHash(req);

        KoreanEvalAdminVO koEvalVO = service.korEvalAdmin(param);

        view.addObject("data", koEvalVO);
        view.setViewName("jsonView");

        return view;
    }


    @RequestMapping(value = "/excelDown")
    public ModelAndView excelDown(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        Map<String, Object> param = reqToHash(req);
        param.put("searchType",param.get("excelDownSearchType"));
        param.put("searchText",param.get("excelDownSearchText"));
        param.put("startDate",param.get("excelStartDate"));
        param.put("endDate",param.get("excelEndDate"));
        String date = new SimpleDateFormat("yyyymmddHHmmss", Locale.KOREA).format(new Date());
        String fileName = "EnglishEvalExcel"+ date +".xlsx";
        String title ="한국어 평가리스트";

        excel.setTitle(title);
        Workbook book = excel.createExcel(param);


        view.setView(new ExcelView());
        view.addObject("workBook", book);
        view.addObject("fileName", fileName);
        view.addObject("title", title);

        return view;
    }

}
