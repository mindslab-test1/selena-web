package ai.mindslab.engedu.adminweb.controller.qa;

import ai.mindslab.engedu.admin.login.dao.data.UserVO;
import ai.mindslab.engedu.admin.qa.domain.dao.data.QADomainTreeVO;
import ai.mindslab.engedu.admin.qa.domain.service.QADomainAdminService;
import ai.mindslab.engedu.adminweb.controller.core.CommonController;
import ai.mindslab.engedu.bqa.commons.data.BaseResponse;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("view/qa/domain")
public class QADomainAdminController extends CommonController {

    @Autowired
    QADomainAdminService service;

     @RequestMapping("domainList")
     public ModelAndView getQaList(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        Map<String, Object> param = reqToHash(req);

        List<QADomainTreeVO> list = null;

        //최상위 노드 호출할때...
        if(!param.containsKey("nodeid")
                  || (param.containsKey("nodeid") && (param.get("nodeid") == null || param.get("nodeid") == ""))){

            list =service.getDomainParent(param);
        }else{
            //자식 호출 시 .
             param.put("domainId", param.get("nodeid").toString());
            list = service.getDomainSub(param);
        }


       	view.addObject("records", list.size());
       	view.addObject("total",0 );
       	view.addObject("rows", list);

        view.setViewName("jsonView");

        return view;
    }

     @RequestMapping("addDomain")
     public ModelAndView addDomain(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
         ModelAndView view = new ModelAndView();
         Map<String, Object> param = reqToHash(req);

         UserVO vo = getUserInfo(req);
         param.put("userId", vo.getUserId());

         BaseResponse<Object> resp = service.insertDomainAdmin(param);

         view.addObject("result", resp);
         view.setViewName("jsonView");

        return view;
     }


      @RequestMapping("selectDomain")
     public ModelAndView selectDomain(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
         ModelAndView view = new ModelAndView();
         Map<String, Object> param = reqToHash(req);

         QADomainTreeVO vo = service.selectDomain(param);

         view.addObject("domain", vo);
         view.setViewName("jsonView");

        return view;
     }



      @RequestMapping("updateDomain")
     public ModelAndView updateDomain(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
         ModelAndView view = new ModelAndView();
         Map<String, Object> param = reqToHash(req);

         UserVO vo = getUserInfo(req);
         param.put("userId", vo.getUserId());

         BaseResponse<Object> resp = service.updateDomainAdmin(param);

         view.addObject("result", resp);
         view.setViewName("jsonView");

        return view;
     }

       @RequestMapping("delDomain")
     public ModelAndView deleteDomain(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
         ModelAndView view = new ModelAndView();
         Map<String, Object> param = reqToHash(req);

         UserVO vo = getUserInfo(req);
         param.put("userId", vo.getUserId());

         BaseResponse<Object> resp = service.deleteDoaminAdmin(param);

         view.addObject("result", resp);
         view.setViewName("jsonView");

        return view;
     }
}
