package ai.mindslab.engedu.adminweb.controller.qa;

import ai.mindslab.engedu.admin.login.dao.data.UserVO;
import ai.mindslab.engedu.admin.qa.list.dao.data.QADomainVO;
import ai.mindslab.engedu.admin.qa.list.dao.data.QAListAdminVO;
import ai.mindslab.engedu.admin.qa.list.service.QAListAdminService;
import ai.mindslab.engedu.admin.qa.list.service.QAListExcel;
import ai.mindslab.engedu.adminweb.common.view.DownloadView;
import ai.mindslab.engedu.adminweb.common.view.ExcelView;
import ai.mindslab.engedu.adminweb.controller.core.CommonController;
import ai.mindslab.engedu.bqa.commons.data.BaseResponse;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Controller
@RequestMapping("view/qa/list")
public class QAListAdminControllder extends CommonController {

    private static Logger logger  = LoggerFactory.getLogger(QAListAdminControllder.class);

    @Autowired
    QAListAdminService service;

    @Autowired
    QAListExcel qaListExcel;


    @Autowired
    DownloadView downloadView;

    @RequestMapping("qalist")
     public ModelAndView getQaList(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        Map<String, Object> param = reqToHash(req);

        int rows = 0;
        int page = 0;
        int total_pages = 0;
        int startNum = 0;

        if (param.containsKey("rows") && param.get("rows") != null) {
            rows = Integer.parseInt(param.get("rows").toString());
        } else {
            rows = 30;
        }

        if (param.containsKey("page") && param.get("page") != null) {
            page = Integer.parseInt(param.get("page").toString());
        } else {
            page = 1;
        }

        if(param.containsKey("sidx") && param.get("sidx").equals("endId")){
            param.put("sidx", "end_id");
        }

        startNum = rows * (page - 1) ;

        int totalCount = service.getQAQuestionAdminListCount(param);

        param.put("offset", startNum);
        param.put("limit", rows);

        //게시판 리스트 가져오기
        List<QAListAdminVO> list = service.getQAQuestionAdminList(param);


        if (list.size() > 0) {
            total_pages = (int) Math.ceil((double) totalCount / rows);
        } else {
            total_pages = 0;
        }

        view.addObject("total", total_pages);    // the total pages of the query
        view.addObject("records", totalCount);     // the total records from the query
        view.addObject("rows", list);

        view.setViewName("jsonView");

        return view;
    }



    @RequestMapping("/getQAlistDettail")
      public ModelAndView getQAlistDettail(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        Map<String, Object> param = reqToHash(req);
        view.setViewName("jsonView");

        List<QADomainVO> domainList = service.getSelectDomainList();
        QAListAdminVO vo = service.getQuestionDetailAdmin(param);

        view.addObject("domain", domainList);
        view.addObject("vo", vo);

        return view;
    }


     @RequestMapping("/getdomainList")
      public ModelAndView getDomainList(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        Map<String, Object> param = reqToHash(req);
        view.setViewName("jsonView");

        List<QADomainVO> domainList = service.getSelectDomainList();
        view.addObject("domain", domainList);

        return view;
    }


     @RequestMapping("/addQA")
    public ModelAndView addQA(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        Map<String, Object> param = reqToHash(req);

        view.setViewName("jsonView");

        UserVO vo = this.getUserInfo(req);
        param.put("userId", vo.getUserId());

        BaseResponse<Object> resp = service.addQAandAnswer(param);
        view.addObject("result", resp);

        return view;
    }


    @RequestMapping("/updateQA")
    public ModelAndView updateQA(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        Map<String, Object> param = reqToHash(req);

        view.setViewName("jsonView");

        UserVO vo = this.getUserInfo(req);
        param.put("userId", vo.getUserId());

        BaseResponse<Object> resp = service.updateQAandAnswer(param);
        view.addObject("result", resp);

        return view;
    }



    @RequestMapping("/endlist")
    public ModelAndView deleteQAListAdmin(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        Map<String, Object> param = reqToHash(req);
        UserVO vo = this.getUserInfo(req);
        param.put("userId", vo.getUserId());

        BaseResponse<Object> resp = service.deleteQAQuestionAdmin(param);
        view.addObject("result", resp);
        view.setViewName("jsonView");

        return view;
    }


    @RequestMapping(value = "/downSample")
    public ModelAndView downSample(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        Map<String, Object> param = reqToHash(req);

        String fileUrl  = req.getSession().getServletContext().getRealPath("resources/sample/bqa_sample.xlsx");

        logger.info("down sample url : " + fileUrl);

        File file = new File(fileUrl);

        view.addObject("downloadFile", file);
        view.setView(downloadView);

        return view;
    }


     @RequestMapping(value = "/excelDown")
    public ModelAndView excelDown(HttpServletRequest req, HttpServletResponse res) throws EngEduException {
        ModelAndView view = new ModelAndView();
        Map<String, Object> param = reqToHash(req);
         param.put("searchType",param.get("excelDownSearchType"));
         param.put("searchText",param.get("excelDownSearchText"));
         param.put("domainId",param.get("excelDomainId"));

        String date = new SimpleDateFormat("yyyymmddHHmmss", Locale.KOREA).format(new Date());
        String fileName = "QAList"+ date +".xlsx";
        String title ="Q&A";

        qaListExcel.setTitle(title);
        Workbook book = qaListExcel.createExcel(param);


        view.setView(new ExcelView());
        view.addObject("workBook", book);
        view.addObject("fileName", fileName);
        view.addObject("title", title);

        return view;
    }



    @RequestMapping(value="/insertData")
    public ModelAndView uploadExcelData(HttpServletRequest req, @RequestParam("file") MultipartFile uploadfile)throws Exception {
        ModelAndView view = new ModelAndView();
        UserVO vo = getUserInfo(req);
        BaseResponse<Object> resp = service.uploadExcelData(vo, uploadfile);
        view.addObject("result", resp);
        view.setViewName("jsonView");
        return view;

    }


}
