<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<c:set var="path"  value="${pageContext.request.contextPath}" />

	<link rel="stylesheet" type="text/css" media="screen" href="${path}/resources/css/jqueryui/jquery-ui.css" />
	<link rel="stylesheet" type="text/css" media="screen" href="${path}/resources/js/jqgrid/css/ui.jqgrid.css" />
	<link rel="stylesheet" type="text/css" media="screen" href="${path}/resources/css/jqgrid.custom.css" />

	<script  src="${path}/resources/js/jqgrid/js/i18n/grid.locale-en.js"></script>
	<script  src="${path}/resources/js/jqgrid/js/jquery.jqGrid.min.js"></script>
	<script type="text/javascript" src="${path}/resources/js/common.js"></script>


</head>
<body>
<script>
    function inintEvent(){
        $("#searchBtn").on('click', function(e){
            searchDick();
        });
        $("#searchText").on('keyup', function(e){
            if(e.keyCode == 13){
                searchDick();
            }
        })
        $("#btnSubmit").on('click', function (event) {
            //stop submit the form, we will post it manually.
            event.preventDefault();
            uploadExcel();
        });
        $('#delEngTalkBtn').on('click', function (e) {
            deleteEngtakQuestion();
        });
        $('#excelDown').on('click', function(e){
            excelDownLoad();
        });
    }
    function createGrid(){
        $("#grid").jqGrid({
            url : serverUrl('view/engtalk/local/slot/diclist'),
            mtype : "POST",
            datatype : "json",
            postData : {},
            colModel : [
                {label : '기업 ID', name : 'brandId', width : '10%', align : 'center'},
                {label : '책 ID', name : 'bookId', width : '10%', align : 'center'},
                {label : '챕터 ID', name : 'chapterId', width : '10%', align : 'center'},
                {label : '질문 번호', name : 'questionId', width : '5%', align : 'center'},
                {label : '표현', name : 'slotKey', width : '15%', align : 'center'},
                {label : '대체표현', name : 'slotValue', width : '40%', align : 'center', sortable: false},
                {label : 'Action', name : '', width : '10%', align : 'center', sortable: false,  formatter : updateFormatter}
            ],
            rowNum : 50,
            rowList : [50, 100, 150],
            width : '1250',
            height : '350',
            pager : '#pager',
            gridview : true,
            multiselect: true,
            autoencode:true,
            shrinkToFit : true,
            sortname:'brandId',
            sortorder: 'asc',
            loadComplete : function(data) {
                customResize('grid');
            }
        });
    }
    function updateFormatter(cellvalue, options, rowObject){
        var keys = rowObject.brandId +","+ rowObject.bookId +"," +
            rowObject.chapterId +"," + rowObject.questionId  +"," + rowObject.slotKey;
        var updateImage = '<%=request.getContextPath()%>/resources/images/ico_edit_bk.png';
        var html  ='';
        html +=  "<ul class=\"btn_lst\">" +
            "<li><button type=\"button\" onclick=\"showUpdateView('"+keys+"');\" class=\"btn_type_w lyr_mfy\"><img src="+updateImage+" alt=\"수정\">수정</button></li>"
            +"</ul>";
        return  html;
    }
    function searchDick(){
        var param ={
            searchType : $("#ex_select option:selected").val(),
            searchText:  $("#searchText").val()
        };
        reloadGrid(param);
    }
    function reloadGrid(param){
        console.log(param);
        $('#grid').clearGridData();
        $('#grid').setGridParam({
            postData: param
        }).trigger("reloadGrid");
    }
    function showUpdateView(keys){
        $('.lyrWrap').fadeIn(300);
        $('#lyr_modify').show();
        $('#lyr_delete').hide();
        $('#lyr_file').hide();
        var param ={
            unique: keys
        };
        //키가 많아서 미리 만들어서 hidden 둔다..
        // hddien 은 쓰다가 버릴거임...
        var div = $(".lyr_mid");
        var $input =  $('<input  type="hidden"  id="uniqueKey" class="ipt_txt" value="'+keys+'">');
        div.append($input);
        $.ajax({
            type:"post",
            dataType: "json",
            url: serverUrl("view/engtalk/local/slot/selectDetail"),
            data : param,
            success: function(data){
                $( '#data_tbody').empty();
                var result = data['data'];
                var $table = $("#data_tbody");
                var $tr = null;
                var $td = null;
                var $th = null;
                var isDrawFlag = false;
                $.each(result, function(key, value){
                    isDrawFlag = false;
                    $tr = $('<tr/>');
                    $td = $('<td/>');
                    $th = $('<th/>');
                    $th.attr('scope', 'row');
                    if(key =='brandId'){
                        $th.text('기업 ID');
                    }else if(key =='bookId'){
                        $th.text('책 ID');
                    }else if(key =='chapterId'){
                        $th.text('챕터 ID');
                    }else if(key =='questionId'){
                        $th.text('질문 번호');
                    }else if(key =='slotKey'){
                        $th.text('표현');
                    }else if(key =='slotValue'){
                        $th.text('대체표현');
                    }
                    if(value == null || value == 'undefined'){
                        value ='';
                    }
                    if(key =='brandId' || key =='bookId' || key =='chapterId' || key =='questionId'){
                        isDrawFlag = true;
                        $td.html('<span id="'+key +'">'+ value +'</span>');
                    }else if(key =='slotKey' || key =='slotValue'){
                        $td.html('<input type="text" id="'+ key +'" class="ipt_txt" value="'+value+'">');
                        isDrawFlag = true;
                    }
                    if(isDrawFlag){
                        $tr.append($th);
                        $tr.append($td);
                        $table.append($tr);
                    }
                });
            },
            fail:function(data){console.log(data)},
            error: function(data, status, error){
                console.log(error);
            }
        });
    }
    function updateEngTalkLslot(){
        var visibled =  $("#lyr_modify").css('display');
        if(visibled =='block'){
            var uniqueKey = $("#uniqueKey").val();
            var slotKey = $("#slotKey").val();
            var slotValue = $("#slotValue").val();
            var keys = uniqueKey.split(",");
            console.log(keys);
            if($.trim(slotKey).length == 0 ){
                alert('표현 값을 입력하여 주십시오.');
                return false;
            }else{
                var data = {
                    brandId :   keys[0],
                    bookId :keys[1],
                    chapterId : keys[2],
                    questionId : keys[3],
                    oldSlotKey : keys[4],
                    slotKey : slotKey,
                    slotValue: slotValue
                };
                console.log(data);
                var param ={
                    data: JSON.stringify(data)
                }
                $.ajax({
                    type:"post",
                    dataType: "json",
                    url: serverUrl("view/engtalk/local/slot/updateEngTalkSlotAdmin"),
                    data : param,
                    success: function(data){
                        var result = data.result;
                        if(result.code =='200'){
                            alert('업데이트 되었습니다.');
                            $('.lyrWrap').fadeOut(300);
                            $('#lyr_modify').hide();
                            $("#grid").trigger("reloadGrid");
                            $( '#data_tbody').empty();
                        }else{
                            alert('업데이트가 실패하였습니다.');
                        }
                    },
                    fail:function(data){
                        alert('update fail');
                        console.log(data)
                    },
                    error: function(data, status, error){
                        alert('update error');
                        console.log(error);
                    }
                });
            }
        }
    }
    function cancelUpdateDic(){
        $('.lyrWrap').fadeOut(300);
        $('#lyr_modify').hide();
        $('#data_tbody').empty();
    }
    function deleteEngtakQuestion(){
        var ids = jQuery("#grid").jqGrid('getGridParam', 'selarrrow');
        if(ids =='undefined' || ids.length ==0) {
            alert('삭제할 대상을 선택하십시오.');
            return false;
        }else {
            if (confirm('정말 삭제하시겠습니까?') == true) {
                var param = checkClickEvent();
                $.ajax({
                    type: "post",
                    dataType: "json",
                    url: serverUrl("view/engtalk/local/slot/deleteEngTalkSlotAdmin"),
                    data: param,
                    success: function (data) {
                        var result = data.result;
                        if (result.code == '200') {
                            alert('삭제되었습니다.');
                            $("#grid").trigger("reloadGrid");
                        } else {
                            alert('삭제가 실패하였습니다.');
                        }
                    },
                    fail: function (data) {
                        console.log(data)
                    },
                    error: function (data, status, error) {
                        console.log(error);
                    }
                });
            }
        }
    }
    function checkClickEvent(){
        var ids = jQuery("#grid").jqGrid('getGridParam', 'selarrrow');      //체크된 row id들을 배열로 반환
        var keys ={};
        var keyArray =[];
        var rowObject = null;
        for(var i = 0; i < ids.length; i++){
            rowObject = $("#grid").getRowData(ids[i]);      //체크된 id의 row 데이터 정보를 Object 형태로 반환
            var keys ={
                brandId :   rowObject.brandId,
                bookId : rowObject.bookId,
                chapterId : rowObject.chapterId,
                questionId : rowObject.questionId,
                slotKey : rowObject.slotKey
            };
            keyArray.push(keys);
        }
        var param ={
            unique : JSON.stringify(keyArray)
        };
        return param;
    }
    function excelDownLoad(){
        setCookie("fileDownload","false"); //호출
        checkDownloadCheck();
        //$('.wrap-loading').removeClass('display-none');
        loadSpinner();
        $("#excelDownSearchType").val($("#ex_select option:selected").val());
        $("#excelDownSearchText").val($("#searchText").val());
        $("#excelDownForm").submit();
    }
    function setCookie(c_name,value){
        var exdate=new Date();
        var c_value=escape(value);
        document.cookie=c_name + "=" + c_value + "; path=/";
    }
    function checkDownloadCheck(){
        if (document.cookie.indexOf("fileDownload=true") != -1) {
            var date = new Date(1000);
            document.cookie = "fileDownload=; expires=" + date.toUTCString() + "; path=/";
            //프로그래스바 OFF
            //$('.wrap-loading').addClass('display-none');
            stopspin();
            return;
        }
        setTimeout(checkDownloadCheck , 100);
    }
    function downSample(){
        location.href = serverUrl("view/engtalk/local/slot/downSample");
    }
    function downSampleModal(){
        $('.lyrWrap').fadeIn(300);
        $('#lyr_delete').hide();
        $('#lyr_modify').hide();
        $('#lyr_file').show();
    }
    function uploadExcel(file){
        var form = $('#fileUploadForm')[0];
        var data = new FormData(form);
        var fileName = $(file).val();
        var fileExt = fileName.slice(fileName.indexOf(".") + 1).toLowerCase();
        if($.trim(fileName).length ==0){
            alert('업로드할 Excel 파일을 선택하여 주십시오.');
        }else if(fileExt !='xls' && fileExt !='xlsx'){
            alert('업로드는 엑셀파일만 가능합니다.');
        }else {
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: serverUrl("view/engtalk/local/slot/insertData"),
                data: data,
                processData: false, //prevent jQuery from automatically transforming the data into a query string
                contentType: false,
                cache: false,
                timeout: 600000,
                success: function (data) {
                    console.log(data);
                    var result = data.result;
                    if(result.code =='200'){
                        alert('슬롯이 추가되었습니다.');
                        initFile();
                        $('.lyrWrap').fadeOut(300);
                        $('#lyr_file').hide();
                        $("#grid").trigger("reloadGrid");
                    }else{
                        alert('슬롯 추가가 실패하였습니다.');
                    }
                },
                error: function (e) {
                    initFile();
                    alert(e.responseText);
                    console.log("ERROR : ", e);
                }
            });
        }
    }
    function initFile(){
        var agent = navigator.userAgent.toLowerCase();
        if ( (navigator.appName == 'Netscape' && navigator.userAgent.search('Trident') != -1) || (agent.indexOf("msie") != -1) ){
            // ie 일때 input[type=file] init.
            $("#excelFile").replaceWith( $("#excelFile").clone(true) );
        } else {
            $("#excelFile").val("");
        }
    }
    $(document).ready(function(){
        initPage();
        inintEvent();
        createGrid();
        customResize('grid');
    });
    $(window).on('resize', function(){
        customResize('grid');
    });
    function initPage(){
        $('.page_loading').addClass('pageldg_hide').delay(300).queue(function() {
            $(this).remove();
        });
    }
</script>
<form style="display: hidden" action="engtalk/local/slot/excelDown" method="POST" id="excelDownForm">
	<input type="hidden" id="excelDownSearchType" name="excelDownSearchType" value=""/>
	<input type="hidden" id="excelDownSearchText" name="excelDownSearchText" value=""/>
</form>
<div id="page_ldWrap" class="page_loading">
	<div class="loading_itemBox">
		<span></span>
		<span></span>
		<span></span>
		<span></span>
	</div>
</div>
<!-- .lyrWrap -->
<div class="lyrWrap">
	<div class="lyr_bg"></div>
	<div id="lyr_modify" class="lyrBox" >
		<div class="lyr_top">
			<h3>수정하기</h3>
			<button type="button" onclick="cancelUpdateDic();" class="btn_lyr_close">닫기</button>
		</div>
		<div class="lyr_mid">
			<table class="tbl_view">
				<colgroup>
					<col width="20%"><col>
				</colgroup>
				<tbody id="data_tbody"></tbody>
			</table>
		</div>
		<div class="lyr_btm">
			<ul class="btn_lst">
				<li><button type="button" onclick="updateEngTalkLslot();" class="btn_clr">저장</button></li>
				<li><button type="button" onclick="cancelUpdateDic();" class="btn_lyr_cancel">취소</button></li>
			</ul>
		</div>
	</div>
	<div id="lyr_delete" class="lyrBox02">
		<div class="lyr_top">
			<h3>삭제하기</h3>
			<button type="button" class="btn_lyr_close">닫기</button>
		</div>
		<div class="lyr_mid">
			<div class="txtBox">
				<div class="imgBox"><img src="${path}/resources/images/ico_warning_c.png" alt="주의"></div>
				<p class="txt">삭제 하시겠습니까?</p>
			</div>
		</div>
		<div class="lyr_btm">
			<ul class="btn_lst">
				<li><button type="button" onclick="deleteRowData();" class="btn_clr">확인</button></li>
				<li><button type="button" class="btn_lyr_cancel">취소</button></li>
			</ul>
		</div>
	</div>
	<div id="lyr_file" class="lyrBox02">
		<div class="lyr_top">
			<h3>엑셀 업로드</h3>
			<button type="button" class="btn_lyr_close">닫기</button>
		</div>
		<div class="lyr_mid">
			<div class="srchArea">
				<div class="fc">
					<ul class="btn_lst">
						<li>
							<form method="post" enctype="multipart/form-data"  name="fileUploadForm"  id="fileUploadForm">
								<label class="btn_file" for="ipt_file"><img src="${path}/resources/images/ico_upload_bk.png" alt="업로드">엑셀 업로드</label>
								<input type="file" name="file" id="ipt_file" onchange="uploadExcel(this)" class="ipt_file">
							</form>
						</li>
						<li><button type="button" id="downSample" onclick="downSample();"><img src="${path}/resources/images/ico_download_bk.png" alt="다운로드">샘플 다운로드</button></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="lyr_btm">
			<ul class="btn_lst">
				<li><button type="button" class="btn_lyr_cancel">확인</button></li>
			</ul>
		</div>
	</div>
</div>
<!-- //.lyrWrap -->
<!-- .titArea -->
<div class="titArea">
	<h3>슬롯관리</h3>
	<div class="path">
		<span><img src="${path}/resources/images/ico_path_home_bk.png" alt="HOME"></span>
		<span>DB 관리</span>
		<span>영어대화</span>
		<span>슬롯관리</span>
	</div>
</div>
<!-- //.titArea -->
<!-- .srchArea -->
<div class="srchArea">
	<!-- .fl -->
	<div class="fl">
		<div class="selectbox">
			<label for="ex_select">선택</label>
			<select id="ex_select">
				<option value=""  selected="selected">선택</option>
				<option value="brandId">기업 ID</option>
				<option value="bookId">책 ID</option>
				<option value="chapterId">챕터 ID</option>
				<option value="questionId">질문번호</option>
				<option value="slotKey">표현</option>
				<option value="slotValue">대체표현</option>
			</select>
		</div>
		<div class="srchbox">
			<input type="text" id="searchText" class="ipt_txt" placeholder="검색어를 입력해 주세요.">
			<button type="button" id="searchBtn" class="btn_srch"><img src="${path}/resources/images/ico_srch_bk.png" alt="조건검색">조건검색</button>
		</div>
	</div>
	<!-- //.fl -->
	<!-- .fr -->
	<div class="fr">
		<ul class="btn_lst">
			<li>
				<a href="#" onclick="downSampleModal();"><img src="${path}/resources/images/ico_upload_bk.png" alt="업로드" />엑셀 업로드</a>
			</li>
			<li><button type="button" id="excelDown"><img src="${path}/resources/images/ico_download_bk.png" alt="다운로드" />엑셀 다운로드</button></li>
		</ul>
	</div>
	<!-- //.fr -->
</div>
<!-- //.srchArea -->
<!-- .content -->
<!-- .stn -->
<div class="content">
	<div class="tbl_top_btnBox">
		<ul class="fl">
			<li><button id="delEngTalkBtn" type="button" class=""><img src="${path}/resources/images/ico_check_delete.png" alt="삭제">선택삭제</button></li>
		</ul>
	</div>
	<div class="stn">
		<table id="grid"  class="tbl_lst"></table>
		<div id="pager"></div>
	</div> 	<!-- //.stn -->
</div>
</div>

</body>
</html>