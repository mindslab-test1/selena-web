<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <c:set var="path"  value="${pageContext.request.contextPath}" />

    <link rel="stylesheet" type="text/css" media="screen" href="${path}/resources/css/jqueryui/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="${path}/resources/js/jqgrid/css/ui.jqgrid.css" />

    <script  src="${path}/resources/js/jqgrid/js/i18n/grid.locale-en.js"></script>
    <script  src="${path}/resources/js/jqgrid/js/jquery.jqGrid.min.js"></script>
	<script type="text/javascript" src="${path}/resources/js/common.js"></script>





</head>
<body>
<script>
    var path ='${path}';
    var userMike = false; //[D] 사용자 마이크 연결 체크 해주셔야 합니다.

    function initPage(){
        $('.page_loading').addClass('pageldg_hide').delay(300).queue(function() {
            $(this).remove();
        });
    }


    $(document).ready(function (){
        initPage();
        initAudio();
        initEvent();
	});


    function initEvent(){

        $('#textTestBtn').on('click', function (e) {
            getEvalByText();
        });


        //  Step01 (버튼 활성화)
		$('.mobile_lot .dviceBox .txtArea').on('change keyup paste click', function(e) {
			var testValLth = $('.mobile_lot .dviceBox .txtArea').val().length;

			if ( testValLth > 0) {
				$('.btnBox .step01 button').removeClass('btn_disabled');
				$('.btnBox .step01 button').addClass('btn_record');
				$('.btnBox .step01 button').removeAttr('disabled');
				$('.btnBox .step01 .holeBox').css({
					display:'block',
				});
				$('.btnBox .step01 .curtain').remove();
			} else {
				$('.btnBox .step01 button').removeClass('btn_record');
				$('.btnBox .step01 button').addClass('btn_disabled');
				$('.btnBox .step01 button').attr('disabled');
				$('.btnBox .step01 .holeBox').css({
					display:'none',
				});
				$('.btnBox .step01').append('<span class="curtain"></span>');
			}
		});


		//  Step02 > Step03
		$('.btnBox .step02 button').on('click', function() {
			$('.mobile_lot .step02').hide();
			$('.mobile_lot .step03').show();
		});

		//  Step03 > Step04
		$('.recording').on('click', function() {
			//[D] 데이터를 받아 올수 없어서 클릭이벤트로 대체하였습니다.
			//    데이터가 넘어오면 Step04로 넘어가게 개발 해주시면 됩니다.
			$('.mobile_lot .step03').hide();
			$('.mobile_lot .step04').show();
		});

		//  Step04 > Step01
		$('.btnBox .step04 button').on('click', function() {
			$('.mobile_lot .step04').hide();
			$('.mobile_lot .dviceBox .txtArea').val('');
			$('.mobile_lot .step01').show();
		});


    }

    function recordingStt(){

        if ( userMike == false) {// 사용자 마이크 연결 됐을때
            $('.mobile_lot .step01').hide();
            $('.mobile_lot .step02').show();
        } else {// 사용자 마이크 연결 안 됐을 때
            alert('마이크가 연결 되어있지 않습니다. 마이크를 연결하시고 다시 시도하여 주시기 바랍니다.');
            return false;
        }
    }

    function endRecording(){
        $('.mobile_lot .step02').hide();
        $('.mobile_lot .step04').show();
    }



</script>
<div id="page_ldWrap" class="page_loading">
    <div class="loading_itemBox">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<div class="contents">
    <!-- .titArea -->
    <div class="titArea">
        <h3>평가 테스트</h3>
        <div class="path">
            <span><img src="${path}/resources/images/ico_path_home_bk.png" alt="HOME"></span>
            <span>영어 평가 관리</span>
            <span>평가 테스트</span>
        </div>
    </div>
    <!-- //.titArea -->
    <!-- .content -->
    <div class="content">
        <input id="LANG" type="hidden" value="eng" />
        <input id="TYPE" type="hidden" value="E" />
        <input id="wssUrl" type="hidden" value="${domain}"/>
        <input id="userId" type="hidden" name="userId" value="${userId}"/>
        <!-- .test_m_lot -->
        <div class="test_m_lot">
            <!-- .test_iptBox -->
            <div class="test_iptBox">
                <div class="mobile_lot">
                    <div class="dviceBox">
                        <div class="step01">
                            <textarea class="txtArea" id="answerText" placeholder="정답 텍스트를 입력하신 뒤, Record버튼을 눌러 주세요."></textarea>
                        </div>
                        <div class="step02">
                            <p>음성을 듣고 있습니다. 마이크를 가까이하고, 발음하십시오.</p>
                            <div class="sound-icon">
                                <i></i>
                                <i></i>
                                <i></i>
                                <i></i>
                                <i></i>
                                <i></i>
                                <i></i>
                                <i></i>
                                <i></i>
                                <i></i>
                                <i></i>
                                <i></i>
                                <i></i>
                                <i></i>
                                <i></i>
                                <i></i>
                                <i></i>
                                <i></i>
                                <i></i>
                                <i></i>
                            </div>
                        </div>
                        <div class="step03">
                            <p>음성을 변환하고 있습니다.<br>잠시만 기다려 주시기 바랍니다.</p>
                            <div class="recording">
                                <i></i>
                                <i></i>
                                <i></i>
                                <i></i>
                                <i></i>
                                <i></i>
                            </div>
                        </div>
                        <div class="step04">
                            <p>테스트가 모두 완료 되었습니다.<br>다시 하시려면 Reset 버튼을 클릭해 주세요.</p>
                        </div>
                    </div>
                    <ul class="btnBox">
                        <li class="step01">
                            <div class="holeBox">
                                <div class="hole">
                                    <i></i><i></i><i></i><i></i><i></i>
                                </div>
                            </div>
                            <button type="button" id="record" onclick="toggleRecording(this);" class="btn_disabled" disabled>Record</button>
                            <span class="curtain"></span>
                        </li>
                       <%-- <li class="step02"><button type="button" class="btn_tst_stop">Stop</button></li>--%>
                        <li class="step04"><button type="button" class="btn_tst_reset">Reset</button></li>
                    </ul>
                </div>
            </div>
            <!-- //.test_iptBox -->
            <!-- .test_resultBox -->
            <div class="test_resultBox">
                <div class="dl_resultBox">
                    <dl>
                        <dt>정답 텍스트</dt>
                        <dd><span id="sttresult"></span></dd>
                    </dl>
                    <dl>
                        <dt>사용자 음성 텍스트</dt>
                        <dd><span id="resultText"></span></dd>
                    </dl>
                    <dl>
                        <dt>발음 점수</dt>
                        <dd><span id="pronounScore"></span></dd>
                    </dl>
                    <dl>
                        <dt>문법 점수</dt>
                        <dd><span id="grammerScore"></span></dd>
                    </dl>
                    <dl>
                        <dt>평가점수</dt>
                        <dd><span id="totalScore"></span></dd>
                    </dl>
                </div>
            </div>
            <!-- //.test_resultBox -->
        </div>
        <!-- //.test_m_lot -->
    </div>
    <!-- //.content -->
</div>
</body>
 <script src="${path}/resources/js/recorderjs/recorder.js"></script>
	<script src="${path}/resources/js/main.js"></script>
</html>
