<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />

    <%@ include file="../common/commonHeader.jspf" %>

    <c:set var="path"  value="${pageContext.request.contextPath}" />
    <c:set var="path"  value="${pageContext.request.contextPath}" />

    <link rel="stylesheet" type="text/css" media="screen" href="${path}/resources/css/jqueryui/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="${path}/resources/js/jqgrid/css/ui.jqgrid.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="${path}/resources/css/jqgrid.custom.css" />

    <script  src="${path}/resources/js/jqgrid/js/i18n/grid.locale-en.js"></script>
    <script  src="${path}/resources/js/jqgrid/js/jquery.jqGrid.min.js"></script>

    <!-- icon_favicon -->
    <link rel="apple-touch-icon-precomposed" href="${path}/resources/images/ico_favicon_64x64.png">
    <link rel="shortcut icon" type="image/x-icon" href="${path}/resources/images/ico_favicon_64x64.ico"/>

    <title>maum Admin</title>
    <!-- resources -->

    <link rel="stylesheet" type="text/css" href="${path}/resources/css/reset.css" />
    <link rel="stylesheet" type="text/css" href="${path}/resources/css/font.css" />
    <link rel="stylesheet" type="text/css" href="${path}/resources/css/common.css" />
    <link rel="stylesheet" type="text/css" href="${path}/resources/css/loading.css" />
    <link rel="stylesheet" type="text/css" href="${path}/resources/css/login.css" />

    <link rel="stylesheet" type="text/css" media="screen" href="${path}/resources/css/jqueryui/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="${path}/resources/js/jqgrid/css/ui.jqgrid.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="${path}/resources/css/jqgrid.custom.css" />

    <script type="text/javascript" src="${path}/resources/js/common.js"></script>
    <script type="text/javascript" src="${path}/resources/js/sessionCheck.js"></script>

</head>
<style>
    .srchArea{
        padding-bottom: 50px;
    }
</style>
<body>

<script type="text/javascript">

    function logout(){

        $.ajax({
            type : "post",
            dataType: "json",
            url : serverUrl("login/logout"),
            success : function (data) {
                console.log(data);
                if(data.resultCode == 200){
                    console.log('성공');
                    clearInterval();
                    location.href =serverUrl("login/main");
                }else{
                    alert(data.resultMsg);
                    return false;
                }
            },
            fail:function(data){console.log(data)},
            error: function(data, status, error){
                console.log(error);
            }
        });


    }

    var isAudio = false;

    function validationSession(url){
        if(checkSession()){
            goPage(url);
        }else{
            clearInterval();
            location.href =serverUrl("login/main");
        }
    }

    function goPage(url){

        //평가테스트 화면에서 레코더가 남아잇는 경우가 있음
        // 레코더 객체가 존재하고 레코딩중이면 중지시킴
        if(typeof(audioRecorder) !='undefined'){
            if(recorded){
                audioRecorder.stop();
                recorded = false;
            }
        }

        $('#contentsArea').load(serverUrl(url), null);
    }

    //로드시 첫페이지 자동으로 갱신.
    function initPageGo(){
        var firstPage = '${firstPage}';
        goPage(firstPage);
    }


    $(document).ready(function(){
        initPageGo();
    });

    var sttUrl = '${sttUrl}';

    function initDatePicker(){

        $( "#ipt_date_start" ).datepicker({
            dateFormat: 'yy-mm-dd',
            onSelect: function(d,i){
                $('#ipt_date_end').datepicker("option", "minDate", d);
            }
        });

        $( "#ipt_date_end" ).datepicker({
            dateFormat: 'yy-mm-dd',
            maxDate: 0 // 오늘 이후 날짜 선택 불가
        });

        var startDate = new Date();
        var endDate = new Date();

        startDate.setDate(startDate.getDate() - 1);

        $("#ipt_date_start").datepicker('setDate', startDate);
        $("#ipt_date_end").datepicker('setDate', endDate);
        $('#ipt_date_end').datepicker("option", "minDate", startDate);
    }

    function initPage(){

        $('.page_loading').addClass('pageldg_hide').delay(300).queue(function() {
            $(this).remove();
        });


        $('#btn_srch').on('click', function(e){
            searchEval();
        });

        $('#searchText').on('keyup', function (e) {
            if(e.keyCode ==13){
                searchEval();
            }
        });

    }

    function createGrid(){

        var startDate = $( "#ipt_date_start" ).datepicker({ dateFormat: 'yy-mm-dd' }).val();
        var endDate = $( "#ipt_date_end" ).datepicker({ dateFormat: 'yy-mm-dd' }).val();

        var param ={
            startDate : startDate,
            endDate : endDate
        }


        $("#grid").jqGrid({
            url : serverUrl('view/eng/eval/file/englist'),
            mtype : "POST",
            datatype : "json",
            postData : param,
            colModel : [
                {label : 'ID', name : 'evalId', width : '10%', align : 'center', formatter: formatter_names},
                {label : '학습자 ID', name : 'userId', width : '10%', align : 'center'},
                {label : '평가일시', name : 'createdTime', width : '10%', align : 'center'},
                {label : '정답텍스트', name : 'answerText', width : '10%', align : 'center'},
                {label : '발화텍스트', name : 'userText', width : '25%', align : 'center'},
                {label : '문법점수', name : 'grammarScore', width : '7%', align : 'center'},
                {label : '발음점수', name : 'pronunceScore', width : '7%', align : 'center'},
                {label : '발음세부점수', name : 'pronounceEtcScore', width : '10%', align : 'center'},
                {label : '평가점수', name : 'score', width : '7%', align : 'center'}
            ],
            rowNum : 50,
            rowList : [50, 100, 150],
            width : '1250',
            height : '350',
            pager : '#pager',
            gridview : true,
            autoencode:true,
            shrinkToFit : true,
            sortname:'evalId',
            sortorder: 'desc',
            loadComplete : function(data) {
                customResize('grid');
            }
        });
    }

    function formatter_names(cellvalue, options, rowObject){
        var html = "<a style=\"text-decoration: underline;color: #6BA8D1;\" href='#' onclick=\"showdetailPopUp('"+rowObject.evalId + "')\">" + rowObject.evalId +"</a>";
        return html;
    }

    function searchEval(){

        var startDate = $( "#ipt_date_start" ).datepicker({ dateFormat: 'yy-mm-dd' }).val();
        var endDate = $( "#ipt_date_end" ).datepicker({ dateFormat: 'yy-mm-dd' }).val();

        var param ={
            evalSearchType: $("#evalIdSelect option:selected").val(),
            evalSearchText : $("#evalIdText").val(),
            userIdSearchType: $("#userIdSelect option:selected").val(),
            userId : $("#userIdText").val(),
            grammarScoreSearchType: $("#grammarScoreEqualSelect option:selected").val(),
            grammarScore : $("#grammarScore option:selected").val(),
            pronounceScoreSearchType: $("#pronounceScoreEqualSelect option:selected").val(),
            pronounceScore : $("#pronounceScore option:selected").val(),
            userTextSearchType: $("#userTextSelect option:selected").val(),
            userText : $("#userText").val(),
            answerTextSearchType: $("#answerTextSelect option:selected").val(),
            answerText : $("#answerText").val(),
            startDate: startDate,
            endDate: endDate
        };

        reloadGrid(param);
    }


    function reloadGrid(param){
        $('#grid').clearGridData();
        $('#grid').setGridParam({
            postData: param
        }).trigger("reloadGrid");
    }


    function audioDownLoad(){
        setCookie("fileDownload","false"); //호출
        checkDownloadCheck();
        //$('.wrap-loading').removeClass('display-none');

        var startDate = $( "#ipt_date_start" ).datepicker({ dateFormat: 'yy-mm-dd' }).val();
        var endDate = $( "#ipt_date_end" ).datepicker({ dateFormat: 'yy-mm-dd' }).val();

        $("#audioEvalSearchType").val($("#evalIdSelect option:selected").val());
        $("#audioEvalSearchText").val($("#evalIdText").val());
        $("#audioUserIdSearchType").val($("#userIdSelect option:selected").val());
        $("#audioUserId").val($("#userIdText").val());
        $("#audioGrammarScoreSearchType").val($("#grammarScoreEqualSelect option:selected").val());
        $("#audioGrammarScore").val($("#grammarScore option:selected").val());
        $("#audioPronounceScoreSearchType").val($("#pronounceScoreEqualSelect option:selected").val());
        $("#audioPronounceScore").val($("#pronounceScore option:selected").val());
        $("#audioUserTextSearchType").val($("#userTextSelect option:selected").val());
        $("#audioUserText").val($("#userText").val());
        $("#audioAnswerTextSearchType").val($("#answerTextSelect option:selected").val());
        $("#audioAnswerText").val($("#answerText").val());
        $("#audioStartDate").val(startDate).val();
        $("#audioEndDate").val(endDate).val();
        $("#audioDownForm").submit();

    }


    function setCookie(c_name,value){
        var exdate=new Date();
        var c_value=escape(value);
        document.cookie=c_name + "=" + c_value + "; path=/";
    }

    function checkDownloadCheck(){
        if (document.cookie.indexOf("fileDownload=true") != -1) {
            var date = new Date(1000);
            document.cookie = "fileDownload=; expires=" + date.toUTCString() + "; path=/";
            //프로그래스바 OFF
            //$('.wrap-loading').addClass('display-none');
            stopspin();
            return;
        }
        setTimeout(checkDownloadCheck , 100);
    }



    $(document).ready(function (){
        initPage();
        initDatePicker();
        createGrid();
        customResize('grid');

    });

    $(window).on('resize', function () {
        customResize('grid');
    }).trigger('resize');
</script>
<form style="display: hidden" action="/admin/view/eng/eval/file/ext2" method="POST" id="audioDownForm">
    <input type="hidden" id="audioEvalSearchType" name="audioEvalSearchType" value=""/>
    <input type="hidden" id="audioEvalSearchText" name="audioEvalSearchText" value=""/>
    <input type="hidden" id="audioUserIdSearchType" name="audioUserIdSearchType" value=""/>
    <input type="hidden" id="audioUserId" name="audioUserId" value=""/>
    <input type="hidden" id="audioGrammarScoreSearchType" name="audioGrammarScoreSearchType" value=""/>
    <input type="hidden" id="audioGrammarScore" name="audioGrammarScore" value=""/>
    <input type="hidden" id="audioPronounceScoreSearchType" name="audioPronounceScoreSearchType" value=""/>
    <input type="hidden" id="audioPronounceScore" name="audioPronounceScore" value=""/>
    <input type="hidden" id="audioUserTextSearchType" name="audioUserTextSearchType" value=""/>
    <input type="hidden" id="audioUserText" name="audioUserText" value=""/>
    <input type="hidden" id="audioAnswerTextSearchType" name="audioAnswerTextSearchType" value=""/>
    <input type="hidden" id="audioAnswerText" name="audioAnswerText" value=""/>
    <input type="hidden" id="audioStartDate" name="audioStartDate" value=""/>
    <input type="hidden" id="audioEndDate" name="audioEndDate" value=""/>
</form>

<!-- .page loading -->
<div id="page_ldWrap" class="page_loading">
    <div class="loading_itemBox">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- wrap -->
<div id="wrap">
    <!-- #header -->
    <div id="header">
        <h1><img src="${path}/resources/images/logo_mindslab_admin.png" alt="MINDsLab Admin"></h1>
        <div class="hamBox">
            <a class="btn_ham" href="#none">
                <span>주메뉴 버튼</span>
            </a>
        </div>
        <!-- .etcmenu -->
        <div class="etcmenu">
            <div class="userBox">
                <dl>
                    <dt class="ico_user">User</dt>
                    <dd>
                        <a target="_self" href="#none">admin@console.com</a>
                    </dd>
                    <ul class="lst">
                        <%-- <li class="ico_profile"><a class="lyr_profile" href="#none">프로필</a></li>--%>
                        <li class="ico_logout"><a target="_self" href="#" onclick="logout();">로그아웃</a></li>
                    </ul>
                </dl>
            </div>
        </div>
        <!-- //.etcmenu -->
    </div>
    <!-- //#header -->

    <!-- #container -->
    <div id="container">
        <!-- .snb -->
        <div class="snb" id="menuArea">
            <h2 class="ico_pjt_name"><img src="${path}/resources/images/logo_mindslab_w.png" alt="mindslab"></h2>

            <ul class="nav">
                <li class="active"><a href="#"><i class="ico_ass"></i><span>영어 평가 관리</span></a>
                    <ul class="sub_nav">
                        <li><a class="active" href="#" onclick="goPage('view/eval/eng/monitor/list');">평가 모니토링 음원 추출</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <!-- //.snb -->
        <!-- .contents -->
        <div class="contents" id="contentsArea">
            <div id="page_ldWrap" class="page_loading">
                <div class="loading_itemBox">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
            <div class="lyrWrap">
                <div class="lyr_bg"></div>
                <div id="lyr_detail" class="lyrBox">
                    <div class="lyr_top">
                        <h3>평가모니터링</h3>
                        <button type="button"  onclick="closeModal();" class="btn_lyr_close">닫기</button>
                    </div>
                    <div class="lyr_mid">
                        <table class="tbl_view">
                            <colgroup>
                                <col width="20%"><col>
                            </colgroup>
                            <tbody>
                            <tr>
                                <th scope="row">ID</th>
                                <td id="view_evalId"></td>
                            </tr>
                            <tr>
                                <th scope="row">학습자 ID</th>
                                <td id="view_userId"></td>
                            </tr>
                            <tr>
                                <th scope="row">평가 일시</th>
                                <td id="view_createdTime"></td>
                            </tr>
                            <tr>
                                <th scope="row">정답 텍스트</th>
                                <td id="view_answerText"></td>
                            </tr>
                            <tr>
                                <th scope="row">발화 텍스트</th>
                                <td id="view_userText"></td>
                            </tr>
                            <tr>
                                <th scope="row">발화 음원</th>
                                <td>
                                    <a class="link btn_audio_play" href="#none"><span id="view_record"></span></a>
                                    <div class="audioBox">
                                        <audio id="myAudio" controls>
                                            <source type="audio/mpeg" src="${path}/resources/audio/aekukka.mp3">
                                        </audio>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">문법 점수</th>
                                <td id="view_grammer"></td>
                            </tr>
                            <tr>
                                <th scope="row">발음 점수</th>
                                <td id="view_pronounce"></td>
                            </tr>
                            <tr>
                                <th scope="row">발음세부점수</th>
                                <td id="view_pronounce_etc"></td>
                            </tr>
                            <tr>
                                <th scope="row">평가 점수</th>
                                <td id="view_score"></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="lyr_btm">
                        <ul class="btn_lst">
                            <li><button type="button" onclick="closeModal();" class="btn_lyr_cancel">확인</button></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="titArea">
                <h3>평가 모니터링</h3>
                <div class="path">
                    <span><img src="${path}/resources/images/ico_path_home_bk.png" alt="HOME"></span>
                    <span>영어 평가 관리</span>
                    <span>평가 모니터링</span>
                </div>
            </div>
            <!-- //.titArea -->
            <!-- .srchArea -->
            <div class="srchArea">
                <!-- .fl -->
                <div class="fl">
                    <div class="dateBox">
                        <input type="text" id="ipt_date_start" class="ipt_txt" placeholder="시작일">
                        <span class="hyphen">-</span>
                        <input type="text" id="ipt_date_end" class="ipt_txt" placeholder="종료일">
                    </div>
                    <div class="selectbox">
                        <label for="evalIdSelect">선택</label>
                        <select id="evalIdSelect">
                            <option value="" selected>선택</option>
                            <option value="evalId" >세션 ID</option>
                        </select>
                    </div>
                    <div class="srchbox">
                        <input type="text" id="evalIdText" class="ipt_txt" placeholder="검색어를 입력해 주세요.">
                        <%--<button type="button" id="btn_srch" class="btn_srch"><img src="${path}/resources/images/ico_srch_bk.png" alt="조건검색">조건검색</button>--%>
                    </div>
                    <div class="selectbox" style="margin-left: 15px;">
                        <label for="userIdSelect">선택</label>
                        <select id="userIdSelect">
                            <option value="" selected>선택</option>
                            <option value="userId" >학습자 ID</option>
                        </select>
                    </div>
                    <div class="srchbox">
                        <input type="text" id="userIdText" class="ipt_txt" placeholder="검색어를 입력해 주세요.">
                        <%--<button type="button" id="btn_srch" class="btn_srch"><img src="${path}/resources/images/ico_srch_bk.png" alt="조건검색">조건검색</button>--%>
                    </div>
                    <br/><br/>
                    <div class="selectbox" style="margin-left: 15px;">
                        <label for="grammarScoreEqualSelect">선택</label>
                        <select id="grammarScoreEqualSelect">
                            <option value="" selected>선택</option>
                            <option value="grammarScoreEqualg" >문법 평가 점수 >=</option>
                            <option value="grammarScoreEqual" >문법 평가 점수 =</option>
                            <option value="grammarScoreEquals" >문법 평가 점수 <=</option>
                        </select>
                    </div>
                    <div class="selectbox">
                        <label for="grammarScore">선택</label>
                        <select id="grammarScore">
                            <option value="" selected>선택</option>
                            <c:forEach begin="0" end="100" varStatus="i">
                                <option value="${i.index}">${i.index}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="selectbox">
                        <label for="pronounceScoreEqualSelect">선택</label>
                        <select id="pronounceScoreEqualSelect">
                            <option value="" selected>선택</option>
                            <option value="pronounceScoreEqualg" >발음 평가 점수 >=</option>
                            <option value="pronounceScoreEqual" >발음 평가 점수 =</option>
                            <option value="pronounceScoreEquals" >발음 평가 점수 <=</option>
                        </select>
                    </div>
                    <div class="selectbox">
                        <label for="pronounceScore">선택</label>
                        <select id="pronounceScore">
                            <option value="" selected>선택</option>
                            <c:forEach begin="0" end="100" varStatus="i">
                                <option value="${i.index}">${i.index}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="selectbox" style="margin-left: 15px;">
                        <label for="userTextSelect">선택</label>
                        <select id="userTextSelect">
                            <option value="" selected>선택</option>
                            <option value="userTextEqual" >사용자 발화 = </option>
                            <option value="userTextLike">사용자 발화 like </option>
                        </select>
                    </div>
                    <div class="srchbox">
                        <input type="text" id="userText" class="ipt_txt" placeholder="검색어를 입력해 주세요.">
                        <%--<button type="button" id="btn_srch" class="btn_srch"><img src="${path}/resources/images/ico_srch_bk.png" alt="조건검색">조건검색</button>--%>
                    </div>
                    <div class="selectbox" style="margin-left: 15px;">
                        <label for="answerTextSelect">선택</label>
                        <select id="answerTextSelect">
                            <option value="" selected>선택</option>
                            <option value="answerTextEqual" >정답 문장 = </option>
                            <option value="answerTextLike">정답 문장 like</option>
                        </select>
                    </div>
                    <div class="srchbox">
                        <input type="text" id="answerText" class="ipt_txt" placeholder="검색어를 입력해 주세요.">
                        <button type="button" id="btn_srch" class="btn_srch"><img src="${path}/resources/images/ico_srch_bk.png" alt="조건검색">조건검색</button>
                    </div>


                </div>
                <div class="fr">
                    <ul class="btn_lst">
                        <li><a href="#" onclick="audioDownLoad();"><img src="${path}/resources/images/ico_download_bk.png" alt="다운로드">음원 다운로드</a></li>
                    </ul>
                </div>
                <!-- //.fl -->
                <!-- .fr -->
                <!-- //.fr -->
            </div>
            <!-- //.srchArea -->
            <!-- .content -->
            <div class="content">
                <div class="stn">
                    <table id="grid"  class="tbl_lst"></table>
                    <div id="pager"></div>
                </div>  <!-- //.stn -->
            </div>

        </div>
        <!-- //.contents -->
    </div>
    <!-- //#container -->
    <!-- #footer -->
    <div id="footer">
        <div class="lot_c">
            <div class="cyrt"><span>MINDsLab &copy; 2018</span></div>
        </div>
    </div>
    <!-- //#footer -->
</div>
</body>
</html>