package ai.mindslab.engedu.admin.code.detail.dao;

import ai.mindslab.engedu.admin.code.detail.dao.data.DetailCodeAdminVO;
import ai.mindslab.engedu.admin.code.detail.dao.data.GroupCodeTypeVO;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface CodeDetailAdminMapper {

    List<GroupCodeTypeVO> getGroupCode() throws EngEduException;
    int getDetailCodeAdminCount(Map<String, Object> paramMap) throws EngEduException;
    List<DetailCodeAdminVO> getCodeDetailAdminList(Map<String, Object> paramMap) throws EngEduException;
    DetailCodeAdminVO getCodeDetailAdmin(Map<String, Object> paramMap) throws EngEduException;
    int updateCodeDetail(Map<String, Object> paramMap) throws EngEduException;
    int addCodeDetail(Map<String, Object> paramMap) throws EngEduException;
    int deleteCodeDetail(Map<String, Object> paramMap) throws EngEduException;
}
