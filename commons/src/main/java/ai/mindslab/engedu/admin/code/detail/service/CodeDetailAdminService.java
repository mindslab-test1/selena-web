package ai.mindslab.engedu.admin.code.detail.service;


import ai.mindslab.engedu.admin.code.detail.dao.CodeDetailAdminMapper;
import ai.mindslab.engedu.admin.code.detail.dao.data.DetailCodeAdminVO;
import ai.mindslab.engedu.admin.code.detail.dao.data.GroupCodeTypeVO;
import ai.mindslab.engedu.bqa.commons.data.BaseResponse;
import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class CodeDetailAdminService {

    @Autowired
    CodeDetailAdminMapper mapper;

    public List<GroupCodeTypeVO> getGroupCode() throws EngEduException{
        return mapper.getGroupCode();
    }

    public int getDetailCodeAdminCount(Map<String, Object> paramMap) throws EngEduException{
        return mapper.getDetailCodeAdminCount(paramMap);
    }
    public List<DetailCodeAdminVO> getCodeDetailAdminList(Map<String, Object> paramMap) throws EngEduException{
        return mapper.getCodeDetailAdminList(paramMap);
    }

    public DetailCodeAdminVO getCodeDetailAdmin(Map<String, Object> paramMap) throws EngEduException{
        return mapper.getCodeDetailAdmin(paramMap);
    }

    public BaseResponse<Object> updateCodeDetail(Map<String, Object> paramMap){
        BaseResponse<Object> resp = new BaseResponse<>();
        try {

            int result = mapper.updateCodeDetail(paramMap);

            if (result > -1) {
                resp.setCode(IRestCodes.ERR_CODE_SUCCESS);
                resp.setMsg(IRestCodes.ERR_MSG_SUCCESS);
            }

        }catch (EngEduException e){
            resp.setCode(IRestCodes.ERR_CODE_FAILURE);
            resp.setMsg(e.getMessage());
        }
        return resp;
    }



    public BaseResponse<Object> addCodeDetail(Map<String, Object> paramMap){
        BaseResponse<Object> resp = new BaseResponse<>();
        try {

            int result = mapper.addCodeDetail(paramMap);

            if (result > -1) {
                resp.setCode(IRestCodes.ERR_CODE_SUCCESS);
                resp.setMsg(IRestCodes.ERR_MSG_SUCCESS);
            }

        }catch (EngEduException e){
            resp.setCode(IRestCodes.ERR_CODE_FAILURE);
            resp.setMsg(e.getMessage());
        }
        return resp;
    }


    public BaseResponse<Object> deleteCodeDetail(Map<String, Object> paramMap) throws EngEduException {

        BaseResponse<Object> resp = new BaseResponse<>(IRestCodes.ERR_CODE_SUCCESS, IRestCodes.ERR_MSG_SUCCESS);
        JsonObject obj = new JsonObject();

        try {

            String unique = paramMap.get("unique").toString();

            DetailCodeAdminVO[] keys = new Gson().fromJson(unique, DetailCodeAdminVO[].class);

            int result = 0;

            for(DetailCodeAdminVO svo : keys){
                paramMap.put("fullCode", svo.getFullCode());

                result = mapper.deleteCodeDetail(paramMap);

                if (result > 0) {
                    resp.setCode(IRestCodes.ERR_CODE_SUCCESS);
                    resp.setMsg(IRestCodes.ERR_MSG_SUCCESS);
                }else{
                    resp.setCode(IRestCodes.ERR_CODE_FAILURE);
                    resp.setMsg(IRestCodes.ERR_MSG_FAILURE);
                    throw new EngEduException(IRestCodes.ERR_CODE_FAILURE, IRestCodes.ERR_MSG_FAILURE);
                }
            }

        } catch (Exception e) {
            resp.setCode(IRestCodes.ERR_CODE_FAILURE);
            resp.setMsg(e.getMessage());
        }

        return resp;
    }
}
