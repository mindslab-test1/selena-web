package ai.mindslab.engedu.admin.code.group.dao;

import ai.mindslab.engedu.admin.code.group.dao.data.GroupCodeAdminVO;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface GroupCodeAdminMapper {

    int getGroupCodeAdminCount(Map<String, Object> paramMap) throws EngEduException;
    List<GroupCodeAdminVO> getGroupCodeAdminList(Map<String, Object> paramMap) throws EngEduException;
    GroupCodeAdminVO getGroupCodeAdmin(Map<String, Object> paramMap) throws EngEduException;
    int updateGroupCode(Map<String, Object> paramMap) throws EngEduException;
    int addGroupCode(Map<String, Object> paramMap) throws EngEduException;
    int deleteGroupCode(Map<String, Object> paramMap) throws EngEduException;

}
