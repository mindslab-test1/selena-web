package ai.mindslab.engedu.admin.dialog.service;

import ai.mindslab.engedu.admin.dialog.dao.data.DialogAdminVO;
import ai.mindslab.engedu.common.component.CommonExcelComponent;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class DialogExcel {

    @Autowired
    private CommonExcelComponent excelComponent;

    @Autowired
    private DialogAdminService service;

    private Workbook workbook;
    private String title;


    public Workbook createExcel(Map<String, Object> paramMap) throws EngEduException {

        int[] columnWiths ={300, 3000, 6000, 6000, 15000,6000};
        excelComponent.init();
        excelComponent.setSheetPrintSetting();

        //컬럼 width 세팅
        excelComponent.setColumnWidth(columnWiths);
        //title.
        excelComponent.createTitle(this.title, 1,4);

        List<String> column = new ArrayList<>();

        String[] columnMap ={"ID", "사용자 ID", "대화 텍스트", "결과 문장", "생성일자"};

        List<DialogAdminVO> list = service.dialogAdminExcelList(paramMap);

        int rowCount = excelComponent.getRowCount();

        rowCount++;
        excelComponent.createColumnRow(rowCount, columnMap);

        long start = System.currentTimeMillis();


        for(DialogAdminVO vo : list){
            String[] data = {
                vo.getDialogId()
                , vo.getUserId()
                , vo.getUserText()
                , vo.getAnswerText()
                , vo.getCreatedTime()
            };

            excelComponent.createDataRow(data);
        }

        this.workbook = excelComponent.getWorkBook();

        long end = System.currentTimeMillis();
        System.out.println("writeHSSFWorkbook : "+(end-start));

        return this.workbook;
    }


    public void setTitle(String title){
        this.title = title;
    }



}
