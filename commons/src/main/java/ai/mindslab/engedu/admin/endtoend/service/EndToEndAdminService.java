package ai.mindslab.engedu.admin.endtoend.service;

import ai.mindslab.engedu.admin.endtoend.dao.EndToEndAdminMapper;
import ai.mindslab.engedu.admin.endtoend.dao.data.EndToEndAdminVO;
import ai.mindslab.engedu.admin.login.dao.data.UserVO;
import ai.mindslab.engedu.bqa.commons.data.BaseResponse;
import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import ai.mindslab.engedu.common.utils.ExcelUtils;
import ai.mindslab.engedu.game.dao.data.EndToEndVO;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class EndToEndAdminService {

    @Autowired
    EndToEndAdminMapper endToEndAdminMapper;

    public int getEndToEndAdminListCount(Map<String, Object> paramMap) throws EngEduException{
        return endToEndAdminMapper.getEndToEndAdminListCount(paramMap);
    }

    public List<EndToEndAdminVO> getEndToEndAdminList(Map<String, Object> paramMap) throws EngEduException{
        return endToEndAdminMapper.getEndToEndAdminList(paramMap);
    }

    public List<EndToEndAdminVO> getEndToEndExcelList(Map<String, Object> paramMap) throws EngEduException{
        return endToEndAdminMapper.getEndToEndExcelList(paramMap);
    }


    public EndToEndAdminVO selectEndToEndAdmin(Map<String, Object> paramMap) throws EngEduException{
        return endToEndAdminMapper.selectEndToEndAdmin(paramMap);
    }

    public BaseResponse<Object> updateEndToEndAdmin(Map<String, Object> paramMap){
        BaseResponse<Object> resp = new BaseResponse<>();
        try {

            int result = endToEndAdminMapper.updatEndtoEndAdmin(paramMap);

            if (result > -1) {
                resp.setCode(IRestCodes.ERR_CODE_SUCCESS);
                resp.setMsg(IRestCodes.ERR_MSG_SUCCESS);
            }

        }catch (EngEduException e){
            resp.setCode(IRestCodes.ERR_CODE_FAILURE);
            resp.setMsg(e.getMessage());
        }
        return resp;
    }


    public BaseResponse<Object> deleteEndToEndAdmin(Map<String, Object> paramMap){
        BaseResponse<Object> resp = new BaseResponse<>();
        try {

            int result = endToEndAdminMapper.deleteEndToEndAdmin(paramMap);

            if (result > -1) {
                resp.setCode(IRestCodes.ERR_CODE_SUCCESS);
                resp.setMsg(IRestCodes.ERR_MSG_SUCCESS);
            }

        }catch (EngEduException e){
            resp.setCode(IRestCodes.ERR_CODE_FAILURE);
            resp.setMsg(e.getMessage());
        }
        return resp;
    }


    @Transactional(rollbackFor = { Exception.class })
    public BaseResponse<Object> uploadExcelData(UserVO useVO, MultipartFile file) throws  EngEduException {
        BaseResponse<Object> resp = new BaseResponse<>(IRestCodes.ERR_CODE_SUCCESS, IRestCodes.ERR_MSG_SUCCESS);

        Workbook workbook = ExcelUtils.getExcelWorkBook(file);
        Sheet sheet = (Sheet) workbook.getSheetAt(0);

        int rowCount = sheet.getPhysicalNumberOfRows();
        int lastCellNum = 0;

        try {

            for (int i = 0; i < rowCount; i++) {
                Row row = sheet.getRow(i);

                if (row != null) {
                    if (i == 0) {
                        lastCellNum = row.getLastCellNum();

                        if (lastCellNum != 4) {
                            resp.setCode(IRestCodes.ERR_CODE_FAILURE);
                            resp.setMsg("Excel 양식이 잘못되었습니다.");
                            break;
                        }
                    } else {

                       EndToEndAdminVO vo = new EndToEndAdminVO();

                        vo.setWord(ExcelUtils.getCellValue(row.getCell(0)));
                        vo.setThumb1(ExcelUtils.getCellValue(row.getCell(1)));
                        vo.setThumb2(ExcelUtils.getCellValue(row.getCell(2)));
                        vo.setLevel(ExcelUtils.getCellValue(row.getCell(3)));
                        vo.setUserId(useVO.getUserId());

                        endToEndAdminMapper.insertEndToEndAdmin(vo);

                    }
                }
            }
        }catch (Exception e){
            resp.setCode(IRestCodes.ERR_CODE_FAILURE);
            resp.setMsg(e.getMessage());
            throw new EngEduException(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
        }

        return resp;
    }



}
