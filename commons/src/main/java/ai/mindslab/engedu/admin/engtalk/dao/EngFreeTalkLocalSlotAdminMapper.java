package ai.mindslab.engedu.admin.engtalk.dao;

import ai.mindslab.engedu.admin.engtalk.dao.data.slot.EngTalkLocalSlotAdminSVO;
import ai.mindslab.engedu.admin.engtalk.dao.data.slot.EngTalkLocalSlotAdminVO;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface EngFreeTalkLocalSlotAdminMapper {

    int engTalkLocalSlotAdminCount(Map<String, Object> paramMap) throws EngEduException;
    List<EngTalkLocalSlotAdminVO> engTalkLocalSlotAdminList(Map<String, Object> paramMap) throws EngEduException;
    List<EngTalkLocalSlotAdminVO> engTalkLocalSlotExcelList(Map<String, Object> paramMap) throws EngEduException;
    EngTalkLocalSlotAdminVO engTalkLocalSlotAdmin(Map<String, Object> paramMap) throws EngEduException;
    int updateEngTalkSlotAdmin(Map<String, Object> paramMap) throws EngEduException;
    int deleteEngTalkSlotAdmin(EngTalkLocalSlotAdminVO vo) throws EngEduException;
    int insertEngTalkSlotAdmin(EngTalkLocalSlotAdminSVO svo) throws EngEduException;
}
