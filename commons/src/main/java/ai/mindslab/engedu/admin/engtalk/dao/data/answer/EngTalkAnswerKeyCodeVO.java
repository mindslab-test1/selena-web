package ai.mindslab.engedu.admin.engtalk.dao.data.answer;

import ai.mindslab.engedu.admin.engtalk.dao.data.EngTalkKeyCodeVO;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = true)
public class EngTalkAnswerKeyCodeVO extends EngTalkKeyCodeVO implements Serializable {

    private String answerId;
}
