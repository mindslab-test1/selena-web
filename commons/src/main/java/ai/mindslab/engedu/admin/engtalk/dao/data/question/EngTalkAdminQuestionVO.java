package ai.mindslab.engedu.admin.engtalk.dao.data.question;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class EngTalkAdminQuestionVO implements Serializable {

    private String brandId;
    private String bookId;
    private String chapterId;
    private String questionId;
    private String questionText;
    private String useYn;
}
