package ai.mindslab.engedu.admin.engtalk.dao.data.slot;

import lombok.Data;

import java.io.Serializable;

@Data
public class EngTalkGlobalSlotAdminVO implements Serializable {

  //  private String lineNum;
    private String oldSlotKey;
    private String slotKey;
    private String slotValue;
}
