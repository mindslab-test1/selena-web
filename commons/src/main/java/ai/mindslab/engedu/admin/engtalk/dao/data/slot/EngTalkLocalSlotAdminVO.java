package ai.mindslab.engedu.admin.engtalk.dao.data.slot;

import lombok.Data;

import java.io.Serializable;

@Data
public class EngTalkLocalSlotAdminVO implements Serializable {

    private String brandId;
    private String bookId;
    private String chapterId;
    private String questionId;
    private String oldSlotKey;
    private String slotKey;
    private String slotValue;
}
