package ai.mindslab.engedu.admin.eval.english.dao.data;

import lombok.Data;

import java.io.Serializable;

@Data
public class EngEvalAdminVO  implements Serializable {

    private String evalId;
    private String userId;
    private String createdTime;
    private String answerText;
    private String userText;
    private String filePath;
    private String oralMedia;
    private String grammarScore;
    private String pronunceScore;
    private String pronounceEtcScore;
    private String  score;
}
