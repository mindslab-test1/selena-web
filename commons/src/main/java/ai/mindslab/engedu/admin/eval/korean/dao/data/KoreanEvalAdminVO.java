package ai.mindslab.engedu.admin.eval.korean.dao.data;

import lombok.Data;

import java.io.Serializable;

@Data
public class KoreanEvalAdminVO implements Serializable {


    private String readId;
    private String userId;
    private String createdTime;
    private String answerText;
    private String userText;
    private String filePath;
    private String oralMedia;
    private String grammarScore;
    private String pronunceScore;
    private String  score;

}
