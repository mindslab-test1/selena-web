package ai.mindslab.engedu.admin.kordic.dao;

import ai.mindslab.engedu.admin.kordic.dao.data.KorDicAdminVO;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface KorDicAdminMapper {

    int getKorDicAdminCount(Map<String, Object> paramMap) throws EngEduException;
    List<KorDicAdminVO> getKorDicAdminList(Map<String, Object> paramMap) throws EngEduException;
    KorDicAdminVO selectDetail(Map<String, Object> paramMap) throws EngEduException;
    int                                                                                      updateKorDicAdmin(Map<String, Object> paramMap) throws EngEduException;
    int deleteKorDicAdmin(Map<String, Object> paramMap) throws EngEduException;
    List<KorDicAdminVO> getExcelKorDicAdminList(Map<String, Object> paramMap) throws EngEduException;
    int insertKorDicAdmin(KorDicAdminVO vo) throws EngEduException;
}
