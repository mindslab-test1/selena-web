package ai.mindslab.engedu.admin.kordic.service;

import ai.mindslab.engedu.admin.kordic.dao.KorDicAdminMapper;
import ai.mindslab.engedu.admin.kordic.dao.data.KorDicAdminVO;
import ai.mindslab.engedu.bqa.commons.data.BaseResponse;
import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import ai.mindslab.engedu.common.utils.ExcelUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

@Service("korDicAdminService")
public class KorDicAdminService {



    @Autowired
    KorDicAdminMapper korDicAdminMapper;


    public int getKorDicAdminCount(Map<String, Object> paramMap) throws EngEduException{
        return korDicAdminMapper.getKorDicAdminCount(paramMap);
    }

    public List<KorDicAdminVO> getKorDicAdminList(Map<String, Object> paramMap) throws EngEduException{
        return korDicAdminMapper.getKorDicAdminList(paramMap);
    }

    public List<KorDicAdminVO> getExcelKorDicAdminList(Map<String, Object> paramMap) throws EngEduException{
        return korDicAdminMapper.getExcelKorDicAdminList(paramMap);
    }

    public KorDicAdminVO selectDetail(Map<String, Object> paramMap) throws EngEduException{
        return korDicAdminMapper.selectDetail(paramMap);
    }

    public BaseResponse<Object> updateKorDicAdmin(Map<String, Object> paramMap){
        BaseResponse<Object> resp = new BaseResponse<>();
        try {

            int result = korDicAdminMapper.updateKorDicAdmin(paramMap);

            if (result > -1) {
                resp.setCode(IRestCodes.ERR_CODE_SUCCESS);
                resp.setMsg(IRestCodes.ERR_MSG_SUCCESS);
            }

        }catch (EngEduException e){
            resp.setCode(IRestCodes.ERR_CODE_FAILURE);
            resp.setMsg(e.getMessage());
        }
        return resp;
    }

    public BaseResponse<Object> deleteKorDicAdmin(Map<String, Object> paramMap){
        BaseResponse<Object> resp = new BaseResponse<>();
        try {

            int result = korDicAdminMapper.deleteKorDicAdmin(paramMap);

            if (result > -1) {
                resp.setCode(IRestCodes.ERR_CODE_SUCCESS);
                resp.setMsg(IRestCodes.ERR_MSG_SUCCESS);
            }

        }catch (EngEduException e){
            resp.setCode(IRestCodes.ERR_CODE_FAILURE);
            resp.setMsg(e.getMessage());
        }
        return resp;
    }

    @Transactional(rollbackFor = { Exception.class })
    public BaseResponse<Object> uploadExcelData(MultipartFile file) throws  EngEduException {
        BaseResponse<Object> resp = new BaseResponse<>(IRestCodes.ERR_CODE_SUCCESS, IRestCodes.ERR_MSG_SUCCESS);

        Workbook workbook = ExcelUtils.getExcelWorkBook(file);
        Sheet sheet = (Sheet) workbook.getSheetAt(0);

        int rowCount = sheet.getPhysicalNumberOfRows();
        int lastCellNum = 0;

        try {

            for (int i = 0; i < rowCount; i++) {
                Row row = sheet.getRow(i);

                if (row != null) {
                    if (i == 0) {
                        lastCellNum = row.getLastCellNum();

                        if (lastCellNum != 4) {
                            resp.setCode(IRestCodes.ERR_CODE_FAILURE);
                            resp.setMsg("Excel 양식이 잘못되었습니다.");
                            break;
                        }
                    } else {

                        KorDicAdminVO vo = new KorDicAdminVO();

                        vo.setWord(ExcelUtils.getCellValue(row.getCell(0)));
                        vo.setMeans(ExcelUtils.getCellValue(row.getCell(1)));
                        vo.setPrintMeans(ExcelUtils.getCellValue(row.getCell(2)));
                        vo.setUrl(ExcelUtils.getCellValue(row.getCell(3)));

                        korDicAdminMapper.insertKorDicAdmin(vo);

                    }
                }
            }
        }catch (Exception e){
            throw new EngEduException(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
        }

        return resp;
    }

}
