package ai.mindslab.engedu.admin.kordic.service.excel;

import ai.mindslab.engedu.admin.kordic.dao.data.KorDicAdminVO;
import ai.mindslab.engedu.admin.kordic.service.KorDicAdminService;
import ai.mindslab.engedu.common.component.CommonExcelComponent;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class KorDicAdminExcel {

    @Autowired
    CommonExcelComponent excelComponent;

    @Autowired
    KorDicAdminService korDicAdminService;

    private Workbook workbook;
    private String title;


    public Workbook createExcel(Map<String, Object> paramMap) throws EngEduException {

        int[] columnWiths ={300, 3800, 4000, 6000, 6000, 5000};
        excelComponent.init();
        excelComponent.setSheetPrintSetting();

        //컬럼 width 세팅
        excelComponent.setColumnWidth(columnWiths);
        //title.
        excelComponent.createTitle(this.title, 1,4);

        List<String> column = new ArrayList<>();

        String[] columnMap ={"No", "표제어", "TT용 뜻", "표시용 뜻", "URL"};

        List<KorDicAdminVO> list = korDicAdminService.getExcelKorDicAdminList(paramMap);

        int rowCount = excelComponent.getRowCount();

        rowCount++;
        excelComponent.createColumnRow(rowCount, columnMap);

        long start = System.currentTimeMillis();


        for(KorDicAdminVO vo : list){
            String[] data = {
                    vo.getKorId(),
                    vo.getWord(),
                    vo.getMeans(),
                    vo.getPrintMeans(),
                    vo.getUrl()+""
            };

            excelComponent.createDataRow(data);
        }

        this.workbook = excelComponent.getWorkBook();

        long end = System.currentTimeMillis();
        System.out.println("writeHSSFWorkbook : "+(end-start));

        return this.workbook;
    }


    public void setTitle(String title){
        this.title = title;
    }



}
