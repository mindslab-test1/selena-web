package ai.mindslab.engedu.admin.korengdic.service;

import ai.mindslab.engedu.admin.korengdic.dao.data.KorToEngDicAdminVO;
import ai.mindslab.engedu.common.component.CommonExcelComponent;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Component
public class KorToEngDicExcel{

    @Autowired
    CommonExcelComponent excelComponent;

    @Autowired
    KorToEngDicAdminService korToEngDicAdminService;

    private Workbook workbook;
    private String title;


    public Workbook createExcel(Map<String, Object> paramMap) throws EngEduException {

        int[] columnWiths ={300, 3800, 3800, 3800};
        excelComponent.init();
        excelComponent.setSheetPrintSetting();

        //컬럼 width 세팅
        excelComponent.setColumnWidth(columnWiths);
        //title.
        excelComponent.createTitle(this.title, 1,4);

        List<String> column = new ArrayList<>();

        String[] columnMap ={"No", "한국어", "영어", "우선순위"};

       List<KorToEngDicAdminVO> list = korToEngDicAdminService.getExcelDicAdminList(paramMap);

        int rowCount = excelComponent.getRowCount();

        rowCount++;
        excelComponent.createColumnRow(rowCount, columnMap);

        long start = System.currentTimeMillis();


        for(KorToEngDicAdminVO vo : list){
            String[] data = {
                    vo.getKorId(),
                    vo.getKor(),
                    vo.getEng(),
                    vo.getPriority()+""
            };

            excelComponent.createDataRow(data);
        }

        this.workbook = excelComponent.getWorkBook();

        long end = System.currentTimeMillis();
        System.out.println("writeHSSFWorkbook : "+(end-start));

        return this.workbook;
    }


    public void setTitle(String title){
        this.title = title;
    }
}
