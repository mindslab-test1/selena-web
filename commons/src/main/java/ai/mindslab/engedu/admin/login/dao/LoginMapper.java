package ai.mindslab.engedu.admin.login.dao;

import ai.mindslab.engedu.admin.login.dao.data.UserVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.Map;

@Mapper
public interface LoginMapper {
    public UserVO loginUser(Map<String, String> paramMap);
}
