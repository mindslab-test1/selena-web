package ai.mindslab.engedu.admin.login.dao.data;

import lombok.Data;

@Data
public class UserVO {

    private String userId;
    private String userName;
    private String userPass;
    private String roleId;
    private String roleName;

}
