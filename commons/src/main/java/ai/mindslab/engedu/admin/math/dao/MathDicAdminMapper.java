package ai.mindslab.engedu.admin.math.dao;

import ai.mindslab.engedu.admin.math.dao.data.MathDicAdminSVO;
import ai.mindslab.engedu.admin.math.dao.data.MathDicAdminVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface MathDicAdminMapper {

    public int getMathDicAdminCount(Map<String, Object> hashMap);
    public List<MathDicAdminVO> getMathDicAdminList(Map<String, Object> hashMap);
    public MathDicAdminVO selectDetail(Map<String, Object> hashMap);
    public int updateMathDicAdmin(Map<String, Object> hashMap);
    public int deleteMathDicAdmin(Map<String, Object> hashMap);
    public List<MathDicAdminVO> getExcelMathDicAdminList(Map<String, Object> hashMap);
    public int insetMathDicAdmin(MathDicAdminSVO vo);
}
