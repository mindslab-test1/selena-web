package ai.mindslab.engedu.admin.math.dao.data;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class MathDicAdminSVO extends MathDicAdminVO  {

    private String userId;

}
