package ai.mindslab.engedu.admin.math.dao.data;

import lombok.Data;

import java.io.Serializable;

@Data
public class MathDicAdminVO  implements Serializable {
    private int mathId;
    private String word;
    private String means;
    private String url;
    private int priority;
}
