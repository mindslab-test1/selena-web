package ai.mindslab.engedu.admin.menu.dao.data;

import lombok.Data;

import java.io.Serializable;

@Data
public class AdminMenuTreeVO implements Serializable {

    private int  parent;
    private int parentMenuId;
    private int menuId;
    private String menuName;
    private String enName;
    private String menuPath;
    private String menuIcon;
    private String  roleId;
    private String roleName;
    private int childCount;
    private int  sortNum;
    private int depth;
    private boolean loaded;
    private boolean lev;
    private int zlevel;
    private boolean expanded;
}
