package ai.mindslab.engedu.admin.menu.dao.data;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class AdminMenuVO implements Serializable {

    private int menuId;
    private int  parent;
    private int parentId;
    private String menuName;
    private String enName;
    private String menuPath;
    private String menuIcon;
    private String  roleId;
    private String roleName;
    private int childCount;
    private int  sortNum;
    private String  useYn;
    private List<AdminMenuVO> subMenuList;

}
