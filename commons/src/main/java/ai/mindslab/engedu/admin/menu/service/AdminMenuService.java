package ai.mindslab.engedu.admin.menu.service;

import ai.mindslab.engedu.admin.menu.dao.AdminMenuMapper;
import ai.mindslab.engedu.admin.menu.dao.data.AdminMenuTreeVO;
import ai.mindslab.engedu.admin.menu.dao.data.AdminMenuVO;
import ai.mindslab.engedu.bqa.commons.data.BaseResponse;
import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AdminMenuService {

    @Autowired
    AdminMenuMapper mapper;

    public List<AdminMenuVO> getMenuList(Map<String, Object> param) throws EngEduException {

        List<AdminMenuVO> nodeList = new ArrayList<>();
        List<AdminMenuVO> list = mapper.selectParentMenu(param);

        for(AdminMenuVO vo : list){
            if(vo.getChildCount() > 0){
                getSubMenu(vo, vo.getMenuId());
            }
        }

        return list;
    }


    private void getSubMenu(AdminMenuVO menuVO, int menuId) throws EngEduException{
         Map<String, Object> param = new HashMap<>();
         param.put("menuId", menuId);
         List<AdminMenuVO> list = mapper.selectSubMenu(param);

         menuVO.setSubMenuList(list);

        for(AdminMenuVO vo : list){
            if(vo.getChildCount() > 0){
                getSubMenu(vo, vo.getMenuId());
            }
        }
    }


    public List<AdminMenuTreeVO> selectParentTreeMenu(Map<String, Object> pramMap) throws EngEduException{
        return mapper.selectParentTreeMenu(pramMap);
    }


    public List<AdminMenuTreeVO> selectSubTreeMenu(Map<String, Object> pramMap) throws EngEduException{
        return mapper.selectSubTreeMenu(pramMap);
    }


    public AdminMenuTreeVO selectMenuAdmin(Map<String, Object> pramMap) throws EngEduException{
        return mapper.selectMenuAdmin(pramMap);
    }

    @Transactional(rollbackFor = { Exception.class })
    public BaseResponse<Object> addMenuAdmin(Map<String, Object> paramMap){

        BaseResponse<Object> resp = new BaseResponse<>(IRestCodes.ERR_CODE_SUCCESS, IRestCodes.ERR_MSG_SUCCESS);
        JsonObject obj = new JsonObject();

        try {

            int result = mapper.addMenuAdmin(paramMap);

            if(result > -1){

                result = mapper.addMenuRoleAdmin(paramMap);

                 if(result > -1){
                     resp.setCode(IRestCodes.ERR_CODE_SUCCESS);
                     resp.setMsg(IRestCodes.ERR_MSG_SUCCESS);
                 }else{
                     resp.setCode(IRestCodes.ERR_CODE_FAILURE);
                     resp.setMsg(IRestCodes.ERR_MSG_FAILURE);
                 }
            }
        } catch (EngEduException e){
            resp.setCode(IRestCodes.ERR_CODE_FAILURE);
            resp.setMsg(e.getMessage());
        }

        return resp;
    }


     @Transactional(rollbackFor = { Exception.class })
    public BaseResponse<Object> updateMenu(Map<String, Object> paramMap){

        BaseResponse<Object> resp = new BaseResponse<>(IRestCodes.ERR_CODE_SUCCESS, IRestCodes.ERR_MSG_SUCCESS);
        JsonObject obj = new JsonObject();

        try {

            int result = mapper.updateMenuAdmin(paramMap);

            if(result > -1){

                result = mapper.updateMenuRoleAdmin(paramMap);

                 if(result > -1){
                     resp.setCode(IRestCodes.ERR_CODE_SUCCESS);
                     resp.setMsg(IRestCodes.ERR_MSG_SUCCESS);
                 }else{
                     resp.setCode(IRestCodes.ERR_CODE_FAILURE);
                     resp.setMsg(IRestCodes.ERR_MSG_FAILURE);
                 }
            }
        } catch (EngEduException e){
            resp.setCode(IRestCodes.ERR_CODE_FAILURE);
            resp.setMsg(e.getMessage());
        }

        return resp;
    }


     @Transactional(rollbackFor = { Exception.class })
    public BaseResponse<Object> deleteMenuAdmin(Map<String, Object> paramMap){

        BaseResponse<Object> resp = new BaseResponse<>(IRestCodes.ERR_CODE_SUCCESS, IRestCodes.ERR_MSG_SUCCESS);
        JsonObject obj = new JsonObject();

        try {

            int result = mapper.deleteMenuAdmin(paramMap);

            if(result > -1){
                resp.setCode(IRestCodes.ERR_CODE_SUCCESS);
                resp.setMsg(IRestCodes.ERR_MSG_SUCCESS);

            }else{
                resp.setCode(IRestCodes.ERR_CODE_FAILURE);
                resp.setMsg(IRestCodes.ERR_MSG_FAILURE);
            }
        } catch (EngEduException e){
            resp.setCode(IRestCodes.ERR_CODE_FAILURE);
            resp.setMsg(e.getMessage());
        }

        return resp;
    }



}
