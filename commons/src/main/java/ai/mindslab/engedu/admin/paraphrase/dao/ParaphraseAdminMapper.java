package ai.mindslab.engedu.admin.paraphrase.dao;


import ai.mindslab.engedu.admin.paraphrase.data.ParaphraseAdminVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface ParaphraseAdminMapper {

	public int getParaphraseAdminCount(Map<String, Object> hashMap);
	public List<ParaphraseAdminVO>getParaphraseAdminList(Map<String, Object> hashMap);
	public int insertParaphrase(ParaphraseAdminVO vo);
	public ParaphraseAdminVO selectDetail(Map<String, Object> hashMap);
	public int updateParaphraseDetail(Map<String, Object> hashMap);
	public int deleteParaphrase(Map<String, Object> hashMap);
	public List<ParaphraseAdminVO> getExcelParaphraseAdminList(Map<String, Object> hashMap);

}
