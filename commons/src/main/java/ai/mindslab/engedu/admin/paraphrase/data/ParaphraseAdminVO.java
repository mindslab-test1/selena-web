package ai.mindslab.engedu.admin.paraphrase.data;


import lombok.Data;

@Data
public class ParaphraseAdminVO {

    private int wordId;

    private String mainWord;

    private String paraphraseWord;

    private String useYn;
}
