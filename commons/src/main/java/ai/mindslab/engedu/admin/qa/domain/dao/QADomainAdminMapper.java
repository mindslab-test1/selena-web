package ai.mindslab.engedu.admin.qa.domain.dao;

import ai.mindslab.engedu.admin.qa.domain.dao.data.QADomainTreeVO;
import ai.mindslab.engedu.admin.qa.list.dao.data.QAListAdminVO;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface QADomainAdminMapper {

    List<QADomainTreeVO> getDomainParent(Map<String, Object> param) throws EngEduException;
    List<QADomainTreeVO> getDomainSub(Map<String, Object> param) throws EngEduException;
    QADomainTreeVO selectDomain(Map<String, Object> param) throws EngEduException;
    int insertDomainAdmin(Map<String, Object> param) throws EngEduException;
    int updateDomainAdmin(Map<String, Object> param) throws EngEduException;
    List<QAListAdminVO> selectQuestionAdmin(Map<String, Object> param) throws EngEduException;
    int deleteDomainAdmin(Map<String, Object> param) throws EngEduException;
    int deleteQuestionByDomain(Map<String, Object> param) throws EngEduException;
}
