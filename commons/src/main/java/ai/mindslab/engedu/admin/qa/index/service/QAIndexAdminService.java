package ai.mindslab.engedu.admin.qa.index.service;

import ai.mindslab.engedu.admin.qa.index.dao.QAIndexAdminMapper;
import ai.mindslab.engedu.admin.qa.index.dao.data.QAIndexAdminVO;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class QAIndexAdminService {

    @Autowired
    QAIndexAdminMapper mapper;

     public int getIndexHisgoryAdminListCount(Map<String, Object> param) throws EngEduException{
         return mapper.getIndexHisgoryAdminListCount(param);
     }

     public List<QAIndexAdminVO> getIndexHisgoryAdminList(Map<String, Object> param) throws EngEduException{
         return mapper.getIndexHisgoryAdminList(param);
     }
}
