package ai.mindslab.engedu.admin.qa.list.dao.data;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class QADomainVO implements Serializable {

    private int domainId;
    private int parentDomainId;
    private int depth;
    private String domainName;
    private int childCount;
    private List<QADomainVO> subList;
}
