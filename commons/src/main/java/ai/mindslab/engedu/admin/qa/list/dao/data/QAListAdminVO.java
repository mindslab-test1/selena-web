package ai.mindslab.engedu.admin.qa.list.dao.data;

import lombok.Data;

import java.io.Serializable;

@Data
public class QAListAdminVO implements Serializable {

    private int  questionId;
    private String answerId;
    private int domainId;
    private String question;
    private String questionType;
    private String clientMenuCd;
    private String answer;
    private String UserId;
    private String updateTime;

}
