package ai.mindslab.engedu.admin.servicement.dao;

import ai.mindslab.engedu.admin.servicement.dao.data.SelectOptVO;
import ai.mindslab.engedu.admin.servicement.dao.data.ServiceMentAdminVO;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface GameMentAdminMapper {

    List<SelectOptVO> selectTypeList(Map<String, Object> paramMap) throws EngEduException;
    int getGameMentAdminCount(Map<String, Object> paramMap) throws EngEduException;
    List<ServiceMentAdminVO> getGameMentAdminList(Map<String, Object> paramMap) throws EngEduException;
    ServiceMentAdminVO selectGameMentAdmin(Map<String, Object> paramMap) throws  EngEduException;
    int updateGameMentAdmin(Map<String, Object> paramMap) throws  EngEduException;
    int deleteGameMentAdmin(Map<String, Object> paramMap) throws  EngEduException;
    List<ServiceMentAdminVO> getGameMentExcelList(Map<String, Object> paramMap) throws EngEduException;
    int insertGameMentAdmin(Map<String, Object> paramMap) throws EngEduException;

}
