package ai.mindslab.engedu.admin.servicement.service;


import ai.mindslab.engedu.admin.login.dao.data.UserVO;
import ai.mindslab.engedu.admin.servicement.dao.GameMentAdminMapper;
import ai.mindslab.engedu.admin.servicement.dao.data.SelectOptVO;
import ai.mindslab.engedu.admin.servicement.dao.data.ServiceMentAdminVO;
import ai.mindslab.engedu.bqa.commons.data.BaseResponse;
import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import ai.mindslab.engedu.common.utils.ExcelUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class GameMentService {

    @Autowired
    GameMentAdminMapper gameMentAdminMapper;

    public List<SelectOptVO> selectTypeList(Map<String, Object> paramMap) throws EngEduException{
        return gameMentAdminMapper.selectTypeList(paramMap);
    }

    public int getGameMentAdminCount(Map<String, Object> paramMap) throws EngEduException{
        return gameMentAdminMapper.getGameMentAdminCount(paramMap);
    }

    public List<ServiceMentAdminVO> getGameMentAdminList(Map<String, Object> paramMap) throws EngEduException{
        return gameMentAdminMapper.getGameMentAdminList(paramMap);
    }

    public List<ServiceMentAdminVO> getGameMentExcelList(Map<String, Object> paramMap) throws EngEduException{
        return gameMentAdminMapper.getGameMentExcelList(paramMap);
    }

    public ServiceMentAdminVO selectGameMentAdmin(Map<String, Object> paramMap) throws EngEduException{
        return gameMentAdminMapper.selectGameMentAdmin(paramMap);
    }

    public BaseResponse<Object> updateGameMentAdmin(Map<String, Object> paramMap){
        BaseResponse<Object> resp = new BaseResponse<>();
        try {

            int result = gameMentAdminMapper.updateGameMentAdmin(paramMap);

            if (result > -1) {
                resp.setCode(IRestCodes.ERR_CODE_SUCCESS);
                resp.setMsg(IRestCodes.ERR_MSG_SUCCESS);
            }

        }catch (EngEduException e){
            resp.setCode(IRestCodes.ERR_CODE_FAILURE);
            resp.setMsg(e.getMessage());
        }
        return resp;
    }

    public BaseResponse<Object> deleteGameMentAdmin(Map<String, Object> paramMap){
        BaseResponse<Object> resp = new BaseResponse<>();
        try {

            int result = gameMentAdminMapper.deleteGameMentAdmin(paramMap);

            if (result > -1) {
                resp.setCode(IRestCodes.ERR_CODE_SUCCESS);
                resp.setMsg(IRestCodes.ERR_MSG_SUCCESS);
            }

        }catch (EngEduException e){
            resp.setCode(IRestCodes.ERR_CODE_FAILURE);
            resp.setMsg(e.getMessage());
        }
        return resp;
    }


    @Transactional(rollbackFor = { Exception.class })
    public BaseResponse<Object> uploadExcelData(UserVO vo, MultipartFile file) throws  EngEduException {
        BaseResponse<Object> resp = new BaseResponse<>(IRestCodes.ERR_CODE_SUCCESS, IRestCodes.ERR_MSG_SUCCESS);

        Workbook workbook = ExcelUtils.getExcelWorkBook(file);
        Sheet sheet = (Sheet) workbook.getSheetAt(0);

        int rowCount = sheet.getPhysicalNumberOfRows();
        int lastCellNum = 0;

        try {

            for (int i = 0; i < rowCount; i++) {
                Row row = sheet.getRow(i);

                if (row != null) {
                    if (i == 0) {
                        lastCellNum = row.getLastCellNum();

                        if (lastCellNum != 3) {
                            resp.setCode(IRestCodes.ERR_CODE_FAILURE);
                            resp.setMsg("Excel 양식이 잘못되었습니다.");
                            break;
                        }
                    } else {

                        Map<String, Object> mapVO = new HashMap<>();

                        mapVO.put("service_type", ExcelUtils.getCellValue(row.getCell(0)));
                        mapVO.put("code",ExcelUtils.getCellValue(row.getCell(1)));
                        mapVO.put("msg", ExcelUtils.getCellValue(row.getCell(2)));
                        mapVO.put("user_id", vo.getUserId());


                        gameMentAdminMapper.insertGameMentAdmin(mapVO);

                    }
                }
            }
        }catch (Exception e){
            resp.setCode(IRestCodes.ERR_CODE_FAILURE);
            resp.setMsg(e.getMessage());
            throw new EngEduException(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
        }

        return resp;
    }


}
