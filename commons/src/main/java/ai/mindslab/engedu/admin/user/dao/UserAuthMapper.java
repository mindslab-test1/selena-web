package ai.mindslab.engedu.admin.user.dao;

import ai.mindslab.engedu.admin.user.dao.data.UserAuthVO;
import ai.mindslab.engedu.admin.user.dao.data.UserRoleVO;
import ai.mindslab.engedu.common.exceptions.EngEduException;

import java.util.List;
import java.util.Map;

public interface UserAuthMapper {

    int selectUserAuthCount(Map<String, Object> paramMap) throws EngEduException;
    List<UserAuthVO> selectUserAuthInfo(Map<String, Object> paramMap) throws EngEduException;
    UserAuthVO selectUserAuth(Map<String, Object> paramMap) throws EngEduException;
    List<UserRoleVO> getUserRole()throws EngEduException;
    int updateUserAuth(Map<String, Object> paramMap) throws EngEduException;
    int checkExistUser(Map<String, Object> paramMap) throws EngEduException;
    int addUserAuth(Map<String, Object> paramMap) throws EngEduException;
    int deleteUserAuth(Map<String, Object> paramMap) throws EngEduException;
}
