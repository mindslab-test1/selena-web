package ai.mindslab.engedu.admin.user.dao.data;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserAuthVO implements Serializable {

    private String userId;
    private String userName;
    private String userKey;
    private String email;
    private String roleName;
    private String activeYn;
    private String createdTime;
    /*private String updatedTime;*/

}
