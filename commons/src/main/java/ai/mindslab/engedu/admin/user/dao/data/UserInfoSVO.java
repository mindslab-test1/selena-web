package ai.mindslab.engedu.admin.user.dao.data;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserInfoSVO implements Serializable {

    private String userId;  //암호화된  userId
	private String userKey;  // 암호회된  userpasswd
}
