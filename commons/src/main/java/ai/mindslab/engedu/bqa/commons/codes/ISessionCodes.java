package ai.mindslab.engedu.bqa.commons.codes;

public interface ISessionCodes {

	public enum SESSION_STATUS{
		VALID("VALID", 0),
		EXPIRED("EXPIRED", 1),
		NOT_FOUND("NOT FOUND", 2),
		INVALID("INVALID", 3)
		;
		
		private String status;
		private int statusCode;
		
		SESSION_STATUS(String status, int statusCode) {
			this.status = status;
			this.statusCode = statusCode;
		}
		
		public String getStatus() {
			return status;
		}
		public int getStatusCode() {
			return statusCode;
		}
		
		@Override
		public String toString() {
			// TODO Auto-generated method stub
			return super.toString();
//			return "status: " + this.getStatusCode() + ", " + this.getStatus();
		}
	}
	
	public static int SESSION_EXPIRY = 30*60; //SEC
}
