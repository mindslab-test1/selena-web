package ai.mindslab.engedu.bqa.commons.data;

import ai.mindslab.engedu.common.codes.IRestCodes;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;

/**
 * Created by jaeheoncho on 2018. 4. 23..
 */
@SuppressWarnings("serial")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Data
public class BaseResponse<T> implements Serializable{
    private int code;
    private String msg;
    private T data;

    public BaseResponse(){
    	this.code = IRestCodes.ERR_CODE_SUCCESS;
    	this.msg = IRestCodes.ERR_MSG_SUCCESS;
    }
    public BaseResponse(int code, String msg){
        this.code = code;
        this.msg = msg;
    }
}
