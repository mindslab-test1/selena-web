package ai.mindslab.engedu.bqa.commons.utils;

import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import ai.mindslab.engedu.bqa.dao.data.QaAnswerVo;
import ai.mindslab.engedu.intent.vo.IntentExecuteVO;
import lombok.extern.slf4j.Slf4j;
import net.minidev.json.JSONObject;

@Component
@Slf4j
public class BqaClientApi {
	
    @Value("${client.getUserInfo.domain}")
    private String domain;
    
    @Value("${client.getUserInfo.baseUri}")
    private String baseUri;
    
	private int ZERO_DEPTH=0;
	private int ONE_DEPTH=1;
	private int TWO_DEPTH=2;
	private String MAP_DATA="data";
	private String MAP_RD="RD";
	private String MAP_KO="KO";
	private String MAP_MA="MA";
	private String MAP_HM="HM";
	private String MAP_EN="EN";
	private String MAP_FREE="free";
	private String MAP_PHOTO="photo";
	private String NOT_FOUND_MSG="데이터가 존재하지 않습니다";
    
	
	@Autowired
	private RestTemplate restTemplate;
	
    public String getBqaUserInfo(IntentExecuteVO intentExecuteVO, QaAnswerVo qaAnswerVo){
    	
    	log.debug("getBqaUserInfo::"+qaAnswerVo.toString());

    	String makeAnswer="";
        URI uri = URI.create(domain+baseUri);

        HttpHeaders headers = new HttpHeaders();
        headers.set("charset", "UTF-8");
        
        MultiValueMap<String, String> paramMap = new LinkedMultiValueMap<String, String>();
        paramMap.add("stuId",intentExecuteVO.getUserId());

        HttpEntity<MultiValueMap<String, String>> httpEntityRequest = new HttpEntity<>(paramMap, headers);

        BqaClientApiVO bqaClientApiVO = new BqaClientApiVO();
        
        try{
        	JSONObject jsonObj = restTemplate.postForObject(uri,httpEntityRequest,JSONObject.class);
        	
        	bqaClientApiVO = makeResultClient(jsonObj);        	
        	makeAnswer = makeResultMerge(bqaClientApiVO, qaAnswerVo);

        }catch (Exception e){
            log.warn("BqaClientApi Exception",e);
            makeAnswer="";
		}
        
        return makeAnswer;
    }
    
	private BqaClientApiVO makeResultClient(JSONObject jsonObj) {
		
		BqaClientApiVO vo = new BqaClientApiVO();
		
		String jsonList[] = {"getAge","getName","getPoint","getStudyPrgs","getStudyTime","getDeeptownNotice","getStudyQnaAnswer","getNextBook","getStudyResult","getPointUse","getDeeptownPhotoEvent","getStudyVisitPlan","getLikeBook","getDeeptownLikeCnt"};
		
		for (int i = 0; i < jsonList.length; i++) {
			makeClientVo(jsonObj, vo,jsonList[i]);
		}
		
		return vo;
		
	}
	
	private void makeClientVo(JSONObject jsonObj, BqaClientApiVO bqaClientApiVO,String objStrNm) {
		
		if ( chkSuccess(jsonObj, objStrNm) ) {
			
			String keyNm="";
			
			
			switch (objStrNm) {
				case "getAge":
					bqaClientApiVO.setAge( N2S((getJsonChildObj(jsonObj, objStrNm,ZERO_DEPTH  ).get("data")+""),"") );
					break;
					
				case "getName":
					bqaClientApiVO.setName( N2S(( getJsonChildObj(jsonObj, objStrNm,ZERO_DEPTH ).get("data")+""),"") );
					break;
					
				
				case "getPoint":
					bqaClientApiVO.setRemainPoint(  N2S((getJsonChildObj(jsonObj, objStrNm,ONE_DEPTH ).get("point")+""),"0") );
					break;
					
				case "getDeeptownNotice":
					bqaClientApiVO.setDeeptownNoticeTitle(  N2S((getJsonChildObj(jsonObj, objStrNm,ONE_DEPTH ).get("title")+""),"") );
					bqaClientApiVO.setDeeptownNoticeContents(  N2S((getJsonChildObj(jsonObj, objStrNm,ONE_DEPTH ).get("contents")+""),"").replaceAll("<[^>]*>", " ").replaceAll("&nbsp;", " ") );
					break;
					
				case "getStudyQnaAnswer":
					bqaClientApiVO.setStudyQnaAnswerRepYn(  N2S((getJsonChildObj(jsonObj, objStrNm,ONE_DEPTH ).get("repYn")+""),"") );
					bqaClientApiVO.setStudyQnaAnswerAnswer(  N2S((getJsonChildObj(jsonObj, objStrNm,ONE_DEPTH ).get("answer")+""),"").replaceAll("<[^>]*>", " ").replaceAll("&nbsp;", " ") );
					break;
					
				case "getNextBook":
					bqaClientApiVO.setNextBookName(  N2S((getJsonChildObj(jsonObj, objStrNm,ONE_DEPTH ).get("bookName")+""),"") );
					break;
					
				case "getPointUse":
					bqaClientApiVO.setUsePoint(  N2S((getJsonChildObj(jsonObj, objStrNm,ONE_DEPTH ).get("point")+""),"") );
					break;	
					
				case "getDeeptownPhotoEvent":
					bqaClientApiVO.setDeeptownPhotoEventStartMonth(  N2S((getJsonChildObj(jsonObj, objStrNm,ONE_DEPTH ).get("startmonth")+""),"") );
					bqaClientApiVO.setDeeptownPhotoEventStartDay(  N2S((getJsonChildObj(jsonObj, objStrNm,ONE_DEPTH ).get("startday")+""),"") );
					bqaClientApiVO.setDeeptownPhotoEventEndMonth(  N2S((getJsonChildObj(jsonObj, objStrNm,ONE_DEPTH ).get("endmonth")+""),"") );
					bqaClientApiVO.setDeeptownPhotoEventEndDay(  N2S((getJsonChildObj(jsonObj, objStrNm,ONE_DEPTH ).get("endday")+""),"") );
					bqaClientApiVO.setDeeptownPhotoEventName(  N2S((getJsonChildObj(jsonObj, objStrNm,ONE_DEPTH ).get("eventname")+""),"") );
					break;				
					
				case "getStudyVisitPlan":
					bqaClientApiVO.setStudyVisitPlanDay(  N2S((getJsonChildObj(jsonObj, objStrNm,ONE_DEPTH ).get("day")+""),"") );
					bqaClientApiVO.setStudyVisitPlanHour(  N2S((getJsonChildObj(jsonObj, objStrNm,ONE_DEPTH ).get("hour")+""),"") );
					bqaClientApiVO.setStudyVisitPlanMin(  N2S((getJsonChildObj(jsonObj, objStrNm,ONE_DEPTH ).get("min")+""),"") );
					break;	
					
				case "getLikeBook":					
					bqaClientApiVO.setLikeBookName(  N2S((getJsonChildObj(jsonObj, objStrNm,ONE_DEPTH ).get("bookName")+""),"") );
					break;	
					
					
					
					
				case "getStudyPrgs":
					
					keyNm="chapterName";
					bqaClientApiVO.setStudyPrgsRdChapterName( N2S(( getJsonChildObj(jsonObj, objStrNm,TWO_DEPTH ).get(MAP_RD+keyNm)+"") ,"")  );
					bqaClientApiVO.setStudyPrgsKoChapterName( N2S(( getJsonChildObj(jsonObj, objStrNm,TWO_DEPTH ).get(MAP_KO+keyNm)+"") ,"")  );
					bqaClientApiVO.setStudyPrgsMaChapterName( N2S(( getJsonChildObj(jsonObj, objStrNm,TWO_DEPTH ).get(MAP_MA+keyNm)+"") ,"")  );
					bqaClientApiVO.setStudyPrgsHmChapterName( N2S(( getJsonChildObj(jsonObj, objStrNm,TWO_DEPTH ).get(MAP_HM+keyNm)+"") ,"")  );
					bqaClientApiVO.setStudyPrgsEnChapterName( N2S(( getJsonChildObj(jsonObj, objStrNm,TWO_DEPTH ).get(MAP_EN+keyNm)+"") ,"") );

					keyNm="studySeq";
					bqaClientApiVO.setStudyPrgsRdStudySeq( N2S(( getJsonChildObj(jsonObj, objStrNm,TWO_DEPTH ).get(MAP_RD+keyNm)+"") ,"") );
					bqaClientApiVO.setStudyPrgsKoStudySeq( N2S(( getJsonChildObj(jsonObj, objStrNm,TWO_DEPTH ).get(MAP_KO+keyNm)+"") ,"") );
					bqaClientApiVO.setStudyPrgsMaStudySeq( N2S(( getJsonChildObj(jsonObj, objStrNm,TWO_DEPTH ).get(MAP_MA+keyNm)+"") ,"") );
					bqaClientApiVO.setStudyPrgsHmStudySeq( N2S(( getJsonChildObj(jsonObj, objStrNm,TWO_DEPTH ).get(MAP_HM+keyNm)+"") ,"") );
					bqaClientApiVO.setStudyPrgsEnStudySeq( N2S(( getJsonChildObj(jsonObj, objStrNm,TWO_DEPTH ).get(MAP_EN+keyNm)+"") ,"") );
					break;
					
				case "getStudyTime":
					
					keyNm="hour";
					bqaClientApiVO.setStudyTimeRdHour(  N2S(( getJsonChildObj(jsonObj, objStrNm,TWO_DEPTH ).get(MAP_RD+keyNm)+"") ,"") );
					bqaClientApiVO.setStudyTimeKoHour(  N2S(( getJsonChildObj(jsonObj, objStrNm,TWO_DEPTH ).get(MAP_KO+keyNm)+"") ,"") );
					bqaClientApiVO.setStudyTimeMaHour(  N2S(( getJsonChildObj(jsonObj, objStrNm,TWO_DEPTH ).get(MAP_MA+keyNm)+"") ,"") );
					bqaClientApiVO.setStudyTimeHmHour(  N2S(( getJsonChildObj(jsonObj, objStrNm,TWO_DEPTH ).get(MAP_HM+keyNm)+"") ,"") );
					bqaClientApiVO.setStudyTimeEnHour(  N2S(( getJsonChildObj(jsonObj, objStrNm,TWO_DEPTH ).get(MAP_EN+keyNm)+"") ,"") );

					keyNm="min";
					bqaClientApiVO.setStudyTimeRdMin(  N2S(( getJsonChildObj(jsonObj, objStrNm,TWO_DEPTH ).get(MAP_RD+keyNm)+"") ,"") );
					bqaClientApiVO.setStudyTimeKoMin(  N2S(( getJsonChildObj(jsonObj, objStrNm,TWO_DEPTH ).get(MAP_KO+keyNm)+"") ,"") );
					bqaClientApiVO.setStudyTimeMaMin(  N2S(( getJsonChildObj(jsonObj, objStrNm,TWO_DEPTH ).get(MAP_MA+keyNm)+"") ,"") );
					bqaClientApiVO.setStudyTimeHmMin(  N2S(( getJsonChildObj(jsonObj, objStrNm,TWO_DEPTH ).get(MAP_HM+keyNm)+"") ,"") );
					bqaClientApiVO.setStudyTimeEnMin(  N2S(( getJsonChildObj(jsonObj, objStrNm,TWO_DEPTH ).get(MAP_EN+keyNm)+"") ,"") );
					break;
					
				case "getStudyResult":
					
					keyNm="acmt";
					bqaClientApiVO.setStudyResultRdAcmt(  N2S(( getJsonChildObj(jsonObj, objStrNm,TWO_DEPTH ).get(MAP_RD+keyNm)+"") ,"") );
					bqaClientApiVO.setStudyResultKoAcmt(  N2S(( getJsonChildObj(jsonObj, objStrNm,TWO_DEPTH ).get(MAP_KO+keyNm)+"") ,"") );
					bqaClientApiVO.setStudyResultMaAcmt(  N2S(( getJsonChildObj(jsonObj, objStrNm,TWO_DEPTH ).get(MAP_MA+keyNm)+"") ,"") );
					bqaClientApiVO.setStudyResultHmAcmt(  N2S(( getJsonChildObj(jsonObj, objStrNm,TWO_DEPTH ).get(MAP_HM+keyNm)+"") ,"") );
					bqaClientApiVO.setStudyResultEnAcmt(  N2S(( getJsonChildObj(jsonObj, objStrNm,TWO_DEPTH ).get(MAP_EN+keyNm)+"") ,"") );

					keyNm="rate";
					bqaClientApiVO.setStudyResultRdRate(  N2S(( getJsonChildObj(jsonObj, objStrNm,TWO_DEPTH ).get(MAP_RD+keyNm)+"") ,"") );
					bqaClientApiVO.setStudyResultKoRate(  N2S(( getJsonChildObj(jsonObj, objStrNm,TWO_DEPTH ).get(MAP_KO+keyNm)+"") ,"") );
					bqaClientApiVO.setStudyResultMaRate(  N2S(( getJsonChildObj(jsonObj, objStrNm,TWO_DEPTH ).get(MAP_MA+keyNm)+"") ,"") );
					bqaClientApiVO.setStudyResultHmRate(  N2S(( getJsonChildObj(jsonObj, objStrNm,TWO_DEPTH ).get(MAP_HM+keyNm)+"") ,"") );
					bqaClientApiVO.setStudyResultEnRate(  N2S(( getJsonChildObj(jsonObj, objStrNm,TWO_DEPTH ).get(MAP_EN+keyNm)+"") ,"") );
					
					keyNm="subject";
					bqaClientApiVO.setStudyResultRdSubject(  N2S(( getJsonChildObj(jsonObj, objStrNm,TWO_DEPTH ).get(MAP_RD+keyNm)+"") ,"") );
					bqaClientApiVO.setStudyResultKoSubject(  N2S(( getJsonChildObj(jsonObj, objStrNm,TWO_DEPTH ).get(MAP_KO+keyNm)+"") ,"") );
					bqaClientApiVO.setStudyResultMaSubject(  N2S(( getJsonChildObj(jsonObj, objStrNm,TWO_DEPTH ).get(MAP_MA+keyNm)+"") ,"") );
					bqaClientApiVO.setStudyResultHmSubject(  N2S(( getJsonChildObj(jsonObj, objStrNm,TWO_DEPTH ).get(MAP_HM+keyNm)+"") ,"") );
					bqaClientApiVO.setStudyResultEnSubject(  N2S(( getJsonChildObj(jsonObj, objStrNm,TWO_DEPTH ).get(MAP_EN+keyNm)+"") ,"") );
					break;
					
					
				case "getDeeptownLikeCnt":
					
					keyNm="bbsLikeCnt";
					bqaClientApiVO.setDeeptownLikeCntFreeLikeCnt(  N2S(( getJsonChildObj(jsonObj, objStrNm,TWO_DEPTH ).get(MAP_FREE+keyNm)+"") ,"") );
					bqaClientApiVO.setDeeptownLikeCntPhotoLikeCnt(  N2S(( getJsonChildObj(jsonObj, objStrNm,TWO_DEPTH ).get(MAP_PHOTO+keyNm)+"") ,"") );
					
					keyNm="bbsName";
					bqaClientApiVO.setDeeptownLikeCntFreeBbsName(  N2S(( getJsonChildObj(jsonObj, objStrNm,TWO_DEPTH ).get(MAP_FREE+keyNm)+"") ,"") );
					bqaClientApiVO.setDeeptownLikeCntPhotoBbsName(  N2S(( getJsonChildObj(jsonObj, objStrNm,TWO_DEPTH ).get(MAP_PHOTO+keyNm)+"") ,"") );
					
					break;
					
			}
		} else {
			switch (objStrNm) {
				case "getNextBook":
					bqaClientApiVO.setNextBookMsg(NOT_FOUND_MSG);
				case "getDeeptownPhotoEvent":
					bqaClientApiVO.setDeeptownPhotoEventMsg(NOT_FOUND_MSG);
				case "getDeeptownLikeCnt":
					bqaClientApiVO.setDeeptownLikeCntMsg(NOT_FOUND_MSG);
			}
		}
	}
	
	
	private  String N2S(String original, String target) {
		
		String result="";
		if (  StringUtils.isEmpty(original) ) {
			result = target;
		} else {
			result = original;
		}
		
		return result;
	}
	
	private boolean chkSuccess(JSONObject jsonObj,String objNm) {
		boolean isSuccess= false;
		String result;
		
		HashMap<String,Object> topObj= (HashMap<String,Object>) jsonObj.get(objNm);
		
		if (topObj != null ) {
			result = (String)topObj.get("result");
			
			if (!StringUtils.isEmpty(result) && ("succees".equals(result) || "success".equals(result)   ) ) {
				isSuccess=true;
			}
		} 
		return isSuccess;
	}
	
	private HashMap<String, Object> getJsonChildObj(JSONObject jsonObj,String objNm,int depth){
		
		HashMap<String,Object> hashMap = (HashMap<String,Object>) jsonObj.get(objNm);
		HashMap<String, Object> outputMap = new HashMap<String, Object>();
		
		String keyNm="";
		
		if (depth==0) {
			outputMap = hashMap;
			
		} else if (depth ==1) {
			HashMap<String,Object> innerHashMap = (HashMap<String,Object>)hashMap.get(MAP_DATA);
			outputMap = innerHashMap;
			
		} else if (depth ==2) {
			HashMap<String,Object> innerHashMap = (HashMap<String,Object>)hashMap.get(MAP_DATA);
			
			switch (objNm) {
			case "getStudyPrgs":
				 
				 keyNm="chapterName";
				 outputMap.put(MAP_RD+keyNm, ( (HashMap<String, Object>)innerHashMap.get(MAP_RD) ).get(keyNm) );
				 outputMap.put(MAP_KO+keyNm, ( (HashMap<String, Object>)innerHashMap.get(MAP_KO) ).get(keyNm) );
				 outputMap.put(MAP_MA+keyNm, ( (HashMap<String, Object>)innerHashMap.get(MAP_MA) ).get(keyNm) );
				 outputMap.put(MAP_HM+keyNm, ( (HashMap<String, Object>)innerHashMap.get(MAP_HM) ).get(keyNm) );
				 outputMap.put(MAP_EN+keyNm, ( (HashMap<String, Object>)innerHashMap.get(MAP_EN) ).get(keyNm) );
				 
				 keyNm="studySeq";
				 outputMap.put(MAP_RD+keyNm, ( (HashMap<String, Object>)innerHashMap.get(MAP_RD) ).get(keyNm) );
				 outputMap.put(MAP_KO+keyNm, ( (HashMap<String, Object>)innerHashMap.get(MAP_KO) ).get(keyNm) );
				 outputMap.put(MAP_MA+keyNm, ( (HashMap<String, Object>)innerHashMap.get(MAP_MA) ).get(keyNm) );
				 outputMap.put(MAP_HM+keyNm, ( (HashMap<String, Object>)innerHashMap.get(MAP_HM) ).get(keyNm) );
				 outputMap.put(MAP_EN+keyNm, ( (HashMap<String, Object>)innerHashMap.get(MAP_EN) ).get(keyNm) );
				 
				break;
				
			case "getStudyTime":
				 
				 keyNm="hour";
				 outputMap.put(MAP_RD+keyNm, ( (HashMap<String, Object>)innerHashMap.get(MAP_RD) ).get(keyNm) );
				 outputMap.put(MAP_KO+keyNm, ( (HashMap<String, Object>)innerHashMap.get(MAP_KO) ).get(keyNm) );
				 outputMap.put(MAP_MA+keyNm, ( (HashMap<String, Object>)innerHashMap.get(MAP_MA) ).get(keyNm) );
				 outputMap.put(MAP_HM+keyNm, ( (HashMap<String, Object>)innerHashMap.get(MAP_HM) ).get(keyNm) );
				 outputMap.put(MAP_EN+keyNm, ( (HashMap<String, Object>)innerHashMap.get(MAP_EN) ).get(keyNm) );

				 keyNm="min";
				 outputMap.put(MAP_RD+keyNm, ( (HashMap<String, Object>)innerHashMap.get(MAP_RD) ).get(keyNm) );
				 outputMap.put(MAP_KO+keyNm, ( (HashMap<String, Object>)innerHashMap.get(MAP_KO) ).get(keyNm) );
				 outputMap.put(MAP_MA+keyNm, ( (HashMap<String, Object>)innerHashMap.get(MAP_MA) ).get(keyNm) );
				 outputMap.put(MAP_HM+keyNm, ( (HashMap<String, Object>)innerHashMap.get(MAP_HM) ).get(keyNm) );
				 outputMap.put(MAP_EN+keyNm, ( (HashMap<String, Object>)innerHashMap.get(MAP_EN) ).get(keyNm) );
				break;
				
			case "getStudyResult":
				 
				 keyNm="acmt";
				 outputMap.put(MAP_RD+keyNm, ( (HashMap<String, Object>)innerHashMap.get(MAP_RD) ).get(keyNm) );
				 outputMap.put(MAP_KO+keyNm, ( (HashMap<String, Object>)innerHashMap.get(MAP_KO) ).get(keyNm) );
				 outputMap.put(MAP_MA+keyNm, ( (HashMap<String, Object>)innerHashMap.get(MAP_MA) ).get(keyNm) );
				 outputMap.put(MAP_HM+keyNm, ( (HashMap<String, Object>)innerHashMap.get(MAP_HM) ).get(keyNm) );
				 outputMap.put(MAP_EN+keyNm, ( (HashMap<String, Object>)innerHashMap.get(MAP_EN) ).get(keyNm) );

				 keyNm="rate";
				 outputMap.put(MAP_RD+keyNm, ( (HashMap<String, Object>)innerHashMap.get(MAP_RD) ).get(keyNm) );
				 outputMap.put(MAP_KO+keyNm, ( (HashMap<String, Object>)innerHashMap.get(MAP_KO) ).get(keyNm) );
				 outputMap.put(MAP_MA+keyNm, ( (HashMap<String, Object>)innerHashMap.get(MAP_MA) ).get(keyNm) );
				 outputMap.put(MAP_HM+keyNm, ( (HashMap<String, Object>)innerHashMap.get(MAP_HM) ).get(keyNm) );
				 outputMap.put(MAP_EN+keyNm, ( (HashMap<String, Object>)innerHashMap.get(MAP_EN) ).get(keyNm) );
				 
				 keyNm="subject";
				 outputMap.put(MAP_RD+keyNm, ( (HashMap<String, Object>)innerHashMap.get(MAP_RD) ).get(keyNm) );
				 outputMap.put(MAP_KO+keyNm, ( (HashMap<String, Object>)innerHashMap.get(MAP_KO) ).get(keyNm) );
				 outputMap.put(MAP_MA+keyNm, ( (HashMap<String, Object>)innerHashMap.get(MAP_MA) ).get(keyNm) );
				 outputMap.put(MAP_HM+keyNm, ( (HashMap<String, Object>)innerHashMap.get(MAP_HM) ).get(keyNm) );
				 outputMap.put(MAP_EN+keyNm, ( (HashMap<String, Object>)innerHashMap.get(MAP_EN) ).get(keyNm) );
				break;
				
			case "getDeeptownLikeCnt":
				 keyNm="bbsName";
				 outputMap.put(MAP_FREE+keyNm, ( (HashMap<String, Object>)innerHashMap.get(MAP_FREE) ).get(keyNm) );
				 outputMap.put(MAP_PHOTO+keyNm, ( (HashMap<String, Object>)innerHashMap.get(MAP_PHOTO) ).get(keyNm) );
				 
				 keyNm="bbsLikeCnt";
				 outputMap.put(MAP_FREE+keyNm, ( (HashMap<String, Object>)innerHashMap.get(MAP_FREE) ).get(keyNm) );
				 outputMap.put(MAP_PHOTO+keyNm, ( (HashMap<String, Object>)innerHashMap.get(MAP_PHOTO) ).get(keyNm) );
				 break;
			
			}
		}
		
		return outputMap;
	}
	
    
    private String makeResultMerge(  BqaClientApiVO bqaClientApiVO, QaAnswerVo qaAnswerVo) {
    	String answer = qaAnswerVo.getAnswer();
    	
    	log.info("answer::" + answer);
    	
    	// "학습자나이"
    	answer= answer.replaceAll("_age_", 			bqaClientApiVO.getAge());		
    	
    	// "학습자 이름"
    	answer= answer.replaceAll("_name_", 		bqaClientApiVO.getName());
    	
    	// 학습자 다음주 읽을 책, 	추천책
    	
    	
    	
    	if ( answer.contains("_nextbook_") && "".equals(bqaClientApiVO.getNextBookName())) {
    		answer= bqaClientApiVO.getNextBookMsg() ;
		} else {
			answer= answer.replaceAll("_nextbook_", 	bqaClientApiVO.getNextBookName() );
		}
    	
    	answer= answer.replaceAll("_ranbook_", 		bqaClientApiVO.getLikeBookName() );
    	
    	//	"학습자 포인트 현황"
		answer= answer.replaceAll("_qpoint_", 		bqaClientApiVO.getRemainPoint());
		
		// "학습자 포인트 사용내역"
		answer= answer.replaceAll("_upoint_", 		bqaClientApiVO.getUsePoint());
		
		// "학습자 방문 학습 교사의 방문시간"
		answer= answer.replaceAll("_tday_", 		bqaClientApiVO.getStudyVisitPlanDay() );
		answer= answer.replaceAll("_thh_", 			bqaClientApiVO.getStudyVisitPlanHour() );
		answer= answer.replaceAll("_tmm_", 			bqaClientApiVO.getStudyVisitPlanMin());
		
		
		// "학습 질문 답변 여부 및 내용"
		
		if ("Y".equals( bqaClientApiVO.getStudyQnaAnswerRepYn() )) {
			answer= answer.replaceAll("_anscnt_", bqaClientApiVO.getStudyQnaAnswerAnswer());
			
		} else {
			answer= answer.replaceAll("_anscnt_", "아직 답변이 달리지 않았어");
		}
		
		
		// "딥타운 최근 공지사항"
		answer= answer.replaceAll("_news_", 		bqaClientApiVO.getDeeptownNoticeContents());
		
		
		// 현재 진행중인 이벤트 정보
		if ( answer.contains("_esub_") && "".equals(bqaClientApiVO.getDeeptownPhotoEventName())) {
			answer= bqaClientApiVO.getDeeptownPhotoEventMsg();
		} else {
			answer= answer.replaceAll("_esub_", 		bqaClientApiVO.getDeeptownPhotoEventName());
		}
		
		if ( answer.contains("_esmon_") && "".equals(bqaClientApiVO.getDeeptownPhotoEventStartMonth())) {
			answer= bqaClientApiVO.getDeeptownPhotoEventMsg();
		} else {
			answer= answer.replaceAll("_esmon_", 		bqaClientApiVO.getDeeptownPhotoEventStartMonth());
			answer= answer.replaceAll("_esday_", 		bqaClientApiVO.getDeeptownPhotoEventStartDay());    		
			answer= answer.replaceAll("_eemon_", 		bqaClientApiVO.getDeeptownPhotoEventEndMonth());
			answer= answer.replaceAll("_eeday_", 		bqaClientApiVO.getDeeptownPhotoEventEndDay());
		}
		
		
    	//     	"학습자 과목별 학습 단원"
		answer= answer.replaceAll("_kpgr_", 		bqaClientApiVO.getStudyPrgsKoStudySeq());
		answer= answer.replaceAll("_mpgr_", 		bqaClientApiVO.getStudyPrgsMaStudySeq());
		answer= answer.replaceAll("_epgr_", 		bqaClientApiVO.getStudyPrgsEnStudySeq());       		
		answer= answer.replaceAll("_rpgr_", 		bqaClientApiVO.getStudyPrgsRdStudySeq());   		
		answer= answer.replaceAll("_cpgr_", 		bqaClientApiVO.getStudyPrgsHmStudySeq());  
		
		// "학습자 학습 성취도"
		answer= answer.replaceAll("_kratio_", 		bqaClientApiVO.getStudyResultKoRate());
		answer= answer.replaceAll("_kscore_", 		bqaClientApiVO.getStudyResultKoAcmt());
		
		answer= answer.replaceAll("_mratio_",		bqaClientApiVO.getStudyResultMaRate());
		answer= answer.replaceAll("_mscore_", 		bqaClientApiVO.getStudyResultMaAcmt());
		
		answer= answer.replaceAll("_eratio_", 		bqaClientApiVO.getStudyResultEnRate());
		answer= answer.replaceAll("_escore_", 		bqaClientApiVO.getStudyResultEnAcmt());
		
		answer= answer.replaceAll("_rratio_", 		bqaClientApiVO.getStudyResultRdRate());
		answer= answer.replaceAll("_rscore_", 		bqaClientApiVO.getStudyResultRdAcmt());
		
		answer= answer.replaceAll("_cratio_", 		bqaClientApiVO.getStudyResultHmRate());
		answer= answer.replaceAll("_cscore_", 		bqaClientApiVO.getStudyResultHmAcmt());
		
		// 학습자 게시물 좋아요 개수
		if ( answer.contains("_fnum_") && "".equals(bqaClientApiVO.getDeeptownLikeCntFreeLikeCnt())) {
			answer= bqaClientApiVO.getDeeptownLikeCntMsg() ;
		} else {
			answer= answer.replaceAll("_fnum_", 		bqaClientApiVO.getDeeptownLikeCntFreeLikeCnt());
		}
		
		if ( answer.contains("_pnum_") && "".equals(bqaClientApiVO.getDeeptownLikeCntPhotoLikeCnt())) {
			answer= bqaClientApiVO.getDeeptownLikeCntMsg() ;
		} else {
			answer= answer.replaceAll("_pnum_", 		bqaClientApiVO.getDeeptownLikeCntPhotoLikeCnt());
		}
		
		

    	// "학습자 과목별 학습시간"
    	answer= answer.replaceAll("_khh_", 			bqaClientApiVO.getStudyTimeKoHour());
    	answer= answer.replaceAll("_kmm_", 			bqaClientApiVO.getStudyTimeKoMin());
    	
    	answer= answer.replaceAll("_mhh_", 			bqaClientApiVO.getStudyTimeMaHour());
    	answer= answer.replaceAll("_mmm_", 			bqaClientApiVO.getStudyTimeMaMin());
    	
    	answer= answer.replaceAll("_ehh_", 			bqaClientApiVO.getStudyTimeEnHour());
    	answer= answer.replaceAll("_emm_", 			bqaClientApiVO.getStudyTimeEnMin());
    	
    	answer= answer.replaceAll("_rhh_", 			bqaClientApiVO.getStudyTimeRdHour());
    	answer= answer.replaceAll("_rmm_", 			bqaClientApiVO.getStudyTimeRdMin());
    	
    	answer= answer.replaceAll("_chh_", 			bqaClientApiVO.getStudyTimeHmHour());
    	answer= answer.replaceAll("_cmm_", 			bqaClientApiVO.getStudyTimeHmMin());
    	
    	return answer;
    }

}
