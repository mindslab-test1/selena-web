package ai.mindslab.engedu.bqa.commons.utils;


@lombok.Data
public class BqaClientApiVO {
	
	private String age="";
	private String remainPoint="";
	private String usePoint="";
	private String name="";
	private String studyPrgsRdChapterName="";
	private String studyPrgsRdStudySeq="";
	private String studyPrgsKoChapterName="";
	private String studyPrgsKoStudySeq="";
	private String studyPrgsMaChapterName="";
	private String studyPrgsMaStudySeq="";
	private String studyPrgsHmChapterName="";
	private String studyPrgsHmStudySeq="";
	private String studyPrgsEnChapterName="";
	private String studyPrgsEnStudySeq="";
	
	private String studyTimeRdHour="";
	private String studyTimeRdMin="";
	private String studyTimeKoHour="";
	private String studyTimeKoMin="";
	private String studyTimeMaHour="";
	private String studyTimeMaMin="";
	private String studyTimeHmHour="";
	private String studyTimeHmMin="";
	private String studyTimeEnHour="";
	private String studyTimeEnMin="";
	
	private String deeptownNoticeContents="";
	private String deeptownNoticeTitle="";
	
	private String studyQnaAnswerRepYn="";
	private String studyQnaAnswerAnswer="";
	
	private String nextBookName="";
	private String nextBookWeek="";
	private String nextBookMonth="";
	private String nextBookYear="";
	private String nextBookMsg="";
	
	private String studyResultRdAcmt="";
	private String studyResultRdRate="";
	private String studyResultRdSubject="";

	private String studyResultKoAcmt="";
	private String studyResultKoRate="";
	private String studyResultKoSubject="";
	
	private String studyResultMaAcmt="";
	private String studyResultMaRate="";
	private String studyResultMaSubject="";
	
	private String studyResultHmAcmt="";
	private String studyResultHmRate="";
	private String studyResultHmSubject="";
	
	private String studyResultEnAcmt="";
	private String studyResultEnRate="";
	private String studyResultEnSubject="";
	
	
	
	private String deeptownPhotoEventStartMonth="";
	private String deeptownPhotoEventStartDay="";
	private String deeptownPhotoEventEndDay="";
	private String deeptownPhotoEventEndMonth="";
	private String deeptownPhotoEventName="";
	private String deeptownPhotoEventMsg="";
	
	private String studyVisitPlanHour="";
	private String studyVisitPlanMin="";
	private String studyVisitPlanDay="";
	
	private String likeBookName="";
	
	
	private String deeptownLikeCntMsg="";
	
	private String deeptownLikeCntPhotoLikeCnt="";
	private String deeptownLikeCntFreeLikeCnt="";
	
	private String deeptownLikeCntPhotoBbsName="";
	private String deeptownLikeCntFreeBbsName="";
	
}
