package ai.mindslab.engedu.bqa.commons.utils;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.lucene.analysis.ko.morph.AnalysisOutput;
import org.apache.lucene.analysis.ko.morph.CompoundEntry;
import org.apache.lucene.analysis.ko.morph.MorphAnalyzer;
import org.apache.lucene.analysis.ko.morph.MorphException;
import org.apache.lucene.analysis.ko.morph.WordSegmentAnalyzer;

public class KoreanAnalyze {
	
	public String guideWord(String source) throws MorphException {
		MorphAnalyzer maAnal = new MorphAnalyzer(); // 형태소 분석기

		StringTokenizer stok = new StringTokenizer(source, " "); // 쿼리문을 뛰어쓰기 기준으로 토큰화

		StringBuilder result = new StringBuilder();

		while (stok.hasMoreTokens()) {

			String token = stok.nextToken();

			List<AnalysisOutput> outList = maAnal.analyze(token);
			for (AnalysisOutput o : outList) {

				result.append(o.getStem());

				for (CompoundEntry s : o.getCNounList()) {
					result.append("+" + s.getWord());
				}

				result.append(",");
			}
		}
		String s = result.toString();
		if (s.endsWith(","))
			s = s.substring(0, s.length() - 1);
		return s;
	}
	
	public String wordSpaceAnalyze(String source, boolean force) throws MorphException {
		WordSegmentAnalyzer wsAnal = new WordSegmentAnalyzer();

		StringBuilder result = new StringBuilder();

		String s;
		if (force)
			s = source.replace(" ", "");
		else
			s = source;
		
		List<List<AnalysisOutput>> outList = wsAnal.analyze(s);
		for (List<AnalysisOutput> o : outList) {
			for (AnalysisOutput analysisOutput : o) {
				if (analysisOutput.getSource().trim().length() > 0) {
					result.append(analysisOutput.getSource().trim()).append(",");
				}
			}
		}

		return result.toString();
	}
	
	public String removeDuplicates(String userKeyword) {
		
		if (userKeyword == null || userKeyword.length() < 1) {
			return "";
		}
		if (userKeyword.indexOf(',') < 1) {
			return "";
		}
		
		String[] aKeywords = userKeyword.split(",");
		Arrays.sort(aKeywords);
		aKeywords = new HashSet<String>(Arrays.asList(aKeywords)).toArray(new String[0]);
		
		StringBuffer sb = new StringBuffer();
		int wordLength = aKeywords.length;
		for (int i = 0; i < wordLength; i++) {
			if (i < wordLength -1) {
				sb.append(aKeywords[i]).append(",");
			} else {
				sb.append(aKeywords[i]);
			}				
		}
		return sb.toString();
	}

}
