package ai.mindslab.engedu.bqa.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import ai.mindslab.engedu.bqa.dao.data.CmDomainVo;


@Mapper
public interface CmDomainMapper {
	
	CmDomainVo get(int domainId);

	List<CmDomainVo> gets(Map<String, Object> paramMap);

	String getUsedDomainName(int domainId);

	List<Integer> getDomainIds();
}
