package ai.mindslab.engedu.bqa.dao;

import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface NlpEntityCodeMapper {
	
	String getCodeDesc(String code);
}
