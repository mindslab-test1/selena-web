package ai.mindslab.engedu.bqa.dao.data;

import lombok.Data;

import java.util.List;

@Data
public class NlpAnalysisVo {

	private String text;
	private int textSeq;
	private List<NlpNesVo> nesList;
	private List<NlpMorpVo> morpList;
}
