package ai.mindslab.engedu.bqa.dao.data;

import lombok.Data;

@Data
public class NlpMorpVo {
	
	static final String NC = "자립명사";
	static final String NP = "대명사";
	static final String NB = "의존명사";
	static final String NN = "수사";
	static final String PV = "동사";
	static final String PA = "형용사";
	static final String MAG = "일반부사";
	static final String II = "감탄사";
	static final String JC = "격조사";
	static final String CO = "지정사";
	static final String EF = "종결어미";
	static final String EP = "선어말어미";
	static final String XP = "접두사";
	static final String XSN = "명사 파생 접미사";
	static final String XSV = "동사 파생 접미사";
	static final String XSM = "형용사 파생 접미사";
	static final String S = "기호";
	static final String PX = "보조용언";
	static final String MM = "관형사";
	static final String EC = "연결어미";
	static final String JJ = "접속조사";
	static final String JM = "속격조사";
	static final String MAJ = "접속부사";
	static final String JX = "보조사";
	static final String ETM = "관형형어미";
	static final String ETN = "명사형어미";
	static final String UK = "미등록어";
	static final String NK = "사용자사전등록명사";
	static final String NR = "고유명사";

	private int textSeq;
	private String lemma;
	private int lemmaSeq;
	private String lemmaType;
	private String lemmaTypeDesc;
}
