package ai.mindslab.engedu.bqa.dao.data;

public enum NlpWordClass {

	NC ("자립명사"),
	NP ("대명사"),
	NB ("의존명사"),
	NN ("수사"),
	PV ("동사"),
	PA ("형용사"),
	MAG ("일반부사"),
	II ("감탄사"),
	JC ("격조사"),
	CO ("지정사"),
	EF ("종결어미"),
	EP ("선어말어미"),
	XP ("접두사"),
	XSN ("명사 파생 접미사"),
	XSV ("동사 파생 접미사"),
	XSM ("형용사 파생 접미사"),
	S ("기호"),
	PX ("보조용언"),
	MM ("관형사"),
	EC ("연결어미"),
	JJ ("접속조사"),
	JM ("속격조사"),
	MAJ ("접속부사"),
	JX ("보조사"),
	ETM ("관형형어미"),
	ETN ("명사형어미"),
	UK ("미등록어"),
	NK ("사용자사전등록명사"),
	NR ("고유명사");
	
    private String desc;
    private NlpWordClass(String desc) {
    	 this.desc = desc;
    }
    public String getWordClass() {
        return desc;
    }
    
}
