package ai.mindslab.engedu.bqa.dao.data;

import java.io.Serializable;
import java.time.LocalDateTime;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@SuppressWarnings("serial")
@JsonIgnoreProperties({"createdDtm","updatedDtm","creatorId","updatorId"})
@Data
public class QaAnswerVo implements Serializable {

	private int answerId;
	private String answer;
	private int questionId;
	private String useYn;
	private LocalDateTime createdDtm;
	private LocalDateTime updatedDtm;
	private String creatorId;
	private String updatorId;
	private String clientUrlCd;
	private String clientUrl;
	private String clientMenuCd;
	
}
