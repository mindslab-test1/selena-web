package ai.mindslab.engedu.bqa.dao.data;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


@SuppressWarnings("serial")
@Data
@NoArgsConstructor
public class QaExcelVo implements Serializable {
	private int domainId;
	private int questionId;
	private String question;
	private int answerId;
	private String answer;
	private String userId;
	private String questionType;
	private boolean error = false;

	public QaExcelVo(boolean error) {
		this.error = error;
	};
	public QaExcelVo(double questionId, String question, double answerId, String answer) {
		this.questionId = (int)questionId;
		this.question = question;
		this.answerId = (int)answerId;
		this.answer = answer;
	}
}
