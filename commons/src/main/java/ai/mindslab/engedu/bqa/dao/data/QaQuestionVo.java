package ai.mindslab.engedu.bqa.dao.data;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@SuppressWarnings("serial")
@Data
public class QaQuestionVo implements Serializable {
	
	public final static int USE_YN_Y = 1;
	public final static int USE_YN_N = 0;
	
	private int questionId;
	private String question;
	private String questionMorph;
	private int domainId;
	private String useYn;
	private String questionType;
	private LocalDateTime createdDtm;
	private LocalDateTime updatedDtm;
	private String creatorId;
	private String updatorId;
	private QaAnswerVo qaAnswerVo;
	private List<QaAnswerVo> answerList;
	private String mainYn;
	private Float score;
}
