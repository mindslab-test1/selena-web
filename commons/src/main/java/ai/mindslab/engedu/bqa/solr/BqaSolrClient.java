package ai.mindslab.engedu.bqa.solr;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.*;

import ai.mindslab.engedu.common.exceptions.EngEduException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrQuery.ORDER;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.params.ModifiableSolrParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import ai.mindslab.engedu.bqa.dao.data.QaDebugVo;
import ai.mindslab.engedu.bqa.dao.data.QaIndexVo;
import ai.mindslab.engedu.bqa.dao.data.QaIndexingHistoryVo;
import ai.mindslab.engedu.bqa.commons.codes.IBasicQAConst;
import ai.mindslab.engedu.common.codes.IRestCodes;

@Repository
public class BqaSolrClient implements IRestCodes, IBasicQAConst {

	private Logger logger = LoggerFactory.getLogger(BqaSolrClient.class);

	private SolrClient bqaSolrClient;

	private String urlString;

	private final String SOLR_QUERY_TYPE_AND = " AND ";
	private final String SOLR_QUERY_TYPE_OR = " OR ";

	@Value("${solr.bqa.search.max:10}")
	int maxCount;
	@Value("${maum.chunk.query.count:100}")
	int queryCount;
	@Value("${solr.bqa.search.or.result.count:3}")
	int solrOrResultCount;
	@Value("${solr.bqa.server.zone:Asia/Seoul}")
	String zone;
	BqaSolrClient(@Value("${solr.bqa.server.url}") String urlString) {
		if (urlString.isEmpty()) {
			urlString = "http://10.122.64.155:8983/solr/basicQa";
		}
		bqaSolrClient = new HttpSolrClient.Builder(urlString).build();
		logger.debug("Solr urlString : {}", urlString);
	}

	protected void finalize() throws Throwable {
		bqaSolrClient.close();
	}

	public QaIndexingHistoryVo setResult(QaIndexingHistoryVo qaIndexingHistoryVo, QueryResponse res) {
		String statusMessages = res.getResponse().get("statusMessages").toString();
		qaIndexingHistoryVo.setMsg(statusMessages);

		logger.debug("fullIndexing setResult{}", statusMessages);
		String[] data = statusMessages.split(",");
		try {
			for (String aData : data) {
				if (aData.indexOf("Committed=") > 0) {
					String date = aData.replaceFirst("Committed=", "");
					SimpleDateFormat transFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					Date d = transFormat.parse(date);
					Instant instant = Instant.ofEpochMilli(d.getTime());
					LocalDateTime ldt = LocalDateTime.ofInstant(instant, ZoneId.of(zone));
					qaIndexingHistoryVo.setCommittedDtm(ldt);

				} else if (aData.toLowerCase().indexOf("dump started=") > 0) {
					String date = aData.toLowerCase().split("dump started=")[1];
					SimpleDateFormat transFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					Date d = transFormat.parse(date);
					Instant instant = Instant.ofEpochMilli(d.getTime());
					LocalDateTime ldt = LocalDateTime.ofInstant(instant, ZoneId.of(zone));
					qaIndexingHistoryVo.setIndexingStartedDtm(ldt);
				} else if (aData.indexOf("Time taken=") > 0) {
					String takenTime = aData.replaceFirst("Time taken=", "").replaceFirst("}", "");
					String[] t = takenTime.split(":");
					String sm = t[2];
					String s = sm.substring(0, sm.indexOf("."));
					String m = sm.substring(sm.indexOf(".") + 1, sm.length());

					SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.SSS");
					Calendar now = Calendar.getInstance();
					now.set(Calendar.HOUR_OF_DAY, Integer.parseInt(t[0].trim()));
					now.set(Calendar.MINUTE, Integer.parseInt(t[1].trim()));
					now.set(Calendar.SECOND, Integer.parseInt(s));
					now.set(Calendar.MILLISECOND, Integer.parseInt(m));
					qaIndexingHistoryVo.setTakenTime(sdf.format(now.getTime()));
				}

			}
			logger.debug("fullIndexing qaIndexingHistoryVo{}", qaIndexingHistoryVo.toString());
		} catch (Exception e) {
			logger.warn("Solr result parsing error : {}", e);
		}

		return qaIndexingHistoryVo;
	}

	public QaIndexingHistoryVo fullIndexing(QaIndexingHistoryVo qaIndexingHistoryVo)
			throws SolrServerException, IOException {
		ModifiableSolrParams params = new ModifiableSolrParams();
		params.set("qt", "/dataimport");
		params.set("clean", "true");
		params.set("command", "full-import");

		QueryResponse res = bqaSolrClient.query(params);
		qaIndexingHistoryVo = this.setResult(qaIndexingHistoryVo, res);
		logger.debug("Solr QueryResponse res: {}", res);

		UpdateResponse commit = bqaSolrClient.commit();
		logger.debug("Solr QueryResponse commit: {}", commit);
		if (commit.getStatus() == 0) {
			
			if(qaIndexingHistoryVo.getCommittedDtm() == null) {
				
				Instant instant = Instant.ofEpochMilli(new Date().getTime());
				LocalDateTime ldt = LocalDateTime.ofInstant(instant, ZoneId.of(zone));
				qaIndexingHistoryVo.setCommittedDtm(ldt);
			}
			
			qaIndexingHistoryVo.setStatus(qaIndexingHistoryVo.INDEX_STATUS_SUCCESS);
		} else {

			qaIndexingHistoryVo.setStatus(qaIndexingHistoryVo.INDEX_STATUS_FAIL);
		}

		// qaIndexingHistoryVo.setStatus(qaIndexingHistoryVo.INDEX_STATUS_ERROR);

		return qaIndexingHistoryVo;
	}

	public QaIndexingHistoryVo addIndexing(QaIndexingHistoryVo qaIndexingHistoryVo, int[] removeIds)
			throws SolrServerException, IOException {
		logger.debug("Solr urlString : {}", urlString);

		for (int removeId : removeIds) {
			bqaSolrClient.deleteById(String.valueOf(removeId));
			logger.debug("addIndexing removeIds[i]{}", removeId);
		}

		ModifiableSolrParams params = new ModifiableSolrParams();
		params.set("qt", "/dataimport");
		params.set("command", "delta-import");

		QueryResponse res = bqaSolrClient.query(params);
		UpdateResponse commit = bqaSolrClient.commit();
		logger.debug("addIndexing res{}", res);
		
		qaIndexingHistoryVo = this.setResult(qaIndexingHistoryVo, res);

		if (commit.getStatus() == 0) {
			
			logger.info("qaIndexingHistoryVo.getCommittedDtm() is null{}",qaIndexingHistoryVo.getCommittedDtm() );
			if(qaIndexingHistoryVo.getCommittedDtm() == null) {
				
				Instant instant = Instant.ofEpochMilli(new Date().getTime());
				LocalDateTime ldt = LocalDateTime.ofInstant(instant, ZoneId.of(zone));
				qaIndexingHistoryVo.setCommittedDtm(ldt);
			}
			qaIndexingHistoryVo.setStatus(qaIndexingHistoryVo.INDEX_STATUS_SUCCESS);
		} else {

			qaIndexingHistoryVo.setStatus(qaIndexingHistoryVo.INDEX_STATUS_FAIL);
		}

		return qaIndexingHistoryVo;
	}

	public QaIndexingHistoryVo domainIndexing(QaIndexingHistoryVo qaIndexingHistoryVo, List<QaIndexVo> indexList)
			throws SolrServerException, IOException {

		logger.info("domainIndexing indexList{}", indexList.size());

		bqaSolrClient.deleteByQuery("domain_id:" + String.valueOf(qaIndexingHistoryVo.getDomainId()));

		Collection<SolrInputDocument> docs = new ArrayList<>();
		qaIndexingHistoryVo.setIndexingStartedDtm(LocalDateTime.now());
		for (QaIndexVo qaIndexVo : indexList) {
			SolrInputDocument solrInputDocument = new SolrInputDocument();
			solrInputDocument.addField("id", qaIndexVo.getIndexId());
			solrInputDocument.addField("question_id", qaIndexVo.getQuestionId());
			solrInputDocument.addField("question_no", qaIndexVo.getQuestionNo());
			solrInputDocument.addField("question", qaIndexVo.getQuestion());
			solrInputDocument.addField("morph_count", qaIndexVo.getMorphCount());
			solrInputDocument.addField("main_yn", qaIndexVo.getMainYn());
			solrInputDocument.addField("question_morph", qaIndexVo.getQuestionMorph());
			solrInputDocument.addField("domain_id", qaIndexVo.getDomainId());

			docs.add(solrInputDocument);
		}
		UpdateResponse res = bqaSolrClient.add(docs);
		
		UpdateResponse commit = bqaSolrClient.commit();
		qaIndexingHistoryVo.setCommittedDtm(LocalDateTime.now());
		
		try {
			LocalDateTime startTime =  qaIndexingHistoryVo.getIndexingStartedDtm();
			LocalDateTime endTime = qaIndexingHistoryVo.getCommittedDtm();
			long millis = startTime.until(endTime, ChronoUnit.MILLIS);
			qaIndexingHistoryVo.setTakenTime(DurationFormatUtils.formatDuration(millis, "HH:mm:ss.SSS"));
		}catch(Exception e) {
			logger.warn("setTakenTime error", e);
		}
		
		logger.debug("domainIndexing qaIndexingHistoryVo{}", qaIndexingHistoryVo);

		qaIndexingHistoryVo.setTotal(indexList.size());
		qaIndexingHistoryVo.setMsg(res.getResponse().toString());

		if (commit.getStatus() == 0) {

			qaIndexingHistoryVo.setStatus(qaIndexingHistoryVo.INDEX_STATUS_SUCCESS);
		} else {

			qaIndexingHistoryVo.setStatus(qaIndexingHistoryVo.INDEX_STATUS_FAIL);
		}

		return qaIndexingHistoryVo;
	}

	public boolean removeIndexing(int... qnaIndexId) throws SolrServerException, IOException {
		logger.info("removeIndexing");
		boolean result = false;

		logger.debug("Solr urlString : {}", urlString);

		for (int i = 0; i < qnaIndexId.length; i++) {
			bqaSolrClient.deleteById(String.valueOf(i));
		}
		UpdateResponse res = bqaSolrClient.commit();

		if (res != null && res.getStatus() == 0) {
			result = true;
		}
		return result;
	}

	public QaIndexVo search(String questionMorph, int... domainIds)
			throws SolrServerException, IOException, EngEduException {
		logger.debug("Repository search() ,{},{}", questionMorph, domainIds);
		QaIndexVo qaIndexVo = new QaIndexVo();
		qaIndexVo.setRes(QaIndexVo.SEARCH_CODE_FAIL);

		SolrQuery solrQuery = buildAndQuery(questionMorph, domainIds);
		QueryResponse queryResponse = bqaSolrClient.query(solrQuery);
		SolrDocumentList documentList = queryResponse.getResults();

		logger.info("documentList size : " + documentList.size());

		// 너무많은 결과가 나왔을떄 처리
		if (documentList.size() > maxCount) {

			throw new EngEduException(ERR_CODE_SEARCH_SOLR_TOO_MANY_RESULT, ERR_MSG_SEARCH_SOLR_TOO_MANY_RESULT);
		} else if (documentList.size() >= 1) {

			int index = 0;
			for(SolrDocument doc : documentList){
				String solrQuestionMorph = String.valueOf(doc.get("question_morph"));
				if(StringUtils.equalsIgnoreCase(questionMorph, solrQuestionMorph)) {
					index = documentList.indexOf(doc);
				}
			}

			SolrDocument document = documentList.get(index);
			logger.debug("document {}", document);
			qaIndexVo.setIndexId(Integer.parseInt(String.valueOf(document.get("id"))));
			qaIndexVo.setDomainId(Integer.parseInt(String.valueOf(document.get("domain_id"))));
			qaIndexVo.setQuestionMorph(String.valueOf(document.get("question_morph")));
			qaIndexVo.setQuestionId(Integer.parseInt(String.valueOf(document.get("question_id"))));
			qaIndexVo.setQuestionNo(Integer.parseInt(String.valueOf(document.get("question_no"))));
			qaIndexVo.setQuestion(String.valueOf(document.get("question")));
			qaIndexVo.setMainYn(String.valueOf(document.get("main_yn")));
			qaIndexVo.setScore(Float.parseFloat(String.valueOf(document.get("score"))));
			qaIndexVo.setQuestionNo(Integer.parseInt(String.valueOf(document.get("morph_count"))));

			qaIndexVo.setRes(QaIndexVo.SEARCH_CODE_SUCCESS);
			logger.debug("document qaIndexVo{}", qaIndexVo);
		}
		return qaIndexVo;
	}

	private List<QaIndexVo> getIndexList(int minWordCount, int maxWordCount, SolrDocumentList documentList, int count) {
		List<QaIndexVo> indexList = new ArrayList<>();

		for (SolrDocument document : documentList) {
			logger.debug("document {}", document);
			int indexMorphCount = Integer.parseInt(String.valueOf(document.get("morph_count")));
			if (count == solrOrResultCount || (indexMorphCount < minWordCount) || indexMorphCount > maxWordCount) {
				break;
			}

			QaIndexVo qaIndexVo = new QaIndexVo();
			qaIndexVo.setIndexId(Integer.parseInt(String.valueOf(document.get("id"))));
			qaIndexVo.setDomainId(Integer.parseInt(String.valueOf(document.get("domain_id"))));
			qaIndexVo.setQuestionMorph(String.valueOf(document.get("question_morph")));
			qaIndexVo.setQuestionId(Integer.parseInt(String.valueOf(document.get("question_id"))));
			qaIndexVo.setQuestionNo(Integer.parseInt(String.valueOf(document.get("question_no"))));
			qaIndexVo.setQuestion(String.valueOf(document.get("question")));
			qaIndexVo.setScore(Float.parseFloat(String.valueOf(document.get("score"))));
			qaIndexVo.setRes(QaIndexVo.SEARCH_CODE_SUCCESS);
			qaIndexVo.setMainYn(String.valueOf(document.get("main_yn")));
			qaIndexVo.setMorphCount(indexMorphCount);
			logger.debug("document qaIndexVo{}", qaIndexVo);
			count++;
			indexList.add(qaIndexVo);
		}
		return indexList;
	}

	private SolrQuery buildAndQuery(String questionMorph, int[] domainIds) {
		logger.info("buildAndQuery:questionMorph {}, domainIds {}", questionMorph, domainIds);
		SolrQuery solrQuery = new SolrQuery();
		String[] morphQuery = StringUtils.split(questionMorph, " ");
		int morph_count = morphQuery.length;
		logger.debug("buildAndQuery:morphQuery {}, morph_count {}", morphQuery, morph_count);
		for (int i = 0; i < morphQuery.length; i++) {
			morphQuery[i] = "question_morph:" + morphQuery[i];
		}
		StringBuffer qBuffer = new StringBuffer("(" + StringUtils.join(morphQuery, SOLR_QUERY_TYPE_AND) + ")");
		logger.debug("qBuffer.toString{}", qBuffer.toString());

		buildQueryDomainIdsPart(domainIds, qBuffer);

		// morph_count : nlp token count
		qBuffer.append(SOLR_QUERY_TYPE_AND).append("( morph_count:").append(morph_count).append(")");
		logger.debug("qBuffer.toString{}", qBuffer.toString());
		solrQuery.setQuery(qBuffer.toString());
		solrQuery.setFields("*", "score");
		solrQuery.addSort("score", ORDER.desc);

		logger.info("buildAndQuery: {}", solrQuery.toString());
		return solrQuery;
	}

	private void buildQueryDomainIdsPart(int[] domainIds, StringBuffer qBuffer) {
		if (domainIds != null && domainIds.length > 0 && domainIds[0] != 0) {
			String[] domainQuery = new String[domainIds.length];
			for (int i = 0; i < domainQuery.length; i++) {
				domainQuery[i] = "domain_id:" + String.valueOf(domainIds[i]);
			}
			qBuffer.append(SOLR_QUERY_TYPE_AND + "(").append(StringUtils.join(domainQuery, SOLR_QUERY_TYPE_OR)).append(")");
		}
	}

	public void removeOneDomain(int domainId) throws Exception {

		bqaSolrClient.deleteByQuery("domain_id:" + String.valueOf(domainId));
		logger.debug("removeOneDomain solr{}",domainId);
		bqaSolrClient.commit();
	}

}
