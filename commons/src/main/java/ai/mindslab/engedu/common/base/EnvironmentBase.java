package ai.mindslab.engedu.common.base;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class EnvironmentBase {

    @Autowired
    private Environment environment;

    /**
     * 현재 설정된 application.properties를 가져온다.
     * ex) local or dev or prod ..
     * @return
     */
    public String getActiveProfile(){

        String[] profiles = environment.getActiveProfiles();

        for(String profile : profiles) log.debug("profile ............... {}", profile);
        return profiles.length == 1 ? profiles[0].toLowerCase() : "local";

    }

    /**
     * 현재 설정된 application.properties의 값 불러온다.
     * ex) brain.tts.ip = 192.168...
     * @param paramProperty
     * @return
     */
    public String getProperties(String paramProperty){

        return environment.getProperty(paramProperty);
    }


}
