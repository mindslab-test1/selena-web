package ai.mindslab.engedu.common.codes;

public interface IGrammarEvaluationCodes {

    // DiffMatchPatchUtil 클래스를 이용하여 문법 평가 점수 측정
    public static final String DIFF = "DIFF";
    // 각각의 문자로 비교하여 문법 평가 점수 측정
    public static final String EQUAL = "EQUAL";

}
