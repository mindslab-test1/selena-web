package ai.mindslab.engedu.common.codes;

public interface IMarkCodes {

    public static final String FORWARD_SLASH = "/";
    public static final String UNDERSCORE = "_";
    public static final String BACKSLASH = "\\";
    public static final String COMMA = ",";
    public static final String PERIOD = ".";
    public static final String QUESTION_MARK  = "?";
    public static final String EXCLAMATION_MARK = "!";
    public static final String ANSWER_SPLIT_TEXT = " ";
    public static final String SINGLE_QUOTATION_MARKS = "'";
    public static final String EMPTY_TEXT = "";

}
