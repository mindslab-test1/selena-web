package ai.mindslab.engedu.common.codes;

public interface IPronounceCodes {

    public static final String WORD = "WORD";
    public static final String SENTENCE = "SENTENCE";

    // 정답 문장
    public static final String PRONOUNCE_ANSWER = "ANSWER";
    // 사용자 발화
    public static final String PRONOUNCE_UTTER = "UTTER";
}
