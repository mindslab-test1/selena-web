package ai.mindslab.engedu.common.dao;

import ai.mindslab.engedu.admin.eval.english.dao.data.EngEvalFileExtVO;
import ai.mindslab.engedu.common.data.EvaluationVO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface EvaluationMapper {

    int insertUserEvaluation(EvaluationVO evaluationVO);

    EngEvalFileExtVO getUserEvaluation(String evaluationDir);
    
}
