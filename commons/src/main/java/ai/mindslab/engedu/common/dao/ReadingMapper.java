package ai.mindslab.engedu.common.dao;

import ai.mindslab.engedu.common.data.ReadingVO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ReadingMapper {

    int insertUserReading(ReadingVO readingVO);
    
}
