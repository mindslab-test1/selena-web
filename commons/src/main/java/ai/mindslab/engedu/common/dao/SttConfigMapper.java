package ai.mindslab.engedu.common.dao;

import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SttConfigMapper {

	String getServiceType(Map<String, String> hashMap);
	String getSpeedGameModel(Map<String, String> hashMap);
	String getModel(Map<String, String> hashMap);
}
