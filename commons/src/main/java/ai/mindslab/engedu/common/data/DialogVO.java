package ai.mindslab.engedu.common.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DialogVO implements Serializable {

    /** 학습자 아이디 */
    private String userId;
    
    /** 파일 경로*/
    private String filePath;
    
    /** stt 텍스트 파일명 */
    private String sttFileTextName;
    
    /** stt raw 파일명*/
    private String sttFileWaveName;

    /** tts raw 파일명*/
    private String ttsFileMp3Name;   
    
    /** 생성자 아이디 */
    private String creatorId;

    /** 사용자 발화 문장 */
    private String userText;

    /** 대답 문장 */
    private String answerText;
}
