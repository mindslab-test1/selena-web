package ai.mindslab.engedu.common.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EvaluationVO implements Serializable {

    /** 학습자 아이디 */
    private String userId;
    /** 파일 경로*/
    private String filePath;
    /** 텍스트 파일명 */
    private String fileTextName;
    /** raw 파일명*/
    private String fileWaveName;
    /** MP3 파일명*/
    private String fileMp3Name;
    /** 생성자 아이디 */
    private String creatorId;
    /** 문법 평가 점수 */
    private String grammarScore;
    /** 발음 평가 점수 */
    private String pronounceScore;
    /** 발음 평가 기타 점수*/
    private String pronounceEtcScore;
    /** 문법(75%) + 발음(25%) 평균 점수 */
    private String averageScore;
    /** 학습자 발화 문구*/
    private String userText;
    /** 정답 문구*/
    private String answerText;

}
