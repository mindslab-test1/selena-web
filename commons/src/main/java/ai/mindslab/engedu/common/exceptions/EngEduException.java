package ai.mindslab.engedu.common.exceptions;

public class EngEduException extends Exception {
	
	private int errCode;
	private String errMsg;
	
	public EngEduException() {
	}
	
	public EngEduException(int code, String msg) {
		super(msg);
		this.errCode = code;
		this.errMsg = msg;
	}

	public EngEduException(int code, String msg, String message) {
		super(message);
		this.errCode = code;
		this.errMsg = msg;
	}
	
	public EngEduException(String message) {
		super(message);
	}

	public EngEduException(Throwable cause) {
		super(cause);
	}

	public EngEduException(String message, Throwable cause) {
		super(message, cause);
	}

	public EngEduException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
	
	public int getErrCode() {
		return errCode;
	}
	
	public String getErrMsg() {
		return errMsg;
	}


}
