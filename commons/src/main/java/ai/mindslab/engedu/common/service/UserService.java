package ai.mindslab.engedu.common.service;

import ai.mindslab.engedu.admin.eval.english.dao.data.EngEvalFileExtVO;
import ai.mindslab.engedu.common.dao.DialogMapper;
import ai.mindslab.engedu.common.dao.EvaluationMapper;
import ai.mindslab.engedu.common.dao.ReadingMapper;
import ai.mindslab.engedu.common.data.DialogVO;
import ai.mindslab.engedu.common.data.EvaluationVO;
import ai.mindslab.engedu.common.data.ReadingVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class UserService {

    @Autowired
    private DialogMapper dialogMapper;

    @Autowired
    private EvaluationMapper evaluationMapper;

    @Autowired
    private ReadingMapper readingMapper;

    public int insertUserDialog(DialogVO dialogVO){

        return dialogMapper.insertUserDialog(dialogVO);
    }

    public int insertUserEvaluation(EvaluationVO evaluationVO){

        return evaluationMapper.insertUserEvaluation(evaluationVO);
    }

    public EngEvalFileExtVO getUserEvaluation(String evaluationDir){

        return evaluationMapper.getUserEvaluation(evaluationDir);
    }

    public int insertUserReading(ReadingVO readingVO){

        return readingMapper.insertUserReading(readingVO);
    }
}
