package ai.mindslab.engedu.common.sttPost.dialog;

import ai.mindslab.engedu.common.codes.IMathCodes;

public class MathDicSttPostUtil {

    public String getNumberToMessage(String paramMessage){

        String result = paramMessage;

        if (IMathCodes.ONE_NUMBER_TEXT.equals(paramMessage.trim())) {
            result = IMathCodes.ONE_STRING_TEXT;
        }

        return  result;

    }

}
