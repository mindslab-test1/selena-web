package ai.mindslab.engedu.common.sttPost.dialog;

public class TimesTableSttPostUtil {

    /**
     * 첫번째 문자 / 마지막 문자열 조합해서 반환한다. ex) 203 = 23
     * @param paramMessage 사용자 발화 문장
     * @return
     */
    public String getFirstPlusLastMessage(String paramMessage){

        String result = paramMessage.substring(0,1) + paramMessage.substring(paramMessage.length()-1);
        return result;

    }


}
