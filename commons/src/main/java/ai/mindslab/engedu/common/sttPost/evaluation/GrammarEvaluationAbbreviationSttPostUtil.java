package ai.mindslab.engedu.common.sttPost.evaluation;


import ai.mindslab.engedu.common.TempPostProcess;
import ai.mindslab.engedu.common.codes.IMarkCodes;
import ai.mindslab.engedu.evaluation.dao.data.GrammarEvaluationAbbreviationVO;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class GrammarEvaluationAbbreviationSttPostUtil {

    /**
     * 문법 평가 축약어 처리
     * @param paramInputStr 발화문장
     * @param paramAnswerStr 정답문장
     * @param paramAbbreviationList 축약어 리스트
     * @return
     */
    public ArrayList<String> getAbbreviationStr(
            String paramInputStr,
            String paramAnswerStr,
            List<GrammarEvaluationAbbreviationVO> paramAbbreviationList)
    {

//        String result = paramInputStr;
        ArrayList<String> result = new ArrayList<>();
        result.add(paramInputStr);
        result.add(paramAnswerStr);

        try{

            // 발화 문장 소문자로 변경
            String inputSQMReplaceStr = processingComparableStr(paramInputStr).toLowerCase();

            // 정답 문장에서 축약형이 있는지 여부 확인
//            int answerSQMStrCnt = paramAnswerStr.indexOf(IMarkCodes.SINGLE_QUOTATION_MARKS);
            paramAnswerStr = paramAnswerStr.toLowerCase();

            int answerSQMStrCnt = isAnswerAbbreivation(paramAnswerStr, paramAbbreviationList);

            // 발화문에 축약형이 있을 경우 원형으로 변환
            inputSQMReplaceStr = convertUtterAbbToOriginalStr(inputSQMReplaceStr,paramAbbreviationList);

            // 정답 문장에 축약형이 있을 경우 inputSQMReplaceStr 값을 축약형으로 변환
            inputSQMReplaceStr = answerSQMStrCnt >= 0 ? convertUtterOriginalToAbbStr(inputSQMReplaceStr,paramAbbreviationList) : inputSQMReplaceStr;

//            result = inputSQMReplaceStr;
            result.set(0,inputSQMReplaceStr);

            TempPostProcess tempPostProcess = new TempPostProcess();
            boolean valid = tempPostProcess.chekPostValid(result.get(1));
            if (valid) {
                // 정답 문자 소문자로 변경
                String outputSQMReplaceStr = processingComparableStr(paramAnswerStr).toLowerCase();

                outputSQMReplaceStr = convertUtterAbbToOriginalStr(outputSQMReplaceStr, paramAbbreviationList);

                outputSQMReplaceStr = answerSQMStrCnt >=0 ?
                        convertUtterOriginalToAbbStr(outputSQMReplaceStr, paramAbbreviationList) : outputSQMReplaceStr;

                result.set(1,outputSQMReplaceStr);

            }

        }catch (Exception e){

            log.error("getAbbreviationStr Error : " + e.getMessage());

        }

        return result;

    }

    /**
     * 정답문장에서 축약형이 있을 경우에만 해당 ( 발화 문장 모두 푼 것을 다시 축약형으로 변환 해서 비교하기 위하여 )
     *  db 축약형에서 발화 문장 모두 푼 것을 찾아서, 다시 축약형으로 변환
     *  ex) db { 축약형 i'm , 원 형태 i am } -> 발화 문장 i am ground 를 다시 i'm ground 로 변경
     */
    private String convertUtterOriginalToAbbStr(
            String paramInputSQMReplaceStr,
            List<GrammarEvaluationAbbreviationVO> paramAbbreviationList) throws Exception
    {

        String result = paramInputSQMReplaceStr;

        for (GrammarEvaluationAbbreviationVO abbreviationVO : paramAbbreviationList) {

            //축약형
            String abbreviationText = abbreviationVO.getAbbreviationText();
            // 원본
            String originalText = abbreviationVO.getOriginalText();

            int utterOriginalTextFindLocation = result.indexOf(originalText);

            if (utterOriginalTextFindLocation >= 0) {

                int startNum = utterOriginalTextFindLocation;
                int endNum = 0;
                if(result.length() == originalText.length()){
                    endNum = startNum + originalText.length();
                }else{

                    endNum = startNum + originalText.length()+1;

                    if(endNum > result.length()){
                        endNum = endNum -1;
                    }
                }

                if(result.substring(startNum,endNum).trim().equals(originalText)){
                    result = result.replaceAll(originalText, abbreviationText);
                }

            }

        }

        return result;

    }


    private int isAnswerAbbreivation(
            String paramInputSQMReplaceStr,
            List<GrammarEvaluationAbbreviationVO> paramAbbreviationList) throws Exception
    {

        String result = paramInputSQMReplaceStr;
        int isAbbreivation = -1;

        for (GrammarEvaluationAbbreviationVO abbreviationVO : paramAbbreviationList) {

            // 축약형
            String abbreviationText = abbreviationVO.getAbbreviationText();
            // 원본
            String originalText = abbreviationVO.getOriginalText();

            int utterAbbreviationTextFindLocation = result.indexOf(abbreviationText);

            if(utterAbbreviationTextFindLocation >=0){

                isAbbreivation = 1;

            }

        }

        return isAbbreivation;

    }

    /**
     * 발화 문장에서 축약형이 있다면 발화 문장을 축약형에서 원 형태로 변환
     * ex) 발화 문장 : i'm ground / db { 축약형 i'm , 원 형태 i am } -> i am ground 로 변경
     */
    private String convertUtterAbbToOriginalStr(
            String paramInputSQMReplaceStr,
            List<GrammarEvaluationAbbreviationVO> paramAbbreviationList) throws Exception
    {

        String result = paramInputSQMReplaceStr;

        for (GrammarEvaluationAbbreviationVO abbreviationVO : paramAbbreviationList) {

            // 축약형
            String abbreviationText = abbreviationVO.getAbbreviationText();
            // 원본
            String originalText = abbreviationVO.getOriginalText();

            int utterAbbreviationTextFindLocation = result.indexOf(abbreviationText);

            if(utterAbbreviationTextFindLocation >=0){

                result = result.replaceAll(abbreviationText,originalText);

            }

        }

        return result;

    }

    private String processingComparableStr(String str) {

        // 특수문자 제거
        str = str.replaceAll("[,.!?;]*", "");

        // 연속공백을 공백으로 대체
        str = str.replaceAll("\\s{2,}", " ");

        // 모든 문자 소문자로 변경
        str = str.toLowerCase().trim();

        return str;
    }

}
