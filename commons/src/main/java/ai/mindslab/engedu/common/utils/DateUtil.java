package ai.mindslab.engedu.common.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 날짜와 관련된 유틸리티 클래스
 * @author  risingsuntae
 */
 public class DateUtil {

    /** 년 단위 ex) 2018 */
    public static final String TO_YEAR = "yyyy";

    /** 년월 단위 ex) 201809*/
    public static final String TO_MONTH = "yyyyMM";

    /** 년월일 단위 ex) 20180901 */
    public static final String TO_DAY = "yyyyMMdd";

    /** 년월일시 단위 ex) 2018090115 */
    public static final String TO_HOUR = "yyyyMMddHH";

    /** 년월일시분 단위 ex) 201809050900 */
    public static final String TO_MINUTE = "yyyyMMddHHmm";

    /** 년월일시분초 단위 ex) 20180905090000*/
    public static final String TO_SECOND = "yyyyMMddHHmmss";


    /**
     * 현재 시간 return
     * @param paramDateFormat 날짜/시간 형식
     * @return 현재 시간 형식에 맞춰 반환
     */
    public String getCurrentTime(String paramDateFormat){

        long time = System.currentTimeMillis();
        SimpleDateFormat dayTime = new SimpleDateFormat(paramDateFormat);

        return dayTime.format(new Date(time));

    }


}
