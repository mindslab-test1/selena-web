package ai.mindslab.engedu.common.utils;

import ai.mindslab.engedu.common.codes.IMarkCodes;

import java.net.InetAddress;


public class DevelopCheckUtil {

    public boolean isDev() {

        boolean result = false;

        try{

            String ip = InetAddress.getLocalHost().getHostAddress();
            result = getDevIP(ip);

        }catch (Exception e){
            e.printStackTrace();
        }


        return result;

    }

    private boolean getDevIP(String ip) {

        String[] ipArr = ip.split("\\.");

        StringBuilder tempVal = new StringBuilder();

        boolean result = false;

        for (int i=0; i<ipArr.length-1; i++){
            tempVal.append(ipArr[i]).append(IMarkCodes.PERIOD);
        }

        if(tempVal.toString().equals("10.122.64.") && !ip.equals("10.122.64.57") && !ip.equals("10.122.64.58")){

            result = true;
        }

        return result;

    }

}
