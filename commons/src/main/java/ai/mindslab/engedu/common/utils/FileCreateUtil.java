package ai.mindslab.engedu.common.utils;

import ai.mindslab.engedu.common.codes.IMarkCodes;
import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermission;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

@Slf4j
public class FileCreateUtil {


    /**
     * <pre>
     *     현재 시간을 기준으로 년월일까지 잘라서 스토리지에 폴더가 없다면 생성
     *     ex) 20180808 -> 2018 / 08 / 08
     * </pre>
     * @param paramFilePath 파일경로
     * @param paramCurrentTime  현재 시간
     * @return
     */
    public  String makeFolder(String paramFilePath,String paramCurrentTime){

        String year = paramCurrentTime.substring(0,4);
        String month = paramCurrentTime.substring(4,6);
        String day = paramCurrentTime.substring(6,8);

        Path targetYearPath = Paths.get(paramFilePath + year);
        Path targetMonthPath = Paths.get(paramFilePath + year + IMarkCodes.FORWARD_SLASH + month);
        Path targetDayPath = Paths.get(paramFilePath + year + IMarkCodes.FORWARD_SLASH + month +IMarkCodes.FORWARD_SLASH+ day);

        try {

            Files.createDirectories(targetYearPath);
            Files.createDirectories(targetMonthPath);
            Files.createDirectories(targetDayPath);

            /*
            String [] pathArr = String.valueOf(targetDayPath).split(IMarkCodes.FORWARD_SLASH);

            String tempVal = "";

            String osType = getOS();
            String permission = "rwxrwxr-x";

           if(osType.equals("unix")){

                for(int i=1; i<pathArr.length; i++){

                    tempVal += IMarkCodes.FORWARD_SLASH + pathArr[i];

                    // 디렉토리 권한 확인 ex) rwxrwxr-x
                    Set<PosixFilePermission> directoryGetPermission = Files.getPosixFilePermissions(Paths.get(tempVal));

                    // 디렉토리 권한이 775가 아닌 경우 && 디렉토리가 존재할 경우에 디렉토리 권한을 775로 변경
                    if(!PosixFilePermissions.toString(directoryGetPermission).equals(permission) && Files.exists(Paths.get(tempVal))){
                        Set<PosixFilePermission> perms = PosixFilePermissions.fromString(permission);
                        Files.setPosixFilePermissions(Paths.get(tempVal),perms);
                    }
                }
            }
            */


        } catch (Exception e) {
            e.printStackTrace();
        }

        return String.valueOf(targetDayPath);

    }

    /**
     * 파일명 구하는 메서드
     * ex) risingsuntae_20180808135901.txt / risingsuntae_20180808135901.raw
     * @param paramUserId 학습자 id
     * @param paramFileName 파일명
     * @param fileExt 파일 확장자
     * @return 파일명
     */
    public String getFileName(String paramUserId,String paramFileName,String paramRandomNum,String fileExt){

        StringBuilder result = new StringBuilder();
        try{
            result.append(paramUserId);
            result.append(IMarkCodes.UNDERSCORE);
            result.append(paramFileName);
            result.append(IMarkCodes.UNDERSCORE);
            result.append(paramRandomNum);
            result.append(fileExt);
        }catch (Exception e){

            e.printStackTrace();
        }
        return result.toString();
    }

    /**
     * full 파일명 구하는 메서드 ( 스토리지 + 파일명 )
     * E\\storate\\2018\\08\08\\risingsuntae_20180808135901.txt
     * @param paramFilepath 파일경로
     * @return 파일경로 + 파일명
     */
    public String getFolderFileName(String paramFileName,  String paramFilepath){

        StringBuilder result = new StringBuilder();
        try {
            result.append( paramFilepath + IMarkCodes.FORWARD_SLASH);
            result.append( paramFileName);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return result.toString();

    }

    /**
     * 파일이 저장할 폴더 구하는 메서드 (도메인 제외)
     * paramFolderSplitText = / 고정
     * ex) /2018/08/08/
     * @param paramFolderSplitText /
     * @param paramCurrentTime 생성시간
     * @return 파일 저장 폴더
     */
    public String getFileSubPath(String paramFolderSplitText,String paramCurrentTime){

        StringBuilder result = new StringBuilder();
        try{

            result.append(paramCurrentTime, 0, 4).append(paramFolderSplitText);
            result.append(paramCurrentTime, 4, 6).append(paramFolderSplitText);
            result.append(paramCurrentTime, 6, 8).append(paramFolderSplitText);

        }catch(Exception e){
            e.printStackTrace();
        }

        return result.toString();

    }

    /**
     * 파일 생성
     * @param paramMsg 사용자 문장
     * @param paramFileName 파일명
     */
    public void writeHandFile(String paramMsg, String paramFileName) {
        try {
            String fileName = paramFileName;

            File file = new File(fileName);

            fileName = fileName.replaceAll("\\\\","/");

            BufferedWriter writer = new BufferedWriter(new FileWriter( file));

            log.info("DialogWebSocket writeFile msg:{}", paramMsg);

            writer.write(paramMsg);
            writer.close();

            //setFilePermission(fileName);

        } catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * 파일 생성 wav
     * @param paramByteArrayOutputStream paramByteArrayOutputStream
     * @param paramFileName 파일명
     * @param paramSampleRate 비트레이트
     */
    public void writeHandPcm(ByteArrayOutputStream paramByteArrayOutputStream, String paramFileName, int paramSampleRate) {

        byte result[] = paramByteArrayOutputStream.toByteArray();
        String fileName = paramFileName;
        short  numChannels = 1; // mono
        int SAMPLE_RATE = paramSampleRate;
        int BITS_PER_SAMPLE = 16;

        try {

            File file = new File(fileName);

            fileName = fileName.replaceAll("\\\\","/");

            DataOutputStream outStream = new DataOutputStream(new FileOutputStream(file));

            // write the wav file per the wav file format
            outStream.write(result); // 44 - the actual data itself - just a long string of numbers

            outStream.close();

            //setFilePermission(fileName);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    
    public void writeHandWav(ByteArrayOutputStream paramByteArrayOutputStream, String paramFileName, int paramSampleRate) {

        byte result[] = paramByteArrayOutputStream.toByteArray();
        String fileName = paramFileName;
        short  numChannels = 1; // mono
        int SAMPLE_RATE = paramSampleRate;
        int BITS_PER_SAMPLE = 16;

        try {

            File file = new File(fileName);

            fileName = fileName.replaceAll("\\\\","/");

            DataOutputStream outStream = new DataOutputStream(new FileOutputStream(file));

            // write the wav file per the wav file format
            outStream.writeBytes("RIFF"); // 00 - RIFF
            outStream.write(intToByteArray(32 + result.length), 0, 4); // 04 - how big is the rest of this file?
            outStream.writeBytes("WAVE"); // 08 - WAVE
            outStream.writeBytes("fmt "); // 12 - fmt
            outStream.write(intToByteArray(16), 0, 4); // 16 - size of this chunk
            outStream.write(shortToByteArray((short) 1), 0, 2); // 20 - what is the audio format? 1 for PCM = Pulse Code Modulation
            outStream.write(shortToByteArray(numChannels), 0, 2); // 22 - mono or stereo? 1 or 2? (or 5 or ???)
            outStream.write(intToByteArray(SAMPLE_RATE), 0, 4); // 24 - samples per second (numbers per second)
            outStream.write(intToByteArray((BITS_PER_SAMPLE / 8) * SAMPLE_RATE * numChannels), 0, 4); // 28 - bytes per second
            outStream.write(shortToByteArray((short) ((BITS_PER_SAMPLE / 8) * numChannels)), 0, 2); // 32 - # of bytes in one sample, for all channels
            outStream.write(shortToByteArray((short) BITS_PER_SAMPLE), 0, 2); // 34 - how many bits in a sample(number)? usually 16 or 24
            outStream.writeBytes("data"); // 36 - data
            outStream.write(intToByteArray(result.length), 0, 4); // 40 - how big is this data chunk
            outStream.write(result); // 44 - the actual data itself - just a long string of numbers

            outStream.close();

            //setFilePermission(fileName);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * 권한 설정
     * @param fileName 파일명
     * @throws Exception
     */
    private void setFilePermission(String fileName) throws  Exception{

        String osType = getOS();
        if(osType.equals("unix")){

            Set<PosixFilePermission> perms = new HashSet<>();

            //add owners permission
            perms.add(PosixFilePermission.OWNER_READ);
            perms.add(PosixFilePermission.OWNER_WRITE);
            perms.add(PosixFilePermission.OWNER_EXECUTE);
            //add group permissions
            perms.add(PosixFilePermission.GROUP_READ);
            perms.add(PosixFilePermission.GROUP_WRITE);
            perms.add(PosixFilePermission.GROUP_EXECUTE);
            //add others permissions
            perms.add(PosixFilePermission.OTHERS_READ);
            perms.add(PosixFilePermission.OTHERS_EXECUTE);
            Files.setPosixFilePermissions(Paths.get(fileName),perms);
        }
    }


    private byte[] intToByteArray(int i) {
        byte[] b = new byte[4];
        b[0] = (byte) (i & 0x00FF);
        b[1] = (byte) ((i >> 8) & 0x000000FF);
        b[2] = (byte) ((i >> 16) & 0x000000FF);
        b[3] = (byte) ((i >> 24) & 0x000000FF);
        return b;
    }

    private byte[] shortToByteArray(short data) {
        return new byte[] { (byte) (data & 0xff), (byte) ((data >>> 8) & 0xff) };
    }

    public String getRandomNum(){

        Random random = new Random();

        StringBuilder result = new StringBuilder();

        for (int i=0; i<3; i++){
            int tempVal = random.nextInt(9)+1;
            result.append(String.valueOf(tempVal));
        }

        return result.toString();

    }

    public void writeHandPcmMp3(String wavName, String mp3Name) {

        Runtime rt = Runtime.getRuntime();
        Process pc = null;

        try {
            //외부 프로세스 실행
            // pc = rt.exec("lame -V2 --quiet "+wavName+" "+mp3Name);
            pc = rt.exec("lame -r -s16000 -b128 -m m -V2 --quiet "+wavName+" "+mp3Name);
            log.debug("lame -r -s16000 -b128 -m m -V2 --quiet "+wavName+" "+mp3Name + " start");
            pc.waitFor();
            pc.destroy();
            log.debug("lame -r -s16000 -b128 -m m -V2 --quiet "+wavName+" "+mp3Name + " end");

        } catch (Exception e) {
            log.error("Exception : " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void writeHandWavMp3(String wavName, String mp3Name) {

        Runtime rt = Runtime.getRuntime();
        Process pc = null;

        try {
            //외부 프로세스 실행
            pc = rt.exec("lame -s16000 -b128 -m m -V2 --quiet "+wavName+" "+mp3Name);
            log.debug("lame -s16000 -b128 -m m -V2 --quiet "+wavName+" "+mp3Name + " start");
            pc.waitFor();
            pc.destroy();
            log.debug("lame -s16000 -b128 -m m -V2 --quiet "+wavName+" "+mp3Name + " end");

        } catch (Exception e) {
            log.error("Exception : " + e.getMessage());
            e.printStackTrace();
        }
    }
    private String getOS(){

        String OS = System.getProperty("os.name").toLowerCase();
        String result = "window";
        if(OS.contains("win")){

            return "window";

        } else if(OS.contains("nix") || OS.contains("nux") || OS.contains("aix")){

            return "unix";
        }

        return result;

    }
}
