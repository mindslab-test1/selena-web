package ai.mindslab.engedu.dic.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import ai.mindslab.engedu.dic.dao.data.KorDicSynonymVO;

@Mapper
public interface KorDicSynonymMapper {
	int getCount();

	List<KorDicSynonymVO> getSearch(Map<String, String> hashMap);
}
