package ai.mindslab.engedu.dic.service;


import ai.mindslab.engedu.dic.dao.KorDicMapper;
import ai.mindslab.engedu.dic.dao.data.KorDicVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class KorDicService {
	
	@Autowired
	private KorDicMapper korDicMapper;
	
	public int getCount() {
		//log.info(new Object(){}.getClass().getEnclosingMethod().getName()+">>>>"+korDicMapper.getCount());
		return korDicMapper.getCount();
	}

	public List<KorDicVO> getSearch(String searchKey) {

		Map<String, String> hashMap = new HashMap<>();
		hashMap.put("searchKey", searchKey);
		
		return korDicMapper.getSearch(hashMap);
	}

}
