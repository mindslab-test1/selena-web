package ai.mindslab.engedu.dic.service;


import ai.mindslab.engedu.dic.dao.KorDicSynonymMapper;
import ai.mindslab.engedu.dic.dao.data.KorDicSynonymVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class KorDicSynonymService {
	
	@Autowired
	private KorDicSynonymMapper korDicSynonymMapper ;
	
	public int getCount() {
		return korDicSynonymMapper.getCount();
	}

	public List<KorDicSynonymVO> getSearch(String searchKey) {

		Map<String, String> hashMap = new HashMap<>();
		hashMap.put("searchKey", searchKey);
		
		return korDicSynonymMapper.getSearch(hashMap);
	}

}
