package ai.mindslab.engedu.dic.service;


import ai.mindslab.engedu.dic.dao.KortoEngDicMapper;
import ai.mindslab.engedu.dic.dao.data.KorToEngDicVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class KortoEngDicService {
	
	@Autowired
	private KortoEngDicMapper kortoEngDicMapper;



	public int getCount() {
		//log.info(new Object(){}.getClass().getEnclosingMethod().getName()+">>>>"+kortoEngDicMapper.getCount());
		return kortoEngDicMapper.getCount();
	}


	public List<KorToEngDicVO> getSearch(String searchKey) {

		Map<String, String> hashMap = new HashMap<>();
		hashMap.put("searchKey", searchKey);
		
		return kortoEngDicMapper.getSearch(hashMap);
	}

}
