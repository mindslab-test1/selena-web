package ai.mindslab.engedu.evaluation.dao.data;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GrammarEvaluationDetailVO implements Serializable {

    /** 문장 */
    private String word;
    /** 점수*/
    private String score;
}
