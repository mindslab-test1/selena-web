package ai.mindslab.engedu.excel;

import ai.mindslab.engedu.common.component.CommonExcelComponent;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;


public class CommonExcelService {

    @Autowired
    CommonExcelComponent excelComponent;

    private Workbook workbook;
    private Map<String, Object> model;
    private HttpServletResponse response;

    public CommonExcelService(Workbook workbook, Map<String, Object> model, HttpServletResponse response) {
        this.workbook = workbook;
        this.model = model;
        this.response = response;
    }


    public void createExcel() {

    }
}
