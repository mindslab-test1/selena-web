package ai.mindslab.engedu.freetalk.dao.data;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class EngFreeTalkAnswerVO implements Serializable {
	private String brandId;
	private String bookId;
	private String chapterId;
	private String questionId;
	private String answerId;
	private String answerText;
	private String useYn;
	private Date createdDtm;
	private String creatorId;
	private Date updatedDtm;
	private String updatorId;
}
