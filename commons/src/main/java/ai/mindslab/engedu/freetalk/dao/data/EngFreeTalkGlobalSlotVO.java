package ai.mindslab.engedu.freetalk.dao.data;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class EngFreeTalkGlobalSlotVO implements Serializable {
	private String slotKey;
	private String slotValue;
	private Date createdDtm;
	private String creatorId;
	private Date updatedDtm;
	private String updatorId;
}
