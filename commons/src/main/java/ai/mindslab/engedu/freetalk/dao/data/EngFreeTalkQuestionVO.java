package ai.mindslab.engedu.freetalk.dao.data;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class EngFreeTalkQuestionVO implements Serializable {
	private String brandId;
	private String bookId;
	private String chapterId;
	private String questionId;
	private String questionText;
	private String useYn;
	private Date createdDtm;
	private String creatorId;
	private Date updatedDtm;
	private String updatorId;
}

