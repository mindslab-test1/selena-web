package ai.mindslab.engedu.freetalk.service;


import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.bqa.commons.data.BaseResponse;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import ai.mindslab.engedu.freetalk.dao.EngFreeTalkMapper;
import ai.mindslab.engedu.freetalk.dao.data.EngFreeTalkQuestionVO;
import ai.mindslab.engedu.freetalk.solr.FreeTalkSolrClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class EngFreeTalkImportService {

	@Autowired
	private EngFreeTalkMapper engFreeTalkMapper;

	@Autowired
	private FreeTalkSolrClient freeTalkSolrClient;

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public int insertFromSelectIntoTempDB(String importType, String brandId, String bookId, String chapterId, String questionId) throws EngEduException {

		int resultCount = 0;
		try {
			if(importType.equalsIgnoreCase("full")) {
				brandId = null;
				bookId = null;
				chapterId = null;
				questionId = null;
			}

			Map<String, Object> hashMap = new HashMap<>();
			hashMap.put("brandId", brandId);
			hashMap.put("bookId", bookId);
			hashMap.put("chapterId", chapterId);
			hashMap.put("questionId", questionId);

			List<EngFreeTalkQuestionVO> questionList = engFreeTalkMapper.getQuestionList(hashMap);

			for(EngFreeTalkQuestionVO vo : questionList) {
				Map<String, Object> voMap = new HashMap<>();
				voMap.put("brandId", vo.getBrandId());
				voMap.put("bookId", vo.getBookId());
				voMap.put("chapterId", vo.getChapterId());
				voMap.put("questionId", vo.getQuestionId());
				List<String> slotKeyList = engFreeTalkMapper.getSlotKeyList(voMap);

				voMap.put("slotKeyList", slotKeyList);
				resultCount += engFreeTalkMapper.insertFromSelectIntoTempDB(voMap);
			}
		} catch (Exception e) {
			log.warn("FTK insertFromSelectIntoTempDB Error/{}", e);
			throw new EngEduException(IRestCodes.ERR_CODE_FAILURE, IRestCodes.ERR_MSG_FAILURE);
		}

		return resultCount;
	}


	public BaseResponse<Object> fullIndexing() throws EngEduException {
		BaseResponse<Object> resp;
		try {
			resp = freeTalkSolrClient.fullIndexing();

		} catch (Exception e) {
			log.warn("Solr fullIndexing Error/{}", e);
			throw new EngEduException(IRestCodes.ERR_CODE_INDEX_SOLR_SYSTEM_ERROR, IRestCodes.ERR_MSG_INDEX_SOLR_SYSTEM_ERROR);
		}
		return resp;
	}

	public BaseResponse<Object> addIndexing(String questionId) throws EngEduException {
		BaseResponse<Object> resp;
		try {
			resp = freeTalkSolrClient.addIndexing(questionId);

		} catch (Exception e) {
			log.warn("Solr addIndexing Error/{}", e);
			throw new EngEduException(IRestCodes.ERR_CODE_INDEX_SOLR_SYSTEM_ERROR, IRestCodes.ERR_MSG_INDEX_SOLR_SYSTEM_ERROR);
		}
		return resp;
	}
}
