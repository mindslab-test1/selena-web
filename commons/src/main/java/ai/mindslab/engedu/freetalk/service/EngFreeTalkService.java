package ai.mindslab.engedu.freetalk.service;


import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import ai.mindslab.engedu.freetalk.dao.EngFreeTalkMapper;
import ai.mindslab.engedu.freetalk.dao.data.EngFreeTalkQuestionVO;
import ai.mindslab.engedu.freetalk.solr.FreeTalkSolrClient;
import lombok.extern.slf4j.Slf4j;
import org.apache.solr.client.solrj.SolrServerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
public class EngFreeTalkService {

	@Autowired
	private EngFreeTalkMapper engFreeTalkMapper;

	@Autowired
	private FreeTalkSolrClient freeTalkSolrClient;

	public EngFreeTalkQuestionVO getQuestion (String brandId, String bookId, String chapterId, String questionId, String questionText) {
		Map<String, Object> hashMap = new HashMap<>();
		hashMap.put("brandId", brandId);
		hashMap.put("bookId", bookId);
		hashMap.put("chapterId", chapterId);
		hashMap.put("questionId", questionId);
		hashMap.put("questionText", questionText);

		return engFreeTalkMapper.getQuestion(hashMap);
	}

	public boolean search(String brandId, String bookId, String chapterId, String questionId, String inputStr) throws EngEduException {
		boolean isExist;
		try {
			int morphCount = inputStr.split(" ").length;
			isExist = freeTalkSolrClient.search(brandId, bookId, chapterId, questionId, inputStr, morphCount);

		} catch (SolrServerException e) {
			log.warn("SolrServerException search Error/{}", e);
			throw new EngEduException(IRestCodes.ERR_CODE_SEARCH_SOLR_SYSTEM_ERROR, IRestCodes.ERR_MSG_SEARCH_SOLR_SYSTEM_ERROR);
		} catch (IOException e) {
			log.warn("IOException search Error/{}", e);
			throw new EngEduException(IRestCodes.ERR_CODE_SEARCH_SOLR_IO_ERROR, IRestCodes.ERR_MSG_SEARCH_SOLR_IO_ERROR);
		} catch (Exception e) {
			log.warn("Solr search Error/{}", e);
			throw new EngEduException(IRestCodes.ERR_CODE_SEARCH_SOLR_FAILURE, IRestCodes.ERR_MSG_SEARCH_SOLR_FAILURE);
		}
		return isExist;
	}
}
