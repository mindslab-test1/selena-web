package ai.mindslab.engedu.freetalk.solr;

import ai.mindslab.engedu.bqa.commons.codes.IBasicQAConst;
import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.bqa.commons.data.BaseResponse;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.params.ModifiableSolrParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.util.ObjectUtils;

import java.io.IOException;

@Repository
public class FreeTalkSolrClient implements IRestCodes, IBasicQAConst {

	private Logger logger = LoggerFactory.getLogger(FreeTalkSolrClient.class);

	private SolrClient ftkSolrClient;

	FreeTalkSolrClient(@Value("${solr.ftk.server.url}") String urlString) {
		if (urlString.isEmpty()) {
			urlString = "http://10.122.64.57:8983/solr/ftk";
		}
		ftkSolrClient = new HttpSolrClient.Builder(urlString).build();
		logger.debug("Solr urlString : {}", urlString);
	}

	protected void finalize() throws Throwable {
		ftkSolrClient.close();
	}

	public BaseResponse<Object> fullIndexing() throws SolrServerException, IOException {
		logger.info("fullIndexing");
		BaseResponse<Object> resp = new BaseResponse<>();

		ModifiableSolrParams params = new ModifiableSolrParams();
		params.set("qt", "/dataimport");
		params.set("clean", "true");
		params.set("command", "full-import");

		QueryResponse res = ftkSolrClient.query(params);
		logger.debug("Solr QueryResponse res: {}", res);

		UpdateResponse commit = ftkSolrClient.commit();
		logger.debug("Solr QueryResponse commit: {}", commit);

		if(ObjectUtils.isEmpty(commit) || commit.getStatus() != 0) {
			resp.setCode(ERR_CODE_INDEX_SOLR_SYSTEM_ERROR);
			resp.setMsg(ERR_MSG_INDEX_SOLR_SYSTEM_ERROR);
		}

		return resp;
	}

	public BaseResponse<Object> addIndexing(String questionId) throws SolrServerException, IOException {
		logger.info("addIndexing");
		BaseResponse<Object> resp = new BaseResponse<>();

		// 해당 문제 데이터를 먼저 solr에서 제거
		this.removeIndexing(questionId);

		ModifiableSolrParams params = new ModifiableSolrParams();
		params.set("qt", "/dataimport");
		params.set("command", "full-import");

		QueryResponse res = ftkSolrClient.query(params);
		logger.debug("Solr QueryResponse res: {}", res);

		UpdateResponse commit = ftkSolrClient.commit();
		logger.debug("Solr QueryResponse commit: {}", commit);

		if(ObjectUtils.isEmpty(commit) || commit.getStatus() != 0) {
			resp.setCode(ERR_CODE_INDEX_SOLR_SYSTEM_ERROR);
			resp.setMsg(ERR_MSG_INDEX_SOLR_SYSTEM_ERROR);
		}

		return resp;
	}

	private BaseResponse<Object> removeIndexing(String questionId) throws SolrServerException, IOException {
		logger.info("removeIndexing");
		BaseResponse<Object> resp = new BaseResponse<>();

		ftkSolrClient.deleteByQuery("question_id:"+questionId);

		UpdateResponse commit = ftkSolrClient.commit();

		if(ObjectUtils.isEmpty(commit) || commit.getStatus() != 0) {
			resp.setCode(ERR_CODE_INDEX_SOLR_SYSTEM_ERROR);
			resp.setMsg(ERR_MSG_INDEX_SOLR_SYSTEM_ERROR);
		}

		return resp;
	}

	public boolean search(String brandId, String bookId, String chapterId, String questionId, String inputStr, int morphCount)
			throws SolrServerException, IOException {
		logger.debug("Solr ftk search() ,{},{},{},{},{}", brandId, bookId, chapterId, questionId, inputStr);
		boolean isExist = false;

		SolrQuery solrQuery = buildQuery(brandId, bookId, chapterId, questionId, inputStr, morphCount);

		long size = ftkSolrClient.query(solrQuery).getResults().getNumFound();

		if (size > 0) {
			isExist = true;
		}

		return isExist;
	}

	private SolrQuery buildQuery(String brandId, String bookId, String chapterId, String questionId, String inputStr, int morphCount) {
		logger.info("buildQuery:brandId {}, bookId {}, chapterId {}, questionId {}, inputStr {}, morphCount {}"
				, brandId, bookId, chapterId, questionId, inputStr, morphCount);
		SolrQuery solrQuery = new SolrQuery();
		StringBuilder builder = new StringBuilder();

		builder.append("brand_id:\"").append(brandId).append("\"");
		builder.append(" AND book_id:\"").append(bookId).append("\"");
		builder.append(" AND chapter_id:\"").append(chapterId).append("\"");
		builder.append(" AND question_id:\"").append(questionId).append("\"");
		builder.append(" AND answer_text:\"").append(inputStr).append("\"");
		builder.append(" AND morph_count:\"").append(morphCount).append("\"");

		logger.debug("builder.toString{}", builder.toString());
		solrQuery.setQuery(builder.toString());
		// solrQuery.setFields("*", "score");
		solrQuery.setRows(0); // 가져오는 데이터의 row 수를 제한

		logger.info("buildAndQuery: {}", solrQuery.toString());
		return solrQuery;
	}
}
