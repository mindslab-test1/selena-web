package ai.mindslab.engedu.game.dao.data;

import lombok.Data;

import java.io.Serializable;

@Data
public class SpeedGameUsedVO implements Serializable {
	private String userId;
	private Integer speedGameId;
	private String thumb;
	private Integer thumbIndex;
	private String kind;
	private Integer seq;
}
