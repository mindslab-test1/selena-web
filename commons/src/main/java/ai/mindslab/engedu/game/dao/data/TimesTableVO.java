package ai.mindslab.engedu.game.dao.data;

import lombok.Data;

import java.io.Serializable;

@Data
public class TimesTableVO implements Serializable {
	private Integer timesId;
	private Integer seq;
	private String question;
	private String answer;
}
