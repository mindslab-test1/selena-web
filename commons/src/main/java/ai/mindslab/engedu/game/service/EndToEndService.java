package ai.mindslab.engedu.game.service;


import ai.mindslab.engedu.game.dao.EndToEndMapper;
import ai.mindslab.engedu.game.dao.data.EndToEndUsedWordVO;
import ai.mindslab.engedu.game.dao.data.EndToEndVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class EndToEndService {
	
	@Autowired
	private EndToEndMapper endToEndMapper;

	public Integer getMaxSeq(String userId) {
		return endToEndMapper.getMaxSeq(userId);
	}
	
	public EndToEndVO getSearch(String searchKey) {

		Map<String, String> hashMap = new HashMap<>();
		hashMap.put("searchKey", searchKey);
		
		return endToEndMapper.getSearch(hashMap);
	}
	
	public EndToEndVO getEndToEndWord(String userId, List<String> levelList, List<String> lastThumb) {

		Map<String, Object> hashMap = new HashMap<>();
		hashMap.put("userId", userId);
		hashMap.put("levelList", levelList);
		hashMap.put("lastThumb", lastThumb);
		
		return endToEndMapper.getEndToEndWord(hashMap);
	}
	
	public int insertUsedWord(String userId, EndToEndVO endToEndVO, String kind, Integer seq) {
		
		// 마지막 음절(두음 법칙 포함) 세팅
		String lastThumb = "";
		String word = endToEndVO.getWord();
		
		lastThumb += word.charAt(word.length() - 1);
				
		if(!StringUtils.isEmpty(endToEndVO.getThumb1())) {
			lastThumb += ","+endToEndVO.getThumb1();
		}
		
		if(!StringUtils.isEmpty(endToEndVO.getThumb2())) {
			lastThumb += ","+endToEndVO.getThumb2();
		}
		
		Map<String, Object> hashMap = new HashMap<>();
		hashMap.put("userId", userId);
		hashMap.put("word", word);
		hashMap.put("lastThumb", lastThumb);
		hashMap.put("kind", kind);
		hashMap.put("seq", seq);
		
		return endToEndMapper.insertUsedWord(hashMap);
	}
	
	public EndToEndUsedWordVO getUsedWord(String userId, Integer seq, String word) {
		
		Map<String, Object> hashMap = new HashMap<>();
		hashMap.put("userId", userId);
		hashMap.put("seq", seq);
		hashMap.put("word", word);
		
		return endToEndMapper.getUsedWord(hashMap);
	}
	
	public int deleteUsedWords(String userId) {
		return endToEndMapper.deleteUsedWords(userId);
	}

}
