package ai.mindslab.engedu.game.service;


import ai.mindslab.engedu.game.dao.SpeedGameMapper;
import ai.mindslab.engedu.game.dao.data.SpeedGameUsedVO;
import ai.mindslab.engedu.game.dao.data.SpeedGameVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
public class SpeedGameService {

	@Autowired
	private SpeedGameMapper speedGameMapper;

    public Integer getMaxSeq(String userId) {
        return speedGameMapper.getMaxSeq(userId);
    }

    public Integer getRandomSpeedGameId() {
        return speedGameMapper.getRandomSpeedGameId();
    }

	public SpeedGameVO getGameWord(Integer speedGameId) {

		Map<String, Object> hashMap = new HashMap<>();
		hashMap.put("speedGameId", speedGameId);

		return speedGameMapper.getGameWord(hashMap);
	}

	public int insertUsedThumb(String userId, int speedGameId, String thumb, int thumbIndex, String kind, int seq) {
        Map<String, Object> hashMap = new HashMap<>();
        hashMap.put("userId", userId);
        hashMap.put("speedGameId", speedGameId);
        hashMap.put("thumb", thumb);
        hashMap.put("thumbIndex", thumbIndex);
        hashMap.put("kind", kind);
        hashMap.put("seq", seq);

        return speedGameMapper.insertUsedThumb(hashMap);
    }

    public SpeedGameUsedVO getUsedThumb(String userId, int seq) {
        Map<String, Object> hashMap = new HashMap<>();
        hashMap.put("userId", userId);
        hashMap.put("seq", seq);

        return speedGameMapper.getUsedThumb(hashMap);
    }

    public int deleteUsedThumbs(String userId) {
        return speedGameMapper.deleteUsedThumbs(userId);
    }
}
