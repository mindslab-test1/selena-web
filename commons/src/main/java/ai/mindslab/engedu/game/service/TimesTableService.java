package ai.mindslab.engedu.game.service;


import ai.mindslab.engedu.game.dao.TimesTableMapper;
import ai.mindslab.engedu.game.dao.data.TimesTableVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
public class TimesTableService {

    @Autowired
    private TimesTableMapper timesTableMapper;

	public TimesTableVO getQuestion(String userId, Integer seq) {

		Map<String, Object> hashMap = new HashMap<>();
		hashMap.put("userId", userId);
		hashMap.put("seq", seq);

		return timesTableMapper.getQuestion(hashMap);
	}

	public int insertUsedQuestion(String userId, Integer timesId, Integer seq) {
        Map<String, Object> hashMap = new HashMap<>();
        hashMap.put("userId", userId);
        hashMap.put("timesId", timesId);
        hashMap.put("seq", seq);

        return timesTableMapper.insertUsedQuestion(hashMap);
    }

    public TimesTableVO getAnswer(String userId) {
        Map<String, Object> hashMap = new HashMap<>();
        hashMap.put("userId", userId);

        return timesTableMapper.getAnswer(hashMap);
    }

    public int updateUsedQuestionStatus(String userId, Integer timesId) {
        Map<String, Object> hashMap = new HashMap<>();
        hashMap.put("userId", userId);
        hashMap.put("timesId", timesId);

        return timesTableMapper.updateUsedQuestionStatus(hashMap);
    }

    public Integer getSeqCorrectCount(String userId, Integer seq) {
        Map<String, Object> hashMap = new HashMap<>();
        hashMap.put("userId", userId);
        hashMap.put("seq", seq);

        return timesTableMapper.getSeqCorrectCount(hashMap);
    }

    public int deleteUsedQuestions(String userId, Integer seq) {
        Map<String, Object> hashMap = new HashMap<>();
        hashMap.put("userId", userId);
        hashMap.put("seq", seq);

        return timesTableMapper.deleteUsedQuestions(hashMap);
    }
}
