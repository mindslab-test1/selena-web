package ai.mindslab.engedu.intent;

import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import ai.mindslab.engedu.intent.vo.ExtInfo;
import ai.mindslab.engedu.intent.vo.IntentExecuteMessageVO;
import ai.mindslab.engedu.intent.vo.IntentExecuteVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
@Component
public class DayIntentExecute implements IntentExecute {
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public IntentExecuteMessageVO execute(IntentExecuteVO intentExecuteVO) throws EngEduException {
		
		IntentExecuteMessageVO intentExecuteMessageVO = new IntentExecuteMessageVO();

		try {
			String inputStr = intentExecuteVO.getInputStr();

			StringBuilder stringBuilder = new StringBuilder();
			String[] pStrArr = {"오늘", "내일", "모레", "어제", "그저께"};
			String[] pAddStrArr = {"은", "은", "는", "는", "는"};
			int[] addDayArr = {0, 1, 2, -1, -2};
			Integer addDay = null;

			int index = -1;
			for(int i=0; i<pStrArr.length; i++) {
				Pattern p = Pattern.compile(pStrArr[i]);
				Matcher matcher = p.matcher(inputStr);

				if(matcher.find()) {
					index = i;
					addDay = addDayArr[i];
					break;
				}
			}

			Integer month = null;
			Integer date = null;
			if(addDay == null) {
				Pattern pMonth = Pattern.compile("\\d+월");
				Pattern pDate = Pattern.compile("\\d+일");

				Matcher mMatcher = pMonth.matcher(inputStr);
				if(mMatcher.find()) {
					stringBuilder.append(inputStr, mMatcher.start(), mMatcher.end()).append(" ");
					month = Integer.parseInt(inputStr.substring(mMatcher.start(), mMatcher.end() - 1));
					if(month < 1 || month > 12) {
						month = null;
					}
				}

				Matcher dMatcher = pDate.matcher(inputStr);
				if(dMatcher.find()) {
					stringBuilder.append(inputStr, dMatcher.start(), dMatcher.end()).append(" ");
					date = Integer.parseInt(inputStr.substring(dMatcher.start(), dMatcher.end() - 1));

				}

				if(date == null) {
					stringBuilder.setLength(0);
				}
			}

			Calendar cal = Calendar.getInstance();
			if(addDay != null) {
				cal.set(Calendar.DATE, cal.get(Calendar.DATE) + addDay);
			} else if(date != null) {
				cal.set(Calendar.DATE, date);

				if(month != null) {
					cal.set(Calendar.MONTH, month - 1);
				}
			}

			String korDayOfWeek = "";
			int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
			switch(dayOfWeek) {
				case 1:
					korDayOfWeek = "일";
					break;
				case 2:
					korDayOfWeek = "월";
					break;
				case 3:
					korDayOfWeek = "화";
					break;
				case 4:
					korDayOfWeek = "수";
					break;
				case 5:
					korDayOfWeek = "목";
					break;
				case 6:
					korDayOfWeek = "금";
					break;
				case 7:
					korDayOfWeek = "토";
					break;
			}


			if(ObjectUtils.isEmpty(stringBuilder)){
				// 기본 설정 - 오늘
				if(index == -1) {
					index = 0;
				}
				stringBuilder.append(pStrArr[index]).append(pAddStrArr[index]).append(" ");
			}
			stringBuilder.append(korDayOfWeek).append("요일이야");

			String resultMsg = stringBuilder.toString();
			ExtInfo extInfo = new ExtInfo();
			extInfo.setCurrentDomain(IntentServiceType.ServiceEnum.ST0026.getDomainName());

			log.info("DayIntentExecute ResultMsg:"+resultMsg);
			intentExecuteMessageVO.setResultMsg(resultMsg);
			intentExecuteMessageVO.setExtInfo(extInfo);

		} catch (Exception e) {
			throw new EngEduException(IRestCodes.ERR_CODE_FAILURE, IRestCodes.ERR_MSG_FAILURE);
		}

		return intentExecuteMessageVO;
	}

	@Override
	public IntentExecuteMessageVO forceQuit(IntentExecuteVO intentExecuteVO) {

		return null;
	}
}