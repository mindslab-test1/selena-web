package ai.mindslab.engedu.intent;

import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.bqa.commons.data.BaseResponse;
import ai.mindslab.engedu.bqa.commons.utils.BqaClientApi;
import ai.mindslab.engedu.bqa.dao.data.QaAnswerVo;
import ai.mindslab.engedu.bqa.service.BqaSearchService;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import ai.mindslab.engedu.intent.service.IntentMsgService;
import ai.mindslab.engedu.intent.vo.ExtInfo;
import ai.mindslab.engedu.intent.vo.IntentExecuteMessageVO;
import ai.mindslab.engedu.intent.vo.IntentExecuteVO;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

@Slf4j
@Component
public class GeneralTalkIntentExecute implements IntentExecute {

	@Autowired
	private BqaSearchService bqaSearchService;
	
	@Autowired
    private IntentMsgService intentMsgService;
	
	@Autowired
	private BqaClientApi bqaClientApi;

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public IntentExecuteMessageVO execute(IntentExecuteVO intentExecuteVO) throws EngEduException {
		
		IntentExecuteMessageVO intentExecuteMessageVO = new IntentExecuteMessageVO();
		ExtInfo extInfo = new ExtInfo();
		try {

			String question = intentExecuteVO.getInputStr();
			
			//question="디봇! 포인트 알려줘."; 	// for test
//			question = "현 싶다";		// for test
			
			//question= question.replaceAll("디봇", "디 봇");
			//log.debug("GeneralTalkIntentExecute execute question '디 봇'으로 분리:["+question+"]");
			
			
			int searchFlowType = 1;
			int[] domainIds = bqaSearchService.getDomainIds();

			BaseResponse<Object> resp = bqaSearchService.search(question,searchFlowType,domainIds);

			// 솔라 조회 후 데이터가 없으면 아래 객체에 String 문자열을 세팅해서 넘어온다   			
			Object obj = resp.getData();
			
			
			if (obj instanceof String) {
				question= question.replaceAll("디 봇","");
				log.debug("GeneralTalkIntentExecute execute question '디 봇' 문자열 삭제:["+question+"]");
				resp = bqaSearchService.search(question,searchFlowType,domainIds);
				obj = resp.getData();
			} 
			
			QaAnswerVo qaAnswerVo;
			String resultMsg ="";

			if (obj instanceof String) {
				resultMsg = resp.getData().toString();
				
	        } else if (obj instanceof QaAnswerVo) {
	        	qaAnswerVo = (QaAnswerVo) obj;
	        	
	        	if (ObjectUtils.isEmpty(qaAnswerVo)) {
					resultMsg = intentMsgService.getServiceMsg(IntentServiceType.BASICTALK,IntentServiceMsg.BQA_NOT_FOUND);
	        		
				} else {
					
					if(qaAnswerVo.getAnswer().contains("_")){
						//resultMsg= bqaClientApi.getBqaUserInfo(intentExecuteVO, qaAnswerVo);
						resultMsg = qaAnswerVo.getAnswer().replaceAll("_+[a-zA-Z]+_", "테스트");
						
						
						if ("".equals(resultMsg)) {
							resultMsg = intentMsgService.getServiceMsg(IntentServiceType.BASICTALK,IntentServiceMsg.BQA_CLIENT_ERROR);
						}
						
					} else {
						resultMsg = qaAnswerVo.getAnswer();
						
						if (StringUtils.isNotEmpty(qaAnswerVo.getClientMenuCd())  ) {
							extInfo.setMenuId(qaAnswerVo.getClientMenuCd());
						}
					}
				}
	        }

			extInfo.setCurrentDomain(IntentServiceType.ServiceEnum.ST0001.getDomainName());
			intentExecuteMessageVO.setExtInfo(extInfo);
			log.info("GeneralTalkIntentExecute ResultMsg:"+resultMsg);
			
			intentExecuteMessageVO.setResultMsg(resultMsg);


		} catch (EngEduException e) {
			throw new EngEduException(e.getErrCode(), e.getErrMsg());
		} catch (Exception e) {
			throw new EngEduException(IRestCodes.ERR_CODE_FAILURE, IRestCodes.ERR_MSG_FAILURE);
		}

		return intentExecuteMessageVO;
	}

	@Override
	public IntentExecuteMessageVO forceQuit(IntentExecuteVO intentExecuteVO) {
		return null;
	}
}
