package ai.mindslab.engedu.intent;

import ai.mindslab.engedu.common.exceptions.EngEduException;
import ai.mindslab.engedu.intent.vo.IntentExecuteMessageVO;
import ai.mindslab.engedu.intent.vo.IntentExecuteVO;


public interface IntentExecute {
	
	IntentExecuteMessageVO execute(IntentExecuteVO intentExecuteVO) throws EngEduException;

	IntentExecuteMessageVO forceQuit(IntentExecuteVO intentExecuteVO) throws EngEduException;
}
