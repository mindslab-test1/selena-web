package ai.mindslab.engedu.intent;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import ai.mindslab.engedu.intent.dao.data.UserServiceVO;
import ai.mindslab.engedu.intent.service.IntentFinderService;
import ai.mindslab.engedu.intent.vo.IntentFinderVO;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class IntentFinder {
	
	@Autowired
	private IntentFinderService intentFinderService;
	
	@Value("${engedu.dics.timeout}")
	private String dicsTimeout;

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public void finder(IntentFinderVO intentFinderVO) {

		// 서비스 타입이 없으면 현재 저장된 serviceType을 가지고 온다.
		// 현재 저장된 서비스 조회시 사전이면 5초이내의 현재 서비스를 찾는다.
		// 현재 저장된 것이 없으면 패턴을 매칭한다.

		String currentServiceType;
		
		log.debug("dicsTimeout:"+dicsTimeout);


		if (StringUtils.isEmpty(intentFinderVO.getServiceType())) {

			UserServiceVO userServiceVO = intentFinderService.getCurrentIntent(intentFinderVO.getUserId(),dicsTimeout);

			//log.info("userServiceVO=====>"+userServiceVO.toString());


			// 객체가 없거나
			// 객체는 있는데  serviceType이 없거나

			if (ObjectUtils.isEmpty(userServiceVO) || StringUtils.isEmpty(userServiceVO.getServiceType())) {

				currentServiceType = findServiceType(intentFinderVO);
				intentFinderService.updateServiceType(currentServiceType, intentFinderVO.getUserId());


				// 객체와 서비스 타입은 있는데
			} else {

				// 사전일 경우 5초 시간이 지났으면  입력된 텍스트에서 서비스타입을 다시 찾는다
/*				if (userServiceVO.getServiceType().equals(IntentServiceType.KORTOENGDIC) || userServiceVO.getServiceType().equals(IntentServiceType.KORDIC) ||
					userServiceVO.getServiceType().equals(IntentServiceType.MATHDIC) || userServiceVO.getServiceType().equals(IntentServiceType.ENCYCLOPDIC) ||
					userServiceVO.getServiceType().equals(IntentServiceType.KORDICSYNONYM) || userServiceVO.getServiceType().equals(IntentServiceType.KORDICANTONYM)   ) {

					int compare = userServiceVO.getServiceLimitTime().compareTo(userServiceVO.getCurrentTime());

					// 현재시간이 더 크면   입력된 텍스트에서 서비스타입을 다시 찾는다
					if (compare < 0) {
						currentServiceType = findServiceType(intentFinderVO);
					} else {
						currentServiceType = userServiceVO.getServiceType();
					}

					intentFinderVO.setServiceType(currentServiceType);

				} else {
					currentServiceType = userServiceVO.getServiceType();
					intentFinderVO.setServiceType(currentServiceType);
				}*/
				
				currentServiceType = userServiceVO.getServiceType();
				intentFinderVO.setServiceType(currentServiceType);

				intentFinderService.updateServiceType(currentServiceType, intentFinderVO.getUserId());
			}

			// 서비스 타입이 있으면 db만 업데이트
		} else {
			intentFinderService.updateServiceType(intentFinderVO.getServiceType(), intentFinderVO.getUserId());
		}
	}

	private String findServiceType(IntentFinderVO intentFinderVO) {
		log.info("findServiceType===================>intentFinderVO::::"+intentFinderVO);
		
		String inputStr = StringUtils.isEmpty(intentFinderVO.getInputStr()) ? "" : intentFinderVO.getInputStr();

		boolean firstServiceEntry=false;

		String serviceType = classifyServiceType(inputStr);
		switch(serviceType) {
			case IntentServiceType.KORTOENGDIC:
			case IntentServiceType.KORDIC:
			case IntentServiceType.MATHDIC:
			case IntentServiceType.ENDTOEND:
			case IntentServiceType.SPEEDGAME:
			case IntentServiceType.TIMESTABLE:
			case IntentServiceType.KORDICSYNONYM:
			case IntentServiceType.KORDICANTONYM:
			case IntentServiceType.ENCYCLOPDIC:
			case IntentServiceType.UNIT_CONVERSION:
			case IntentServiceType.CALCULATE:
			case IntentServiceType.WEATHER:
			case IntentServiceType.DUST:
			case IntentServiceType.EXCHANGE_RATE:
			case IntentServiceType.SEASONAL_FOOD:

				firstServiceEntry = true;
				break;
		}
		
		intentFinderVO.setFirstServiceEntry(firstServiceEntry);
		intentFinderVO.setServiceType(serviceType);
		
		return intentFinderVO.getServiceType();
		
	}

	private String classifyServiceType(String inputStr) {
		
		String trimInputStr = inputStr.replaceAll(" " , "").replaceAll("\\p{Z}", "");
		log.debug("classifyServiceType trimInputStr["+trimInputStr+"]");
		
		ArrayList<String> serviceTypeList = intentFinderService.getServiceTypeList();
		String serviceType = IntentServiceType.BASICTALK; // 이외는 Basic QA 로 판단한다
		
		for(String s : serviceTypeList) {
			Matcher matcher = Pattern.compile(intentFinderService.getRegexOfServiceType(s)).matcher(trimInputStr);
			
			if(matcher.matches()) {
				serviceType = s;
				break;
			}
		}
		
		return serviceType;
	}
}
