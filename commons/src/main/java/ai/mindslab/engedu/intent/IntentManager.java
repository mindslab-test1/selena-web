package ai.mindslab.engedu.intent;

import ai.mindslab.engedu.intent.vo.IntentExecuteMessageVO;
import ai.mindslab.engedu.intent.vo.IntentExecuteVO;
import ai.mindslab.engedu.intent.vo.IntentFinderVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class IntentManager {
	
	private IntentFinderVO intentFinderVO;
	
	@Autowired
	private IntentFinder intentFinder;
	
	@Autowired
	private IntentResolve intentResolve;
	
	
	public void init(IntentFinderVO intentFinderVO) {
		this.intentFinderVO = intentFinderVO;
		
	}
	
	public void getCurrentIntent() {
		
		intentFinder.finder( this.intentFinderVO );
		
	}
	
	public IntentExecuteMessageVO getIntent(IntentExecuteVO intentExecuteVO) {

		return intentResolve.getIntent(intentExecuteVO);
	}

	public IntentExecuteMessageVO forceQuit(IntentExecuteVO intentExecuteVO) {

		return intentResolve.forceQuit(intentExecuteVO);
	}
	

}
