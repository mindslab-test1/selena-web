package ai.mindslab.engedu.intent;

public interface IntentServiceMsg {

	// --- start of BQA
	public final String BQA_NOT_FOUND 				= "SM1001";
	public final String BQA_CLIENT_ERROR			= "SM1002";
	public final String BQA_COMMON_EXIT				= "SM1003";
	
	public final String UNIT_CONVERSION_START				= "SM1004";
	public final String UNIT_CONVERSION_NOT_FOUND			= "SM1005";
	public final String UNIT_CONVERSION_MAKE_MENT			= "SM1006";
	public final String UNIT_CONVERSION_START_PRINTMENT	= "SM1007";
	
	
	public final String CALCULATE_START				= "SM1008";
	public final String CALCULATE_START_PRINTMENT	= "SM1009";
	public final String CALCULATE_DO_NOT_CAL		= "SM1010";
	
	
	
	// --- end of 	BQA

	// --- start of 사전
	public final String KORTOENGDIC_START			= "SM2001";
	public final String KORTOENGDIC_NOT_FOUND		= "SM2002";
	public final String KORTOENGDIC_MAKE_MENT		= "SM2003";
	public final String KORTOENGDIC_START_PRINTMENT	= "SM2016";

	public final String KORDIC_START				= "SM2004";
	public final String KORDIC_NOT_FOUND			= "SM2005";
	public final String KORDIC_MAKE_MENT			= "SM2006";
	public final String KORDIC_START_PRINTMENT		= "SM2017";

	public final String MATHDIC_START				="SM2007";
	public final String MATHDIC_NOT_FOUND			="SM2008";
	public final String MATHDIC_MAKE_MENT			="SM2009";
	public final String MATHDIC_START_PRINTMENT		="SM2018";

	public final String KORDICSYNONYM_START			="SM2010";
	public final String KORDICSYNONYM_NOT_FOUND		="SM2011";
	public final String KORDICSYNONYM_MAKE_MENT		="SM2012";

	public final String KORDICANTONYM_START			="SM2013";
	public final String KORDICANTONYM_NOT_FOUND		="SM2014";
	public final String KORDICANTONYM_MAKE_MENT		="SM2015";

	public final String ENCYCLOPDIC_MAKE_MENT = "SM2016";
	public final String ENCYCLOPDIC_START_PRINTMENT = "SM2019";
	public final String ENCYCLOPDIC_NOT_FOUND 				= "SM2028";

	public final String WEATHER_MAKE_MENT = "SM2020";
	public final String WEATHER_START_PRINTMENT = "SM2021";
	public final String WEATHER_NOT_FOUND 				= "SM2029";

	public final String DUST_MAKE_MENT = "SM2022";
	public final String DUST_START_PRINTMENT = "SM2023";
	public final String DUST_NOT_FOUND 				= "SM2030";

	public final String EXCHANGE_RATE_MAKE_MENT = "SM2024";
	public final String EXCHANGE_RATE_START_PRINTMENT = "SM2025";
	public final String EXCHANGE_RATE_NOT_FOUND 				= "SM2031";

	public final String SEASONAL_FOOD_MAKE_MENT = "SM2026";
	public final String SEASONAL_FOOD_START_PRINTMENT = "SM2027";
	public final String SEASONAL_FOOD_NOT_FOUND 				= "SM2032";



	// --- end of 	사전

	// --- start of 게임
	public final String ENDTOEND_START				="SM3001";
	public final String ENDTOEND_USERWIN			="SM3002";
	public final String ENDTOEND_DUPLICATEWORD		="SM3003";
	public final String ENDTOEND_TIMELIMIT			="SM3004";
	public final String ENDTOEND_NOWORD_IN_DB		="SM3005";
	public final String ENDTOEND_WRONGWORD			="SM3006";
	public final String ENDTOEND_START_PRINTMENT	="SM3011";

	public final String SPEEDGAME_START				="SM3007";
	public final String SPEEDGAME_USERWIN			="SM3008";
	public final String SPEEDGAME_TIMELIMIT			="SM3009";
	public final String SPEEDGAME_WRONGWORD			="SM3010";
	public final String SPEEDGAME_START_PRINTMENT	="SM3012";
	
	public final String TIMESTABLE_START_PRINTMENT	="SM3013";
	// --- end of 	게임


}

