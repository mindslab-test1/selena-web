package ai.mindslab.engedu.intent;

public interface IntentServiceType {
	
	public final String BASICTALK		="ST0001";		//  BASIC QA	ST0000
	
	public final String KORTOENGDIC="ST0015";	// 한영사전
	public final String KORDIC="ST0016";		// 국어사전
	public final String MATHDIC="ST0017";		// 수학사전
	public final String ENDTOEND="ST0018";				// 끝말잇기게임
	public final String SPEEDGAME="ST0019";				// 탕수육게임
	public final String ENCYCLOPDIC="ST0020";	// 백과사전
	public final String TIMESTABLE="ST0021";			// 구구단게임
	public final String ENGFREETALK		="ST0022";		// 영어대화
	public final String KORDICSYNONYM="ST0023";	// 국어사전 유의어
	public final String KORDICANTONYM="ST0024";	// 국어사전  반의어
	public final String CALCULATE		="ST0025";		// 계산기
	public final String DAY				="ST0026";		// 요일
	public final String TIME			="ST0027";		// 시각
	public final String DATE			="ST0028";		// 날짜
	public final String KORTOENGDIC_FLASH="ST0029";	// 한영사전 즉시 발화			ST0015F
	public final String KORDIC_FLASH="ST0030";			// 국어사전 즉시 발화			ST0016F
	public final String MATHDIC_FLASH="ST0031";		// 수학사전 즉시 발화			ST0017F
	public final String ENCYCLOPDIC_FLASH="ST0032";	// 백과사전 즉시 발화			ST0020F
	public final String KORDICSYNONYM_FLASH="ST0033";	// 국어사전 유의어 즉시 발화	ST0023F
	public final String KORDICANTONYM_FLASH="ST0034";	// 국어사전  반의어 즉시 발화	ST0024F
	public final String ENGEVALUATION = "ST0035"; // 영어평가
	public final String KORREADING = "ST0036"; // 한국어 따라읽기_EvalKorea
	public final String PHONICS = "ST0037"; // 파닉스
	public final String ALPAHBET = "ST0038"; // 알파벳
	public final String TUTORIAL = "ST0039"; // 말로 푸는 학습지
	public final String UNIT_CONVERSION = "ST0040"; // 단위변환

	public final String WEATHER = "ST0041"; // 날씨
	public final String DUST = "ST0042"; // 미세먼지
	public final String EXCHANGE_RATE = "ST0043"; // 환율
	public final String SEASONAL_FOOD = "ST0044"; // 제철 음식



	public enum ServiceEnum{

		ST0015("한영사전"),
		ST0029("한영사전 즉시 발화"),
		ST0016("국어사전"),
		ST0030("국어사전 즉시 발화"),
		ST0024("국어사전 반의어"),
		ST0034("국어사전 반의어 즉시 발화"),
		ST0023("국어사전 유의어"),
		ST0033("국어사전 유의어 즉시 발화"),
		ST0017("수학사전"),
		ST0031("수학사전 즉시발화"),
		ST0018("끝말잇기"),
		ST0019("탕수육 게임"),
		ST0021("구구단 게임"),
		ST0025("계산기"),
		ST0026("요일"),
		ST0027("시각"),
		ST0028("날짜"),
		ST0020("백과사전"),
		ST0040("단위변환"),
		ST0036("한국어 따라읽기"),
		ST0039("말로 푸는 학습지"),
		ST0001("기본 대화"),
		ST0022("영어 대화"),
		ST0035("영어 따라읽기 평가"),
		ST0041("날씨"),
		ST0042("미세먼지"),
		ST0043("환율"),
		ST0044("제철음식");


		private String domainName;

		ServiceEnum(String name){ this.domainName = name; }

		public String getDomainName(){ return domainName; }

	}
	




}
