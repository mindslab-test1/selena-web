package ai.mindslab.engedu.intent;

import java.util.List;

import ai.mindslab.engedu.common.codes.IRestCodes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ai.mindslab.engedu.common.exceptions.EngEduException;
import ai.mindslab.engedu.common.utils.WordExtractUtil;
import ai.mindslab.engedu.dic.dao.data.KorDicAntonymVO;
import ai.mindslab.engedu.dic.service.KorDicAntonymService;
import ai.mindslab.engedu.intent.service.IntentMsgService;
import ai.mindslab.engedu.intent.vo.ExtInfo;
import ai.mindslab.engedu.intent.vo.IntentExecuteMessageVO;
import ai.mindslab.engedu.intent.vo.IntentExecuteVO;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class KorDicAntonymIntentExecute implements IntentExecute {
	
	@Autowired
	private KorDicAntonymService korDicAntonymService ;

	@Autowired
	private IntentMsgService intentMsgService;

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public IntentExecuteMessageVO execute(IntentExecuteVO intentExecuteVO) throws EngEduException {
		
		IntentExecuteMessageVO executeMessageVO = new IntentExecuteMessageVO();
		try {
			WordExtractUtil wordExtractUtil = new WordExtractUtil();

			intentExecuteVO.setInputStr(wordExtractUtil.extractWord(intentExecuteVO.getServiceType(), intentExecuteVO.getInputStr()));

			List<KorDicAntonymVO> list = korDicAntonymService.getSearch(intentExecuteVO.getInputStr());

			StringBuilder resultMsgBuilder = new StringBuilder();
			StringBuilder urlBuilder = new StringBuilder();
			StringBuilder printMeansBuilder = new StringBuilder();

			for (KorDicAntonymVO korDicAntonymVO : list) {
				log.info("korDicAntonymVO.toString():" + korDicAntonymVO.toString());

				resultMsgBuilder.append(korDicAntonymVO.getMeans()).append(",");
				urlBuilder.append(korDicAntonymVO.getUrl()).append(",");
				printMeansBuilder.append(korDicAntonymVO.getPrintMeans()).append(",");

			}
			String resultMsg = resultMsgBuilder.toString();

			ExtInfo extInfo = new ExtInfo();
			extInfo.setUrl(urlBuilder.toString());
			extInfo.setPrintMeans(printMeansBuilder.toString());


			if ("".equals(resultMsg)) {
				resultMsg = intentMsgService.getServiceMsg(IntentServiceType.KORDICANTONYM, IntentServiceMsg.KORDICANTONYM_NOT_FOUND);

			} else {
				resultMsg = resultMsg.substring(0, resultMsg.length() - 1);

				String serviceMsg = intentMsgService.getServiceMsg(IntentServiceType.KORDICANTONYM, IntentServiceMsg.KORDICANTONYM_MAKE_MENT);

				resultMsg = serviceMsg.replaceAll("_question_", intentExecuteVO.getInputStr()).replaceAll("_answer_", resultMsg);
			}

			log.info("KorDicAntonymIntentExecute ResultMsg:"+resultMsg);

			executeMessageVO.setData(list);
			executeMessageVO.setResultMsg(resultMsg);
			executeMessageVO.setExtInfo(extInfo);

		} catch (Exception e) {
			throw new EngEduException(IRestCodes.ERR_CODE_FAILURE, IRestCodes.ERR_MSG_FAILURE);
		}
		
		return executeMessageVO;
	}

	@Override
	public IntentExecuteMessageVO forceQuit(IntentExecuteVO intentExecuteVO) {
		return null;
	}


}
