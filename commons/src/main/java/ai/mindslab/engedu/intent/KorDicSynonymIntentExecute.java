package ai.mindslab.engedu.intent;

import java.util.List;

import ai.mindslab.engedu.common.codes.IRestCodes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ai.mindslab.engedu.common.exceptions.EngEduException;
import ai.mindslab.engedu.common.utils.WordExtractUtil;
import ai.mindslab.engedu.dic.dao.data.KorDicSynonymVO;
import ai.mindslab.engedu.dic.service.KorDicSynonymService;
import ai.mindslab.engedu.intent.service.IntentMsgService;
import ai.mindslab.engedu.intent.vo.ExtInfo;
import ai.mindslab.engedu.intent.vo.IntentExecuteMessageVO;
import ai.mindslab.engedu.intent.vo.IntentExecuteVO;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class KorDicSynonymIntentExecute implements IntentExecute {
	
	@Autowired
	private KorDicSynonymService korDicSynonymService ;
	
	@Autowired
	private IntentMsgService intentMsgService;

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public IntentExecuteMessageVO execute(IntentExecuteVO intentExecuteVO) throws EngEduException {
		
		IntentExecuteMessageVO executeMessageVO = new IntentExecuteMessageVO();

		try {
			WordExtractUtil wordExtractUtil = new WordExtractUtil();
			intentExecuteVO.setInputStr(  wordExtractUtil.extractWord(intentExecuteVO.getServiceType(), intentExecuteVO.getInputStr()) );

			List<KorDicSynonymVO> list = korDicSynonymService.getSearch(intentExecuteVO.getInputStr());

			StringBuilder resultMsgBuilder = new StringBuilder();
			StringBuilder urlBuilder = new StringBuilder();
			StringBuilder printMeansBuilder = new StringBuilder();
			for (KorDicSynonymVO korDicSynonymVO : list) {
				log.info("korDicSynonymVO.toString():"+korDicSynonymVO.toString());

				resultMsgBuilder.append(korDicSynonymVO.getMeans()).append(",");
				urlBuilder.append(korDicSynonymVO.getUrl()).append(",");
				printMeansBuilder.append(korDicSynonymVO.getPrintMeans()).append(",");

			}
			String resultMsg = resultMsgBuilder.toString();

			ExtInfo extInfo = new ExtInfo();
			extInfo.setUrl(urlBuilder.toString());
			extInfo.setPrintMeans(printMeansBuilder.toString());


			if ("".equals(resultMsg)) {
				resultMsg = intentMsgService.getServiceMsg(IntentServiceType.KORDICSYNONYM, IntentServiceMsg.KORDICSYNONYM_NOT_FOUND);
			} else {
				resultMsg = resultMsg.substring(0,resultMsg.length()-1);

				String mentMsg = intentMsgService.getServiceMsg(IntentServiceType.KORDICSYNONYM, IntentServiceMsg.KORDICSYNONYM_MAKE_MENT);

				resultMsg= mentMsg.replaceAll("_question_", intentExecuteVO.getInputStr()).replaceAll("_answer_", resultMsg);
			}

			log.info("KorDicSynonymIntentExecute resultMsg:"+resultMsg);

			executeMessageVO.setData(list);
			executeMessageVO.setResultMsg(resultMsg);
			executeMessageVO.setExtInfo(extInfo);

		} catch (Exception e) {
			throw new EngEduException(IRestCodes.ERR_CODE_FAILURE, IRestCodes.ERR_MSG_FAILURE);
		}
		
		return executeMessageVO;
	}

	@Override
	public IntentExecuteMessageVO forceQuit(IntentExecuteVO intentExecuteVO) {
		return null;
	}


}
