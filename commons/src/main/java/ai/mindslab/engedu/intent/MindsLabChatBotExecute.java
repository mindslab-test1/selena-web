package ai.mindslab.engedu.intent;

import ai.mindslab.engedu.common.codes.IMindsChatBotCodes;
import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import ai.mindslab.engedu.dic.service.MindsLabChatBotService;
import ai.mindslab.engedu.intent.service.IntentMsgService;
import ai.mindslab.engedu.intent.vo.IntentExecuteMessageVO;
import ai.mindslab.engedu.intent.vo.IntentExecuteVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Component
public class MindsLabChatBotExecute implements IntentExecute {

    @Autowired
    private IntentMsgService intentMsgService;

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public IntentExecuteMessageVO execute(IntentExecuteVO intentExecuteVO) throws EngEduException {

        IntentExecuteMessageVO intentExecuteMessageVO = new IntentExecuteMessageVO();

        try {

            MindsLabChatBotService mindslabChatbotService = new MindsLabChatBotService();
            String resultMsg = mindslabChatbotService.getChatBotAnswer(intentExecuteVO.getUserId(),intentExecuteVO.getInputStr(),intentExecuteVO.getChatBotType());
            log.info("MindsLabChatBotExecute ResultMsg:"+resultMsg);

            if(IMindsChatBotCodes.EXCHANGE_RATE.equals(intentExecuteVO.getChatBotType())){

                if("조회 실패. 다시 입력해 주세요.".equals(resultMsg)){
                    resultMsg = "";
                }
            }

            if(StringUtils.isEmpty(resultMsg)){
                String type = intentExecuteVO.getChatBotType();
                String serviceType = "";
                String code = "";
                switch (type){
                    case IMindsChatBotCodes.NOAH :
                        serviceType = IntentServiceType.ENCYCLOPDIC;
                        code = IntentServiceMsg.ENCYCLOPDIC_NOT_FOUND;
                        break;
                    case IMindsChatBotCodes.WEATHER :
                        serviceType = IntentServiceType.WEATHER;
                        code = IntentServiceMsg.WEATHER_NOT_FOUND;
                        break;
                    case IMindsChatBotCodes.DUST:
                        serviceType = IntentServiceType.DUST;
                        code = IntentServiceMsg.DUST_NOT_FOUND;
                        break;
                    case IMindsChatBotCodes.EXCHANGE_RATE :
                        serviceType = IntentServiceType.EXCHANGE_RATE;
                        code = IntentServiceMsg.EXCHANGE_RATE_NOT_FOUND;
                        break;
                    case IMindsChatBotCodes.SEASONAL_FOOD :
                        serviceType = IntentServiceType.SEASONAL_FOOD;
                        code = IntentServiceMsg.SEASONAL_FOOD_NOT_FOUND;
                        break;
                }
                resultMsg = intentMsgService.getServiceMsg(serviceType,code);
            }

            intentExecuteMessageVO.setResultMsg(resultMsg);


        } catch (Exception e) {
            throw new EngEduException(IRestCodes.ERR_CODE_FAILURE, IRestCodes.ERR_MSG_FAILURE);
        }

        return intentExecuteMessageVO;
    }

    @Override
    public IntentExecuteMessageVO forceQuit(IntentExecuteVO intentExecuteVO) {
        return null;
    }

}