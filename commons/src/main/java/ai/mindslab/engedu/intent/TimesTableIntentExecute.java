package ai.mindslab.engedu.intent;

import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import ai.mindslab.engedu.game.dao.data.TimesTableVO;
import ai.mindslab.engedu.game.service.TimesTableService;
import ai.mindslab.engedu.intent.service.IntentMsgService;
import ai.mindslab.engedu.intent.vo.ExtInfo;
import ai.mindslab.engedu.intent.vo.IntentExecuteMessageVO;
import ai.mindslab.engedu.intent.vo.IntentExecuteVO;
import lombok.extern.slf4j.Slf4j;
import net.minidev.json.JSONObject;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.ObjectUtils;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Component
public class TimesTableIntentExecute implements IntentExecute {

	@Autowired
	private TimesTableService timesTableService;
	
	@Autowired
	private IntentMsgService intentMsgService;

	@Autowired
	private RestTemplate restTemplate;

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public IntentExecuteMessageVO execute(IntentExecuteVO intentExecuteVO) throws EngEduException {
		
		IntentExecuteMessageVO intentExecuteMessageVO = new IntentExecuteMessageVO();

		try {
			String userId = intentExecuteVO.getUserId();
			String inputStr = intentExecuteVO.getInputStr();
			int seq = getUserTimesTableLevel(userId);   // 클라이언트에서 받아오기
			boolean isFirstServiceEntry = intentExecuteVO.isFirstServiceEntry();
			String resultMsg = "";
			ExtInfo extInfo = new ExtInfo();
			if (!isFirstServiceEntry) {
				// 정답체크
				TimesTableVO answerVO = timesTableService.getAnswer(userId);

				// 인식률로 보정 처리
				String[] korArr = {"이", "오"};
				String[] numArr = {"2", "5"};
				List<String> korArrList = Arrays.asList(korArr);
				if (korArrList.contains(inputStr)) {
					inputStr = numArr[korArrList.indexOf(inputStr)];
				}

				if (answerVO.getAnswer().equals(inputStr)) {
					timesTableService.updateUsedQuestionStatus(userId, answerVO.getTimesId());
					resultMsg += "딩동댕! ";
				} else {
					timesTableService.deleteUsedQuestions(userId, seq);
					resultMsg += "땡! ";
				}

				// TODO: 체크 후 단 클리어
				Integer seqCorrectCount = timesTableService.getSeqCorrectCount(userId, seq);
				if (seqCorrectCount >= 9 && seq <= 9) {
					// TODO: Client에 seq 업데이트 API 호출하기

					// 단 수 올리기
					resultMsg += seq + "단 클리어! ";
					seq++;
					saveTimesTableLevel(userId,seq);

				}
			} else {
				extInfo.setPrintMeans( intentMsgService.getServiceMsg(IntentServiceType.TIMESTABLE, IntentServiceMsg.TIMESTABLE_START_PRINTMENT) );
			}
			extInfo.setCurrentDomain(IntentServiceType.ServiceEnum.ST0021.getDomainName());
			intentExecuteMessageVO.setExtInfo(extInfo);

			// 문제 제출 및 DB에 생성
			resultMsg += getQuestionResultMsg(userId, seq);

			log.info("TimesTableIntentExecute ResultMsg:" + resultMsg);
			intentExecuteMessageVO.setResultMsg(resultMsg);

		} catch (Exception e) {

			throw new EngEduException(IRestCodes.ERR_CODE_FAILURE, IRestCodes.ERR_MSG_FAILURE);
		}

		return intentExecuteMessageVO;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
	public IntentExecuteMessageVO forceQuit(IntentExecuteVO intentExecuteVO) throws EngEduException {

		IntentExecuteMessageVO intentExecuteMessageVO = new IntentExecuteMessageVO();
		try {
			String userId = intentExecuteVO.getUserId();
			int seq = getUserTimesTableLevel(userId);   // 클라이언트에서 받아오기
			String resultMsg = "";

			String exitType = intentExecuteVO.getExitType();
			if (!StringUtils.isEmpty(exitType) && StringUtils.equalsIgnoreCase(exitType, "T")) {
				intentExecuteMessageVO = new IntentExecuteMessageVO();

				timesTableService.deleteUsedQuestions(userId, seq);
				resultMsg += "땡! ";
				resultMsg += getQuestionResultMsg(userId, seq);

				intentExecuteMessageVO.setResultMsg(resultMsg);

			} else {
				// 사용 세션 구구단 문제들 삭제
				timesTableService.deleteUsedQuestions(intentExecuteVO.getUserId(), null);
			}
		} catch (Exception e) {
			throw new EngEduException(IRestCodes.ERR_CODE_FAILURE, IRestCodes.ERR_MSG_FAILURE);
		}

		return intentExecuteMessageVO;
	}

	private String getQuestionResultMsg(String userId, int seq) {
		// 구구단 문제 가져오기
		TimesTableVO questionVO = timesTableService.getQuestion(userId, seq);
		if(ObjectUtils.isEmpty(questionVO)) {
			// 문제 전부 출제시 세션 문제 전부 삭제 후 다시 가져오기
			timesTableService.deleteUsedQuestions(userId, null);
			questionVO = timesTableService.getQuestion(userId, seq);
		}
		// 구구단 문제 저장
		timesTableService.insertUsedQuestion(userId, questionVO.getTimesId(), seq);

		return questionVO.getQuestion();
	}
	
	// 사용자 구구단 레벨 Client에 API로 가져오기
	private int getUserTimesTableLevel(String userId) {

		int timeTableLevel=2;
		String timeTableLevelStr = "1";
		String uri = "https://stu.ideepstudy.com/appv2/api/total.do";

		HttpHeaders headers = new HttpHeaders();
		headers.set("charset", "UTF-8");

		MultiValueMap<String, String> paramMap = new LinkedMultiValueMap<>();
		paramMap.add("stuId", userId);

		HttpEntity<MultiValueMap<String, String>> httpEntityRequest = new HttpEntity<>(paramMap, headers);
		
		try {
			JSONObject jsonObj = restTemplate.postForObject(uri, httpEntityRequest, JSONObject.class);
			
			HashMap<String,Object> gameObj= (HashMap<String,Object>) jsonObj.get("getGameLevel");
			
			if (gameObj != null ) {
				String result = (String)gameObj.get("result");
				log.info("getUserTimesTableLevel result:"+result);
				if (!StringUtils.isEmpty(result) && ("succees".equals(result) || "success".equals(result)   ) ) {
//					timeTableLevelStr = (String)gameObj.get("lastLevel");
					HashMap<String,Object> levelData = (HashMap<String,Object>) gameObj.get("data");
					timeTableLevelStr = (String)levelData.get("level");
					log.info("getUserTimesTableLevel timeTableLevelStr:"+timeTableLevelStr);
					
					try {
						timeTableLevel = Integer.parseInt(timeTableLevelStr);
//						timeTableLevel=timeTableLevel+1;
					} catch (Exception e) {
						timeTableLevel = 2;
					}
				} 
			} 
				
		} catch (Exception e){
			log.warn("TimesTableIntentExecute ClientApiException", e);
			timeTableLevel=2; 
		}

		return timeTableLevel;
	}
	
	
	private String saveTimesTableLevel(String userId, int level) {
		String result="";
		
		String uri = "https://stu.ideepstudy.com/appv2/api/game/saveGameLevel.do";
		
		HttpHeaders headers = new HttpHeaders();
		headers.set("charset", "UTF-8");

		MultiValueMap<String, String> paramMap = new LinkedMultiValueMap<>();
		paramMap.add("stuId", userId);
		paramMap.add("gameDiv", "G");
		paramMap.add("lastLevel", level+"");

		HttpEntity<MultiValueMap<String, String>> httpEntityRequest = new HttpEntity<>(paramMap, headers);

		try {
			ResponseEntity<String> mapResponse = restTemplate.exchange(uri, HttpMethod.POST, httpEntityRequest, String.class);

			log.info("saveTimesTableLevel getStatusCode:"+mapResponse.getStatusCode());
			log.info("saveTimesTableLevel getBody:"+mapResponse.getBody()); // return "success" 쌍 따옴표 있음 
			
			result= mapResponse.getBody();
			
		} catch (Exception e){
			log.warn("TimesTableIntentExecute ClientApiException", e);
		}

		
		return result;
	}
}
