package ai.mindslab.engedu.intent.dao;

import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface IntentMsgMapper {
	String getServiceMsg(Map<String, Object> hashMap);
}
