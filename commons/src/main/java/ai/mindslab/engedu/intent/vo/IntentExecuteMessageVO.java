package ai.mindslab.engedu.intent.vo;

import java.io.Serializable;

import ai.mindslab.engedu.bqa.commons.data.BaseResponse;
import lombok.Data;

@Data
public class IntentExecuteMessageVO implements Serializable {
	
	private Integer code;
	private String msg;
	private String errText;
	private String resultMsg;
	private Object data;
	
	private ExtInfo extInfo;
	
	
}
