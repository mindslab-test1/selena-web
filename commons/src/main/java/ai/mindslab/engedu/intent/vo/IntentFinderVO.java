package ai.mindslab.engedu.intent.vo;

import java.io.Serializable;

import lombok.Data;

@Data
public class IntentFinderVO  implements Serializable{
	private String userId;
	private String inputStr;
	private String serviceType;
	private boolean isFirstServiceEntry;
	 
	

}
