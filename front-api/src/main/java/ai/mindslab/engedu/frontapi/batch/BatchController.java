package ai.mindslab.engedu.frontapi.batch;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Slf4j
@Controller
public class BatchController {

    /*
    ** 매일 새벽 4시에 기간이 지난 파일을 삭제한다.
    */
    @Scheduled(cron = "0 0 4 * * ?")
    public void batchRemoveLogs() throws IOException {
        final String LOG_PATH = "/home/minds/apache-tomcat-8.5.32/logs";
        final int KEEPING_DAYS = 5;

        log.error("#######################################################################################################");
        log.error("remove old log-files");
        log.error("#######################################################################################################");

        File dir = new File(LOG_PATH);
        File[] fileList = dir.listFiles();

        for(int xx = 0 ; xx < fileList.length ; xx++){
            File file = fileList[xx];
            if(file.isDirectory()) continue;

            long lastModified = file.lastModified();
            String pattern = "yyyy-MM-dd hh:mm aa";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            Date lastModifiedDate = new Date( lastModified );
            Date today = new Date();

            Calendar cal = Calendar.getInstance();
            cal.setTime(today);
            cal.add(Calendar.DATE, -KEEPING_DAYS);
            if(lastModifiedDate.compareTo(cal.getTime()) < 0) {
                file.delete();
                log.info("Deleted.......... {}", file.getName());
            }
        }
    }
}
