package ai.mindslab.engedu.frontapi.client;

import ai.mindslab.engedu.common.exceptions.EngEduException;
import ai.mindslab.engedu.common.utils.SttPostUtil;
import ai.mindslab.engedu.frontapi.common.SttTimeoutJob;
import ai.mindslab.engedu.frontapi.service.WebSocketCallbackInterface;
import com.google.protobuf.ByteString;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Metadata;
import io.grpc.stub.MetadataUtils;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import maum.brain.w2l.SpeechToTextGrpc;
import maum.brain.w2l.W2L;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketSession;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;

@Slf4j
public class CommonCnnSttClient {

	private final int NUM_MODE___NONE = 0;
	private final int NUM_MODE___STAND_ALONE = 1;
	private final int NUM_MODE___COMBI = 2;

	private final int NUM_TYPE___NON_NUMBER = 0;
	private final int NUM_TYPE___DIGIT_NUMBER = 1;
	private final int NUM_TYPE___ALPHABET_NUMBER = 2;

	private final int TIMEOUT = 10000; // msec

	private ManagedChannel channel;
	private SpeechToTextGrpc.SpeechToTextStub asyncStub;
	private SpeechToTextGrpc.SpeechToTextBlockingStub blockingStub;
	private StreamObserver<W2L.Speech> requestObserver;
	private StreamObserver<W2L.TextSegment> responseObserver;

	private String resp = "";
	private WebSocketSession session;

	private ByteArrayOutputStream byteArryOutputStream ;

	private Map<String, String> clientParam;

	private WebSocketCallbackInterface webSocket;

	private SttTimeoutJob sttTimeoutJob;

	private int timeOut;

	private String userId;
	private String answer_text;

	public CommonCnnSttClient(String sttIp, int sttPort,
							  String lang, String model, String sampleRate,
							  WebSocketCallbackInterface webSocket, int timeOut,
							  String answer_text) {
		this.channel = ManagedChannelBuilder.forAddress(sttIp, sttPort).usePlaintext().build();
		this.asyncStub = SpeechToTextGrpc.newStub(channel);
		this.blockingStub = SpeechToTextGrpc.newBlockingStub(channel);

		byteArryOutputStream = new ByteArrayOutputStream( );

		this.clientParam = new HashMap<>();
		this.clientParam.putAll(clientParam);

		this.webSocket = webSocket;

		this.timeOut = timeOut;

		this.answer_text = answer_text;
	}

	public void shutdown() throws InterruptedException {
//		channel.shutdown().awaitTermination(5,  TimeUnit.SECONDS);
		channel.shutdownNow();
	}
	public void init(WebSocketSession session, String userId) {
		init(session);
		this.userId = userId;

	}
	public void init(WebSocketSession session) {
		log.debug("init CommonSttClient Session: {}" , session.getId());


		Timer timer = new Timer(true);

		this.session = session;

		Map<String, String> resultMap = new HashMap<>();


		responseObserver = new StreamObserver<W2L.TextSegment>() {
			String resultUtter="";
			Boolean do_complete = false;

			@Override
			public void onCompleted() {
				timer.cancel();
				timer.purge();
				log.debug("CommonSttClient EVENT: onCompleted");

				/* 사용자 발화 결과를 바탕으로 후처리 */
				log.debug("utter value!!!!!!!!!!!!!!!!!!!!!!!!!!!" + resultUtter);
				resultUtter = afterProcessing(answer_text, resultUtter);
				log.debug("afterProcessing(utter value) !!!!!!!!!!!!!!!!!!!!!!!!!!! " + resultUtter);

				resultMap.put("utter",resultUtter);

				try {

					webSocket.onSttResult(session,resultMap, byteArryOutputStream);
					channel.shutdownNow();

				} catch (EngEduException e) {
					e.printStackTrace();
				}

			}

			@Override
			public void onError(Throwable arg0) {
				timer.cancel();
				timer.purge();
				log.debug("CommonSttClient EVENT: onError:{}", arg0.getMessage());
				//arg0.printStackTrace();e
				try {
					channel.shutdownNow();
					session.close(CloseStatus.SERVER_ERROR);
				} catch (IOException e) {
					e.printStackTrace();
					log.error("CommonSttClient EVENT: onError session.close {} : " + e.getMessage());
				}
			}

			@Override
			public void onNext(W2L.TextSegment segment) {

				log.debug("CommonSttClient EVENT: onNext:{} {}", segment.getTxt(), segment.getEnd());

				try {
					if(!resultUtter.isEmpty()) {
						resultUtter += " ";

					}
					resultUtter += new SttPostUtil().getSttPostStr(segment.getTxt()).trim();
					log.debug("do_complete value is " + do_complete);
					log.debug("utterValue is " + resultUtter);
					if (!do_complete) {
						requestObserver.onCompleted();
						log.debug("onCompleted Request!!!!!!!");
						do_complete = true;
					}

				} catch (Exception e ) {
					e.printStackTrace();
				}
			}

		};
		requestObserver = asyncStub.streamRecognize(responseObserver);

		sttTimeoutJob = new SttTimeoutJob(webSocket,session, channel, null, requestObserver);
		timer.schedule(sttTimeoutJob, TIMEOUT);  // 주석을 막으면 작동 안함
	}

	public void sendData(byte[] buffer) {
		W2L.Speech speech = W2L.Speech.newBuilder().setBin(ByteString.copyFrom(buffer)).build();

		try {
			byteArryOutputStream.write(buffer);
		} catch (IOException e) {
			byteArryOutputStream.reset();
		}

		requestObserver.onNext(speech);
		String sessionInfo = this.session.getId();

		log.debug("CommonSttClient send , sessionId =" + sessionInfo + ",userId =" + userId);
//		doCutTest();
	}

	public void doCutTest() {
		try{
			String filePath = "/home/minds/pcmData/cutFilePcm/lee942_20190219124356_121.pcm";
			FileInputStream fileInputStream = null;
			fileInputStream = new FileInputStream(filePath);

			byte[] buf = new byte[1024];
			while (fileInputStream.read(buf, 0, buf.length) != -1) {
				W2L.Speech speech = W2L.Speech.newBuilder().setBin(ByteString.copyFrom(buf)).build();
				requestObserver.onNext(speech);
				log.debug("speech" + speech);
			}

		} catch (RuntimeException e) {
			System.out.println("on error");
			requestObserver.onError(e);
			throw e;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		requestObserver.onCompleted();
	}

	/* ================================================================================================================= */
	// 삼성영어의 요청에 의한 후처리
	/* ================================================================================================================= */

	private String afterProcessing(String answer_text, String user_text) {
		String result_text = user_text;
		String[] split_text  = answer_text.split(" ");

		for(int xx = 0; xx < split_text.length; xx++) {
			result_text = afterProcessing_Split(split_text[xx], result_text);
			if(split_text[xx].contains("-")) result_text = afterProcessing_Hyphen(split_text[xx], result_text);
		}
		log.debug("after processing.... hypen !!!!!!!!!!!!!!!!!!!!!!!!!!!" + result_text);
		result_text = afterProcessing_replace(result_text);
		log.debug("after processing.... replace !!!!!!!!!!!!!!!!!!!!!!!!!!!" + result_text);
		result_text = afterProcessing_Incorrect2(answer_text, result_text);
		log.debug("after processing.... incorrect !!!!!!!!!!!!!!!!!!!!!!!!!!!" + result_text);

//		이미 후처리 기능으로 개발되어 있음. (축약형)
//		result_text = afterProcessing_Contraction(result_text);

//		이미 후처리 기능으로 개발되어 있음. (숫자 변환) - 기존 로직에 문제가 있어서 추가 구현함.
		result_text = afterProcessing_Number(answer_text, result_text);
		return result_text.trim();
	}

	/*
	*
	*  ............................................................
	*
	*
	*  차후엔, 아래 배열을 DB에서 관리하도록 변경해야 함.
	*
	* ............................................................
	*
	*
	*/
	/* 하나의 워드가 분리 되었을 경우, 붙여주는 기능 */
	private String afterProcessing_Split(String word, String user_text) {
		String[][] check_words = new String[][] {
				{"anymore", "any more"},
				{"today", "to day"},
				{"tomorrow", "to morrow"},
				{"someone", "some one"},
				{"maybe", "may be"}
		};

		String result_text = user_text;
		for(int xx = 0; xx < check_words.length; xx++) {
			if(word.equalsIgnoreCase(check_words[xx][0])) {
				result_text = result_text.replace(check_words[xx][1], check_words[xx][0]);
			}
		}

		return result_text.trim();
	}

	/* 하이픈으로 연결된 단어가 하이픈 없이 두 단어로 분리된 경우 합쳐 주는 기능 */
	private String afterProcessing_Hyphen(String word, String user_text) {
//		log.debug("afterProcessing_Hyphen ............ {}", word);
//		String result_text = user_text;
//		String[] split_text = word.split("-");
//		if(split_text.length <= 1) return result_text;
//
//		return result_text.replace(split_text[0] + " " + split_text[1], word).trim();

		if(word.contains("-") == false) return user_text;

		/* 해당 단어가 다른 단어의 일부분일 경우를 방지하게 위해 공백으로 패딩 처리하여 기능 수행한다. */
		String padded_user_text = " " + user_text + " ";
		String origin_word = " " + word.toLowerCase().replace(".", "").replace("?", "").replace("!", "").replace(",", "") + " ";
		String replace_word = " " + word.replaceAll("-", " ").toLowerCase().replace(".", "").replace("?", "").replace("!", "").replace(",", "") + " ";
		String result_text = padded_user_text.replaceAll(replace_word,  origin_word);

		return result_text.trim();
	}

	/*
	 *
	 *  ............................................................
	 *
	 *
	 *  차후엔, 아래 배열을 DB에서 관리하도록 변경해야 함.
	 *
	 * ............................................................
	 *
	 *
	 */
	/* 지정 단어를 다른 형태로 치환 */
	private String afterProcessing_replace(String user_text) {
		// [0] -> [1]
		String[][] replace_words = new String[][] {
				{"alright", "all right"},
				{"no offence", "no offense"},
				{"some one", "someone"}
		};

		String result_text = user_text;
		for(int xx = 0; xx < replace_words.length; xx++) {
			result_text = result_text.replace(replace_words[xx][0], replace_words[xx][1]);
		}

		return result_text.trim();
	}

	/* 인식이 잘못 된 경우 중에서, 정답 단어로 변환 ::: 본 코드는 삭제 예정 */
	private String afterProcessing_Incorrect(String answer_text, String user_text) {
		String[][] replace_words = new String[][] {
				{"know", "no"},
				{"know", "now"}
		};

		String[] split_answer_text = answer_text.split(" ");
		String[] split_user_text = user_text.split(" ");
		String result_text = "";
		int xx = 0, yy = 0;
		for(xx = 0; xx < split_answer_text.length; xx++) {
			for(yy = 0; yy < replace_words.length && xx < split_user_text.length; yy++) {
				/* 체크 단어가 존재하며 오류 단어와 일치 하면, 오류 단어를 체크 단어로 변환한다.
				   알고리즘의 복잡성을 줄이기 위해, 동일 위치의 단어로만 확인한다. */
				if(split_answer_text[xx].equalsIgnoreCase(replace_words[yy][0])
				 	&& split_user_text[xx].equalsIgnoreCase(replace_words[yy][1])) {
					result_text += " " + replace_words[yy][0];
					break;
				}
			}

			/* 체크되지 않은 단어는 사용자 발화 단어를 결과 단어로 등록 */
			if(yy == replace_words.length && xx < split_user_text.length) {
				result_text += " " + split_user_text[xx];
			}

		}

		for(xx = split_answer_text.length; xx < split_user_text.length; xx++) {
			result_text += " " + split_user_text[xx];
		}

		return result_text.trim();
	}

	/*
	** [0]:정답, [1]: 잘못 인식
	**
	** 잘못된 인식을 정답으로 변환하여 인식률을 높여줌.
	*/
	private String afterProcessing_Incorrect2(String answer_text, String user_text) {
		String[][] incorrect_words = new String[][] {
				{"know", "no"},
				{"know", "now"},
				{"follow", "followed"},
				{"way to go", "we to go"},
				{"way to go", "we go"},
				{"way to go", "wait to go"},
				{"yummy", "you me"},
				{"yummy", "your me"},
				{"yummy", "hear me"},
				{"hey", "he"},
				{"no", "know"},
				{"no", "now"},
				{"eat", "it"},
				{"one dollar", "one dollars"},
				{"invade", "vade"},
				{"invade", "nvade"},
				{"invade", "node"},
				{"invade", "nade"},
				{"invade", "not"},
				{"invade", "never"},
				{"invade", "envade"},
				{"invade", "need"},
				{"nine", "fine"}
		};

		String result_text = user_text;
		for(int xx = 0; xx < incorrect_words.length; xx++) {
			String[] split_result_text = result_text.trim().split(" ");

			String padded_user_text = " " + result_text.replaceAll("[.!?,]", " ").replaceAll("  ", " ").toLowerCase() + " ";
			String padded_answer_text = " " + answer_text.replaceAll("[.!?,]", " ").replaceAll("  ", " ").toLowerCase() + " ";

			String padded_correct_text = " " + incorrect_words[xx][0] + " ";
			String padded_incorrect_text = " " + incorrect_words[xx][1] + " ";

			/* 정답 문장에 오류 발생 가능한 정답 텍스트가 존재하는 지 확인, 위치 값 반환 */
			int correct_pos = find_word_position(padded_answer_text, padded_correct_text);
			log.debug("find_word_position........... {} , {} , {}", padded_answer_text, padded_correct_text, correct_pos);
			if(correct_pos < 0) continue;

			/* 사용자 발화 문장에 오류 텍스트가 동일 위치에 존재 하는 지 확인, 위치 값 반환 */
			int incorrect_pos = find_word_position(correct_pos, padded_user_text, padded_incorrect_text);
			log.debug("find_word_position........... {} , {} , {} , {}", correct_pos, padded_answer_text, padded_correct_text, correct_pos);
			if(incorrect_pos != correct_pos) continue;

			result_text = "";
			for(int zz = 0; zz < incorrect_pos; zz++) {
				result_text += " " + split_result_text[zz];
			}
			result_text += " " + incorrect_words[xx][0];
			for(int zz = incorrect_pos + incorrect_words[xx][1].split(" ").length; zz < split_result_text.length; zz++) {
				result_text += " " + split_result_text[zz];
			}

			log.debug("afterProcessing_Incorrect2 ...... {} -> {}", padded_user_text, result_text);
		}

		return result_text.trim();
	}

	/**
	 *
	 * */
	private int find_word_position(String text, String pattern) {
		return find_word_position(0, text, pattern);
	}

	private int find_word_position(int from_pos, String text, String pattern) {
		String[] split_text = text.trim().split(" ");
		String[] split_pattern = pattern.trim().split(" ");

		int yy = 0;
		for(int xx = 0; xx < split_text.length; xx++) {
			/* 체크할 패턴이 텍스트 범위를 넘어서면 안됨. */
			for(yy = 0; yy < split_pattern.length && yy+xx < split_text.length; yy++) {
				log.debug("find_word_position .... {}:{} / {}:{} ", xx+yy, split_text[xx+yy], yy, split_pattern[yy]);
				if(split_text[xx+yy].equalsIgnoreCase(split_pattern[yy]) == false) break;
			}

			if(yy == split_pattern.length) return xx;
		}

		return -1;
	}

	/*
	 *
	 *  ............................................................
	 *
	 *
	 *  차후엔, 아래 배열을 DB에서 관리하도록 변경해야 함.
	 *
	 * ............................................................
	 *
	 *
	 */
	/* 축약형 적용 */
	private String afterProcessing_Contraction(String user_text) {
		String[][] replace_words = new String[][] {
				{"anything", "is", "anything's"},
				{"cannot", "can't"},
				{"do", "not", "you", "don't you"},
				{"do", "not", "they", "don't they"},
				{"do", "not", "we", "don't we"},
				{"does", "not", "he", "doesn't he"},
				{"does", "not", "she", "doesn't she"},
				{"does", "not", "it", "doesn't it"},
				{"everything", "is", "everything's"},
				{"he", "is", "not", "he's not"},
				{"he", "is", "he's"},
				{"he", "will", "not", "he won't"},
				{"he", "will", "he'll"},
				{"he", "would", "he'd"},
				{"here", "are", "here're"},
				{"here", "is", "here's"},
				{"how", "is", "how's"},
				{"how", "are", "how're"},
				{"i", "am", "I'm"},
				{"i", "have", "I've"},
				{"i", "will", "not", "i won't"},
				{"i", "will", "I'll"},
				{"i", "would", "I'd"},
				{"it", "is", "not", "it's not"},
				{"it", "is", "it's"},
				{"is", "not", "isn't"},
				{"it", "will", "not", "it won't"},
				{"it", "will", "it'll"},
				{"jenny", "is", "Jenny's"},
				{"let", "us", "let's"},
				{"mine", "is", "mine's"},
				{"nothing", "is", "nothing's"},
				{"she", "is", "she's"},
				{"she", "is", "not", "she's not"},
				{"she", "will", "not", "she won't"},
				{"she", "will", "she'll"},
				{"she", "would", "she'd"},
				{"something", "is", "something's"},
				{"that", "is", "that's"},
				{"that", "is", "not", "that's not"},
				{"that", "will", "that'll"},
				{"there", "are", "there're"},
				{"there", "is", "there's"},
				{"there", "will", "there'll"},
				{"these", "are", "these're"},
				{"they", "are", "they're"},
				{"they", "are", "not", "they're not"},
				{"they", "have", "they've"},
				{"they", "will", "not", "they won't"},
				{"they", "will", "they'll"},
				{"they", "would", "they'd"},
				{"time", "is", "time's"},
				{"today", "is", "today's"},
				{"we", "are", "we're"},
				{"we", "are", "not", "we're not"},
				{"we", "have", "we've"},
				{"we", "will", "not", "we won't"},
				{"we", "will", "we'll"},
				{"we", "would", "we'd"},
				{"what", "are", "what're"},
				{"what", "is", "what's"},
				{"what", "will", "what'll"},
				{"when", "is", "when's"},
				{"where", "are", "where're"},
				{"where", "is", "where's"},
				{"where", "will", "where'll"},
				{"who", "are", "who're"},
				{"who", "is", "who's"},
				{"who", "will", "who'll"},
				{"why", "is", "why's"},
				{"you", "are", "you're"},
				{"you", "are", "not", "you're not"},
				{"you", "have", "you've"},
				{"you", "will", "not", "you won't"},
				{"you", "will", "you'll"},
				{"you", "would", "you'd"},
				{"are", "not", "aren't"},
				{"could", "have", "could've"},
				{"could", "not", "couldn't"},
				{"did", "not", "didn't"},
				{"do", "not", "don't"},
				{"does", "not", "doesn't"},
				{"had", "not", "hadn't"},
				{"has", "not", "hasn't"},
				{"have", "not", "haven't"},
				{"should", "have", "should've"},
				{"should", "not", "shouldn't"},
				{"was", "not", "wasn't"},
				{"were", "not", "weren't"},
				{"will", "not", "won't"},
				{"would", "have", "would've"},
				{"would", "not", "wouldn't"},
		};

		String result_text = "";
		String[] split_text = user_text.split(" ");
		int xx = 0, yy = 0, zz = 0;

		for(xx = 0; xx < split_text.length; xx++) {
			for(yy = 0; yy < replace_words.length; yy++) {
				for(zz = 0; zz < replace_words[yy].length -1; zz++) {
//					log.info("====> {}/{}/{}  ==== {} - {}", xx, yy, zz, split_text[xx+zz], replace_words[yy][zz]);

					/* 고객 문장보다 길 경우엔 축약 대상 아님. */
					if(xx + zz >= split_text.length) break;

					if (split_text[xx + zz].equalsIgnoreCase(replace_words[yy][zz]) == false) break;
				}

				/* 끝까지 일치하면, 변환 텍스트를 결과 텍스트에 공백과 함께 추가한다. */
				if(zz == replace_words[yy].length-1) {
					result_text += " " + replace_words[yy][zz];
					xx += zz - 1;
					break;
				}
			}

			/* 일치된 경우가 없으면, 현재 단어를 공백과 함께 추가한다. */
			if(yy == replace_words.length) {
				result_text += " " + split_text[xx];
			}
		}

		return result_text.trim();
	}

	/*
	**
	*/
	private String afterProcessing_Number(String answer_text, String user_text) {
		String result_text = "";
		String origin_text = "";
		String[] split_user = user_text.split(" ");
		String[] split_answer = answer_text.split(" ");
		int num = -1;
		int num_mode = 0;
		int num_mode___before = 0;
		int non_num_count = 0;
		int word_pos = -1;

		log.info("@ afterProcessing_Number ==> split_text length = {}", split_user.length);
		for(int xx = 0; xx < split_user.length; xx++) {

			/* 현재 숫자임 */
			if((num_mode = checkNumber(split_user[xx])) > 0) {
				log.debug("@ afterProcessing_Number ==> number : mode = {} / {} ", num_mode, split_user[xx]);

				/* 처음 숫자 */
				if(num == -1) {
					log.debug("@ afterProcessing_Number ==> first number ");
					word_pos = xx;
					result_text += " ";
					num = convNumber(split_user[xx]);
					origin_text = split_user[xx];
				}
				/* 새로운 번호 */
				else if(num_mode == 1 && num_mode___before == 1){
					log.debug("@ afterProcessing_Number ==> new number ");

					// 숫자로 할지 텍스트 숫자로 할지 판단 필요...................................
					int num_type = checkNumberType(split_answer, word_pos);
					switch(num_type) {
						case NUM_TYPE___NON_NUMBER:
						case NUM_TYPE___ALPHABET_NUMBER:
							result_text += origin_text;
							break;

						case NUM_TYPE___DIGIT_NUMBER:
							result_text += num;
							break;
					}


					/* 새로운 번호 설정 */
					word_pos = xx;
					num = convNumber(split_user[xx]);
					origin_text = split_user[xx];
				}
				/* 진행중 */
				else if(num_mode == 1) {
					log.debug("@ afterProcessing_Number ==> additional number : +");

					num += convNumber(split_user[xx]);
					origin_text += " " + split_user[xx];
				}
				else if(num_mode == 2) {
					log.debug("@ afterProcessing_Number ==> additional number : *");

					num *= convNumber(split_user[xx]);
					origin_text += " " + split_user[xx];
				}

				num_mode___before = num_mode;
			}
			/* 현재 숫자가 아님, 처리중인 숫자가 있음 */
			else if(num >= 0) {
				log.debug("@ afterProcessing_Number ==> non number : in processing number : {} / {}", split_user[xx], origin_text);

				// 숫자로 할지 텍스트 숫자로 할지 판단 필요...................................
				int num_type = checkNumberType(split_answer, word_pos);
				switch(num_type) {
					case NUM_TYPE___NON_NUMBER:
					case NUM_TYPE___ALPHABET_NUMBER:
						result_text += origin_text + " " + split_user[xx];
						break;

					case NUM_TYPE___DIGIT_NUMBER:
						result_text += num + " " + split_user[xx];
						break;
				}



				num = -1;
				non_num_count++;
			}
			/* 현재 숫자가 아님, 처리중인 숫자가 없음 */
			else {
				log.debug("@ afterProcessing_Number ==> non number : {}", split_user[xx]);

				result_text += " " + split_user[xx];

				num = -1;
				non_num_count++;
			}
		}

		/* 미처리된 숫자가 있음 */
		if(num >= 0) {
			log.debug("@ afterProcessing_Number ==> end : in processing number : {}", origin_text);

			// 숫자로 할지 텍스트 숫자로 할지 판단 필요...................................
			int num_type = checkNumberType(split_answer, word_pos);
			switch(num_type) {
				case NUM_TYPE___NON_NUMBER:
				case NUM_TYPE___ALPHABET_NUMBER:
					result_text += origin_text;
					break;

				case NUM_TYPE___DIGIT_NUMBER:
					result_text += num;
					break;
			}

			num = -1;
		}

		return result_text.trim().replaceAll("  ", " ");
	}

	/**
	 * 축약형 등을 고려해서 앞뒤까지 확인한다.
	 * */
	private int checkNumberType(String[] split_answer, int word_pos) {
		int num_type;
		num_type = isNumberType(split_answer, word_pos);
		if(num_type != NUM_TYPE___NON_NUMBER) return num_type;

		num_type = isNumberType(split_answer, word_pos-1);
		if(num_type != NUM_TYPE___NON_NUMBER) return num_type;

		num_type = isNumberType(split_answer, word_pos+1);
		if(num_type != NUM_TYPE___NON_NUMBER) return num_type;

		return NUM_TYPE___NON_NUMBER;
	}

	private int isNumberType(String[] split_answer, int word_pos) {
		if(word_pos < 0) return NUM_TYPE___NON_NUMBER;
		if(word_pos >= split_answer.length) return NUM_TYPE___NON_NUMBER;

		else if(split_answer[word_pos].getBytes()[0] >= '0' && split_answer[word_pos].getBytes()[0] <= '9') return NUM_TYPE___DIGIT_NUMBER;
		else if(checkNumber(split_answer[word_pos]) == NUM_MODE___STAND_ALONE) return NUM_TYPE___ALPHABET_NUMBER;
		else return NUM_TYPE___NON_NUMBER;
	}

	/**
	 * 결과: 0 - 숫자 아님
	 *       1 - 독립 숫자
	 *       2 - 추가 결합 가능한 숫자
	 * */
	private int checkNumber(String word) {
		if(word.equalsIgnoreCase("zero")) return NUM_MODE___STAND_ALONE;
		if(word.equalsIgnoreCase("one")) return NUM_MODE___STAND_ALONE;
		if(word.equalsIgnoreCase("two")) return NUM_MODE___STAND_ALONE;
		if(word.equalsIgnoreCase("three")) return NUM_MODE___STAND_ALONE;
		if(word.equalsIgnoreCase("four")) return NUM_MODE___STAND_ALONE;
		if(word.equalsIgnoreCase("five")) return NUM_MODE___STAND_ALONE;
		if(word.equalsIgnoreCase("six")) return NUM_MODE___STAND_ALONE;
		if(word.equalsIgnoreCase("seven")) return NUM_MODE___STAND_ALONE;
		if(word.equalsIgnoreCase("eight")) return NUM_MODE___STAND_ALONE;
		if(word.equalsIgnoreCase("nine")) return NUM_MODE___STAND_ALONE;
		if(word.equalsIgnoreCase("ten")) return NUM_MODE___STAND_ALONE;
		if(word.equalsIgnoreCase("eleven")) return NUM_MODE___STAND_ALONE;
		if(word.equalsIgnoreCase("twelve")) return NUM_MODE___STAND_ALONE;
		if(word.equalsIgnoreCase("thirteen")) return NUM_MODE___STAND_ALONE;
		if(word.equalsIgnoreCase("fourteen")) return NUM_MODE___STAND_ALONE;
		if(word.equalsIgnoreCase("fifteen")) return NUM_MODE___STAND_ALONE;
		if(word.equalsIgnoreCase("sixteen")) return NUM_MODE___STAND_ALONE;
		if(word.equalsIgnoreCase("seventeen")) return NUM_MODE___STAND_ALONE;
		if(word.equalsIgnoreCase("eighteen")) return NUM_MODE___STAND_ALONE;
		if(word.equalsIgnoreCase("nineteen")) return NUM_MODE___STAND_ALONE;
		if(word.equalsIgnoreCase("twenty")) return NUM_MODE___COMBI;
		if(word.equalsIgnoreCase("thirty")) return NUM_MODE___COMBI;
		if(word.equalsIgnoreCase("forty")) return NUM_MODE___COMBI;
		if(word.equalsIgnoreCase("fifty")) return NUM_MODE___COMBI;
		if(word.equalsIgnoreCase("sixty")) return NUM_MODE___COMBI;
		if(word.equalsIgnoreCase("seventy")) return NUM_MODE___COMBI;
		if(word.equalsIgnoreCase("eighty")) return NUM_MODE___COMBI;
		if(word.equalsIgnoreCase("ninety")) return NUM_MODE___COMBI;
		if(word.equalsIgnoreCase("hundred")) return NUM_MODE___COMBI;
		if(word.equalsIgnoreCase("thousand")) return NUM_MODE___COMBI;
		if(word.equalsIgnoreCase("million")) return NUM_MODE___COMBI;

		return NUM_MODE___NONE;
	}

	private int convNumber(String word) {
		if(word.equalsIgnoreCase("zero")) return 0;
		if(word.equalsIgnoreCase("one")) return 1;
		if(word.equalsIgnoreCase("two")) return 2;
		if(word.equalsIgnoreCase("three")) return 3;
		if(word.equalsIgnoreCase("four")) return 4;
		if(word.equalsIgnoreCase("five")) return 5;
		if(word.equalsIgnoreCase("six")) return 6;
		if(word.equalsIgnoreCase("seven")) return 7;
		if(word.equalsIgnoreCase("eight")) return 8;
		if(word.equalsIgnoreCase("nine")) return 9;
		if(word.equalsIgnoreCase("ten")) return 10;
		if(word.equalsIgnoreCase("eleven")) return 11;
		if(word.equalsIgnoreCase("twelve")) return 12;
		if(word.equalsIgnoreCase("thirteen")) return 13;
		if(word.equalsIgnoreCase("fourteen")) return 14;
		if(word.equalsIgnoreCase("fifteen")) return 15;
		if(word.equalsIgnoreCase("sixteen")) return 16;
		if(word.equalsIgnoreCase("seventeen")) return 17;
		if(word.equalsIgnoreCase("eighteen")) return 18;
		if(word.equalsIgnoreCase("nineteen")) return 19;
		if(word.equalsIgnoreCase("twenty")) return 20;
		if(word.equalsIgnoreCase("thirty")) return 30;
		if(word.equalsIgnoreCase("forty")) return 40;
		if(word.equalsIgnoreCase("fifty")) return 50;
		if(word.equalsIgnoreCase("sixty")) return 60;
		if(word.equalsIgnoreCase("seventy")) return 70;
		if(word.equalsIgnoreCase("eighty")) return 80;
		if(word.equalsIgnoreCase("ninety")) return 90;
		if(word.equalsIgnoreCase("hundred")) return 100;
		if(word.equalsIgnoreCase("thousand")) return 1000;
		if(word.equalsIgnoreCase("million")) return 1000000;

		return 0;
	}
}
