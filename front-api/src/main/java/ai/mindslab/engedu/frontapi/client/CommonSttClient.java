package ai.mindslab.engedu.frontapi.client;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import ai.mindslab.engedu.common.utils.SttPostUtil;
import ai.mindslab.engedu.frontapi.controller.data.Response;
import ai.mindslab.engedu.frontapi.service.EvaluationSttService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import com.google.protobuf.ByteString;

import ai.mindslab.engedu.frontapi.common.SttTimeoutJob;
import ai.mindslab.engedu.frontapi.service.WebSocketCallbackInterface;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Metadata;
import io.grpc.stub.MetadataUtils;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import maum.brain.stt.SpeechToTextServiceGrpc;
import maum.brain.stt.SpeechToTextServiceGrpc.SpeechToTextServiceBlockingStub;
import maum.brain.stt.SpeechToTextServiceGrpc.SpeechToTextServiceStub;
import maum.brain.stt.Stt.Segment;
import maum.brain.stt.Stt.Speech;

@Slf4j
public class CommonSttClient {
	private final int TIMEOUT = 10000; // msec

	private ManagedChannel channel;
	private SpeechToTextServiceStub asyncStub;
	private SpeechToTextServiceBlockingStub blockingStub;
	private StreamObserver<Speech> requestObserver;
	private StreamObserver<Segment> responseObserver;

	private String resp = "";
	private WebSocketSession session;

	private ByteArrayOutputStream byteArryOutputStream ;

	private Map<String, String> clientParam;

	private WebSocketCallbackInterface webSocket;

	private SttTimeoutJob sttTimeoutJob;

	private int timeOut;

	private String userId;
	private String mAnswerText;

	public CommonSttClient(String sttIp, int sttPort, String lang, String model, String sampleRate, WebSocketCallbackInterface webSocket, int timeOut, String answer_text) {
		log.debug("@ CommonSttClient: ip={}, port={}", sttIp, sttPort);

		this.channel = ManagedChannelBuilder.forAddress(sttIp, sttPort).usePlaintext().build();
		this.asyncStub = SpeechToTextServiceGrpc.newStub(channel);
		this.blockingStub = SpeechToTextServiceGrpc.newBlockingStub(channel);

		Metadata meta = new Metadata();
		Metadata.Key<String> key1 = Metadata.Key.of("in.lang", Metadata.ASCII_STRING_MARSHALLER);
		Metadata.Key<String> key2 = Metadata.Key.of("in.samplerate", Metadata.ASCII_STRING_MARSHALLER);
		Metadata.Key<String> key3 = Metadata.Key.of("in.model", Metadata.ASCII_STRING_MARSHALLER);
		meta.put(key1, lang.toLowerCase());
		meta.put(key2, sampleRate);
		meta.put(key3, model);
		asyncStub = MetadataUtils.attachHeaders(asyncStub, meta);

		byteArryOutputStream = new ByteArrayOutputStream( );


		this.clientParam = new HashMap<>();
		this.clientParam.putAll(clientParam);

		this.webSocket = webSocket;

		this.timeOut = timeOut;

		this.mAnswerText = answer_text;
	}

	public void shutdown() throws InterruptedException {
//		channel.shutdown().awaitTermination(5,  TimeUnit.SECONDS);
		channel.shutdownNow();
	}
	public void init(WebSocketSession session, String userId) {
		init(session);
		this.userId = userId;

	}
	public void init(WebSocketSession session) {
		log.debug("init CommonSttClient Session: {}" , session.getId());

		Timer timer = new Timer(true);

		this.session = session;

		Map<String, String> resultMap = new HashMap<>();

		responseObserver = new StreamObserver<Segment>() {
			String resultUtter="";
			Boolean do_complete = false;

			@Override
			public void onCompleted() {
				timer.cancel();
				timer.purge();
				log.debug("CommonSttClient EVENT: onCompleted");

				/* 사용자 발화 결과를 바탕으로 후처리 */
				log.debug("utter value!!!!!!!!!!!!!!!!!!!!!!!!!!!" + resultUtter);
				if(mAnswerText != null) resultUtter = afterProcessing(mAnswerText, resultUtter);
				log.debug("afterProcessing(utter value) !!!!!!!!!!!!!!!!!!!!!!!!!!! " + resultUtter);

				resultMap.put("utter",resultUtter);
				log.debug("utter value!!!!!!!!!!!!!!!!!!!!!!!!!!!" +resultUtter);

				try {

					webSocket.onSttResult(session,resultMap, byteArryOutputStream);
					channel.shutdownNow();

				} catch (EngEduException e) {
					e.printStackTrace();
				}

			}

			@Override
			public void onError(Throwable arg0) {
				timer.cancel();
				timer.purge();

				log.debug("CommonSttClient EVENT: onError:{}", arg0.getMessage());
				//arg0.printStackTrace();e
				try {
					channel.shutdownNow();
					session.close(CloseStatus.SERVER_ERROR);
				} catch (IOException e) {
					e.printStackTrace();
					log.error("CommonSttClient EVENT: onError session.close {} : " + e.getMessage());
				}
			}

			@Override
			public void onNext(Segment segment) {
				log.debug("CommonSttClient EVENT: onNext:{} {}", segment.getTxt(), segment.getEnd());

				try {
					if(!resultUtter.isEmpty()) {
						resultUtter += " ";

					}
					resultUtter += new SttPostUtil().getSttPostStr(segment.getTxt()).trim();
					log.debug("do_complete value is " + do_complete);
					log.debug("utterValue is " + resultUtter);
					if (!do_complete) {
						requestObserver.onCompleted();
						log.debug("onCompleted Request!!!!!!!");
						do_complete = true;
					}

				} catch (Exception e ) {
					e.printStackTrace();
				}
			}

		};
		requestObserver = asyncStub.streamRecognize(responseObserver);

		sttTimeoutJob = new SttTimeoutJob(webSocket,session, channel, requestObserver, null);
		timer.schedule(sttTimeoutJob, TIMEOUT);
	}



	public void sendData(byte[] buffer) {
		Speech speech = Speech.newBuilder().setBin(ByteString.copyFrom(buffer)).build();

		try {
			byteArryOutputStream.write(buffer);
		} catch (IOException e) {
			byteArryOutputStream.reset();
		}

		requestObserver.onNext(speech);
		String sessionInfo = this.session.getId();

		log.debug("CommonSttClient send , sessionId =" + sessionInfo + ",userId =" + userId + ", size=" + buffer.length);
//		doCutTest();
	}

	public void doCutTest() {
		try{
			String filePath = "/home/minds/pcmData/cutFilePcm/lee942_20190219124356_121.pcm";
			FileInputStream fileInputStream = null;
			fileInputStream = new FileInputStream(filePath);

			byte[] buf = new byte[1024];
			while (fileInputStream.read(buf, 0, buf.length) != -1) {
				maum.brain.stt.Stt.Speech speech = maum.brain.stt.Stt.Speech.newBuilder().setBin(ByteString.copyFrom(buf)).build();
				requestObserver.onNext(speech);
				log.debug("speech" + speech);
			}

		} catch (RuntimeException e) {
			System.out.println("on error");
			requestObserver.onError(e);
			throw e;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		requestObserver.onCompleted();
	}

	/* ================================================================================================================= */
	// 삼성영어의 요청에 의한 후처리
	/* ================================================================================================================= */

	private String afterProcessing(String answer_text, String user_text) {
		String result_text = user_text;
		String[] split_text  = answer_text.split(" ");
		log.debug("after processing.... origin !!!!!!!!!!!!!!!!!!!!!!!!!!!" + result_text);

		result_text = afterProcessing_replace(result_text);
		log.debug("after processing.... replace !!!!!!!!!!!!!!!!!!!!!!!!!!!" + result_text);

		return result_text.trim();
	}


	/*
	 *
	 *  ............................................................
	 *
	 *
	 *  차후엔, 아래 배열을 DB에서 관리하도록 변경해야 함.
	 *
	 * ............................................................
	 *
	 *
	 */
	/* 지정 단어를 다른 형태로 치환 */
	private String afterProcessing_replace(String user_text) {
		// [0] -> [1]
		String[][] replace_words = new String[][] {
				{"alright", "all right"},
				{"no offence", "no offense"},
				{"some one", "someone"}
		};

		String result_text = user_text;
		for(int xx = 0; xx < replace_words.length; xx++) {
			result_text = result_text.replace(replace_words[xx][0], replace_words[xx][1]);
		}

		return result_text.trim();
	}
}
