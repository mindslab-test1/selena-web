package ai.mindslab.engedu.frontapi.client;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.websocket.Session;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.protobuf.ByteString;

import ai.mindslab.engedu.frontapi.controller.data.Result;
import ai.mindslab.engedu.frontapi.controller.data.Response;
import ai.mindslab.engedu.frontapi.controller.websocket.DialogWebSocket;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Metadata;
import io.grpc.stub.MetadataUtils;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import maum.brain.stt.SpeechToTextServiceGrpc;
import maum.brain.stt.SpeechToTextServiceGrpc.SpeechToTextServiceBlockingStub;
import maum.brain.stt.SpeechToTextServiceGrpc.SpeechToTextServiceStub;
import maum.brain.stt.Stt.Segment;
import maum.brain.stt.Stt.Speech;

@Slf4j
public class DialogSttClient_NOT_USED {

	private ManagedChannel channel;
	private SpeechToTextServiceStub asyncStub;
	private SpeechToTextServiceBlockingStub blockingStub;
	private StreamObserver<Speech> requestObserver;
	private StreamObserver<Segment> responseObserver;
	
	private String resp = "";
	private Session session;
	
	private ByteArrayOutputStream byteArryOutputStream ;
	private String type;
	
	private Map<String, String> sttClientParam;
	
	private DialogWebSocket dialogWebSocket;
	
	
	
	
	public DialogSttClient_NOT_USED(String host, int port, String lang, String type, Map<String, String> sttClientParam, DialogWebSocket dialogWebSocket) {
		this.channel = ManagedChannelBuilder.forAddress(host, port).usePlaintext().build();
		this.asyncStub = SpeechToTextServiceGrpc.newStub(channel);
		this.blockingStub = SpeechToTextServiceGrpc.newBlockingStub(channel);
		
		Metadata meta = new Metadata();
		Metadata.Key<String> key1 = Metadata.Key.of("in.lang", Metadata.ASCII_STRING_MARSHALLER);
		Metadata.Key<String> key2 = Metadata.Key.of("in.samplerate", Metadata.ASCII_STRING_MARSHALLER);
		Metadata.Key<String> key3 = Metadata.Key.of("in.model", Metadata.ASCII_STRING_MARSHALLER);
		meta.put(key1, lang);
		meta.put(key2, "8000");
		meta.put(key3, "baseline");
		asyncStub = MetadataUtils.attachHeaders(asyncStub, meta);
		
		byteArryOutputStream = new ByteArrayOutputStream( );
		this.sttClientParam = new HashMap<>();
		this.sttClientParam.putAll(sttClientParam);
		
		this.type =type; 
		
		this.dialogWebSocket = dialogWebSocket;
		
	}
	
	public void shutdown() throws InterruptedException {
//		channel.shutdown().awaitTermination(5,  TimeUnit.SECONDS);
		channel.shutdownNow();
	}
	
	public void init(Session session) {

		log.info("init START");

		this.session = session;
		
		responseObserver = new StreamObserver<Segment>() {

			@Override
			public void onCompleted() {
				log.info("onCompleted");
			}

			@Override
			public void onError(Throwable arg0) {
				log.info("onError");
				arg0.printStackTrace();
				
			}

			@Override
			public void onNext(Segment segment) {
				log.info("SttClient onNext:" + segment.getTxt());

				try {
					
					writeFile(session, segment.getTxt());
					writeWav();
					
					Result result = new Result();
					
					Response response = new Response();
					response.setResCode("200");
					response.setResMsg("SUCCESS");
					response.setResult(result);
					
					
					if ("S".equals(type)) {

						//result.setAnswerText("answerText");
						result.setUserText(segment.getTxt());
						result.setRecordUrl("uapi/SampleAudio.mp3");
						//result.setTtsUrl("ttsUrl");
						//result.setTtsText(segment.getTxt());
						//result.setExtType("extType");
						//result.setExtData("extData");
						
					} else if ("E".equals(type)) {

						result.setAnswerText( sttClientParam.get("answerText") ); 
						result.setUserText(segment.getTxt());
						result.setRecordUrl("uapi/SampleAudio.mp3");
						//result.setTtsUrl("ttsUrl");
						//result.setTtsText(segment.getTxt());
						//result.setExtType("extType");
						//result.setExtData("extData");
						
					} else if ("D".equals(type)) {
						//result.setScore("60");
						//result.setScoreDetail("10 10 10 10 10 10");
						//result.setAnswerText("uapi/rightAnswer.html");
						result.setUserText(segment.getTxt());
						result.setRecordUrl("uapi/SampleAudio.mp3");
						result.setTtsUrl("uapi/SampleAudio.mp3");
						result.setTtsText(segment.getTxt());
//						result.setExtType("");
//						result.setExtData("");
					}
					
					ObjectMapper mapper = new ObjectMapper();
					
					String jsonString = mapper.writeValueAsString(response);
					log.info(jsonString);
					
					session.getBasicRemote().sendText(jsonString); // 브라우저에게 결과 전달
					//session.getBasicRemote().sendText(segment.getTxt()); // 브라우저에게 결과 전달

				} catch (Exception e ) {
					e.printStackTrace();
				}
			}
			
			public void writeWav() {
				byte result[] = byteArryOutputStream.toByteArray();
				
				String fileName = makeFileName("result.wav");
				
				short  numChannels = 1; // mono
				int SAMPLE_RATE = 8000;
				int BITS_PER_SAMPLE = 16;
				
		        try {
		            DataOutputStream outStream = new DataOutputStream(new FileOutputStream(new File(fileName)));

		            // write the wav file per the wav file format
		            outStream.writeBytes("RIFF"); // 00 - RIFF
		            outStream.write(intToByteArray(32 + result.length), 0, 4); // 04 - how big is the rest of this file?
		            outStream.writeBytes("WAVE"); // 08 - WAVE
		            outStream.writeBytes("fmt "); // 12 - fmt
		            outStream.write(intToByteArray(16), 0, 4); // 16 - size of this chunk
		            outStream.write(shortToByteArray((short) 1), 0, 2); // 20 - what is the audio format? 1 for PCM = Pulse Code Modulation
		            outStream.write(shortToByteArray(numChannels), 0, 2); // 22 - mono or stereo? 1 or 2? (or 5 or ???)
		            outStream.write(intToByteArray(SAMPLE_RATE), 0, 4); // 24 - samples per second (numbers per second)
		            outStream.write(intToByteArray((BITS_PER_SAMPLE / 8) * SAMPLE_RATE * numChannels), 0, 4); // 28 - bytes per second
		            outStream.write(shortToByteArray((short) ((BITS_PER_SAMPLE / 8) * numChannels)), 0, 2); // 32 - # of bytes in one sample, for all channels
		            outStream.write(shortToByteArray((short) BITS_PER_SAMPLE), 0, 2); // 34 - how many bits in a sample(number)? usually 16 or 24
		            outStream.writeBytes("data"); // 36 - data
		            outStream.write(intToByteArray(result.length), 0, 4); // 40 - how big is this data chunk
		            outStream.write(result); // 44 - the actual data itself - just a long string of numbers

		            outStream.close();
		        } catch (Exception e) {
		            e.printStackTrace();
		        }
			}
			
		    private byte[] intToByteArray(int i) {
		        byte[] b = new byte[4];
		        b[0] = (byte) (i & 0x00FF);
		        b[1] = (byte) ((i >> 8) & 0x000000FF);
		        b[2] = (byte) ((i >> 16) & 0x000000FF);
		        b[3] = (byte) ((i >> 24) & 0x000000FF);
		        return b;
		    }

		    public byte[] shortToByteArray(short data) {
		        return new byte[] { (byte) (data & 0xff), (byte) ((data >>> 8) & 0xff) };
		    }
			
			public void writeFile(Session session, String msg) {
				try {
					String fileName = makeFileName("result.txt");
					
					BufferedWriter writer = new BufferedWriter(new FileWriter( new File(fileName ) ));
				    writer.write(msg);
				    writer.close();
					
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			public String makeFileName(String fileName) {
				Properties prop = new Properties();
				InputStream inputStream = null;
				StringBuffer strbuffer = new StringBuffer();
				try {
					inputStream = DialogSttClient_NOT_USED.class.getClassLoader().getResourceAsStream("config.properties");
					prop.load(inputStream);
					strbuffer.append( prop.getProperty("savefolder") );
					strbuffer.append( session.getId() );
					strbuffer.append( "_" );
					strbuffer.append( System.currentTimeMillis() );
					strbuffer.append( "_" );
					strbuffer.append( fileName );
					
					
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				return strbuffer.toString(); 
			}
			
		};
		requestObserver = asyncStub.streamRecognize(responseObserver);
		log.info("init END");
	}
	
	public void sendSttClient(byte[] buffer) {
		Speech speech = Speech.newBuilder().setBin(ByteString.copyFrom(buffer)).build();
		
		try {
			byteArryOutputStream.write(buffer);
		} catch (IOException e) {
			byteArryOutputStream.reset();
		}
		
		requestObserver.onNext(speech);
		log.info("SttClient send");
	}
}
