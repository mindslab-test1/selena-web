package ai.mindslab.engedu.frontapi.client;

import ai.mindslab.engedu.common.codes.IMarkCodes;
import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.evaluation.dao.data.GrammarEvaluationDetailVO;
import ai.mindslab.engedu.frontapi.controller.data.Response;
import ai.mindslab.engedu.frontapi.controller.data.Result;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Arrays;

@NoArgsConstructor
@Slf4j
public class GrammarEvaluationEqualClient implements  IGrammarEvaluationClient {

    @Override
    public Response getGrammarEvaluationInfo(String paramInputStr, String paramAnswerStr) {

        Response response = new Response();
        Result grammarResult = new Result();

        try{

            String inputStr = processingComparableStr(paramInputStr);
            String answerStr = processingComparableStr(paramAnswerStr);

            ArrayList<String> inputList = new ArrayList<>();
            ArrayList<String> answerList = new ArrayList<>();

            char first_char = answerStr.charAt(0);
            int check_eng;

            if ( (first_char >= 'a' && first_char <= 'z') || (first_char >= 'A' && first_char <= 'Z') )
                check_eng = 1; //영어 문장일 경우
            else
                check_eng = 0; //그 외 문장(한국어)일 경우

            Arrays.stream(inputStr.split(IMarkCodes.ANSWER_SPLIT_TEXT)).forEach(inputList :: add); //띄어쓰기 기준으로 발화 문장 split
            Arrays.stream(answerStr.split(IMarkCodes.ANSWER_SPLIT_TEXT)).forEach(answerList :: add); //띄어쓰기 기준으로 정답 문장 split

            ArrayList<GrammarEvaluationDetailVO> grammarDetailList = new ArrayList<>();

            int answerCnt =getSpaceSplitCnt(answerStr); //문장이 몇 어절로 이루어져 있나 확인
            int answerScoreMix = calcScoreMix(answerCnt); //각 어절의 점수 배분
            ArrayList<Integer> grammarScore = getGrammarEvaluationScore(answerCnt,answerScoreMix);

            // 정답 문장이 발화 문장보다 길 경우 정답 문장에 null ++
            if(answerList.size() > inputList.size()){

                int answerSizeMinusInputListSize = answerList.size() - inputList.size();

                for(int j=0; j<answerSizeMinusInputListSize; j++){
                    inputList.add(null);
                }

            }

            int score = 0;

            for(int i=0; i<answerList.size(); i++){

                String answer = answerList.get(i); //정답지에서 어절 순서대로 가져 옴
                String input = inputList.get(i); //발화문에서 어절 순서대로 가져 옴
                int eachScore;

                if(check_eng == 1) { //영어 문장일 경우
                    eachScore = answer.equals(input) ? grammarScore.get(i) : 0;
                    score += eachScore;
                    grammarDetailList.add(new GrammarEvaluationDetailVO(answer,String.valueOf(eachScore)));
                }
                else {
                    // 한국어 문장 일 경우
                    inputStr = inputStr.replaceAll(" ", "");
                    answer = answerList.get(i);
                    eachScore = inputStr.contains(answer) ? grammarScore.get(i) : 0;
                        // 총합 점수를 구하기 위해서 eachScore 점수를 더한다.
                    score += eachScore;
                    grammarDetailList.add(new GrammarEvaluationDetailVO(answer,String.valueOf(eachScore)));

                }

            }

            // 정답 문장의 자릿수를 넘어서는 발화가 들어오면 무조건 0점으로 처리
            // 단 한국어인 경우 적용 x
            if(inputList.size() > answerList.size() && check_eng ==1 ){

                score = 0;

                for(int i=0; i<grammarDetailList.size(); i++){
                    grammarDetailList.get(i).setScore(String.valueOf(0));
                }

            }

            grammarResult = setGrammarResult(paramInputStr,paramAnswerStr,score,grammarDetailList);
            response.setResCode(String.valueOf(IRestCodes.ERR_CODE_SUCCESS));
            response.setResMsg(IRestCodes.ERR_MSG_SUCCESS);
            response.setResult(grammarResult);

        }catch(Exception e){
            e.printStackTrace();
            log.error("getGrammarEvaluationInfo Error:{}" + e.getMessage());
            response.setResCode(String.valueOf(IRestCodes.ERR_CODE_GRAMMAR_EVALUATION_ERROR));
            response.setResMsg(IRestCodes.ERR_MSG_GRAMMAR_EVALUATION_ERROR);
            response.setResult(grammarResult);
        }

        return response;

    }

    private Result setGrammarResult(
            String paramInputStr,
            String paramAnswerStr,
            int paramScore,
            ArrayList<GrammarEvaluationDetailVO> paramGrammarDetailList)
    {

        Result result = new Result();

        result.setGrammarScore(String.valueOf(paramScore));
        result.setGrammarScoreDetail(paramGrammarDetailList);
        result.setUserText(paramInputStr);
        result.setAnswerText(paramAnswerStr);

        return result;
    }

    /**
     * 정답 문장 공백으로 구분하여 갯수 체크
     *  ex) i love you -> i / love / you 3개
     * @param paramAnswer 정답 문장
     * @return
     */
    private int getSpaceSplitCnt(String paramAnswer){

        int result = paramAnswer.split(IMarkCodes.ANSWER_SPLIT_TEXT).length;
        return result;

    }

    /**
     * 정답 배분율을 구하기 위해서 100점 / 정답 문장 split 갯수
     * ex) i love you 3개면 33/33/33
     * @param paramAnswerCnt getSpaceSplitCnt() 메서드로 나온 결과 문장 SPLIT 갯수
     * @return
     */
    private int calcScoreMix(int paramAnswerCnt){

        int result = 100 / paramAnswerCnt;
        return result;

    }

    /**
     * grammarScore = 문법 평가 각각 점수 배분
     * ex) 34 33 33
     * if 나누어 떨어지지 않을 경우 앞부분 부터 +1
     * @param paramAnswerCnt getSpaceSplitCnt() 메서드로 나온 결과 문장 SPLIT 갯수
     * @param paramAnswerScoreMix 각 문장에 대한 점수 배분
     * @return
     */
    private ArrayList<Integer> getGrammarEvaluationScore(int paramAnswerCnt, int paramAnswerScoreMix ) {

        ArrayList<Integer> result = new ArrayList<>();

        int remainder = 100 % paramAnswerCnt;

        for (int i = 0; i < paramAnswerCnt; i++) {

            int tempNumber = remainder <= 0 ? 0 : 1;

            result.add(paramAnswerScoreMix + tempNumber);

            remainder--;

        }

        return result;

    }

    private String processingComparableStr(String str) {
        // 연속공백을 공백으로 대체
        str = str.replaceAll("\\s{2,}", " ");

        // 특수문자 제거
        str = str.replaceAll("[,.!?;]*", "");

        // 모든 문자 소문자로 변경
        str = str.toLowerCase().trim();

        return str;
    }

    @Override
    public void shutdown() throws InterruptedException {

    }
}
