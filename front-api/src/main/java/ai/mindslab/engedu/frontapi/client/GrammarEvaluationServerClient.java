package ai.mindslab.engedu.frontapi.client;

import ai.mindslab.engedu.common.codes.IMarkCodes;
import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.evaluation.dao.data.GrammarEvaluationDetailVO;
import ai.mindslab.engedu.frontapi.controller.data.Response;
import ai.mindslab.engedu.frontapi.controller.data.Result;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import maum.brain.eev.Evaluation;
import maum.brain.eev.EvaluationServiceGrpc;

import java.util.ArrayList;
import java.util.HashMap;

@NoArgsConstructor
@AllArgsConstructor
public class GrammarEvaluationServerClient implements  IGrammarEvaluationClient{

    private ManagedChannel channel;
    private EvaluationServiceGrpc.EvaluationServiceBlockingStub blockingStub;

    public GrammarEvaluationServerClient(String paramServerIp, int paramServerPort){

        this.channel = ManagedChannelBuilder.forAddress(paramServerIp, paramServerPort).usePlaintext().build();
        this.blockingStub = EvaluationServiceGrpc.newBlockingStub(channel);

    }

    @Override
    public Response getGrammarEvaluationInfo(String paramInputStr, String paramAnswerStr){

        // 정답 문구 갯수 체크 i am a boy = 4 ( 공백으로 split)
        int answerCnt =getSpaceSplitCnt(paramAnswerStr);

        // 정답 문구 점수 계산 i am a boy = 25점
        int answerScoreMix = calcScoreMix(answerCnt);

        /**
         * grammarScore = 문법 평가 각각 점수 배분
         * 34 33 33
         * if 나누어 떨어지지 않을 경우 앞부분 부터 +1
         */
        String grammarScore = getGrammarEvaluationScore(answerCnt,answerScoreMix);

        /**
         * Evaluation.Analysis analysis = 문법 평가 서버 결과 값
         * { resultCode_ = 200000 , score_ = 20 , sentence_ = i am a boy... }
         */
        Evaluation.Analysis analysis = getGrammarAnalyzeResult(paramAnswerStr,paramInputStr,grammarScore);

        /**
         * response = 문법 평가 최종 결과 값
         * {resCode = 200 , resMsg = SUCCESS , result = { grammarScore = 20 , grammarScoreDetail = ... }
         */
        Response response = getGrammarResponse(analysis,paramInputStr,paramAnswerStr);

        return response;

    }

    /**
     * 문법 평가 최종 결과 값 set
     * @param paramAnalysis
     * @param paramInputStr
     * @param paramAnswerStr
     * @return
     */
    private Response getGrammarResponse(Evaluation.Analysis paramAnalysis, String paramInputStr, String paramAnswerStr) {

        Response response = new Response();
        Result grammarResult = new Result();

        if(paramAnalysis != null){
            grammarResult = setGrammarResult(paramAnalysis,paramInputStr,paramAnswerStr);
            response.setResCode(String.valueOf(IRestCodes.ERR_CODE_SUCCESS));
            response.setResMsg(IRestCodes.ERR_MSG_SUCCESS);
            response.setResult(grammarResult);
        }else{
            response.setResCode(String.valueOf(IRestCodes.ERR_CODE_GRAMMAR_EVALUATION_ERROR));
            response.setResMsg(IRestCodes.ERR_MSG_GRAMMAR_EVALUATION_ERROR);
            response.setResult(grammarResult);
        }

        return response;
    }

    /**
     * <pre>
     *     문법 평가 분석 결과
     * </pre>
     * @param paramAnswerStr
     * @param paramInputStr
     * @param grammarScore
     * @return
     */
    private Evaluation.Analysis getGrammarAnalyzeResult(String paramAnswerStr, String paramInputStr, String grammarScore) {

        try{

            HashMap<String,String> hashMap  = removeSpecialCharacter(paramInputStr,paramAnswerStr);

            String grammarInputStr = hashMap.get("inputStr");
            String grammarAnswerStr = hashMap.get("answerStr");

            Evaluation.TextInfo textInfo = Evaluation.TextInfo.newBuilder()
                    .setType(Evaluation.EvaluationType.SENTENCE)
                    .setExpected(grammarAnswerStr)
                    .setUtter(grammarInputStr)
                    .setScore(grammarScore)
                    .setKeywordPos(5)
                    .build();

            return this.blockingStub.textAnalyze(textInfo);

        }catch(Exception e){
            e.printStackTrace();
            return null;
        }

    }

    /**
     * <pre>
     *     정답 문장 공백으로 구분하여 갯수 체크
     *     ex) i love you -> i / love / you 3개
     * </pre>
     * @Class GrammarEvaluationServerClient.java
     * @Method getSpaceSplitCnt
     * @param paramAnswer
     * @return
     */
    private int getSpaceSplitCnt(String paramAnswer){

        int result = paramAnswer.split(IMarkCodes.ANSWER_SPLIT_TEXT).length;
        return result;

    }

    /**
     * <pre>
     *     정답 배분율을 구하기 위해서 100점 / 정답 문장 split 갯수
     *     ex) i love you 3개면 33/33/33
     * </pre>
     * @param paramAnswerCnt
     * @return
     */
    private int calcScoreMix(int paramAnswerCnt){

        int result = 100 / paramAnswerCnt;
        return result;

    }

    /**
     * <pre>
     *     문법 평가 서버에 보낼 score 배분 점수 ( 나눠 떨어지지 않을 경우 앞 부분부터 +1 채운다.)
     *     ex) 25 25 25 25 / 34 33 33
     * </pre>
     * @param paramAnswerCnt
     * @param paramAnswerScoreMix
     * @return
     */
    private String getGrammarEvaluationScore(int paramAnswerCnt, int paramAnswerScoreMix ) {

        String result = "";

        int remainder = 100 % paramAnswerCnt;

        for (int i = 0; i < paramAnswerCnt; i++) {

            int tempNumber = remainder <= 0 ? 0 : 1;

            result += (paramAnswerScoreMix + tempNumber) + IMarkCodes.ANSWER_SPLIT_TEXT;

            remainder--;
        }

        return result;

    }

    /**
     * 문법 평가 결과 값 셋팅
     * @param paramAnalysis
     * @param paramInputStr
     * @param paramAnswerStr
     * @return
     */
    private Result setGrammarResult(Evaluation.Analysis paramAnalysis, String paramInputStr, String paramAnswerStr) {

        Result result = new Result();

        int score =  paramAnalysis.getScore() > 0 ? paramAnalysis.getScore() : 0 ;

        ArrayList<GrammarEvaluationDetailVO> scoreDetail = new ArrayList<>();

        paramAnalysis.getWordScoreList().stream().forEach(
                p-> scoreDetail.add(new GrammarEvaluationDetailVO(p.getWord(),String.valueOf(p.getScore())))
        );

        result.setGrammarScore(String.valueOf(score));
        result.setGrammarScoreDetail(scoreDetail);
        result.setUserText(paramInputStr);
        result.setAnswerText(paramAnswerStr);
        return result;

    }

    /**
     * 특수문자 제거 메서드
     * 학습자 발화, 정답 문구 ! and , and  ? 제외
     * @param paramInputStr
     * @param paramAnswerStr
     * @return
     */
    private HashMap<String,String> removeSpecialCharacter(String paramInputStr, String paramAnswerStr){

        String inputStr = paramInputStr;
        String answerStr = paramAnswerStr;

        inputStr = inputStr.replaceAll(IMarkCodes.BACKSLASH + IMarkCodes.COMMA,"")
                .replaceAll(IMarkCodes.BACKSLASH +IMarkCodes.EXCLAMATION_MARK,"")
                .replaceAll(IMarkCodes.BACKSLASH +IMarkCodes.QUESTION_MARK,"")
                .replaceAll(IMarkCodes.BACKSLASH +IMarkCodes.PERIOD,"");

        answerStr = answerStr.replaceAll(IMarkCodes.BACKSLASH +IMarkCodes.COMMA,"")
                .replaceAll(IMarkCodes.BACKSLASH +IMarkCodes.EXCLAMATION_MARK,"")
                .replaceAll(IMarkCodes.BACKSLASH +IMarkCodes.QUESTION_MARK,"")
                .replaceAll(IMarkCodes.BACKSLASH +IMarkCodes.PERIOD,"");


        HashMap<String,String> result = new HashMap<>();

        result.put("inputStr",inputStr);
        result.put("answerStr",answerStr);

        return result;

    }

    public void shutdown() throws InterruptedException {
        this.channel.shutdownNow();
    }

}
