package ai.mindslab.engedu.frontapi.client;

import ai.mindslab.engedu.frontapi.controller.data.Response;

public interface IGrammarEvaluationClient {

    Response getGrammarEvaluationInfo(String paramInputStr, String paramAnswerStr);

    void shutdown() throws InterruptedException ;

}
