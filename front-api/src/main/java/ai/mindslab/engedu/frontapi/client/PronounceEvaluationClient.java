package ai.mindslab.engedu.frontapi.client;

import ai.mindslab.engedu.common.codes.IMarkCodes;
import ai.mindslab.engedu.common.codes.IPronounceCodes;
import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.common.utils.EngStringUtil;
import ai.mindslab.engedu.frontapi.controller.data.Response;
import ai.mindslab.engedu.frontapi.controller.data.Result;
import engedu.eng_evaluation.EnglishEvaluation;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;

@NoArgsConstructor
@Slf4j
public class PronounceEvaluationClient {

    private ManagedChannel channel;
    private engedu.eng_evaluation.EvaluationServiceGrpc.EvaluationServiceBlockingStub blockingStub;
    private String activeProfile;

    public PronounceEvaluationClient(String paramServerIP, int paramServerPort,String paramActiveProfile){

        this.channel = ManagedChannelBuilder.forAddress(paramServerIP, paramServerPort).usePlaintext().build();
        this.blockingStub = engedu.eng_evaluation.EvaluationServiceGrpc.newBlockingStub(channel);
        this.activeProfile = paramActiveProfile;

    }

    public Response getPronounceEvaluationInfo(
            String paramInputStr,
            String paramAnswerStr,
            String paramFileWriteWavName,
            String paramPronounceStandard)
    {

        String pronounceServerSendStr = getPronounceServerSendStr(paramPronounceStandard,paramInputStr,paramAnswerStr);

        EnglishEvaluation.EvaluationResponse pronounceResponse = getPronounceAnalyzeResult(pronounceServerSendStr,paramFileWriteWavName);

        Response response = getPronounceResponse(pronounceResponse,pronounceServerSendStr);

        return response;

    }

    private Response getPronounceResponse(
            EnglishEvaluation.EvaluationResponse paramPronounceResponse,
            String paramPronounceServerSendStr)
    {

        Response response = new Response();
        Result  pronounceResult = new Result();

        if(paramPronounceResponse == null){

            log.error("paramPronounceResponse null");
            pronounceResult = setPronounceResult(paramPronounceResponse,paramPronounceServerSendStr);
            response.setResCode(String.valueOf(IRestCodes.ERR_CODE_PRONOUNCE_EVALUATION_ERROR));
            response.setResMsg(IRestCodes.ERR_MSG_PRONOUNCE_EVALUATION_ERROR);
            response.setResult(pronounceResult);

        }else if( activeProfile.equals("local") || paramPronounceResponse.getResultCode() != -1){
            pronounceResult = setPronounceResult(paramPronounceResponse,paramPronounceServerSendStr);
            response.setResCode(String.valueOf(IRestCodes.ERR_CODE_SUCCESS));
            response.setResMsg(IRestCodes.ERR_MSG_SUCCESS);
            response.setResult(pronounceResult);
        }else{
            log.error("getPronounceResponse( Pronounce Error : {}  " + paramPronounceResponse.getResultMsg());
            response.setResCode(String.valueOf(IRestCodes.ERR_CODE_PRONOUNCE_EVALUATION_ERROR));
            response.setResMsg(IRestCodes.ERR_MSG_PRONOUNCE_EVALUATION_ERROR);
            response.setResult(pronounceResult);
        }

        return response;

    }

    /**
     * 발음 평가 분석 결과
     * @param paramPronounceServerSendStr 발음 서버에 보낼 문장 -> 사용자 발화 문장 | 정답 문장
     * @param paramFileWriteWavName 사용자 발화 저장된 wav 파일 경로
     * @return
     */
    private EnglishEvaluation.EvaluationResponse getPronounceAnalyzeResult(
            String paramPronounceServerSendStr,
            String paramFileWriteWavName)
    {


        try{

            EnglishEvaluation.EvaluationRequest evaluationRequest = EnglishEvaluation.EvaluationRequest.newBuilder()
                    .setUtter(paramPronounceServerSendStr)
                    .setFilename(paramFileWriteWavName)
                    .build();

            return this.blockingStub.withDeadlineAfter(30, TimeUnit.SECONDS).simpleAnalyze(evaluationRequest);

        }catch (Exception e){

            log.error("getPronounceAnalyzeResult Error : {} " + e.getMessage());
            //e.printStackTrace();
            return null;

        }

    }

    /**
     * 발음 평가 결과 값 셋팅
     * @param response 발음 평가 결과 Object
     * @param paramStr 문장 -> 사용자 발화 문장 | 정답 문장
     * @return
     */
    private Result setPronounceResult(EnglishEvaluation.EvaluationResponse response,String paramStr) {

        Result result = new Result();

        // 단어 점수
        int evalScore = 0;
        // 문장 점수
        int holisticScore = 0;

        int speedScore = 0;
        int rhythmScore = 0;
        int intonationScore = 0;
        int segmentalScore = 0;
        int segmentalFeat19Score = 0;
        int finalScore = 0;
        int score = holisticScore;


        if(response == null){

        }else if(response.getResultCode() != -1){

            evalScore = response.getEvalScore();
            holisticScore = (int)response.getRegressionHolistic();

            speedScore = (int)response.getRegressionSpeed();
            rhythmScore = (int)response.getRegressionRhythm();
            intonationScore = (int)response.getRegressionIntonation();
            segmentalScore = (int)response.getRegressionSegmental();
            segmentalFeat19Score = (int)response.getRegressionSegmentalFeat19();

            score = holisticScore;

            String answerType = getStrType(paramStr);

            switch (answerType){

                case IPronounceCodes.SENTENCE :
                    score = holisticScore;
                    break;
                case IPronounceCodes.WORD:
                    score = evalScore;
                    break;

            }

            finalScore = getCalcScore(score);

        }

        log.info("activeProfile : " + activeProfile);
        log.info("pronounceScore : " + score);
        log.info("finalScore : " + finalScore);
        log.info("pronounceFinalScore : " + finalScore);
        log.info("pronounceHolisticScore : " + holisticScore);
        log.info("pronounceSpeedScore : " + speedScore);
        log.info("pronounceRhythmScore : " + rhythmScore);
        log.info("pronounceIntonationScore : " + intonationScore);
        log.info("pronounceSegmentalScore : " + segmentalScore);
        log.info("pronounceSegmentalFeat19Score : " + segmentalFeat19Score);
        result.setPronounceScore(String.valueOf(finalScore));
        result.setEvalScore(String.valueOf(evalScore));
        result.setPronounceHolisticScore(String.valueOf(holisticScore));
        result.setPronounceSpeedScore(String.valueOf(speedScore));
        result.setPronounceRhythmScore(String.valueOf(rhythmScore));
        result.setPronounceIntonationScore(String.valueOf(intonationScore));
        result.setPronounceSegmentalScore(String.valueOf(segmentalScore));
        result.setPronounceSegmentalFeat19Score(String.valueOf(segmentalFeat19Score));

        return result;

    }

    public String getStrType(String paramAnswerStr){

        String result;

        String tempStr = EngStringUtil.specialCharacterRemove(paramAnswerStr);

        int tempDivCnt = tempStr.split(IMarkCodes.ANSWER_SPLIT_TEXT).length;
        if(tempDivCnt > 1){

            result = IPronounceCodes.SENTENCE;

        }else{

            result = IPronounceCodes.WORD;

        }

        return result;

    }

    /**
     * 발음 점수 계산 분기
     * @param pronounceScore 발음 점수
     * @return
     */
    private int getCalcScore(int pronounceScore){

        int result;

        switch (activeProfile){
            case "dev156":
            case "samsung":

                result = getSamsungCalculScore(pronounceScore);
                break;

            default:
                result = pronounceScore;

        }

        log.debug("Calc PronScore .................... {} / {} -> {}", activeProfile, pronounceScore, result);
        return result;
    }

    /**
     * 삼성영어 발음 점수
     * @param paramPronounceScore 발음 점수
     * @return
     */
    private int getSamsungCalculScore(int paramPronounceScore){

        // 0보다 점수가 크면 계산, 0점인 경우 0점으로 return
        //return paramPronounceScore > 0 ? 50 + (int)Math.ceil((double)paramPronounceScore / 2) : 0;

        return paramPronounceScore > 0 ? 75 + (int)Math.ceil((double)paramPronounceScore / 4) : 0;
    }

    /**
     * 발음 서버에 보낼 문장 기준
     * @param paramPronounceStandard 사용자 발화 | 정답 문장
     * @param paramInputStr 사용자 발화 문장
     * @param paramAnswerStr 정답 문장
     * @return
     */
    private String getPronounceServerSendStr(String paramPronounceStandard, String paramInputStr, String paramAnswerStr) {

        String result;
        switch (paramPronounceStandard){

            case IPronounceCodes.PRONOUNCE_ANSWER:
                result  = paramAnswerStr;
                break;
            case IPronounceCodes.PRONOUNCE_UTTER:
                result = paramInputStr;
                break;

            default:
                result = paramAnswerStr;
        }

        result = EngStringUtil.specialCharacterRemove(result).toLowerCase();

        log.debug("pronounceServerSendStr Standard :"+ paramPronounceStandard);
        log.debug("inputStr : " + paramInputStr);
        log.debug("answerStr : " + paramAnswerStr);
        log.debug("pronounceSendStr : " + result);
        return result;

    }

    public void shutdown() throws InterruptedException {

        if (this.channel != null) {

            this.channel.shutdownNow();

        }
    }

}