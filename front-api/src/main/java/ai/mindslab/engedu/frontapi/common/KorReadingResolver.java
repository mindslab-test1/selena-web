package ai.mindslab.engedu.frontapi.common;

import ai.mindslab.engedu.common.utils.DiffMatchPatchUtil;
import ai.mindslab.engedu.frontapi.client.GrammarEvaluationEqualClient;
import ai.mindslab.engedu.frontapi.controller.data.Result;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import ai.mindslab.engedu.frontapi.controller.data.Response;

import java.util.*;

@Component
@Slf4j
public class KorReadingResolver {

	public Result getReadingResult(String inputStr, String answerStr) {

		Result result = new Result();

//		DiffMatchPatchUtil dmp = new DiffMatchPatchUtil();
//		LinkedList<DiffMatchPatchUtil.Diff> diffs = dmp.diffWordMode(answerStr, inputStr);
//
//		String score = String.valueOf(dmp.getScore(diffs, answerStr));
//		if(StringUtils.isEmpty(score)) {
//			score = "0";
//		}
		Response response = new Response();
		GrammarEvaluationEqualClient grammarEvaluationEqualClient = new GrammarEvaluationEqualClient();
		response = grammarEvaluationEqualClient.getGrammarEvaluationInfo(inputStr, answerStr);
		String score = response.getResult().getGrammarScore();

		result.setAverageScore(score);

		return result;
	}
}
