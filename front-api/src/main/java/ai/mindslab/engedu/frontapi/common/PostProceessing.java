package ai.mindslab.engedu.frontapi.common;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PostProceessing {

    //삼성 영어쪽 후처리 단어
    public String getSttMessgage(String parm){

        String utter = parm;

        Pattern checkNum = Pattern.compile(".*[0-9].*");
        Matcher m = checkNum.matcher(utter);

        // 문장안에 숫자가 있는지 여부 체크
        if(m.find()) {
            Pattern checkSpace = Pattern.compile("([0-9].\\s)");
            Matcher m2 = checkSpace.matcher(utter);

            // 숫자 뒤에 공백이 있을경우
            if(m2.find()) {
                log.info("");
            } else {
                // 공백이 없을 경우
                String temp = utter;
                temp = temp.replaceAll("[^0-9]", "");
                utter = utter.replace(temp, temp +" ");

            }
        }

        if(utter.contains("vitamind")) {
            utter = utter.replace("vitamind", "vitamin d");
        } else if(utter.contains("thats amazing")) {
            utter = utter.replace("thats amazing", "that's amazing");
        } else if(utter.contains("Thats amazing")) {
            utter = utter.replace("Thats amazing", "That's amazing");
        } else  if(utter.contains("dont")) {
            utter = utter.replace("dont", "don't");
        }

        return utter;

    }
}
