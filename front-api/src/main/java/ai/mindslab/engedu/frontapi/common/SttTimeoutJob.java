package ai.mindslab.engedu.frontapi.common;

import java.util.TimerTask;

import io.grpc.ManagedChannel;
import io.grpc.stub.StreamObserver;
import maum.brain.stt.Stt;
import maum.brain.w2l.W2L;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import com.fasterxml.jackson.databind.ObjectMapper;

import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.frontapi.controller.data.Response;
import ai.mindslab.engedu.frontapi.service.WebSocketCallbackInterface;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SttTimeoutJob extends TimerTask {
	
	private  WebSocketCallbackInterface webSocket;
	private  WebSocketSession session;
	private ManagedChannel channel;
	private StreamObserver<W2L.Speech> requestObserverCnn;
	private StreamObserver<Stt.Speech> requestObserverDnn;

	public SttTimeoutJob(WebSocketCallbackInterface webSocket, WebSocketSession session,
						 ManagedChannel channel, StreamObserver<Stt.Speech> requestObserverDnn, StreamObserver<W2L.Speech> requestObserverCnn) {
		this.webSocket = webSocket;
		this.session = session;
		this.channel = channel;
		this.requestObserverDnn = requestObserverDnn;
		this.requestObserverCnn = requestObserverCnn;
	}
	
	@Override
	public void run() {
		log.debug("SttTimeoutJob RUN!!!!!!!!!!!!!!!!!!!");
		
		try {

			Response response = new Response(Integer.toString(IRestCodes.ERR_CODE_SOCKET_TIMEOUT), IRestCodes.ERR_MSG_SOCKET_TIMEOUT, "", "", null);
			ObjectMapper mapper = new ObjectMapper();
			String jsonString = mapper.writeValueAsString(response);
			log.info("jsonString: {} ", jsonString);
			session.sendMessage(new TextMessage(jsonString));

			webSocket.removeSttClient(session);

			if(channel != null && channel.isTerminated() == false) {
				if(requestObserverDnn != null) requestObserverDnn.onError(new Throwable("timeout"));
				if(requestObserverCnn != null) requestObserverCnn.onError(new Throwable("timeout"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}
