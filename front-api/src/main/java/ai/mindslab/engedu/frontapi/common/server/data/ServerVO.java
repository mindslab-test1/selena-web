package ai.mindslab.engedu.frontapi.common.server.data;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ServerVO {

    public String serverIp;

    public int serverPort;

    public int sttSampleRate;

    public String sttModel;

    public String serverEnable;

    public String ttsRecDir;

    public String ttsDevDir;

    public String domain;

}
