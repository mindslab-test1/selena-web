package ai.mindslab.engedu.frontapi.config;

import ai.mindslab.engedu.frontapi.controller.websocket.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

import ai.mindslab.engedu.frontapi.common.WsHandshakeInterceptor;

@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {

	@Autowired
	private WsSimpleSttController ssc;

	@Autowired
	private WsEvaluationSttController esc;

	@Autowired
	private WsDialogSttController dsc;

	@Autowired
	private WsTutorialSttController tsc;

	@Autowired
	private WsKorReadingSttController krsc;


	@Autowired
	private WsEngFreeTalkSttController eftc ;

	@Autowired
	private WsPhonicsSttController pssc ;


	@Autowired
	private WsLetterSttController ltsc ;

	@Autowired
	private  WsDailySttController dysc;



	@Override
	public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
		// register WebSocket Handler
		registry.addHandler(ssc, "/stt/websocket/simpleStt").addInterceptors(new WsHandshakeInterceptor()).setAllowedOrigins("*");
		registry.addHandler(esc, "/stt/websocket/evaluationStt").addInterceptors(new WsHandshakeInterceptor()).setAllowedOrigins("*");
		registry.addHandler(dsc, "/stt/websocket/dialogStt").addInterceptors(new WsHandshakeInterceptor()).setAllowedOrigins("*");
		registry.addHandler(tsc, "/stt/websocket/tutorialStt").addInterceptors(new WsHandshakeInterceptor()).setAllowedOrigins("*");
		registry.addHandler(krsc, "/stt/websocket/korReadingStt").addInterceptors(new WsHandshakeInterceptor()).setAllowedOrigins("*");
		registry.addHandler(eftc, "/stt/websocket/engFreeTalkStt").addInterceptors(new WsHandshakeInterceptor()).setAllowedOrigins("*");
		registry.addHandler(pssc, "/stt/websocket/phonicsStt").addInterceptors(new WsHandshakeInterceptor()).setAllowedOrigins("*");
		registry.addHandler(ltsc, "/stt/websocket/letterStt").addInterceptors(new WsHandshakeInterceptor()).setAllowedOrigins("*");
		registry.addHandler(dysc, "/stt/websocket/dailyStt").addInterceptors(new WsHandshakeInterceptor()).setAllowedOrigins("*");
	}


}
