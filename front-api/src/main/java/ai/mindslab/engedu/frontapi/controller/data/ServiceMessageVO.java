package ai.mindslab.engedu.frontapi.controller.data;

import lombok.Data;

@Data
public class ServiceMessageVO {
	private String userId;
	private String serviceType;
	private String inputStr;
	private String exitType;	// T:Timeout, E:Exit
}
