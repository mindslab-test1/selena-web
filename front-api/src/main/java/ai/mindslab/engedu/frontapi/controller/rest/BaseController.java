package ai.mindslab.engedu.frontapi.controller.rest;


import ai.mindslab.engedu.common.base.EnvironmentBase;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@Slf4j
@RequestMapping("/base")
public class BaseController {

    @Autowired
    private EnvironmentBase environmentBase;

    @RequestMapping("/getForceQuitUrl")
    public String getForceQuitUrl(HttpServletRequest request){

        String result = "https://localhost:8080/uapi/client/forceQuit";
        String pathUrl = request.getServerName();
        int port = request.getServerPort();

        String activeProfile = environmentBase.getActiveProfile();
        if(!activeProfile.equals("local")) {

            if(pathUrl.equals("maieng.maum.ai")){

                switch (port){
                    case 8080 :
                        result = "https://maieng.maum.ai:8080/uapi/client/forceQuit";
                        break;
                    case 8082 :
                        result = "https://maieng.maum.ai:8082/uapi/client/forceQuit";
                        break;
                    default:
                }

        } else if(pathUrl.contains("ideepstudy.com")){

            result = "https://" + pathUrl + "/uapi/client/forceQuit";

        } else if (pathUrl.equals("10.122.64.57")) {
                result = "https://10.122.64.57:8080/uapi/client/forceQuit";
            } else if (pathUrl.equals("10.122.64.58")) {
                result = "https://10.122.64.58:8080/uapi/client/forceQuit";
            }

    }

        return result;

    }


    @RequestMapping("/getWssUrl")
    public String getWssUrl(HttpServletRequest request){

        String result = "ws:///127.0.0.1:8080/uapi/stt/websocket";

        String activeProfile = environmentBase.getActiveProfile();
        if(!activeProfile.equals("local")){

            String pathUrl = request.getServerName();
            int port = request.getServerPort();

            if(pathUrl.equals("maieng.maum.ai")){

                switch (port){
                    case 8080 :
                        result = "wss:///maieng.maum.ai:8080/uapi/stt/websocket";
                        break;
                    case 8082 :
                        result = "wss:///maieng.maum.ai:8082/uapi/stt/websocket";
                        break;
                     default:
                }

            }
            if (pathUrl.equals("10.122.64.57")) {
                result = "wss:///10.122.64.57:8080/uapi/stt/websocket";
            } else if (pathUrl.equals("10.122.64.58")) {
                result = "wss:///10.122.64.58:8080/uapi/stt/websocket";
            }
            else if(pathUrl.contains("ideepstudy.com")){
                result = "wss:///ai.ideepstudy.com:8080/uapi/stt/websocket";
            }
            else if(pathUrl.contains("119.207.75.34")){
                result = "wss:///119.207.75.34:8080/uapi/stt/websocket";
            }
            else if(pathUrl.contains("119.207.75.41")){
                result = "wss:///119.207.75.41:10080/uapi/stt/websocket";
            }
            else if(request.getProtocol().toLowerCase().equals("http")) result = "ws://" +  request.getHeader("Host") + "/uapi/stt/websocket";
            else result = "wss://" +  request.getHeader("Host")  + "/uapi/stt/websocket";

        }

        return result;

    }


      @RequestMapping("/getWssUrl2")
    public ResponseEntity<String> getWssUrl2(HttpServletRequest request){

        String result = "ws:///127.0.0.1:8080/uapi/stt/websocket";
        String callBack = request.getParameter("callback");

        JSONObject jsonObj = new JSONObject();

        String activeProfile = environmentBase.getActiveProfile();
        if(!activeProfile.equals("local")){

            String pathUrl = request.getServerName();
            int port = request.getServerPort();

            if(pathUrl.equals("maieng.maum.ai")){

                switch (port){
                    case 8080 :
                        result = "wss:///maieng.maum.ai:8080/uapi/stt/websocket";
                        break;
                    case 8082 :
                        result = "wss:///maieng.maum.ai:80802/uapi/stt/websocket";
                        break;
                    default:
                }

            }
            if (pathUrl.equals("10.122.64.57")) {
                result = "wss://10.122.64.57:8080/uapi/client/websocket";
            } else if (pathUrl.equals("10.122.64.58")) {
                result = "wss://10.122.64.58:8080/uapi/client/websocket";
            }

        }

        jsonObj.put("data", result);
        callBack = callBack +"("+ jsonObj.toString()+")";

        return new ResponseEntity<>(callBack, HttpStatus.OK);

    }
}
