package ai.mindslab.engedu.frontapi.controller.rest;

import ai.mindslab.engedu.common.base.EnvironmentBase;
import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.common.service.ApiLogService;
import ai.mindslab.engedu.frontapi.common.ResponseLogResolver;
import ai.mindslab.engedu.frontapi.controller.data.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ai.mindslab.engedu.frontapi.controller.data.Result;
import ai.mindslab.engedu.frontapi.controller.data.ServiceMessageVO;
import ai.mindslab.engedu.intent.IntentManager;
import ai.mindslab.engedu.intent.vo.IntentExecuteMessageVO;
import ai.mindslab.engedu.intent.vo.IntentExecuteVO;
import ai.mindslab.engedu.intent.vo.IntentFinderVO;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class DialogSttController {
	
	@Autowired
	private IntentManager intentManager;

	@Autowired
	private ApiLogService apiLogService;
	
	@Value("${brain.tts.dev.rec.dir}")
	private String ttsDevDir;
	
	@Value("${brain.dialog.tts.rec.dir}")
	private String ttsRecDir;
	
	@Value("${client.record.domain}")
	private String domain;
	
	@Autowired
	private EnvironmentBase environmentBase;
	
	
	
	@RequestMapping(value="/dialogStartService", method= {RequestMethod.GET,RequestMethod.POST})
	@ResponseBody
	public Response startService(ServiceMessageVO serviceMessageVO ) {
		
		log.info(new Object(){}.getClass().getEnclosingMethod().getName()+" serviceMessageVO.toString() >>>>"+serviceMessageVO.toString());

		Response response = null;
		Result result = new Result();
		String resultId="";
		String errText = null;
		
		
		if (ObjectUtils.isEmpty( serviceMessageVO.getUserId() )) {
			response = new Response(Integer.toString(IRestCodes.ERR_CODE_NOT_FOUND), "USER " + IRestCodes.ERR_MSG_NOT_FOUND, IRestCodes.RESULT_TYPE_CLIENT_API, resultId, result);

		} else {
			
			//현재 사용자가 접속한 서비스를 찾는다.
			IntentFinderVO intentFinderVO = new IntentFinderVO();
			intentFinderVO.setUserId( serviceMessageVO.getUserId()  );
			intentFinderVO.setInputStr(serviceMessageVO.getInputStr() );
			intentFinderVO.setServiceType(serviceMessageVO.getServiceType());
			intentFinderVO.setFirstServiceEntry(false);


			intentManager.init(intentFinderVO);
			intentManager.getCurrentIntent();

			log.info( "intentFinderVO.toString()  =============================================>"+intentFinderVO.toString() );
			log.info( "currentServiceType =============================================>"+intentFinderVO.getServiceType()+"<============================================="  );


			IntentExecuteVO intentExecuteVO = new IntentExecuteVO();
			intentExecuteVO.setUserId( serviceMessageVO.getUserId() );
			intentExecuteVO.setInputStr( serviceMessageVO.getInputStr() );
			intentExecuteVO.setServiceType(intentFinderVO.getServiceType());
			intentExecuteVO.setFirstServiceEntry(intentFinderVO.isFirstServiceEntry());


			IntentExecuteMessageVO intentExecuteMessageVO   =  intentManager.getIntent(intentExecuteVO);
			if (intentExecuteMessageVO.getCode() != IRestCodes.ERR_CODE_SUCCESS) {
				response =  new Response(Integer.toString(intentExecuteMessageVO.getCode()), intentExecuteMessageVO.getMsg(), IRestCodes.RESULT_TYPE_DIALOG_STT, resultId, result);
				errText = intentExecuteMessageVO.getErrText();

			} else {

					response =  new Response(Integer.toString(IRestCodes.ERR_CODE_SUCCESS), IRestCodes.ERR_MSG_SUCCESS, IRestCodes.RESULT_TYPE_DIALOG_STT,resultId,result);
					
					  // DB 저장

					result.setUserText( intentFinderVO.getInputStr());
					result.setRecordUrl("rest stt no recordurl");
					result.setTtsUrl("rest stt no tts url");
					result.setTtsText(intentExecuteMessageVO.getResultMsg());

					result.setExtInfo(intentExecuteMessageVO.getExtInfo());
					

			}
			log.info("intentExecuteMessageVO.toString()================>"+intentExecuteMessageVO.toString());

		}

		if(ObjectUtils.isEmpty(response)) {
			response = new Response(Integer.toString(IRestCodes.ERR_CODE_SUCCESS), IRestCodes.ERR_MSG_SUCCESS, IRestCodes.RESULT_TYPE_CLIENT_API, "", result);
		}
		ResponseLogResolver.insertResponseLog(response, serviceMessageVO.getUserId(), null);

		return response;
	}
}
