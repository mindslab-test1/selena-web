package ai.mindslab.engedu.frontapi.controller.rest;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@RequestMapping("/app/api")
public class DummyClientController {
	/*
	
	// 학습자 나이
	@RequestMapping(value = "/member/age", method = { RequestMethod.POST })
    public String memberAge(   @RequestParam(name = "stuId") String stuId){
    	
    	String jsonString="";
    	
    	try {
	
	        Map map = new HashMap<>();
	        map.put("result", "success");
	        map.put("data", "8");
	        
			jsonString = objectToString(map);
			
    	} catch (Exception e) {
			e.printStackTrace();
		}
        return jsonString;

    }
	
	// 학습자의 이름 조회 
    @RequestMapping(value = "/member/name", method = { RequestMethod.POST })
    public String memberName(   @RequestParam(name = "stuId") String stuId){
    	
    	String jsonString="";
    	
    	try {
	
    		Map map = new HashMap<>();
	        map.put("result", "success");
	        map.put("data", "이순신");
	        
			jsonString = objectToString(map);
			
    	} catch (Exception e) {
			e.printStackTrace();
		}
        return jsonString;

    }
    
	// 학습자의 과목별 학습시간  
    @RequestMapping(value = "/study/timeplan", method = { RequestMethod.POST })
    public String studyTimeplan(   
    		@RequestParam(name = "stuId") String stuId, 
    		@RequestParam(name = "areaCd") String areaCd
    		){
    	
    	String jsonString="";
    	
    	try {
	
	        Map map = new HashMap<>();
	        map.put("result", "success");
	        
	        Map innerMap = new HashMap<>();
	        
	        if ("KO".equals(areaCd)) {
	        	innerMap.put("name", "국어성명");
	        	innerMap.put("hour", "01");
	        	innerMap.put("min", "01");
	        	
	        } else if ("MA".equals(areaCd)) {
	        	innerMap.put("name", "수학성명");
	        	innerMap.put("hour", "02");
	        	innerMap.put("min", "02");
	        	
	        } else if ("EN".equals(areaCd)) {
	        	innerMap.put("name", "영어성명");
	        	innerMap.put("hour", "03");
	        	innerMap.put("min", "03");
	        	
	        } else if ("RD".equals(areaCd)) {
	        	innerMap.put("name", "리딩성명");
	        	innerMap.put("hour", "04");
	        	innerMap.put("min", "04");
	        	
	        } else if ("HM".equals(areaCd)) {
	        	innerMap.put("name", "한문성명");
	        	innerMap.put("hour", "05");
	        	innerMap.put("min", "05");
			}
	        
	        map.put("data", innerMap);
	        
			jsonString = objectToString(map);
			
    	} catch (Exception e) {
			e.printStackTrace();
		}
        return jsonString;

    }
    
    
	// 학습자의 과목별 학습 단원   
    @RequestMapping(value = "/study/prgs", method = { RequestMethod.POST })
    public String studyPrgs(   
    		@RequestParam(name = "stuId") String stuId, 
    		@RequestParam(name = "areaCd") String areaCd
    		){
    	
    	String jsonString="";
    	
    	try {
	
	        Map map = new HashMap<>();
	        map.put("result", "success");
	        
	        Map innerMap = new HashMap<>();
	        
	        if ("KO".equals(areaCd)) {
	        	innerMap.put("name", "국어성명");
	        	innerMap.put("studySeq", "01");
	        	innerMap.put("chapterName", "국어 진도 단원");
	        	
	        } else if ("MA".equals(areaCd)) {
	        	innerMap.put("name", "수학성명");
	        	innerMap.put("studySeq", "02");
	        	innerMap.put("chapterName", "수학 진도 단원");
	        	
	        } else if ("EN".equals(areaCd)) {
	        	innerMap.put("name", "영어성명");
	        	innerMap.put("studySeq", "03");
	        	innerMap.put("chapterName", "영어 진도 단원");
	        	
	        } else if ("RD".equals(areaCd)) {
	        	innerMap.put("name", "리딩성명");
	        	innerMap.put("studySeq", "04");
	        	innerMap.put("chapterName", "리딩 진도 단원");
	        	
	        } else if ("HM".equals(areaCd)) {
	        	innerMap.put("name", "한문성명");
	        	innerMap.put("studySeq", "05");
	        	innerMap.put("chapterName", "한문 진도 단원");
			}
	        
	        map.put("data", innerMap);
	        
			jsonString = objectToString(map);
			
    	} catch (Exception e) {
			e.printStackTrace();
		}
        return jsonString;

    }
    
    
	// 과목별 학습 성취도    /app/api/study/result
    @RequestMapping(value = "/study/result", method = { RequestMethod.POST })
    public String studyResult(   
    		@RequestParam(name = "stuId") String stuId, 
    		@RequestParam(name = "areaCd") String areaCd
    		){
    	
    	String jsonString="";
    	
    	try {
	
	        Map map = new HashMap<>();
	        map.put("result", "success");
	        
	        Map innerMap = new HashMap<>();
	        
	        if ("KO".equals(areaCd)) {
	        	innerMap.put("name", "국어성명");
	        	innerMap.put("subject", "국어");
	        	innerMap.put("rate", "10");
	        	innerMap.put("acmt", "10");
	        	
	        } else if ("MA".equals(areaCd)) {
	        	innerMap.put("name", "수학성명");
	        	innerMap.put("subject", "수학");
	        	innerMap.put("rate", "20");
	        	innerMap.put("acmt", "20");
	        	
	        } else if ("EN".equals(areaCd)) {
	        	innerMap.put("name", "영어성명");
	        	innerMap.put("subject", "영어");
	        	innerMap.put("rate", "30");
	        	innerMap.put("acmt", "30");
	        	
	        } else if ("RD".equals(areaCd)) {
	        	innerMap.put("name", "리딩성명");
	        	innerMap.put("subject", "리딩");
	        	innerMap.put("rate", "40");
	        	innerMap.put("acmt", "40");
	        	
	        } else if ("HM".equals(areaCd)) {
	        	innerMap.put("name", "한문성명");
	        	innerMap.put("subject", "한문");
	        	innerMap.put("rate", "50");
	        	innerMap.put("acmt", "50");
			}
	        
	        map.put("data", innerMap);
	        
			jsonString = objectToString(map);
			
    	} catch (Exception e) {
			e.printStackTrace();
		}
        return jsonString;

    }
    
    
	// 학습자의 다음 주 읽을 책 
    @RequestMapping(value = "/study/reading/nextweek", method = { RequestMethod.POST })
    public String studyReadingNextweek(   @RequestParam(name = "stuId") String stuId){
    	
    	String jsonString="";
    	
    	try {
	
	        Map map = new HashMap<>();
	        map.put("result", "success");
	        
	        Map innerMap = new HashMap<>();
	        innerMap.put("bookName", "다음주 읽을 책");
	        innerMap.put("year", "2018");
	        innerMap.put("month", "08");
	        innerMap.put("week", "1");
	        
	        
	        map.put("data", innerMap);
	        
			jsonString = objectToString(map);
			
    	} catch (Exception e) {
			e.printStackTrace();
		}
        return jsonString;

    }
    
	// 학습자가 좋아하는 책(책 추천)
    @RequestMapping(value = "/study/reading/bookrank", method = { RequestMethod.POST })
    public String studyReadingBookrank(   @RequestParam(name = "stuId") String stuId){
    	
    	String jsonString="";
    	
    	try {
	
	        Map map = new HashMap<>();
	        map.put("result", "success");
	        
	        Map innerMap = new HashMap<>();
	        innerMap.put("bookName", "추천 책 이름");
	        
	        map.put("data", innerMap);
	        
			jsonString = objectToString(map);
			
    	} catch (Exception e) {
			e.printStackTrace();
		}
        return jsonString;

    } 
    
	// 학습자 포인트 현황
    @RequestMapping(value = "/member/point/current", method = { RequestMethod.POST })
    public String memberPointCurrent(   @RequestParam(name = "stuId") String stuId){
    	
    	String jsonString="";
    	
    	try {
	
	        Map map = new HashMap<>();
	        map.put("result", "success");
	        
	        Map innerMap = new HashMap<>();
	        innerMap.put("name", "포인트 현황 이름");
	        innerMap.put("point", "123456789");
	        
	        map.put("data", innerMap);
	        
			jsonString = objectToString(map);
			
    	} catch (Exception e) {
			e.printStackTrace();
		}
        return jsonString;

    } 
    
	// 학습자 포인트 사용내역
    @RequestMapping(value = "/member/point/use", method = { RequestMethod.POST })
    public String memberPointUse(   @RequestParam(name = "stuId") String stuId){
    	
    	String jsonString="";
    	
    	try {
	
	        Map map = new HashMap<>();
	        map.put("result", "success");
	        
	        Map innerMap = new HashMap<>();
	        innerMap.put("name", "포인트 사용 이름");
	        innerMap.put("point", "987654321");
	        
	        map.put("data", innerMap);
	        
			jsonString = objectToString(map);
			
    	} catch (Exception e) {
			e.printStackTrace();
		}
        return jsonString;

    } 
    
    
    
	// 방문학습 교사의 방문시간  /app/api/study/visitplan
    @RequestMapping(value = "/study/visitplan", method = { RequestMethod.POST })
    public String studyVisitplan(   @RequestParam(name = "stuId") String stuId, @RequestParam(name = "studayDate") String studayDate ){
    	
    	String jsonString="";
    	
    	try {
	
	        Map map = new HashMap<>();
	        map.put("result", "success");
	        
	        Map innerMap = new HashMap<>();
	        innerMap.put("name", "교사방문시간 사용 이름");
	        innerMap.put("day", "금");
	        innerMap.put("hour", "15");
	        innerMap.put("min", "30");
	        
	        map.put("data", innerMap);
	        
			jsonString = objectToString(map);
			
    	} catch (Exception e) {
			e.printStackTrace();
		}
        return jsonString;

    } 
    
    
	// 학습 질문 답변 여부  /app/api/study/qna/getanswer
    @RequestMapping(value = "/study/qna/getanswer", method = { RequestMethod.POST })
    public String studyQnaGetanswer(   @RequestParam(name = "stuId") String stuId ){
    	
    	String jsonString="";
    	
    	try {
	
	        Map map = new HashMap<>();
	        map.put("result", "success");
	        
	        Map innerMap = new HashMap<>();
	        innerMap.put("repYn", "N");
	        innerMap.put("answer", "답변내용이 있을 경우 나온다");
	        
	        map.put("data", innerMap);
	        
			jsonString = objectToString(map);
			
    	} catch (Exception e) {
			e.printStackTrace();
		}
        return jsonString;

    } 
    
    
	// 학습자 게시물 좋아요 개수  /app/api/deeptown/getlikes
    @RequestMapping(value = "/deeptown/getlikes", method = { RequestMethod.POST })
    public String deeptownGetlikes(   @RequestParam(name = "stuId") String stuId, @RequestParam(name = "bbsCd") String bbsCd ){
    	
    	String jsonString="";
    	
    	try {
	
	        Map map = new HashMap<>();
	        map.put("result", "success");
	        
	        Map innerMap = new HashMap<>();
	        
	        if ("FREE".equals(bbsCd)) {
			
	        	innerMap.put("name", "자유 게시판 사용자 이름");
		        innerMap.put("bbsName", "자유 게시판");
		        innerMap.put("bbsLikeCnt", "10");
	        	
			} else if ("PHTO".equals(bbsCd)) {
				
				innerMap.put("name", "사진 게시판 사용자 이름");
		        innerMap.put("bbsName", "사진 게시판");
		        innerMap.put("bbsLikeCnt", "5");
				
			}
	        
	        map.put("data", innerMap);
	        
			jsonString = objectToString(map);
			
    	} catch (Exception e) {
			e.printStackTrace();
		}
        return jsonString;

    } 
    
 // 현재 진행중 이벤트 정보   /app/api/deeptown/photo/getevent
    @RequestMapping(value = "/deeptown/photo/getevent", method = { RequestMethod.POST })
    public String deeptownPhotoGetevent(   @RequestParam(name = "stuId") String stuId ){
    	
    	String jsonString="";
    	
    	try {
	
	        Map map = new HashMap<>();
	        map.put("result", "success");
	        
	        Map innerMap = new HashMap<>();
			
        	innerMap.put("eventName", "이벤트의 이름");
	        innerMap.put("startMonth", "01");
	        innerMap.put("startDay", "01");
	        innerMap.put("endMonth", "12");
	        innerMap.put("endDay", "31");
	        	
	        
	        map.put("data", innerMap);
	        
			jsonString = objectToString(map);
			
    	} catch (Exception e) {
			e.printStackTrace();
		}
        return jsonString;

    } 
    
    
    // 딥타운 최근 공지사항    /app/api/deeptown/notice/getnew
    @RequestMapping(value = "/deeptown/notice/getnew", method = { RequestMethod.POST })
    public String deeptownNoticeGetnew(   @RequestParam(name = "stuId") String stuId ){
    	
    	String jsonString="";
    	
    	try {
	
	        Map map = new HashMap<>();
	        map.put("result", "success");
	        
	        Map innerMap = new HashMap<>();
			
        	innerMap.put("name", "사용자 이름");
	        innerMap.put("title", "공지사항 제목");
	        innerMap.put("contents", "공지사항 내용 공지사항 내용 공지사항 내용 공지사항 내용");
	        	
	        
	        map.put("data", innerMap);
	        
			jsonString = objectToString(map);
			
    	} catch (Exception e) {
			e.printStackTrace();
		}
        return jsonString;

    }
    
    
    private static String objectToString(Map map) {
    	
    	String jsonString="";
    	
    	try {
	        ObjectMapper mapper = new ObjectMapper();
			jsonString = mapper.writeValueAsString(map);
		
			log.info("jsonString: {} ",jsonString);
			
    	} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
        return jsonString;
    }
*/

}
