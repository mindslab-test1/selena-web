package ai.mindslab.engedu.frontapi.controller.rest;


import ai.mindslab.engedu.common.codes.ILanguageCodes;
import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.frontapi.client.IGrammarEvaluationClient;
import ai.mindslab.engedu.frontapi.common.evaluation.GrammarEvaluationFactory;
import ai.mindslab.engedu.frontapi.controller.data.Response;
import ai.mindslab.engedu.frontapi.controller.data.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;



@RestController
@Slf4j
@RequestMapping("/evaluation")
public class EvaluationController {


    @Value("${engedu.grammar.evaluation.type}")
    private String grammarEvaluationType;

    @RequestMapping("/getTestResult")
    public Response getTestResult(
            @RequestParam String inputStr,
            @RequestParam String answerStr,
            @RequestParam String language)
    {

        Response response = new Response();

        try{

            IGrammarEvaluationClient iGrammarEvaluationClient = new GrammarEvaluationFactory().getClient(grammarEvaluationType,language);

            Response grammarResponse = iGrammarEvaluationClient.getGrammarEvaluationInfo(inputStr,answerStr);
            Result grammarResult = grammarResponse.getResult();

            Result result = getResult(inputStr,answerStr,grammarResult,language);

            response = getResponse(result);


        }catch(Exception e){

            response.setResCode(Integer.toString(IRestCodes.ERR_CODE_FAILURE));
            response.setResMsg(IRestCodes.ERR_MSG_FAILURE);
            response.setResultType(IRestCodes.RESULT_TYPE_EVAL_STT);
            response.setResult(new Result());

            e.printStackTrace();

        }finally {

            return response;

        }

    }

    private Response getResponse(Result paramResult) {

        Response response = new Response();
        response.setResCode(String.valueOf(IRestCodes.ERR_CODE_SUCCESS));
        response.setResMsg(IRestCodes.ERR_MSG_SUCCESS);
        response.setResultType(IRestCodes.RESULT_TYPE_EVAL_STT);
        response.setResult(paramResult);

        return response;

    }

    private Result getResult(String paramInputStr, String paramAnswerStr, Result paramGrammarResult,String language) {

        Result result = new Result();
        result.setUserText(paramInputStr);
        result.setAnswerText(paramAnswerStr);

        switch (language.toUpperCase()){

            case ILanguageCodes.KOR :
                result.setAverageScore(paramGrammarResult.getAverageScore());
                break;
            case ILanguageCodes.ENG :
                result.setGrammarScore(paramGrammarResult.getGrammarScore());
                result.setGrammarScoreDetail(paramGrammarResult.getGrammarScoreDetail());
                int averageScore = getAverageScore(paramGrammarResult);
                result.setAverageScore(String.valueOf(averageScore));
                break;

        }

        return result;

    }

    private int getAverageScore(Result paramGrammarResult) {

        int result = Integer.parseInt(paramGrammarResult.getGrammarScore());

        return result;

    }

}
