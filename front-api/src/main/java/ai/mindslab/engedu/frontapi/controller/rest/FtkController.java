package ai.mindslab.engedu.frontapi.controller.rest;


import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.bqa.commons.data.BaseResponse;
import ai.mindslab.engedu.common.exceptions.EngEduException;
import ai.mindslab.engedu.freetalk.service.EngFreeTalkImportService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RestController
@RequestMapping("/ftk")
public class FtkController implements IRestCodes {

	private Logger logger = LoggerFactory.getLogger(FtkController.class);

	@Autowired
	private EngFreeTalkImportService engFreeTalkImportService;
	
	@RequestMapping(value = "/dataimport", method = { RequestMethod.POST })
	public @ResponseBody BaseResponse<?> dataimport(
			@RequestParam(name = "importType", required = true) String importType
			, @RequestParam(name = "brandId", required = false) String brandId
			, @RequestParam(name = "bookId", required = false) String bookId
			, @RequestParam(name = "chapterId", required = false) String chapterId
			, @RequestParam(name = "questionId", required = false) String questionId
	) {
		logger.info("RestController dataimport/{}/{}/{}/{}/{}", importType, brandId, bookId, chapterId, questionId);

		BaseResponse<Object> resp = null;
		try {

			// TempDB에 데이터 생성
			int count = engFreeTalkImportService.insertFromSelectIntoTempDB(importType, brandId, bookId, chapterId, questionId);

			if(count > 0) {
				// 전체 추가
				if(importType.equalsIgnoreCase("full")) {

					resp = engFreeTalkImportService.fullIndexing();
					// 수정 및 부분 추가
				} else if (importType.equalsIgnoreCase("modify")) {

					resp = engFreeTalkImportService.addIndexing(questionId);
				}
			}

			if(resp != null) {
				logger.info("dataimport Object/{}", resp.getData());
			}
		} catch (EngEduException e) {
			resp = new BaseResponse<>(e.getErrCode(), e.getErrMsg());
//			throw new EngEduException(e.getErrCode(), e.getErrMsg());
		}
		return resp;
	}
}