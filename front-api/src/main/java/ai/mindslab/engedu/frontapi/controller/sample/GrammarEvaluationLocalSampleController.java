package ai.mindslab.engedu.frontapi.controller.sample;


import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.common.utils.DiffMatchPatchUtil;
import ai.mindslab.engedu.evaluation.dao.data.GrammarEvaluationDetailVO;
import ai.mindslab.engedu.frontapi.controller.data.Response;
import ai.mindslab.engedu.frontapi.controller.data.Result;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@RestController
@RequestMapping("/grammarEvaluationLocal")
public class GrammarEvaluationLocalSampleController {


    @RequestMapping("/sample")
    public Response sample(
            @RequestParam(name="inputStr",defaultValue = "i love you") String paramInputStr,
            @RequestParam(name="answerStr",defaultValue = "i like you") String paramAnswerStr)
    {
        Response response = new Response();
        Result grammarResult = new Result();

        try{

            paramAnswerStr = paramAnswerStr.replaceAll("’","'");
            DiffMatchPatchUtil dmp = new DiffMatchPatchUtil();
            LinkedList<DiffMatchPatchUtil.Diff> diffs = dmp.diffWordMode(paramAnswerStr, paramInputStr);

            int score = dmp.getScore(diffs, paramAnswerStr);
            List<DiffMatchPatchUtil.WordScore> scoreDetail = dmp.getScoreDetail(diffs, paramAnswerStr);

            grammarResult = setGrammarResult(paramInputStr,paramAnswerStr,score,scoreDetail);
            response.setResCode(String.valueOf(IRestCodes.ERR_CODE_SUCCESS));
            response.setResMsg(IRestCodes.ERR_MSG_SUCCESS);
            response.setResult(grammarResult);

        }catch(Exception e){
            e.printStackTrace();
            response.setResCode(String.valueOf(IRestCodes.ERR_CODE_GRAMMAR_EVALUATION_ERROR));
            response.setResMsg(IRestCodes.ERR_MSG_GRAMMAR_EVALUATION_ERROR);
            response.setResult(grammarResult);
        }

        return response;
    }

    private Result setGrammarResult(
            String paramInputStr,
            String paramAnswerStr,
            int paramScore,
            List<DiffMatchPatchUtil.WordScore> paramScoreDetail)
    {

        Result result = new Result();

        result.setGrammarScore(String.valueOf(paramScore));

        ArrayList<GrammarEvaluationDetailVO> scoreDetail = new ArrayList<>();

        for(DiffMatchPatchUtil.WordScore detail : paramScoreDetail) {
            scoreDetail.add(new GrammarEvaluationDetailVO(detail.getWord(),detail.getScore()));
        }

        result.setGrammarScoreDetail(scoreDetail);
        result.setUserText(paramInputStr);
        result.setAnswerText(paramAnswerStr);
        return result;
    }
}
