package ai.mindslab.engedu.frontapi.controller.sample;

import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.frontapi.controller.data.Response;
import ai.mindslab.engedu.frontapi.controller.data.Result;
import engedu.eng_evaluation.EnglishEvaluation;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/pronounceEvaluation")
public class PronounceEvaluationSampleController {

    private final static String SERVER_IP = "10.122.64.57";
    private final static int SERVER_PORT = 9993;

    private ManagedChannel channel;
    private engedu.eng_evaluation.EvaluationServiceGrpc.EvaluationServiceBlockingStub blockingStub;

    @RequestMapping("/sample")
    public Response sample(@RequestParam(name="answerStr",defaultValue = "i like you") String answerStr){

        this.channel = ManagedChannelBuilder.forAddress(SERVER_IP, SERVER_PORT).usePlaintext().build();
        this.blockingStub = engedu.eng_evaluation.EvaluationServiceGrpc.newBlockingStub(channel);

        EnglishEvaluation.EvaluationRequest evaluationRequest = EnglishEvaluation.EvaluationRequest.newBuilder()
                .setUtter(answerStr)
                // 서버에 저장된 파일이 있어야 한다.(로컬 경로는 error)
                .setFilename("/record/sample/risingsuntae_20180827104604_526.wav")
                .build();

        EnglishEvaluation.EvaluationResponse analysis = this.blockingStub.simpleAnalyze(evaluationRequest);

        Response response  = new Response();
        response.setResCode(String.valueOf(IRestCodes.ERR_CODE_SUCCESS));
        response.setResMsg(IRestCodes.ERR_MSG_SUCCESS);
        Result result = new Result();
        result.setPronounceScore("30");
        response.setResult(result);

        this.channel.shutdown();

        return  response;

    }
}
