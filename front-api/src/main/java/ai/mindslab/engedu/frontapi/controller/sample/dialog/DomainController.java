package ai.mindslab.engedu.frontapi.controller.sample.dialog;

import ai.mindslab.engedu.common.base.EnvironmentBase;
import ai.mindslab.engedu.common.codes.IExtensionCodes;
import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.common.service.ApiLogService;
import ai.mindslab.engedu.common.utils.DateUtil;
import ai.mindslab.engedu.common.utils.FileCreateUtil;
import ai.mindslab.engedu.frontapi.client.ITtsClient;
import ai.mindslab.engedu.frontapi.client.TtsSelvasClient;
import ai.mindslab.engedu.frontapi.common.ResponseLogResolver;
import ai.mindslab.engedu.frontapi.common.server.ServerManageFactory;
import ai.mindslab.engedu.frontapi.common.server.data.ServerVO;
import ai.mindslab.engedu.frontapi.controller.data.Response;
import ai.mindslab.engedu.frontapi.controller.data.Result;
import ai.mindslab.engedu.frontapi.controller.data.ServiceMessageVO;
import ai.mindslab.engedu.intent.IntentManager;
import ai.mindslab.engedu.intent.vo.IntentExecuteMessageVO;
import ai.mindslab.engedu.intent.vo.IntentExecuteVO;
import ai.mindslab.engedu.intent.vo.IntentFinderVO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequestMapping("/domainService")
public class DomainController {

    @Autowired
    private IntentManager intentManager;

    @Autowired
    private ApiLogService apiLogService;

    @Value("${brain.tts.dev.rec.dir}")
    private String ttsDevDir;

    @Value("${brain.dialog.tts.rec.dir}")
    private String ttsRecDir;

    @Value("${client.record.domain}")
    private String domain;

    @Autowired
    private EnvironmentBase environmentBase;

    @Autowired
    private ServerManageFactory serverManageFactory;


    @RequestMapping(value = "/changeDomain")
    @SuppressWarnings("unchecked")
    public String startService(@RequestParam(name = "service") String paramServiceType,
                               @RequestParam(name = "userId") String paramUserId) {

        String domain = null;

        if(paramServiceType.equals("ST0015")) {
            domain = "한영사전";
        } else if (paramServiceType.equals("ST0016")) {
            domain = "국어사전";
        } else if (paramServiceType.equals("ST0017")) {
            domain = "수학사전";
        } else if (paramServiceType.equals("ST0020")) {
            domain = "백과사전";
        }

        paramServiceType = null;




        ServiceMessageVO serviceMessageVO = new ServiceMessageVO();
        serviceMessageVO.setUserId(paramUserId);
        serviceMessageVO.setServiceType(paramServiceType);
        serviceMessageVO.setInputStr(domain);

        //현재 사용자가 접속한 서비스를 찾는다.
        IntentFinderVO intentFinderVO = new IntentFinderVO();
        intentFinderVO.setUserId(serviceMessageVO.getUserId());
        intentFinderVO.setInputStr(serviceMessageVO.getInputStr());
        intentFinderVO.setServiceType(serviceMessageVO.getServiceType());
        intentFinderVO.setFirstServiceEntry(false);


        intentManager.init(intentFinderVO);
        intentManager.getCurrentIntent();


        log.info( "intentFinderVO.toString()  =============================================>"+intentFinderVO.toString() );
        log.info( "currentServiceType =============================================>"+intentFinderVO.getServiceType()+"<============================================="  );


        IntentExecuteVO intentExecuteVO = new IntentExecuteVO();
        intentExecuteVO.setUserId(serviceMessageVO.getUserId());
        intentExecuteVO.setInputStr(serviceMessageVO.getInputStr());
        intentExecuteVO.setServiceType(intentFinderVO.getServiceType());
        intentExecuteVO.setFirstServiceEntry(intentFinderVO.isFirstServiceEntry());


        Response response;

        String resultId = "";
        String errText = null;

        Result result = new Result();

        response = new Response(Integer.toString(IRestCodes.ERR_CODE_SUCCESS), IRestCodes.ERR_MSG_SUCCESS, IRestCodes.RESULT_TYPE_DIALOG_STT, resultId, result);

        IntentExecuteMessageVO intentExecuteMessageVO = intentManager.getIntent(intentExecuteVO);

        if (intentExecuteMessageVO.getCode() != IRestCodes.ERR_CODE_SUCCESS) {
            response =  new Response(Integer.toString(intentExecuteMessageVO.getCode()), intentExecuteMessageVO.getMsg(), IRestCodes.RESULT_TYPE_DIALOG_STT, resultId, result);
            errText = intentExecuteMessageVO.getErrText();

        } else {

            String waveFileName = writeTtsFile(paramUserId);

            ITtsClient ttsClient = new TtsSelvasClient(
                    this.ttsDevDir,
                    "ko_KR",
                    intentExecuteMessageVO.getResultMsg(),
                    intentExecuteVO.getServiceType(),
                    ttsRecDir,
                    waveFileName,
                    environmentBase.getActiveProfile(),
                    domain
            );


            String language = "ko_KR";

            ttsClient = setTtsServerInfo(ttsClient, language);

            Response ttsResponse = ttsClient.getTtsResponse();
            Result ttsResult = ttsResponse.getResult();

            this.closeChannel(ttsClient);

            result.setUserText(domain);
            result.setTtsUrl(ttsResult.getTtsUrl());
            result.setTtsText(intentExecuteMessageVO.getResultMsg());

            result.setExtInfo(intentExecuteMessageVO.getExtInfo());
        }

        ResponseLogResolver.insertResponseLog(response, paramUserId, errText);

        ObjectMapper mapper = new ObjectMapper();
        String jsonString = null;

        try {
            jsonString = mapper.writeValueAsString(response);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        log.info("jsonString: {} ", jsonString);

        return jsonString;

    }


    private String writeTtsFile(String paramUserid) {
        FileCreateUtil fileCreateUtil = new FileCreateUtil();

        String currentTime = new DateUtil().getCurrentTime(DateUtil.TO_SECOND);
        String userId;
        if( paramUserid != null) {
            userId = paramUserid;
        } else {
            userId = "notUse";
        }

        String randomNum = fileCreateUtil.getRandomNum();

        String fileWavName;
        fileWavName = fileCreateUtil.getFileName(userId, currentTime, randomNum, IExtensionCodes.EXT_WAV);
        log.debug(fileWavName);

        return  fileWavName;
    }


    private ITtsClient setTtsServerInfo(ITtsClient paramTtsClient, String paramLanguage) {

        ITtsClient result = paramTtsClient;
        ServerVO serverVO = serverManageFactory.getTtsServerInfo();

        if (serverVO != null) {

            log.info("setTtsServerInfo serverVO : " + serverVO);
            result.setServerInfo(serverVO.getServerIp(), serverVO.getServerPort());

        } else {

            log.info("setTtsServerInfo serverVO null");
            result = new TtsSelvasClient();

        }

        return result;

    }

    private void closeChannel(ITtsClient paramTtsClient) {

        try{
            paramTtsClient.shutdown();
        }catch (Exception e){
            e.printStackTrace();
            log.error("closeChannel :{}" + e.getMessage());
        }

    }

}

