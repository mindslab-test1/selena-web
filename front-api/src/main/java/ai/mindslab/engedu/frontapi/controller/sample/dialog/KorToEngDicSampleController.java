package ai.mindslab.engedu.frontapi.controller.sample.dialog;


import ai.mindslab.engedu.intent.KorToEngDicIntentExecute;
import ai.mindslab.engedu.intent.vo.IntentExecuteMessageVO;
import ai.mindslab.engedu.intent.vo.IntentExecuteVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 한영사전 sample rest api
 */
@RestController
@RequestMapping("/korToEngDic")
public class KorToEngDicSampleController {


    @Autowired
    private KorToEngDicIntentExecute korToEngDicIntentExecute;

    /***
     * @param paramInputStr 한영사전 단어 ex) 가구
     * @return
     * @throws Exception
     */
    @RequestMapping("/sample")
    public IntentExecuteMessageVO sample(
            @RequestParam(name="inputStr") String paramInputStr) throws Exception
    {
        IntentExecuteVO intentExecuteVO = new IntentExecuteVO();
        intentExecuteVO.setInputStr(paramInputStr);
        IntentExecuteMessageVO intentExecuteMessageVO = korToEngDicIntentExecute.execute(intentExecuteVO);
        return intentExecuteMessageVO;

    }
}
