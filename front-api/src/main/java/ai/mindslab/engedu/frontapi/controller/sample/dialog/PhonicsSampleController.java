package ai.mindslab.engedu.frontapi.controller.sample.dialog;


import ai.mindslab.engedu.intent.TimeIntentExecute;
import ai.mindslab.engedu.intent.vo.IntentExecuteMessageVO;
import ai.mindslab.engedu.intent.vo.IntentExecuteVO;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

//import com.mindslab.edu.assessment.testWord;

@RestController
@RequestMapping("/phonics")
@Slf4j
public class PhonicsSampleController {

    @RequestMapping("/sample")
    public IntentExecuteMessageVO sample(
            @RequestParam(name="word") String word,
            @RequestParam(name="targetLetter") String targetLetter) 
    {
    	
    	try {



// ==================================================================================================================================
// POM.xml 에서 일부 라이브러리 삭제에 의한 에러 방지
// ==================================================================================================================================
//    		testWord testWord = new testWord();
// ----------------------------------------------------------------------------------------------------------------------------------


			String targetLetters[] = targetLetter.split(",");
    		
    		String meaningDicLocation="/record/phonicsDict/english.json";
    		String wordnetDicLocation = "/record/phonicsDict/";
    		
    		String meaningLst = "";
    		String phonemes =  "";
    		String phonemes_count = ""; 
    		String stresses =  "";
    		String syllables =  "";
    		String ARPA =  "";
    		String IPA =  "";
    		String wordList =  "";
    		String imageOutFile ="";

// ==================================================================================================================================
// POM.xml 에서 일부 라이브러리 삭제에 의한 에러 방지
// ==================================================================================================================================
//    		Map<String, String> returnMap =testWord.calculate(word, targetLetters, meaningDicLocation, wordnetDicLocation);
//
//    		meaningLst = returnMap.get("meaningLst");
//    		phonemes = returnMap.get("phonemes");
//    		phonemes_count = returnMap.get("phonemes_count");
//    		stresses = returnMap.get("stresses");
//    		syllables = returnMap.get("syllables");
//    		ARPA = returnMap.get("ARPA");
//    		IPA = returnMap.get("IPA");
//    		wordList = returnMap.get("wordList");
// ----------------------------------------------------------------------------------------------------------------------------------

    		System.out.println("meaningLst:"+ meaningLst );
    		System.out.println("phonemes:"+ phonemes );
    		System.out.println("phonemes_count:"+ phonemes_count );
    		System.out.println("stresses:"+ stresses );
    		System.out.println("syllables:"+ syllables );
    		System.out.println("ARPA:"+ ARPA );
    		System.out.println("IPA:"+ IPA );
    		
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
		
	//	System.out.println("result:"+result);
    	
        IntentExecuteMessageVO intentExecuteMessageVO = new IntentExecuteMessageVO();
        return intentExecuteMessageVO;

    }
}
