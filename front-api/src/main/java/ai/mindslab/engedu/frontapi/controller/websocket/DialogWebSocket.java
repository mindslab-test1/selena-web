package ai.mindslab.engedu.frontapi.controller.websocket;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import ai.mindslab.engedu.frontapi.controller.data.Response;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import com.fasterxml.jackson.databind.ObjectMapper;

import ai.mindslab.engedu.frontapi.client.CommonSttClient;
import ai.mindslab.engedu.frontapi.service.WebSocketCallbackInterface;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@ServerEndpoint(value="/stt/websocket/dialogStt/NOTUSED")
public class DialogWebSocket implements WebSocketCallbackInterface {

	private HashMap<Session, CommonSttClient> clients = new HashMap<Session, CommonSttClient>();
	
	private String v 			= "";    			
	private String biz 			= "";  			
	private String channel 		= ""; 		
	private String language 	= "";	
	private String service 		= "";
	private String userId 		= "";  		
	private String lectureId 	= ""; 		
	private String chapterId 	= ""; 		
	private String contentId 	= ""; 		
	private String sequence 	= "";  		
	private String answerText 	= ""; 	
	private String recordYn 	= ""; 	
	
	private String sttServerIp="";
	private int sttServerPort=0;
	private String sttSaveDir= "";
	
	private String engeduRestapiDomain= null;
	private String engeduDialogUri= null;

	private int TIME_OUT=0;
	
	public void initProperties() {
		try {
			Properties prop = new Properties();
			InputStream inputStream =  DialogWebSocket.class.getClassLoader().getResourceAsStream("config.properties");
		
			prop.load(inputStream);
			sttServerIp = prop.getProperty("engedu.stt.serverip");
			sttServerPort  = Integer.parseInt( prop.getProperty("engedu.stt.serverport") )  ;
			engeduRestapiDomain = prop.getProperty("engedu.restapi.domain");
			engeduDialogUri = prop.getProperty("engedu.restapi.dialoguri");
			sttSaveDir = prop.getProperty("engedu.stt.savedir");
			
			TIME_OUT = Integer.parseInt(prop.getProperty("engedu.restapi.timeout"));
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	@OnOpen
	public void handleOpen(Session session) {
		
		initProperties();
		
		session.setMaxBinaryMessageBufferSize(8192);
		log.info("DialogWebSocket handleOpen start session:{}", session);
		
		Map<String, List<String>> requestMap = session.getRequestParameterMap();
		
		v =    			CollectionUtils.isEmpty( requestMap.get("v") ) ? "" :    requestMap.get("v").get(0);
		biz =  			CollectionUtils.isEmpty( requestMap.get("biz") ) ? "" :    requestMap.get("biz").get(0); 
		channel = 		CollectionUtils.isEmpty( requestMap.get("channel") ) ? "" :    requestMap.get("channel").get(0); 
		language = 		CollectionUtils.isEmpty( requestMap.get("language") ) ? "" :    requestMap.get("language").get(0); 
		service = 		CollectionUtils.isEmpty( requestMap.get("service") ) ? "" :    requestMap.get("service").get(0); 
		userId =  		CollectionUtils.isEmpty( requestMap.get("userId") ) ? "" :    requestMap.get("userId").get(0); 
		lectureId = 	CollectionUtils.isEmpty( requestMap.get("lectureId") ) ? "" :    requestMap.get("lectureId").get(0); 
		chapterId = 	CollectionUtils.isEmpty( requestMap.get("chapterId") ) ? "" :    requestMap.get("chapterId").get(0); 
		contentId = 	CollectionUtils.isEmpty( requestMap.get("contentId") ) ? "" :    requestMap.get("contentId").get(0); 
		sequence =  	CollectionUtils.isEmpty( requestMap.get("sequence") ) ? "" :    requestMap.get("sequence").get(0); 
		answerText = 	CollectionUtils.isEmpty( requestMap.get("answerText") ) ? "" :    requestMap.get("answerText").get(0); 
		recordYn = 		CollectionUtils.isEmpty( requestMap.get("recordYn") ) ? "" :    requestMap.get("recordYn").get(0);  
		
		Map<String, String> sttClientParam = new HashMap<>();
		sttClientParam.put("v", v);
		sttClientParam.put("biz", biz);
		sttClientParam.put("channel", channel);
		sttClientParam.put("language", language);
		sttClientParam.put("userId", userId);
		sttClientParam.put("lectureId", lectureId);
		sttClientParam.put("chapterId", chapterId);
		sttClientParam.put("contentId", contentId);
		sttClientParam.put("sequence", sequence);
		sttClientParam.put("answerText", answerText);
		sttClientParam.put("recordYn", recordYn);
		
		log.info(this.getClass().getSimpleName()+">>>>>>>>>> v:{} biz:{} channel:{} language:{} service:{}",v,biz,channel,language,service);
		log.info(this.getClass().getSimpleName()+">>>>>>>>>> userId:{} lectureId:{} chapterId:{} contentId:{} sequence:{} answerText:{} recordYn:{}",userId,lectureId,chapterId,contentId,sequence,answerText,recordYn);
	
		log.info("sttServerIp:{} , sttServerPort:{}", sttServerIp,sttServerPort);
		
//		SttClient sttclient = new SttClient("10.122.66.72", 9801, language);
//		DialogSttClient sttclient = new DialogSttClient(sttServerIP, sttServerPort, language,"D",sttClientParam, this);
		
		CommonSttClient commonSttClient = new CommonSttClient("10.122.64.155", 9801, language, "baseline", "16000", this,30000
				, answerText);
		
//		commonSttClient.init(session);
//		clients.put(session, commonSttClient);
	}	
	
	@Override
	public void onSttResult(WebSocketSession session, Map resultMap, ByteArrayOutputStream byteArryOutputStream)  {
		log.info("DialogWebSocket callBySttClient session:{}", session);
		log.info("DialogWebSocket callBySttClient inputStr:{}", (String)resultMap.get("utter"));
		
		Response response = null;
		
/*	    Iterator<String> keys = clientParam.keySet().iterator();
        while( keys.hasNext() ){
            String key = keys.next();
        }*/
		
		
		try {
			
			log.info("engeduRestapiDomain:{} , engeduDialogUri:{}", engeduRestapiDomain,engeduDialogUri);
			
				
//			RestTemplate restTemplate = getRestTemplate();
//			URI uri = URI.create(engeduRestapiDomain+engeduDialogUri);
//			
//			HttpHeaders headers = new HttpHeaders();
//			headers.set("charset", "UTF-8");
//			
//			MultiValueMap<String, String> multivalueMap = new LinkedMultiValueMap<String, String>();
//			multivalueMap.setAll(clientParam);
//			
//			HttpEntity<MultiValueMap<String, String>> httpEntityRequest = new HttpEntity<MultiValueMap<String, String>>(multivalueMap,headers);
//			String str = restTemplate.postForObject(uri, httpEntityRequest, String.class);
//			log.info("str >>>>"+str);
//			
//			writeFile(session, (String)clientParam.get("inputStr"));
//			writeWav(session,byteArryOutputStream);
//			
			ObjectMapper mapper = new ObjectMapper();
			String jsonString = mapper.writeValueAsString(resultMap);
			log.info("jsonString: {} ",jsonString);
			
			
			session.sendMessage(new TextMessage(jsonString));
			
		} catch (Exception e1) {
			e1.printStackTrace();
			try {
				session.sendMessage(new TextMessage(""));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	private void writeWav( Session session, ByteArrayOutputStream byteArryOutputStream) {
		byte result[] = byteArryOutputStream.toByteArray();
		
		String fileName = makeFileName(session,"result.wav");
		
		short  numChannels = 1; // mono
		int SAMPLE_RATE = 8000;
		int BITS_PER_SAMPLE = 16;
		
        try {
            DataOutputStream outStream = new DataOutputStream(new FileOutputStream(new File(fileName)));

            // write the wav file per the wav file format
            outStream.writeBytes("RIFF"); // 00 - RIFF
            outStream.write(intToByteArray(32 + result.length), 0, 4); // 04 - how big is the rest of this file?
            outStream.writeBytes("WAVE"); // 08 - WAVE
            outStream.writeBytes("fmt "); // 12 - fmt
            outStream.write(intToByteArray(16), 0, 4); // 16 - size of this chunk
            outStream.write(shortToByteArray((short) 1), 0, 2); // 20 - what is the audio format? 1 for PCM = Pulse Code Modulation
            outStream.write(shortToByteArray(numChannels), 0, 2); // 22 - mono or stereo? 1 or 2? (or 5 or ???)
            outStream.write(intToByteArray(SAMPLE_RATE), 0, 4); // 24 - samples per second (numbers per second)
            outStream.write(intToByteArray((BITS_PER_SAMPLE / 8) * SAMPLE_RATE * numChannels), 0, 4); // 28 - bytes per second
            outStream.write(shortToByteArray((short) ((BITS_PER_SAMPLE / 8) * numChannels)), 0, 2); // 32 - # of bytes in one sample, for all channels
            outStream.write(shortToByteArray((short) BITS_PER_SAMPLE), 0, 2); // 34 - how many bits in a sample(number)? usually 16 or 24
            outStream.writeBytes("data"); // 36 - data
            outStream.write(intToByteArray(result.length), 0, 4); // 40 - how big is this data chunk
            outStream.write(result); // 44 - the actual data itself - just a long string of numbers

            outStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
	}
	
    private byte[] intToByteArray(int i) {
        byte[] b = new byte[4];
        b[0] = (byte) (i & 0x00FF);
        b[1] = (byte) ((i >> 8) & 0x000000FF);
        b[2] = (byte) ((i >> 16) & 0x000000FF);
        b[3] = (byte) ((i >> 24) & 0x000000FF);
        return b;
    }

    private byte[] shortToByteArray(short data) {
        return new byte[] { (byte) (data & 0xff), (byte) ((data >>> 8) & 0xff) };
    }
	
    private void writeFile(Session session, String msg) {
		try {
			String fileName = makeFileName(session, "result.txt");
			
			BufferedWriter writer = new BufferedWriter(new FileWriter( new File(fileName ) ));
			
			log.info("DialogWebSocket writeFile msg:{}", msg);
			
		    writer.write(msg);
		    writer.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
    private String makeFileName(Session session, String fileName) {
		StringBuffer strbuffer = new StringBuffer();
		try {
			strbuffer.append( sttSaveDir );
			strbuffer.append( session.getId() );
			strbuffer.append( "_" );
			strbuffer.append( System.currentTimeMillis() );
			strbuffer.append( "_" );
			strbuffer.append( fileName );
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return strbuffer.toString(); 
	}
	
	private RestTemplate getRestTemplate() {
		log.info("DialogWebSocket getRestTemplate TIME_OUT:{}", TIME_OUT);
		
	    HttpComponentsClientHttpRequestFactory factory  = new HttpComponentsClientHttpRequestFactory();
	    factory.setConnectTimeout(TIME_OUT);
	    factory.setReadTimeout(TIME_OUT);
	    RestTemplate restTemplate = new RestTemplate(factory);
	    return restTemplate;
	}

	
	@OnClose
	public void onClose(Session session) {
		try {
			clients.get(session).shutdown();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		clients.remove(session);
		log.info("DialogWebSocket onClose session:{}", session);
	}
	
	
	@OnMessage
	public void onMessage(String base64Audio, boolean last, Session session) {
		log.info("DialogWebSocket onMessage session:{}", session);
		log.info("DialogWebSocket onMessage base64Audio:{}", base64Audio);
		
		Decoder decoder = Base64.getDecoder();
		byte[] decodedByte = decoder.decode(base64Audio.split(",")[1]);
		clients.get(session).sendData(decodedByte);
		
		log.info("DialogWebSocket Partial onMessage:{}", decodedByte.toString());
		
	}
	
	@OnError
	public void onError(Throwable e) {
		e.printStackTrace();
	}


	@Override
	public void removeSttClient(WebSocketSession session) {
		// TODO Auto-generated method stub
		
	}
	

}
