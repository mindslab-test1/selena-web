package ai.mindslab.engedu.frontapi.controller.websocket;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.BinaryMessage;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.BinaryWebSocketHandler;

import ai.mindslab.engedu.frontapi.service.LetterSttService;
import ai.mindslab.engedu.frontapi.service.EngFreeTalkSttService;
import ai.mindslab.engedu.frontapi.service.PhonicsSttService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class WsLetterSttController extends BinaryWebSocketHandler {

	@Autowired
	private LetterSttService letterSttService ;
	
	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		try {
			//super.afterConnectionEstablished(session);

			Map<String, Object> requestMap = session.getAttributes();
			log.info(requestMap.toString());
			letterSttService.openSession(session, requestMap);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@Override
	protected void handleBinaryMessage(WebSocketSession session, BinaryMessage message) throws Exception {
		letterSttService.onMessage(session, message.getPayload(), false);
		super.handleBinaryMessage(session, message);
	}
	
	
    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
    	try {
    	log.debug("afterConnectionClosed {}", status.toString());
    	letterSttService.onClose(session);
    	super.afterConnectionClosed(session, status);
    	} catch(Exception e) {
    		e.printStackTrace();
    	}
    }
}
