package ai.mindslab.engedu.frontapi.service;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ai.mindslab.engedu.common.base.EnvironmentBase;
import ai.mindslab.engedu.common.data.DialogVO;
import ai.mindslab.engedu.common.service.DialogSttPostService;
import ai.mindslab.engedu.common.service.UserService;
import ai.mindslab.engedu.frontapi.client.ITtsClient;
import ai.mindslab.engedu.frontapi.client.TtsMindsLabClient;
import ai.mindslab.engedu.frontapi.client.TtsSelvasClient;
import ai.mindslab.engedu.frontapi.common.SttWriteFile;
import ai.mindslab.engedu.frontapi.common.server.ServerManageFactory;
import ai.mindslab.engedu.frontapi.common.server.data.ServerVO;
import ai.mindslab.engedu.frontapi.controller.data.Response;
import ai.mindslab.engedu.frontapi.controller.data.SttFileResponseVO;
import ai.mindslab.engedu.frontapi.service.data.Parameters;
import ai.mindslab.engedu.intent.service.IntentMsgService;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import com.fasterxml.jackson.databind.ObjectMapper;

import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.frontapi.client.CommonSttClient;
import ai.mindslab.engedu.frontapi.common.ResponseLogResolver;
import ai.mindslab.engedu.frontapi.common.SttModelResolver;
import ai.mindslab.engedu.frontapi.controller.data.Result;
import ai.mindslab.engedu.frontapi.controller.data.ServiceMessageVO;
import ai.mindslab.engedu.intent.IntentManager;
import ai.mindslab.engedu.intent.IntentServiceMsg;
import ai.mindslab.engedu.intent.IntentServiceType;
import ai.mindslab.engedu.intent.vo.IntentExecuteMessageVO;
import ai.mindslab.engedu.intent.vo.IntentExecuteVO;
import ai.mindslab.engedu.intent.vo.IntentFinderVO;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class DialogSttService extends SttService {


    @Autowired
    private IntentManager intentManager;

    @Value("${brain.dialog.stt.rec.dir}")
    private String sttRecDir;

    @Value("${brain.dialog.tts.rec.dir}")
    private String ttsRecDir;

    @Value("${brain.stt.sampleRate}")
    private int sampleRate;

    @Value("${brain.stt.ip}")
    private String sttIp;

    @Value("${brain.stt.port}")
    private int sttPort;

    @Value("${brain.eng.tts.ip}")
    private String engTtsIp;

    @Value("${brain.eng.tts.port}")
    private String engTtsPort;

    @Value("${brain.kor.tts.ip}")
    private String korTtsIp;

    @Value("${brain.kor.tts.port}")
    private String korTtsPort;

    @Autowired
    private UserService userService;

    @Value("${brain.tts.dev.rec.dir}")
    private String ttsDevDir;

    @Value("${client.record.domain}")
    private String domain;


    @Value("${engedu.socket.timeout}")
    private int timeout;

    @Autowired
    private EnvironmentBase environmentBase;

	/*
	@Autowired
	private IntentFinderService intentFinderService;
	*/

    @Autowired
    private IntentMsgService intentMsgService;

    @Autowired
    private ServerManageFactory serverManageFactory;

    @Override
    public void onSttResult(WebSocketSession session, Map resultMap, ByteArrayOutputStream byteArrayOutputStream) {


        Parameters parameters = new Parameters();
        try {
            // send EPD message
            sendEpdMessage(session);

            String utter = String.valueOf(resultMap.get("utter"));
            SttWriteFile sttWriteFile = new SttWriteFile();
            if (getParameterMap(session) != null) {
                parameters = getParameterMap(session);
            } else {
                throw new IllegalStateException();
            }
            removeSttClient(session);

            SttFileResponseVO sttFileResponseVO = sttWriteFile.write(byteArrayOutputStream, utter, sttRecDir, domain, parameters);

            // SttFileResponseVO sttFileResponseVO = writeSttFile(session,byteArrayOutputStream,utter,sttRecDir,domain);

            log.debug("parameters.getUserId() :" + parameters.getUserId());
            log.debug("parameters.getService() :" + parameters.getService());
            log.debug("resultMap.get(\"utter\") :" + utter);


            ServiceMessageVO serviceMessageVO = new ServiceMessageVO();
            serviceMessageVO.setUserId(parameters.getUserId());
            serviceMessageVO.setServiceType(parameters.getService());
            serviceMessageVO.setInputStr(utter);

            Response response;
            Result result = new Result();
            String resultId = "";
            String errText = null;

            if (ObjectUtils.isEmpty(serviceMessageVO.getUserId())) {
                response = new Response(Integer.toString(IRestCodes.ERR_CODE_PARAMS_INVALID), IRestCodes.ERR_MSG_PARAMS_INVALID, IRestCodes.RESULT_TYPE_DIALOG_STT, resultId, result);

            } else {

                //현재 사용자가 접속한 서비스를 찾는다.
                IntentFinderVO intentFinderVO = new IntentFinderVO();
                intentFinderVO.setUserId(serviceMessageVO.getUserId());
                intentFinderVO.setInputStr(serviceMessageVO.getInputStr());
                intentFinderVO.setServiceType(serviceMessageVO.getServiceType());
                intentFinderVO.setFirstServiceEntry(false);


                intentManager.init(intentFinderVO);
                intentManager.getCurrentIntent();

                log.info("intentFinderVO.toString()  =============================================>" + intentFinderVO.toString());
                log.info("currentServiceType =============================================>" + intentFinderVO.getServiceType() + "<=============================================");


                IntentExecuteVO intentExecuteVO = new IntentExecuteVO();
                intentExecuteVO.setUserId(serviceMessageVO.getUserId());
                intentExecuteVO.setInputStr(serviceMessageVO.getInputStr());
                intentExecuteVO.setServiceType(intentFinderVO.getServiceType());
                intentExecuteVO.setFirstServiceEntry(intentFinderVO.isFirstServiceEntry());


                String existPatternStr = ".*(디봇).*(그만).*$";
                Pattern pattern = Pattern.compile(existPatternStr);
                Matcher matcher = pattern.matcher(utter);
                boolean isDomainExit = matcher.matches();

                String existPatternStr2 = "그만";
                Pattern pattern2 = Pattern.compile(existPatternStr2);
                Matcher matcher2 = pattern2.matcher(utter);
                boolean isDomainExit2 = matcher2.matches();

                // 도메인 종료 조건
                if (isDomainExit || isDomainExit2) {
                    intentExecuteVO.setExitType("E");
                    intentManager.forceQuit(intentExecuteVO); // ROLLBACK 처리해주는 함수

                    response = new Response(Integer.toString(IRestCodes.ERR_CODE_SUCCESS), IRestCodes.ERR_MSG_SUCCESS, IRestCodes.RESULT_TYPE_DIALOG_STT, resultId, result);

                    String resultMsg = intentMsgService.getServiceMsg(IntentServiceType.BASICTALK, IntentServiceMsg.BQA_COMMON_EXIT);
                    ITtsClient ttsClient = new TtsSelvasClient(
                            this.ttsDevDir,
                            parameters.getLanguage(),
                            resultMsg,
                            intentExecuteVO.getServiceType(),
                            ttsRecDir,
                            sttFileResponseVO.getFileWavName(),
                            environmentBase.getActiveProfile(),
                            domain
                    );

                    ttsClient = setTtsServerInfo(ttsClient, parameters.getLanguage());

                    Response ttsResponse = ttsClient.getTtsResponse();
                    Result ttsResult = ttsResponse.getResult();

                    this.closeChannel(ttsClient);


                    result.setRecordUrl(sttFileResponseVO.getRecordUrl());
                    result.setTtsUrl(ttsResult.getTtsUrl());
                    result.setTtsText(resultMsg);
                    result.setUserText(utter);


                    ObjectMapper mapper = new ObjectMapper();
                    String jsonString = mapper.writeValueAsString(response);
                    log.info("jsonString: {} ", jsonString);

                    session.sendMessage(new TextMessage(jsonString));

                    // 도메인이 종료 조건이 아니면 기존 서비스 계속 실행
                } else {

                    DialogSttPostService dialogSttPostService = new DialogSttPostService();
                    String message = dialogSttPostService.getSttPostMessage(
                            intentExecuteVO.isFirstServiceEntry(),
                            intentExecuteVO.getServiceType(),
                            intentExecuteVO.getInputStr(),
                            environmentBase.getActiveProfile()
                    );

                    log.info("sttPost before =============================================>" + utter);
                    log.info("sttPost after =============================================>" + message);
                    intentExecuteVO.setInputStr(message);
                    utter = message;
                    sttFileResponseVO.setUtter(message);

                    IntentExecuteMessageVO intentExecuteMessageVO = intentManager.getIntent(intentExecuteVO);
                    if (intentExecuteMessageVO.getCode() != IRestCodes.ERR_CODE_SUCCESS) {
                        response = new Response(Integer.toString(intentExecuteMessageVO.getCode()), intentExecuteMessageVO.getMsg(), IRestCodes.RESULT_TYPE_DIALOG_STT, resultId, result);
                        errText = intentExecuteMessageVO.getErrText();

                    } else {

                        String postMessage = intentExecuteMessageVO.getResultMsg();
                        postMessage = postMessage.replace("\r\n", ". ");
                        postMessage = postMessage.replace("$$NL$$$$NL$$", " ");

                        ITtsClient ttsClient = new TtsSelvasClient(
                                this.ttsDevDir,
                                parameters.getLanguage(),
                                postMessage,
                                intentExecuteVO.getServiceType(),
                                ttsRecDir,
                                sttFileResponseVO.getFileWavName(),
                                environmentBase.getActiveProfile(),
                                domain
                        );

                        ttsClient = setTtsServerInfo(ttsClient, parameters.getLanguage());

                        Response ttsResponse = ttsClient.getTtsResponse();
                        Result ttsResult = ttsResponse.getResult();

                        this.closeChannel(ttsClient);

                        if (ttsResult == null || ttsResponse.getResCode().equals(String.valueOf(IRestCodes.ERR_CODE_TTS_EVALUATION_ERROR))) {
                            ttsResult.setTtsText(intentExecuteMessageVO.getResultMsg());
                            insertDialog(sttFileResponseVO, ttsResult);
                            response = new Response(Integer.toString(IRestCodes.ERR_CODE_TTS_EVALUATION_ERROR), IRestCodes.ERR_MSG_TTS_EVALUATION_ERROR, IRestCodes.RESULT_TYPE_DIALOG_STT, resultId, result);
                            result.setUserText(utter);
                            result.setRecordUrl(sttFileResponseVO.getRecordUrl());
                            result.setTtsText(postMessage);
                            result.setExtInfo(intentExecuteMessageVO.getExtInfo());
                            //intentManager.forceQuit(intentExecuteVO); // ROLLBACK 처리해주는 함수

                        } else {
                            response = new Response(Integer.toString(IRestCodes.ERR_CODE_SUCCESS), IRestCodes.ERR_MSG_SUCCESS, IRestCodes.RESULT_TYPE_DIALOG_STT, resultId, result);

                            // DB 저장
                            insertDialog(sttFileResponseVO, ttsResult);

                            result.setUserText(utter);
                            result.setRecordUrl(sttFileResponseVO.getRecordUrl());
                            result.setTtsUrl(ttsResult.getTtsUrl());
                            result.setTtsText(intentExecuteMessageVO.getResultMsg().replace("$$NL$$$$NL$$", "\r\n"));

                            result.setExtInfo(intentExecuteMessageVO.getExtInfo());

                            log.debug("ttsResult.getTtsUrl():" + ttsResult.getTtsUrl());


                            // 사전이면 service_start_time을 tts 재생시간만큼 늘려준다
							
							/*if (intentExecuteVO.getServiceType().equals(IntentServiceType.KORTOENGDIC) || intentExecuteVO.getServiceType().equals(IntentServiceType.KORDIC) ||
								intentExecuteVO.getServiceType().equals(IntentServiceType.MATHDIC) || intentExecuteVO.getServiceType().equals(IntentServiceType.ENCYCLOPDIC) ||
								intentExecuteVO.getServiceType().equals(IntentServiceType.KORDICSYNONYM) || intentExecuteVO.getServiceType().equals(IntentServiceType.KORDICANTONYM)   ) {
								
								
								String absTtsFileName=  ttsResult.getTtsUrl();
								
								absTtsFileName=  absTtsFileName.substring( absTtsFileName.indexOf("/record") ).replaceAll(".mp3", ".wav"); 
								log.debug("absTtsFileName:"+absTtsFileName);
								
								// local test file path
								//absTtsFileName="/record/tts/dev/custuser1_20181017130503_172.wav";
								File file =  new File(absTtsFileName);
								
								if (file.exists()) {
									AudioInputStream audioInputStream;
									audioInputStream = AudioSystem.getAudioInputStream(file);
									AudioFormat format = audioInputStream.getFormat();
									long frames = audioInputStream.getFrameLength();
									double durationInSeconds = (frames+0.0) / format.getFrameRate();  
									
									log.debug("frames:"+frames);
									log.debug("format.getFrameRate:"+format.getFrameRate());
									log.debug("durationInSeconds:"+durationInSeconds);
									log.debug("math round:"+Math.round(durationInSeconds));
									
									
									intentFinderService.updateDicsTimeOut(parameters.getUserId(), Long.toString(Math.round(durationInSeconds)));
								}
							}*/


//						result.setExtType(""); // 메뉴이동 타입
//						result.setExtData("");	// url및 기타 값

                        }

                    }
                    log.info("intentExecuteMessageVO.toString()================>" + intentExecuteMessageVO.toString());

                    ResponseLogResolver.insertResponseLog(response, parameters.getUserId(), errText);

                    ObjectMapper mapper = new ObjectMapper();
                    String jsonString = mapper.writeValueAsString(response);
                    log.info("jsonString: {} ", jsonString);

                    session.sendMessage(new TextMessage(jsonString));
                    // 도메인 종료 조건 END
                }
            }

        } catch (IllegalStateException illegalStateException) {

            illegalStateException.printStackTrace();

            try {
                Response response = new Response(
                        Integer.toString(IRestCodes.ERR_CODE_WEB_SOCKET_ILLEGAL_STATE_ERROR)
                        , IRestCodes.ERR_MSG_WEB_SOCKET_ILLEGAL_STATE_ERROR
                        , IRestCodes.RESULT_TYPE_DIALOG_STT
                        , null
                        , null
                );
                ResponseLogResolver.insertResponseLog(response, parameters.getUserId(), null);

                ObjectMapper mapper = new ObjectMapper();
                String jsonString = mapper.writeValueAsString(response);
                log.info("jsonString: {} ", jsonString);

            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e1) {

            e1.printStackTrace();

            try {

                Response response = new Response(
                        Integer.toString(IRestCodes.ERR_CODE_FAILURE)
                        , IRestCodes.ERR_MSG_FAILURE
                        , IRestCodes.RESULT_TYPE_DIALOG_STT
                        , null
                        , null
                );
                ResponseLogResolver.insertResponseLog(response, parameters.getUserId(), ExceptionUtils.getStackTrace(e1));

                ObjectMapper mapper = new ObjectMapper();
                String jsonString = mapper.writeValueAsString(response);
                log.info("jsonString: {} ", jsonString);

                session.sendMessage(new TextMessage(jsonString));

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private ITtsClient setTtsServerInfo(ITtsClient paramTtsClient, String paramLanguage) {

        ITtsClient result = paramTtsClient;
        ServerVO serverVO = serverManageFactory.getSelvasServerInfo();

        if (serverVO != null) {

            log.info("setSelvasServerInfo serverVO : " + serverVO);
            result.setServerInfo(serverVO.getServerIp(), serverVO.getServerPort());

        } else {

            log.info("setSelvasServerInfo serverVO null");
            result = new TtsSelvasClient();

        }

        return result;

    }

    private void closeChannel(ITtsClient paramTtsClient) {

        try {
            paramTtsClient.shutdown();
        } catch (Exception e) {
            e.printStackTrace();
            log.error("closeChannel :{}" + e.getMessage());
        }

    }


    private void insertDialog(SttFileResponseVO sttFileResponseVO, Result ttsResult) {
        log.info("sttFileResponseVO.toString():::::" + sttFileResponseVO.toString());
        log.info("ttsResult.toString():::::" + ttsResult.toString());

        DialogVO dialogVO = new DialogVO();
        dialogVO.setUserId(sttFileResponseVO.getUserId());
        dialogVO.setFilePath(sttFileResponseVO.getFilePath());
        dialogVO.setSttFileTextName(sttFileResponseVO.getFileTextName());
        dialogVO.setSttFileWaveName(sttFileResponseVO.getFileWavName());
        dialogVO.setCreatorId(sttFileResponseVO.getUserId());


        dialogVO.setTtsFileMp3Name(ttsResult.getTtsUrl().substring(ttsResult.getTtsUrl().lastIndexOf("/") + 1).trim());

        dialogVO.setUserText(sttFileResponseVO.getUtter());

        dialogVO.setAnswerText(ttsResult.getTtsText());

        // dialogVO.setServiceType(serviceType);

        userService.insertUserDialog(dialogVO);
    }

    @Override
    public void openSession(WebSocketSession session, Map<String, Object> params) {
        log.debug("open SimpleStt Session: {}", session.getId());
        //find model
        String model = SttModelResolver.getModel(session, params);

        log.debug("session !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! " + session);
        log.debug("session !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! " + params);
        String lang = (String) params.get("language");

        log.debug("open DialogStt model: {} lang:{} sampleRate: {}", model, lang, Integer.toString(sampleRate));
        log.debug("open DialogStt socket timeout: {} ", Integer.toString(timeout));

        CommonSttClient sttClient = new CommonSttClient(sttIp, sttPort, lang, model, Integer.toString(sampleRate),
                this, timeout, (String) params.get("answerText"));
        sttClient.init(session);

        putClient(session, sttClient);
        putParameterMap(session, params);


    }

    public void removeSttClient(WebSocketSession session) {
        CommonSttClient client = getClient(session);
        if (client != null) {
//				getClient(session).shutdown();
            removeClient(session);
            log.info("close DialogStt Session: {}", session.getId());
        }
        removeParameterMap(session);
    }

    public void closeSttClient(WebSocketSession session) {
        try {
            CommonSttClient client = getClient(session);
            if (client != null) {
                getClient(session).shutdown();
                removeClient(session);
                log.info("close EvaluationStt Session: {}", session.getId());
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        removeParameterMap(session);
    }

    @Override
    public void onClose(WebSocketSession session) {
        removeSttClient(session);
    }

    @Override
    public void onMessage(WebSocketSession session, String base64Audio, boolean last) {
        CommonSttClient client = getClient(session);
        if (client != null) {
            Decoder decoder = Base64.getDecoder();
            byte[] decodedByte = decoder.decode(base64Audio.split(",")[1]);
            client.sendData(decodedByte);
        }
    }

    @Override
    public void onMessage(WebSocketSession session, ByteBuffer buffer, boolean last) {
        CommonSttClient client = getClient(session);
        if (client != null) {
            byte[] decodedByte = buffer.array();
            client.sendData(decodedByte);
        }
    }

}
