package ai.mindslab.engedu.frontapi.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Map;

import ai.mindslab.engedu.frontapi.common.PostProceessing;
import ai.mindslab.engedu.frontapi.common.SttWriteFile;
import ai.mindslab.engedu.frontapi.service.data.Parameters;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import com.fasterxml.jackson.databind.ObjectMapper;

import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.common.data.DialogVO;
import ai.mindslab.engedu.common.service.UserService;
import ai.mindslab.engedu.freetalk.dao.data.EngFreeTalkQuestionVO;
import ai.mindslab.engedu.frontapi.client.CommonSttClient;
import ai.mindslab.engedu.frontapi.common.ResponseLogResolver;
import ai.mindslab.engedu.frontapi.common.SttModelResolver;
import ai.mindslab.engedu.frontapi.controller.data.Response;
import ai.mindslab.engedu.frontapi.controller.data.Result;
import ai.mindslab.engedu.frontapi.controller.data.ServiceMessageVO;
import ai.mindslab.engedu.frontapi.controller.data.SttFileResponseVO;
import ai.mindslab.engedu.intent.IntentManager;
import ai.mindslab.engedu.intent.IntentServiceType;
import ai.mindslab.engedu.intent.service.IntentFinderService;
import ai.mindslab.engedu.intent.vo.IntentExecuteMessageVO;
import ai.mindslab.engedu.intent.vo.IntentExecuteVO;
import ai.mindslab.engedu.intent.vo.IntentFinderVO;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class EngFreeTalkSttService extends SttService {
	
	
	@Autowired
	private IntentManager intentManager;
	
	@Value("${brain.dialog.stt.rec.dir}")
	private String sttRecDir;

	@Value("${brain.dialog.tts.rec.dir}")
	private String ttsRecDir;

	@Value("${brain.stt.sampleRate}")
	private int sampleRate;
	
	@Value("${brain.stt.ip}")
	private String sttIp;
	
	@Value("${brain.stt.port}")
	private int sttPort;

	@Autowired
	private UserService userService;
	
	@Autowired
	private IntentFinderService intentFinderService;

	@Value("${client.record.domain}")
	private String domain;
	
	@Value("${engedu.socket.timeout}")
	private int timeout;

	@Override
	public void onSttResult(WebSocketSession session, Map resultMap, ByteArrayOutputStream byteArrayOutputStream) {

		Parameters parameters = new Parameters();
		try {
			// send EPD message
			sendEpdMessage(session);

			String utter = String.valueOf(resultMap.get("utter"));
			PostProceessing postProceessing = new PostProceessing();
			utter = postProceessing.getSttMessgage(utter);
 			SttWriteFile sttWriteFile = new SttWriteFile();
			if(getParameterMap(session) != null) {
				parameters = getParameterMap(session);
			} else {
				throw new IllegalStateException();
			}
			
			removeSttClient(session);
			
			SttFileResponseVO sttFileResponseVO = sttWriteFile.write(byteArrayOutputStream,utter,sttRecDir,domain,parameters);

			log.debug("parameters.getUserId() :"+parameters.getUserId());
			log.debug("parameters.getService() :"+parameters.getService());
			log.debug("resultMap.get(\"utter\") :"+utter);

			
			
			ServiceMessageVO serviceMessageVO = new ServiceMessageVO();
			serviceMessageVO.setUserId(parameters.getUserId());
			serviceMessageVO.setServiceType(IntentServiceType.ENGFREETALK);
			serviceMessageVO.setInputStr(utter);
			
			Response response;
			Result result = new Result();
			String resultId ="";
			String errText = null;
			
			if ( ObjectUtils.isEmpty( serviceMessageVO.getUserId() ) ) {
				response = new Response(Integer.toString(IRestCodes.ERR_CODE_PARAMS_INVALID), IRestCodes.ERR_MSG_PARAMS_INVALID, IRestCodes.RESULT_TYPE_DIALOG_STT, resultId, result);

			} else {
				
				//현재 사용자가 접속한 서비스를 찾는다.
				IntentFinderVO intentFinderVO = new IntentFinderVO();
				intentFinderVO.setUserId(serviceMessageVO.getUserId());
				intentFinderVO.setInputStr(serviceMessageVO.getInputStr());
				intentFinderVO.setServiceType(serviceMessageVO.getServiceType());
				intentFinderVO.setFirstServiceEntry(false);


				intentManager.init(intentFinderVO);
				intentManager.getCurrentIntent();

				log.info( "intentFinderVO.toString()  =============================================>"+intentFinderVO.toString() );
				log.info( "currentServiceType =============================================>"+intentFinderVO.getServiceType()+"<============================================="  );


				IntentExecuteVO intentExecuteVO = new IntentExecuteVO();
				intentExecuteVO.setUserId(serviceMessageVO.getUserId());
				intentExecuteVO.setInputStr(serviceMessageVO.getInputStr());
				intentExecuteVO.setServiceType(intentFinderVO.getServiceType());
				intentExecuteVO.setFirstServiceEntry(intentFinderVO.isFirstServiceEntry());
				
				EngFreeTalkQuestionVO engFreeTalkQuestionVO = new EngFreeTalkQuestionVO();
				engFreeTalkQuestionVO.setBrandId(parameters.getBiz());
				engFreeTalkQuestionVO.setBookId(parameters.getLectureId());
				engFreeTalkQuestionVO.setChapterId(parameters.getChapterId());
				engFreeTalkQuestionVO.setQuestionId(parameters.getContentId());
				
				intentExecuteVO.setEngFreeTalkQuestionVO(engFreeTalkQuestionVO);

				IntentExecuteMessageVO intentExecuteMessageVO   =  intentManager.getIntent(intentExecuteVO);

				if (intentExecuteMessageVO.getCode() != IRestCodes.ERR_CODE_SUCCESS) {
					result.setUserText(utter);
					response =  new Response(Integer.toString(intentExecuteMessageVO.getCode()), intentExecuteMessageVO.getMsg(), IRestCodes.RESULT_TYPE_DIALOG_STT, resultId, result);
					errText = intentExecuteMessageVO.getErrText();

					if("QUESTION NOT FOUND".equals(intentExecuteMessageVO.getMsg())){
						result.setExtInfo(intentExecuteMessageVO.getExtInfo());
					}

				} else {
					response =  new Response(Integer.toString(IRestCodes.ERR_CODE_SUCCESS), IRestCodes.ERR_MSG_SUCCESS, IRestCodes.RESULT_TYPE_DIALOG_STT,resultId,result);

					sttFileResponseVO.setAnswer(intentExecuteMessageVO.getResultMsg());
					  // DB 저장
		            insertDialog(sttFileResponseVO);

					result.setUserText(utter);
					result.setRecordUrl(sttFileResponseVO.getRecordUrl());
					result.setTtsUrl("");
					result.setTtsText("");
					result.setFreeTalkResult(intentExecuteMessageVO.getResultMsg());

					result.setExtInfo(intentExecuteMessageVO.getExtInfo());
				}

				log.info("intentExecuteMessageVO.toString()================>"+intentExecuteMessageVO.toString());
			}
			
			ResponseLogResolver.insertResponseLog(response, parameters.getUserId(), errText);

			ObjectMapper mapper = new ObjectMapper();
			String jsonString = mapper.writeValueAsString(response);
			log.info("jsonString: {} ",jsonString);
			
			session.sendMessage(new TextMessage(jsonString));

		} catch (IllegalStateException illegalStateException) {

			illegalStateException.printStackTrace();

			try {
				Response response =  new Response(
						Integer.toString(IRestCodes.ERR_CODE_WEB_SOCKET_ILLEGAL_STATE_ERROR)
						, IRestCodes.ERR_MSG_WEB_SOCKET_ILLEGAL_STATE_ERROR
						, IRestCodes.RESULT_TYPE_DIALOG_STT
						, null
						, null
				);
				ResponseLogResolver.insertResponseLog(response, parameters.getUserId(), null);

				ObjectMapper mapper = new ObjectMapper();
				String jsonString = mapper.writeValueAsString(response);
				log.info("jsonString: {} ",jsonString);

			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e1) {

			e1.printStackTrace();

			try {

				Response response =  new Response(
						Integer.toString(IRestCodes.ERR_CODE_FAILURE)
						, IRestCodes.ERR_MSG_FAILURE
						, IRestCodes.RESULT_TYPE_DIALOG_STT
						, null
						, null
				);
				ResponseLogResolver.insertResponseLog(response, parameters.getUserId(), ExceptionUtils.getStackTrace(e1));

				ObjectMapper mapper = new ObjectMapper();
				String jsonString = mapper.writeValueAsString(response);
				log.info("jsonString: {} ",jsonString);

				session.sendMessage(new TextMessage(jsonString));

			} catch (Exception e) {
				e.printStackTrace();
			}
		} finally {
			intentFinderService.purgeServiceType(parameters.getUserId());
		}
	}
	
	private void insertDialog(SttFileResponseVO sttFileResponseVO) {
		log.info("sttFileResponseVO.toString():::::"+sttFileResponseVO.toString());
		
        DialogVO dialogVO = new DialogVO();
        dialogVO.setUserId(sttFileResponseVO.getUserId());
        dialogVO.setFilePath(sttFileResponseVO.getFilePath());
        dialogVO.setSttFileTextName(sttFileResponseVO.getFileTextName());
        dialogVO.setSttFileWaveName(sttFileResponseVO.getFileWavName());
        dialogVO.setCreatorId(sttFileResponseVO.getUserId());
        
        dialogVO.setTtsFileMp3Name("");

		dialogVO.setUserText(sttFileResponseVO.getUtter());

		dialogVO.setAnswerText(sttFileResponseVO.getAnswer());

		userService.insertUserDialog(dialogVO);
	}


	@Override
	public void openSession(WebSocketSession session, Map<String, Object> params) {
		log.debug("open EngFreeTalkStt Session: {}" , session.getId());

		String userId = (String) params.get("userId");
		intentFinderService.updateServiceType(IntentServiceType.ENGFREETALK,userId);


		//find model
		String model = SttModelResolver.getModel(session, params);

		String bpCode = (String) params.get("biz");

		if("mAIEnglish".equals(bpCode)){

			model = "mai_dialog";

		}
		
		
		String lang  = (String)params.get("language");
		
		log.debug("open EngFreeTalkStt model: {} lang:{} sampleRate: {}" , model,lang,Integer.toString(sampleRate));
		log.debug("open EngFreeTalkStt socket timeout: {} " , Integer.toString(timeout));
		
		CommonSttClient sttClient = new CommonSttClient(sttIp, sttPort, lang, model, Integer.toString(sampleRate), this,timeout, (String) params.get("answerText"));
		sttClient.init(session);
		putClient(session, sttClient);
		putParameterMap(session, params);
		
	}
	
	public void removeSttClient(WebSocketSession session) {
		CommonSttClient client = getClient(session);
		if(client != null) {
//			getClient(session).shutdown();
			removeClient(session);
			log.info("close EngFreeTalkStt Session: {}" , session.getId());
		}

		removeParameterMap(session);
	}

	public void closeSttClient(WebSocketSession session) {
		try {
			CommonSttClient client = getClient(session);
			if(client != null) {
				getClient(session).shutdown();
				removeClient(session);
				log.info("close EvaluationStt Session: {}" , session.getId());
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		removeParameterMap(session);
	}

	
	@Override
	public void onClose(WebSocketSession session) {
		removeSttClient(session);
		/*try {
			getClient(session).shutdown();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		removeClient(session);
		removeParameterMap(session);
		log.info("close SimpleStt Session: {}" , session.getId());*/
	}

	@Override
	public void onMessage(WebSocketSession session, String base64Audio, boolean last) {
		CommonSttClient client = getClient(session);
		if(client != null) {
			Decoder decoder = Base64.getDecoder();
			byte[] decodedByte = decoder.decode(base64Audio.split(",")[1]);
			client.sendData(decodedByte);
		}
	}

	@Override
	public void onMessage(WebSocketSession session, ByteBuffer buffer, boolean last) {
		CommonSttClient client = getClient(session);
		if(client != null) {
			byte[] decodedByte = buffer.array();
			client.sendData(decodedByte);
		}
	}

}
