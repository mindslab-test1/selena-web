package ai.mindslab.engedu.frontapi.service;

import ai.mindslab.engedu.common.base.EnvironmentBase;
import ai.mindslab.engedu.common.codes.IChoiceCodes;
import ai.mindslab.engedu.common.codes.ILanguageCodes;
import ai.mindslab.engedu.common.codes.IMarkCodes;
import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.common.data.EvaluationVO;
import ai.mindslab.engedu.common.service.GrammarEvaluationSttPostService;
import ai.mindslab.engedu.common.service.UserService;
import ai.mindslab.engedu.common.sttPost.evaluation.GrammarEvaluationAbbreviationSttPostUtil;
import ai.mindslab.engedu.evaluation.dao.data.GrammarEvaluationAbbreviationVO;
import ai.mindslab.engedu.evaluation.dao.data.GrammarEvaluationDetailVO;
import ai.mindslab.engedu.frontapi.client.*;
import ai.mindslab.engedu.frontapi.common.*;
import ai.mindslab.engedu.frontapi.common.evaluation.GrammarEvaluationFactory;
import ai.mindslab.engedu.frontapi.common.server.ServerManageFactory;
import ai.mindslab.engedu.frontapi.common.server.data.ServerVO;
import ai.mindslab.engedu.frontapi.controller.data.Response;
import ai.mindslab.engedu.frontapi.controller.data.Result;
import ai.mindslab.engedu.frontapi.controller.data.SttFileResponseVO;
import ai.mindslab.engedu.frontapi.service.data.Parameters;
import ai.mindslab.engedu.intent.IntentServiceType;
import ai.mindslab.engedu.intent.service.IntentFinderService;
import ai.mindslab.engedu.intent.vo.ExtInfo;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class EvaluationSttService extends SttService {
    final boolean CNN_MODE = false;

    @Value("${brain.evaluation.eng.stt.rec.dir}")
    private String sttRecDir;

    @Value("${brain.stt.sampleRate}")
    private int sampleRate;

    @Value("${brain.cnn-stt.ip}")
    private String sttCnnIp;

    @Value("${brain.cnn-stt.port}")
    private int sttCnnPort;

    @Value("${brain.stt.ip}")
    private String sttIp;

    @Value("${brain.stt.port}")
    private int sttPort;

    @Value("${engedu.grammar.evaluation.ip}")
    private String grammarEvaluationIp;

    @Value("${engedu.grammar.evaluation.port}")
    private int grammarEvaluationPort;

    @Value("${engedu.pronounce.evaluation.ip}")
    private String pronounceEvaluationIp;

    @Value("${engedu.pronounce.evaluation.port}")
    private int pronounceEvaluationPort;

    @Value("${client.record.domain}")
    private String domain;

    @Value("${client.brand.code}")
    private String brandId;

    @Value("${engedu.grammar.priority}")
    private String grammarPriority;

    @Value("${engedu.grammar.rate}")
    private double grammarRate;

    @Value("${engedu.pronounce.rate}")
    private double pronounceRate;

    @Value("${engedu.grammar.evaluation.type}")
    private String grammarEvaluationType;

    @Value("${engedu.pronounce.evaluation.standard}")
    private String pronounceStandard;

    @Value("${engedu.socket.timeout}")
    private int timeout;

    @Autowired
    private UserService userService;

    @Autowired
    private IntentFinderService intentFinderService;

    @Autowired
    private EnvironmentBase environmentBase;

    @Autowired
    private ServerManageFactory serverManageFactory;

    @Autowired
    private GrammarEvaluationSttPostService grammarEvaluationSttPostService;

    @Override
    public void onSttResult(WebSocketSession session, Map resultMap, ByteArrayOutputStream byteArrayOutputStream) {

        Parameters parameters = new Parameters();

        try {
            // send EPD message
            sendEpdMessage(session);

            String utter = String.valueOf(resultMap.get("utter"));
            // Samsung post processing utter value
            PostProceessing postProceessing = new PostProceessing();
            utter = postProceessing.getSttMessgage(utter).replaceAll("  ", " ");
            SttWriteFile sttWriteFile = new SttWriteFile();
            if(getParameterMap(session) != null) {
                parameters = getParameterMap(session);
            } else {
                throw new IllegalStateException();
            }


            removeSttClient(session);

            SttFileResponseVO sttFileResponseVO = sttWriteFile.write(
                    byteArrayOutputStream,
                    utter,
                    sttRecDir,
                    domain,
                    parameters
            );

            IGrammarEvaluationClient iGrammarEvaluationClient = new GrammarEvaluationFactory().getClient(grammarEvaluationType, ILanguageCodes.ENG);
//            String conversionUtter = getAbbreviationUtter(utter,sttFileResponseVO.getAnswer().replaceAll("’","'"));
            ArrayList<String> conversionUtter = getAbbreviationUtter(utter, sttFileResponseVO.getAnswer().replaceAll("’","'"));
            Response grammarResponse = iGrammarEvaluationClient.getGrammarEvaluationInfo(
                    conversionUtter.get(0),
//                    sttFileResponseVO.getAnswer().replaceAll("’","'")
                    conversionUtter.get(1)
            );

            Result grammarResult = grammarResponse.getResult();

            // 발음 평가 결과
            String activeProfile =  environmentBase.getActiveProfile();

            ServerVO serverVO = serverManageFactory.getPronounceServerInfo();

            PronounceEvaluationClient pronounceEvaluationClient;

            if(serverVO != null){

                pronounceEvaluationClient = new PronounceEvaluationClient(
                        serverVO.getServerIp(),
                        serverVO.getServerPort(),
                        activeProfile
                );

            }else{
                pronounceEvaluationClient = new PronounceEvaluationClient();
            }
            Response pronounceResponse = new Response();

            // BP 코드로 구분 시 해당 부분 주석 해제.
            //boolean isGrammarPriority = getIsGrammarBrandPriority();

            // boolean isGrammarPriority = getIsGrammarPriority();

            // 문법 평가 점수 0점 이상 || grammarPriority Y가 아닐 경우(현재 딥스터디 기준)


            // 사용자 발화 문장 -> 정답 문장으로 변경
            pronounceResponse = pronounceEvaluationClient.getPronounceEvaluationInfo(
                    utter,
                    sttFileResponseVO.getAnswer(),
                    sttFileResponseVO.getFileFolderFilePcmName(),
                    pronounceStandard
            );

            Result pronounceResult = pronounceResponse.getResult();

            int averageScore = getAverageScore(grammarResult,pronounceResult);

            setGrammarScoreToPronounceError(pronounceResponse,grammarResult);
            // DB 저장
            insertEvaluation(sttFileResponseVO,grammarResult,pronounceResult,averageScore);

            this.closeChannel(iGrammarEvaluationClient,pronounceEvaluationClient);

            Response response = getJsonStringResult(
                    utter,
                    conversionUtter.get(0),
                    sttFileResponseVO,
                    grammarResponse,
                    pronounceResponse
            );
            ResponseLogResolver.insertResponseLog(response, parameters.getUserId(), null);

            ObjectMapper mapper = new ObjectMapper();
            String jsonString = mapper.writeValueAsString(response);
            log.info("jsonString: {} ",jsonString);

            session.sendMessage(new TextMessage(jsonString));

        } catch (IllegalStateException illegalStateException) {

            illegalStateException.printStackTrace();

            try {
                Response response = getClientErrorReturnMessage(
                        "",
                        "",
                        "",
                        "",
                        new Result(),
                        new Result(),
                        IRestCodes.ERR_CODE_WEB_SOCKET_ILLEGAL_STATE_ERROR,
                        IRestCodes.ERR_MSG_WEB_SOCKET_ILLEGAL_STATE_ERROR
                );
                ResponseLogResolver.insertResponseLog(response, parameters.getUserId(), null);

                ObjectMapper mapper = new ObjectMapper();
                String jsonString = mapper.writeValueAsString(response);
                log.info("jsonString: {} ",jsonString);

            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e1) {

            e1.printStackTrace();

            try {

                Response response = getClientErrorReturnMessage(
                        "",
                        "",
                        "",
                        "",
                        new Result(),
                        new Result(),
                        IRestCodes.ERR_CODE_FAILURE,
                        IRestCodes.ERR_MSG_FAILURE
                );
                ResponseLogResolver.insertResponseLog(response, parameters.getUserId(), ExceptionUtils.getStackTrace(e1));

                ObjectMapper mapper = new ObjectMapper();
                String jsonString = mapper.writeValueAsString(response);
                log.info("jsonString: {} ",jsonString);

                session.sendMessage(new TextMessage(jsonString));

            } catch (Exception e) {
                e.printStackTrace();
            }
        } finally {
            intentFinderService.purgeServiceType(parameters.getUserId());
        }
    }

    /**
     * 발음 평가 error 시 문법 평가도 0점으로 처리.
     * @param paramPronounceResponse
     * @param paramGrammarResult
     */
    private void setGrammarScoreToPronounceError(
            Response paramPronounceResponse,
            Result paramGrammarResult)
    {

        if (paramPronounceResponse.getResCode().equals(String.valueOf(IRestCodes.ERR_CODE_PRONOUNCE_EVALUATION_ERROR))) {

            paramGrammarResult.setGrammarScore("0");
            paramGrammarResult.setGrammarScoreDetail(null);

        }

    }

    private ArrayList<String> getAbbreviationUtter(
            String paramInputStr,
            String paramAnswerStr)
    {

//        String result = paramInputStr;
        ArrayList<String> result = new ArrayList();
        result.add(paramInputStr);
        result.add(paramAnswerStr);

        List<GrammarEvaluationAbbreviationVO> abbrevieationList =  grammarEvaluationSttPostService.getSttAbbreviationPostMessage(brandId);
        GrammarEvaluationAbbreviationSttPostUtil grammarEvaluationAbbreviationSttPostUtil
                = new GrammarEvaluationAbbreviationSttPostUtil();

        switch (brandId){

            case "BP0002" :

//                result = new GrammarEvaluationAbbreviationSttPostUtil().getAbbreviationStr(result,paramAnswerStr,abbreviationList);
                result = grammarEvaluationAbbreviationSttPostUtil.getAbbreviationStr(result.get(0), result.get(1), abbrevieationList);

                break;

            case "mAIEnglish" :

//                result = new GrammarEvaluationAbbreviationSttPostUtil().getAbbreviationStr(result,paramAnswerStr,abbreviationList);
                result = grammarEvaluationAbbreviationSttPostUtil.getAbbreviationStr(result.get(0), result.get(1), abbrevieationList);

                break;

            default:
//                    result = paramInputStr;
                result.set(0,paramInputStr);
                result.set(1,paramAnswerStr);
        }

        return result;

    }

    private void closeChannel(
            IGrammarEvaluationClient grammarEvaluationClient,
            PronounceEvaluationClient pronounceEvaluationClient)
    {

        try{
            grammarEvaluationClient.shutdown();
            pronounceEvaluationClient.shutdown();
        }catch (Exception e){
            e.printStackTrace();
            log.error("EvaluationSttService closeChannel error :{} " + e.getMessage());
        }

    }

    /**
     * client에 보낼 최종 결과 값 set
     * @param paramUtter 사용자 문장
     * @param sttFileResponseVO stt 결과 정보
     * @param paramGrammarResponse 문법 평가 결과 정보
     * @param paramPronounceResponse 발음 평가 결과 정보
     * @return client 결과 값
     * @throws Exception
     */
    private Response getJsonStringResult(
            String paramUtter,
            String paramConvertUtter,
            SttFileResponseVO sttFileResponseVO,
            Response paramGrammarResponse,
            Response paramPronounceResponse) throws  Exception
    {

        if(paramGrammarResponse.getResCode().equals(String.valueOf(IRestCodes.ERR_CODE_GRAMMAR_EVALUATION_ERROR))){   // 문법 error

            log.error("grammar Error : {} ",paramGrammarResponse.getResMsg());
            return getClientErrorReturnMessage(
                    paramUtter,
                    paramConvertUtter,
                    sttFileResponseVO.getRecordUrl(),
                    sttFileResponseVO.getAnswer(),
                    paramGrammarResponse.getResult(),
                    paramPronounceResponse.getResult(),
                    IRestCodes.ERR_CODE_GRAMMAR_EVALUATION_ERROR,
                    IRestCodes.ERR_MSG_GRAMMAR_EVALUATION_ERROR
            );

        }else if(paramPronounceResponse.getResCode().equals(String.valueOf(IRestCodes.ERR_CODE_PRONOUNCE_EVALUATION_ERROR))){   // 발음 error

            log.error("pronounce Error : {} ",paramPronounceResponse.getResMsg());
            return getClientErrorReturnMessage(
                    paramUtter,
                    paramConvertUtter,
                    sttFileResponseVO.getRecordUrl(),
                    sttFileResponseVO.getAnswer(),
                    paramGrammarResponse.getResult(),
                    paramPronounceResponse.getResult(),
                    IRestCodes.ERR_CODE_PRONOUNCE_EVALUATION_ERROR,
                    IRestCodes.ERR_MSG_PRONOUNCE_EVALUATION_ERROR
            );

        }else{  // 정상일 경우

            return getClientSuccessReturnMessage(
                    paramUtter,
                    paramConvertUtter,
                    sttFileResponseVO.getRecordUrl(),
                    sttFileResponseVO.getAnswer(),
                    paramGrammarResponse.getResult(),
                    paramPronounceResponse.getResult()
            );

        }

    }

    /**
     * 평가 결과 { 파일 경로, 평가 점수} db 저장 -> dl_evaluation_user_tb table
     * @param paramSttFileResponseVO stt 파일 생성 결과 정보
     * @param paramGrammarResult 문법 평가 결과 정보
     * @param paramPronounceResult 발음 평가 결과 정보
     */
    private void insertEvaluation(
            SttFileResponseVO paramSttFileResponseVO,
            Result paramGrammarResult,
            Result paramPronounceResult,
            int paramAverageScore)
    {

        log.info("paramSttFileResponseVO = "+ paramSttFileResponseVO.toString());
        log.info("paramGrammarResult = "+ paramGrammarResult.toString());
        log.info("paramPronounceResult = "+ paramPronounceResult.toString());
        log.info("paramAverageScore = "+ paramAverageScore);

        EvaluationVO evaluationVO = new EvaluationVO();
        evaluationVO.setUserId(paramSttFileResponseVO.getUserId());
        evaluationVO.setFilePath(paramSttFileResponseVO.getFilePath());
        evaluationVO.setFileTextName(paramSttFileResponseVO.getFileTextName());
        evaluationVO.setFileWaveName(paramSttFileResponseVO.getFileWavName());
        evaluationVO.setFileMp3Name(paramSttFileResponseVO.getFileMp3Name());
        evaluationVO.setCreatorId(paramSttFileResponseVO.getUserId());
        evaluationVO.setGrammarScore(paramGrammarResult.getGrammarScore());
        evaluationVO.setPronounceScore(paramPronounceResult.getPronounceScore());
        evaluationVO.setAverageScore(String.valueOf(paramAverageScore));
        evaluationVO.setUserText(paramSttFileResponseVO.getUtter());
        evaluationVO.setAnswerText(paramSttFileResponseVO.getAnswer());

        String pronounceEtcScore = getPronounceEtcScore(paramPronounceResult);
        evaluationVO.setPronounceEtcScore(pronounceEtcScore);

        userService.insertUserEvaluation(evaluationVO);

    }

    private String getPronounceEtcScore(Result paramPronounceResult) {

        StringBuilder result = new StringBuilder();

        if(paramPronounceResult != null){

            result.append(paramPronounceResult.getEvalScore() + IMarkCodes.COMMA);
            result.append(paramPronounceResult.getPronounceHolisticScore() + IMarkCodes.COMMA);
            result.append(paramPronounceResult.getPronounceSpeedScore() + IMarkCodes.COMMA);
            result.append(paramPronounceResult.getPronounceRhythmScore() + IMarkCodes.COMMA);
            result.append(paramPronounceResult.getPronounceIntonationScore() + IMarkCodes.COMMA);
            result.append(paramPronounceResult.getPronounceSegmentalScore() + IMarkCodes.COMMA);
            result.append(paramPronounceResult.getPronounceSegmentalFeat19Score());

        }else {

            result.append("0,0,0,0,0,0,0");
        }

        return String.valueOf(result);

    }

    private Response getClientSuccessReturnMessage(
            String paramUtter,
            String paramConvertUtter,
            String paramFileWriteMp3Name,
            String paramAnswerText,
            Result paramGrammarResult,
            Result paramPronounceResult) throws Exception
    {

        // 문법 75%, 발음 25% 결과 값
        int averageScore = getAverageScore(paramGrammarResult,paramPronounceResult);

        Result result = getClientSuccessResult(
                paramUtter,
                paramConvertUtter,
                paramFileWriteMp3Name,
                paramAnswerText,
                paramGrammarResult,
                paramPronounceResult,
                averageScore
        );

        Response response = getClientSuccessResponse(result);

        return response;

    }

    private Response getClientErrorReturnMessage(
            String paramUtter,
            String paramConvertUtter,
            String paramFileWriteMp3Name,
            String paramAnswerText,
            Result paramGrammarResult,
            Result paramPronounceResult,
            int paramErrorCode,
            String paramErrorMsg) throws  Exception{

        Result result = new Result();
        Response response = new Response();
        int averageScore = getAverageScore(paramGrammarResult,paramPronounceResult);

        result.setUserText(paramUtter);
        result.setUserAbbreviationConvertText(paramConvertUtter);
        result.setRecordUrl(paramFileWriteMp3Name);
        result.setAnswerText(paramAnswerText);
        result.setGrammarScore(paramGrammarResult != null ? paramGrammarResult.getGrammarScore() : "0");
        result.setGrammarScoreDetail(paramGrammarResult!= null ? paramGrammarResult.getGrammarScoreDetail() : new ArrayList<GrammarEvaluationDetailVO>());
        result.setPronounceScore(paramPronounceResult != null ? paramPronounceResult.getPronounceScore() : "0");
        result.setEvalScore(paramPronounceResult != null ? paramPronounceResult.getEvalScore() : "0");
        result.setPronounceHolisticScore(paramPronounceResult != null ? paramPronounceResult.getPronounceHolisticScore() : "0");
        result.setPronounceSpeedScore(paramPronounceResult != null ? paramPronounceResult.getPronounceSpeedScore() : "0");
        result.setPronounceRhythmScore(paramPronounceResult != null ? paramPronounceResult.getPronounceRhythmScore() : "0");
        result.setPronounceIntonationScore(paramPronounceResult != null ? paramPronounceResult.getPronounceIntonationScore() : "0");
        result.setPronounceSegmentalScore(paramPronounceResult != null ? paramPronounceResult.getPronounceSegmentalScore() :"0");
        result.setPronounceSegmentalFeat19Score(paramPronounceResult != null ? paramPronounceResult.getPronounceSegmentalFeat19Score() :"0");
        result.setAverageScore(String.valueOf(averageScore));

        response.setResCode(Integer.toString(paramErrorCode));
        response.setResMsg(paramErrorMsg);
        response.setResultType(IRestCodes.RESULT_TYPE_EVAL_STT);
        result.setUserText(paramUtter);
        response.setResult(result);

        ExtInfo extInfo = new ExtInfo();
        extInfo.setCurrentDomain(IntentServiceType.ServiceEnum.ST0035.getDomainName());
        result.setExtInfo(extInfo);

        return response;

    }

    /**
     * 문법 평가 점수 / 발음 평가 점수 산정 ( 문법 75 %  / 발음 25 % )
     * @param paramGrammarResult 문법 평가 결과
     * @param paramPronounceResult 발음 평가가 결과
     * @return 평균 점수
     */
    private int getAverageScore(Result paramGrammarResult, Result paramPronounceResult) {

        int grammarScore = 0;
        int pronounceScore = 0;

        if(paramGrammarResult != null){
            grammarScore = Integer.parseInt(paramGrammarResult.getGrammarScore());
        }
        if(paramPronounceResult != null){
            pronounceScore = (int) Float.parseFloat(paramPronounceResult.getPronounceScore()) + 1;
        }

        int result = (int)( grammarScore * grammarRate ) + (int)( pronounceScore * pronounceRate);

        /*
        switch (brandId){
            case "BP0001":
                result = grammarScore > 0 ? result : 0;
                break;
        }
        */

        return result;
    }

    private Response getClientSuccessResponse(Result paramResult) {

        Response response = new Response();

        response.setResCode(String.valueOf(IRestCodes.ERR_CODE_SUCCESS));
        response.setResMsg(IRestCodes.ERR_MSG_SUCCESS);
        response.setResultType(IRestCodes.RESULT_TYPE_EVAL_STT);
        response.setResult(paramResult);

        return response;

    }

    private Result getClientSuccessResult(
            String paramUtter,
            String paramConvertUtter,
            String paramFileWriteMp3Name,
            String paramAnswerText,
            Result paramGrammarResult,
            Result paramPronounceResult,
            int averageScore)
    {

        Result result = new Result();
        result.setUserText(paramUtter);
        result.setUserAbbreviationConvertText(paramConvertUtter);
        result.setRecordUrl(paramFileWriteMp3Name);
        result.setAnswerText(paramAnswerText);
        result.setGrammarScore(paramGrammarResult.getGrammarScore());
        result.setGrammarScoreDetail(paramGrammarResult.getGrammarScoreDetail());
        result.setPronounceScore(paramPronounceResult.getPronounceScore());
        result.setEvalScore(paramPronounceResult.getEvalScore());
        result.setPronounceHolisticScore(paramPronounceResult.getPronounceHolisticScore());
        result.setPronounceSpeedScore(paramPronounceResult.getPronounceSpeedScore());
        result.setPronounceRhythmScore(paramPronounceResult.getPronounceRhythmScore());
        result.setPronounceIntonationScore(paramPronounceResult.getPronounceIntonationScore());
        result.setPronounceSegmentalScore(paramPronounceResult.getPronounceSegmentalScore());
        result.setPronounceSegmentalFeat19Score(paramPronounceResult.getPronounceSegmentalFeat19Score());

        result.setAverageScore(String.valueOf(averageScore));

        ExtInfo extInfo = new ExtInfo();
        extInfo.setCurrentDomain(IntentServiceType.ServiceEnum.ST0035.getDomainName());
        result.setExtInfo(extInfo);

        return result;

    }

    /**
     * GRAMMAR PRIORITY 값으로 구분
     * @return true || false
     */
    private boolean getIsGrammarPriority() {

        boolean result = false;

        switch (grammarPriority){

            case IChoiceCodes.RESULT_Y :
                result = true;
                break;
            case IChoiceCodes.RESULT_N :
                result = false;
                break;
        }
        return result;
    }

    /**
     * BP 코드로 구분 후에 변경시 이걸 쓰면 됩니다.
     * @return true || false
     */
    private boolean getIsGrammarBrandPriority(){

        boolean result = false;

        switch (brandId){

            case "BP0001" :
                result = true;
                break;
        }
        return result;
    }



    @Override
    public void openSession(WebSocketSession session, Map<String, Object> params) {

        log.debug("open EvaluationStt Session: {}", session.getId());
        String userId = (String) params.get("userId");
        intentFinderService.updateServiceType(IntentServiceType.ENGEVALUATION,userId);

        String model = SttModelResolver.getModel(session, params);

        String lang  = (String) params.get("language");

        String bpCode = (String) params.get("biz");
        if("BP9999".equals(bpCode)){

            model = "sigong";

        }
        log.debug("model : " + model);
        log.debug("open EvaluationStt model: {} lang:{} sampleRate: {}" , model,lang,Integer.toString(sampleRate));
        log.debug("open EvaluationStt socket timeout: {} " , Integer.toString(timeout));

        if(CNN_MODE == true) {
            CommonCnnSttClient sttClient = new CommonCnnSttClient(
                    sttCnnIp, sttCnnPort,
                    lang, model, Integer.toString(sampleRate),
                    this, timeout,
                    (String) params.get("answerText")
            );
            sttClient.init(session, userId);
            putCnnClient(session, sttClient);
        }
        else {
            CommonSttClient sttClient = new CommonSttClient(
                    sttIp, sttPort,
                    lang, model, Integer.toString(sampleRate),
                    this, timeout
                    , (String) params.get("answerText")
            );

            sttClient.init(session, userId);
            putClient(session, sttClient);
        }
        putParameterMap(session, params);
    }

    public void removeSttClient(WebSocketSession session) {

        if(CNN_MODE == true) {
            CommonCnnSttClient client = getCnnClient(session);
            if (client != null) {
//                getClient(session).shutdown();
                removeClient(session);
                log.info("close EvaluationStt Session: {}", session.getId());
            }
        }
        else {
            CommonSttClient client = getClient(session);
            if (client != null) {
//                getClient(session).shutdown();
                removeClient(session);
                log.info("close EvaluationStt Session: {}", session.getId());
            }
        }

        removeParameterMap(session);
    }

    public void closeSttClient(WebSocketSession session) {
        try {
            if(CNN_MODE == true) {
                CommonCnnSttClient client = getCnnClient(session);
                if (client != null) {
                    getClient(session).shutdown();
                    removeClient(session);
                    log.info("close EvaluationStt Session: {}", session.getId());
                }
            }
            else {
                CommonSttClient client = getClient(session);
                if (client != null) {
                    getClient(session).shutdown();
                    removeClient(session);
                    log.info("close EvaluationStt Session: {}", session.getId());
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        removeParameterMap(session);
    }


    @Override
    public void onClose(WebSocketSession session) {
        removeSttClient(session);
    }

    @Override
    public void onMessage(WebSocketSession session, String base64Audio, boolean last) {
        if(CNN_MODE == true) {
            CommonCnnSttClient client = getCnnClient(session);
            if (client != null) {
                Base64.Decoder decoder = Base64.getDecoder();
                byte[] decodedByte = decoder.decode(base64Audio.split(",")[1]);
                client.sendData(decodedByte);
            }
        }
        else {
            CommonSttClient client = getClient(session);
            if (client != null) {
                Base64.Decoder decoder = Base64.getDecoder();
                byte[] decodedByte = decoder.decode(base64Audio.split(",")[1]);
                client.sendData(decodedByte);
            }
        }
    }

    @Override
    public void onMessage(WebSocketSession session, ByteBuffer buffer, boolean last) {
        if(CNN_MODE == true) {
            CommonCnnSttClient client = getCnnClient(session);
            if (client != null) {
                byte[] decodedByte = buffer.array();
                client.sendData(decodedByte);
            }
        }
        else {
            CommonSttClient client = getClient(session);
            if (client != null) {
                byte[] decodedByte = buffer.array();
                client.sendData(decodedByte);
            }
        }
    }
}