package ai.mindslab.engedu.frontapi.service;

import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.common.data.ReadingVO;
import ai.mindslab.engedu.common.service.UserService;
import ai.mindslab.engedu.frontapi.client.CommonSttClient;
import ai.mindslab.engedu.frontapi.common.ResponseLogResolver;
import ai.mindslab.engedu.frontapi.common.SttModelResolver;
import ai.mindslab.engedu.frontapi.common.KorReadingResolver;
import ai.mindslab.engedu.frontapi.common.SttWriteFile;
import ai.mindslab.engedu.frontapi.controller.data.Response;
import ai.mindslab.engedu.frontapi.controller.data.Result;
import ai.mindslab.engedu.frontapi.controller.data.SttFileResponseVO;
import ai.mindslab.engedu.frontapi.service.data.Parameters;
import ai.mindslab.engedu.intent.IntentServiceType;
import ai.mindslab.engedu.intent.service.IntentFinderService;
import ai.mindslab.engedu.intent.vo.ExtInfo;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Map;

@Slf4j
@Service
public class KorReadingSttService extends SttService {

	@Value("${brain.evaluation.kor.stt.rec.dir}")
	private String sttRecDir;

	@Value("${brain.stt.sampleRate}")
	private int sampleRate;

	@Value("${brain.stt.ip}")
	private String sttIp;

	@Value("${brain.stt.port}")
	private int sttPort;

	@Value("${client.record.domain}")
	private String domain;
	
	@Value("${engedu.socket.timeout}")
	private int timeout;

	@Autowired
	private KorReadingResolver korReadingResolver;

	@Autowired
	private UserService userService;

	@Autowired
	private IntentFinderService intentFinderService;

	@Override
	public void onSttResult(WebSocketSession session, Map resultMap, ByteArrayOutputStream byteArrayOutputStream) {

		// PINKO
		log.error("### onSttResult()");

		Parameters parameters = new Parameters();
		try {
			// PINKO
			log.error("    --> send message: epd");

			// send EPD message
			sendEpdMessage(session);

			String utter = String.valueOf(resultMap.get("utter"));

			SttWriteFile sttWriteFile = new SttWriteFile();
			if(getParameterMap(session) != null) {
				parameters = getParameterMap(session);
			} else {
				throw new IllegalStateException();
			}
			
			removeSttClient(session);
			
			SttFileResponseVO sttFileResponseVO = sttWriteFile.write(byteArrayOutputStream,utter,sttRecDir,domain,parameters);

			// stt 저장 url
			String recordUrl = sttFileResponseVO.getRecordUrl();

			// 정답 문장
			String answerText = sttFileResponseVO.getAnswer();

			// 평가 결과
			Result result = korReadingResolver.getReadingResult(utter, answerText);

			// PINKO
			log.error("    --> after eval");

			// DB 저장
			insertReading(sttFileResponseVO, result);


			String resultId = "";

			Response response =  new Response(Integer.toString(IRestCodes.ERR_CODE_SUCCESS), IRestCodes.ERR_MSG_SUCCESS, IRestCodes.RESULT_TYPE_UTTER_EVAL_STT, resultId, result);

			result.setAnswerText(answerText);
			result.setUserText(utter);
			result.setRecordUrl(recordUrl);
			ExtInfo extInfo = new ExtInfo();
			extInfo.setCurrentDomain(IntentServiceType.ServiceEnum.ST0036.getDomainName());
			result.setExtInfo(extInfo);
			ResponseLogResolver.insertResponseLog(response, parameters.getUserId(), null);

			ObjectMapper mapper = new ObjectMapper();
			String jsonString = mapper.writeValueAsString(response);
			log.info("jsonString: {} ",jsonString);

			// PINKO
			log.error("    --> before result send");
			session.sendMessage(new TextMessage(jsonString));
			// PINKO
			log.error("    --> after result send");
		} catch (IllegalStateException illegalStateException) {

			illegalStateException.printStackTrace();

			try {
				Response response =  new Response(
						Integer.toString(IRestCodes.ERR_CODE_WEB_SOCKET_ILLEGAL_STATE_ERROR)
						, IRestCodes.ERR_MSG_WEB_SOCKET_ILLEGAL_STATE_ERROR
						, IRestCodes.RESULT_TYPE_UTTER_EVAL_STT
						, null
						, null
				);
				ResponseLogResolver.insertResponseLog(response, parameters.getUserId(), null);

				ObjectMapper mapper = new ObjectMapper();
				String jsonString = mapper.writeValueAsString(response);
				log.info("jsonString: {} ",jsonString);

			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e1) {

			e1.printStackTrace();

			try {

				Response response =  new Response(
						Integer.toString(IRestCodes.ERR_CODE_FAILURE)
						, IRestCodes.ERR_MSG_FAILURE
						, IRestCodes.RESULT_TYPE_UTTER_EVAL_STT
						, null
						, null
				);
				ResponseLogResolver.insertResponseLog(response, parameters.getUserId(), ExceptionUtils.getStackTrace(e1));

				ObjectMapper mapper = new ObjectMapper();
				String jsonString = mapper.writeValueAsString(response);
				log.info("jsonString: {} ",jsonString);

				session.sendMessage(new TextMessage(jsonString));

			} catch (Exception e) {
				e.printStackTrace();
			}
		} finally {
			// PINKO
			log.error("    --> before purgeServiceType");

			intentFinderService.purgeServiceType(parameters.getUserId());

			// PINKO
			log.error("    --> after purgeServiceType");
		}


	}

	private void insertReading(SttFileResponseVO sttFileResponseVO, Result ttsResult) {
		log.info("sttFileResponseVO.toString():::::"+sttFileResponseVO.toString());
		log.info("ttsResult.toString():::::"+ttsResult.toString());

		ReadingVO readingVO = new ReadingVO();
		readingVO.setUserId(sttFileResponseVO.getUserId());
		readingVO.setFilePath(sttFileResponseVO.getFilePath());
		readingVO.setFileTextName(sttFileResponseVO.getFileTextName());
		readingVO.setFileWaveName(sttFileResponseVO.getFileWavName());
		readingVO.setFileMp3Name(sttFileResponseVO.getFileMp3Name());
		readingVO.setCreatorId(sttFileResponseVO.getUserId());
		readingVO.setAverageScore(ttsResult.getAverageScore());
		readingVO.setUserText(sttFileResponseVO.getUtter());
		readingVO.setAnswerText(sttFileResponseVO.getAnswer());


		userService.insertUserReading(readingVO);
	}

	@Override
	public void openSession(WebSocketSession session, Map<String, Object> params) {

		log.debug("open KorReadingStt Session: {}" , session.getId());
		String userId = (String) params.get("userId");
		intentFinderService.updateServiceType(IntentServiceType.KORREADING, userId);

		String model = SttModelResolver.getModel(session, params);

		String lang  = (String)params.get("language");
		
		log.debug("open KorReadingStt model: {} lang:{} sampleRate: {}" , model,lang,Integer.toString(sampleRate));
		log.debug("open KorReadingStt socket timeout: {} " , Integer.toString(timeout));

		CommonSttClient sttClient = new CommonSttClient(sttIp, sttPort, lang, model, Integer.toString(sampleRate), this,timeout, (String) params.get("answerText"));
		sttClient.init(session);

		putClient(session, sttClient);
		putParameterMap(session, params);

	}
	
	public void removeSttClient(WebSocketSession session) {
		CommonSttClient client = getClient(session);
		if(client != null) {
//			getClient(session).shutdown();
			removeClient(session);
			log.info("close KorReadingStt Session: {}" , session.getId());
		}

		removeParameterMap(session);
	}

	public void closeSttClient(WebSocketSession session) {
		try {
			CommonSttClient client = getClient(session);
			if(client != null) {
				getClient(session).shutdown();
				removeClient(session);
				log.info("close EvaluationStt Session: {}" , session.getId());
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		removeParameterMap(session);
	}

	@Override
	public void onClose(WebSocketSession session) {
		removeSttClient(session);
	}

	@Override
	public void onMessage(WebSocketSession session, String base64Audio, boolean last) {
		CommonSttClient client = getClient(session);
		if(client != null) {
			Decoder decoder = Base64.getDecoder();
			byte[] decodedByte = decoder.decode(base64Audio.split(",")[1]);
			client.sendData(decodedByte);
		}
	}

	@Override
	public void onMessage(WebSocketSession session, ByteBuffer buffer, boolean last) {
		CommonSttClient client = getClient(session);
		if(client != null) {
			byte[] decodedByte = buffer.array();
			client.sendData(decodedByte);
		}
	}
}
