package ai.mindslab.engedu.frontapi.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Map;
import java.util.Scanner;

import ai.mindslab.engedu.frontapi.common.SttWriteFile;
import ai.mindslab.engedu.frontapi.service.data.Parameters;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import com.fasterxml.jackson.databind.ObjectMapper;
//import com.mindslab.edu.assessment.testAudioWaveformCreator;
//import com.mindslab.edu.assessment.testWord;

import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.common.data.DialogVO;
import ai.mindslab.engedu.common.service.UserService;
import ai.mindslab.engedu.freetalk.dao.data.EngFreeTalkQuestionVO;
import ai.mindslab.engedu.frontapi.client.CommonSttClient;
import ai.mindslab.engedu.frontapi.common.ResponseLogResolver;
import ai.mindslab.engedu.frontapi.common.SttModelResolver;
import ai.mindslab.engedu.frontapi.controller.data.Response;
import ai.mindslab.engedu.frontapi.controller.data.Result;
import ai.mindslab.engedu.frontapi.controller.data.ServiceMessageVO;
import ai.mindslab.engedu.frontapi.controller.data.SttFileResponseVO;
import ai.mindslab.engedu.intent.IntentManager;
import ai.mindslab.engedu.intent.IntentServiceType;
import ai.mindslab.engedu.intent.service.IntentFinderService;
import ai.mindslab.engedu.intent.vo.IntentExecuteMessageVO;
import ai.mindslab.engedu.intent.vo.IntentExecuteVO;
import ai.mindslab.engedu.intent.vo.IntentFinderVO;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class LetterSttService extends SttService {
	
	
	@Autowired
	private IntentManager intentManager;
	
	@Value("${engedu.phonics.rec.dir}")
	private String sttRecDir;

	@Value("${brain.dialog.tts.rec.dir}")
	private String ttsRecDir;

	@Value("${brain.stt.sampleRate}")
	private int sampleRate;
	
	@Value("${brain.stt.ip}")
	private String sttIp;
	
	@Value("${brain.stt.port}")
	private int sttPort;

	@Autowired
	private UserService userService;
	
	@Autowired
	private IntentFinderService intentFinderService;

	@Value("${client.record.domain}")
	private String domain;
	
	@Value("${engedu.socket.timeout}")
	private int timeout;

	@Override
	public void onSttResult(WebSocketSession session, Map resultMap, ByteArrayOutputStream byteArrayOutputStream) {

		Parameters parameters = new Parameters();
		try {

			String utter = String.valueOf(resultMap.get("utter"));
			String answer = String.valueOf(resultMap.get("answerText"));
			log.info("answer : "+ answer);
			SttWriteFile sttWriteFile = new SttWriteFile();
			if(getParameterMap(session) != null) {
				parameters = getParameterMap(session);
			} else {
				throw new IllegalStateException();
			}
			
			removeSttClient(session);
			
			SttFileResponseVO sttFileResponseVO = sttWriteFile.write(byteArrayOutputStream,utter,sttRecDir,domain,parameters);

			log.debug("parameters.getUserId() :"+parameters.getUserId());			// UserId		
			log.debug("sttFileResponseVO.getFileFolderFileWavName() :"+sttFileResponseVO.getFileFolderFileWavName());	// wav file path /record/kaldi/testUserId_20181025113940_687.wav
			
			
			log.debug("resultMap.get(\"utter\") :"+utter);
			
			String kaldiSTL = kaldiSTLResult16(sttFileResponseVO.getFileFolderFileWavName(), parameters.getUserId());
			log.debug("kaldiSTL :>>>>>>>>>>"+kaldiSTL+"<<<<<<<<<<");
			
			String imageOutFile ="";
			imageOutFile = sttFileResponseVO.getFileFolderFileWavName().replaceAll(".wav", ".png");
			log.debug("imageOutFile:"+ imageOutFile );

// ==================================================================================================================================
// POM.xml 에서 일부 라이브러리 삭제에 의한 에러 방지
// ==================================================================================================================================
//			testAudioWaveformCreator awc = new testAudioWaveformCreator(sttFileResponseVO.getFileFolderFileWavName(), imageOutFile);
//			awc.createAudioInputStream();
// ----------------------------------------------------------------------------------------------------------------------------------

			Response response;
			Result result = new Result();
			String resultId ="";
				
			result.setRecordUrl(sttFileResponseVO.getFileFolderFileWavName());	// recordPath :   /record/kaldi/testUserId_20181025113940_687.wav
			result.setWavDiagramUrl(imageOutFile);
			result.setLetterStlResult(answer);
//			result.setAnswerText(kaldiSTL); 
			result.setUserText(utter);
			
			response =  new Response(Integer.toString(IRestCodes.ERR_CODE_SUCCESS), IRestCodes.ERR_MSG_SUCCESS, IRestCodes.RESULT_TYPE_LETTER_STT,resultId,result);
			
			ResponseLogResolver.insertResponseLog(response, parameters.getUserId(), null);
			
			ObjectMapper mapper = new ObjectMapper();
			String jsonString = mapper.writeValueAsString(response);
			log.info("jsonString: {} ",jsonString);
			
			session.sendMessage(new TextMessage(jsonString));

		} catch (IllegalStateException illegalStateException) {

			illegalStateException.printStackTrace();

			try {
				Response response =  new Response(
						Integer.toString(IRestCodes.ERR_CODE_WEB_SOCKET_ILLEGAL_STATE_ERROR)
						, IRestCodes.ERR_MSG_WEB_SOCKET_ILLEGAL_STATE_ERROR
						, IRestCodes.RESULT_TYPE_LETTER_STT
						, null
						, null
				);
				ResponseLogResolver.insertResponseLog(response, parameters.getUserId(), null);

				ObjectMapper mapper = new ObjectMapper();
				String jsonString = mapper.writeValueAsString(response);
				log.info("jsonString: {} ",jsonString);

			} catch (Exception e) {
				e.printStackTrace();
			}

			
		} catch (Exception e1) {

			e1.printStackTrace();

			try {

				Response response =  new Response(
						Integer.toString(IRestCodes.ERR_CODE_FAILURE)
						, IRestCodes.ERR_MSG_FAILURE
						, IRestCodes.RESULT_TYPE_LETTER_STT
						, null
						, null
				);
				ResponseLogResolver.insertResponseLog(response, parameters.getUserId(), ExceptionUtils.getStackTrace(e1));

				ObjectMapper mapper = new ObjectMapper();
				String jsonString = mapper.writeValueAsString(response);
				log.info("jsonString: {} ",jsonString);

				session.sendMessage(new TextMessage(jsonString));

			} catch (Exception e) {
				e.printStackTrace();
			}

			
		}
	}
	
	
	private String kaldiSTLResult16(String fileNm, String userId) {
		
		String result="";

		
		
		String onlyFileNm = fileNm.substring(fileNm.lastIndexOf("/")+1);
		String txtFileNm = onlyFileNm.replaceAll(".wav", "_.txt");
		
		log.debug("kaldiSTLResult16 fileNm:"+fileNm);
		log.debug("kaldiSTLResult16 userId:"+userId);
		log.debug("kaldiSTLResult16 txtFileNm:"+txtFileNm);
		
        // /home/minds/green/kaldi-dnn-ali-gop/egs/aspire/s5/getPhonemes.sh /record/phonics/2018/10/30/RU3MrOs0hTIfXtP_20181030165146_931.wav pen
        String[] cmd={"/home/minds/green/kaldi-dnn-ali-gop/egs/aspire/s5/getLetters.sh",userId, fileNm,txtFileNm };

        Process process;
        try {
                process = Runtime.getRuntime().exec(cmd);
                InputStream is = process.getInputStream();
                Scanner  sc = new Scanner(is);
                while(sc.hasNextLine())
                        result = sc.nextLine();
                sc.close();
                is.close();
                process.waitFor();
                result = result.trim();
                if ("OH".equals(result)) {
                        result = "O";
                } else if ("QUE".equals(result) || "CUE".equals(result)) {
                        result = "Q";
                } else if ("ARE".equals(result)) {
                        result = "R";
                } else if ("YES".equals(result)) {
                        result = "S";
                } else if ("YOU".equals(result)) {
                        result = "U";
                } else if ("WHY".equals(result)) {
                        result = "Y";
                }

//                System.out.println(result);
        } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        }
	        
	    return result;
	}
	
	
	private String kaldiSTPResult(String fileNm) {
		
		String result="";

		String wavFileNm = fileNm.substring(fileNm.lastIndexOf("/")+1);
		String txtFileNm = wavFileNm.replaceAll(".wav", "_STP.txt");
		
        // /home/minds/green/kaldi-dnn-ali-gop/egs/aspire/s5/getPhonemes.sh hello.wav hello.txt
        String[] cmd={"/home/minds/green/kaldi-dnn-ali-gop/egs/aspire/s5/getPhonemes.sh",
        		wavFileNm, txtFileNm};

        Process process;
        StringBuffer sb = new StringBuffer();
        try {
            process = Runtime.getRuntime().exec(cmd);
            InputStream is = process.getInputStream();
            Scanner  sc = new Scanner(is);
            String tmp;
            while(sc.hasNextLine()){
                tmp = sc.nextLine();
                tmp = tmp.trim();
                if (tmp.indexOf('_') > -1) {
                    tmp = tmp.substring(0,tmp.indexOf('_'));
                }
                sb.append(tmp).append('-');
            }
            sc.close();
            is.close();
            process.waitFor();

            String s1 = sb.toString();

            s1 = s1.replaceAll("sil\\-", "");
            s1 = s1.substring(0, s1.length()-1);
        //    System.out.println(s1);
            result=s1;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
	        
	        return result;
	}
	
	

	@Override
	public void openSession(WebSocketSession session, Map<String, Object> params) {
		log.debug("open AlphabetStt Session: {}" , session.getId());

		String userId = (String) params.get("userId");
		intentFinderService.updateServiceType(IntentServiceType.ALPAHBET,userId);

		//find model
		String model = SttModelResolver.getModel(session, params);
		
		String lang  = (String)params.get("language");
		
		log.debug("open LetterStt model: {} lang:{} sampleRate: {}" , model,lang,Integer.toString(sampleRate));
		log.debug("open LetterStt socket timeout: {} " , Integer.toString(timeout));
		
		CommonSttClient sttClient = new CommonSttClient(sttIp, sttPort, lang, model, Integer.toString(sampleRate), this,timeout, (String) params.get("answerText"));
		sttClient.init(session);
		
		putClient(session, sttClient);
		putParameterMap(session, params);
		
	}
	
	public void removeSttClient(WebSocketSession session) {
		try {
			CommonSttClient client = getClient(session);
			if(client != null) {
				getClient(session).shutdown();
				removeClient(session);
				log.info("close LetterStt Session: {}" , session.getId());				
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		removeParameterMap(session);
	}

	
	@Override
	public void onClose(WebSocketSession session) {
		removeSttClient(session);
		/*try {
			getClient(session).shutdown();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		removeClient(session);
		removeParameterMap(session);
		log.info("close SimpleStt Session: {}" , session.getId());*/
	}

	@Override
	public void onMessage(WebSocketSession session, String base64Audio, boolean last) {
		CommonSttClient client = getClient(session);
		if(client != null) {
			Decoder decoder = Base64.getDecoder();
			byte[] decodedByte = decoder.decode(base64Audio.split(",")[1]);
			client.sendData(decodedByte);
		}
	}

	@Override
	public void onMessage(WebSocketSession session, ByteBuffer buffer, boolean last) {
		CommonSttClient client = getClient(session);
		if(client != null) {
			byte[] decodedByte = buffer.array();
			client.sendData(decodedByte);
		}
	}

}
