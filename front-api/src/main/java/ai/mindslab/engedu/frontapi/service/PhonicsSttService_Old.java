package ai.mindslab.engedu.frontapi.service;

import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.common.service.UserService;
import ai.mindslab.engedu.frontapi.client.CommonSttClient;
import ai.mindslab.engedu.frontapi.common.ResponseLogResolver;
import ai.mindslab.engedu.frontapi.common.SttModelResolver;
import ai.mindslab.engedu.frontapi.common.SttWriteFile;
import ai.mindslab.engedu.frontapi.controller.data.Response;
import ai.mindslab.engedu.frontapi.controller.data.Result;
import ai.mindslab.engedu.frontapi.controller.data.SttFileResponseVO;
import ai.mindslab.engedu.frontapi.service.SttService;
import ai.mindslab.engedu.frontapi.service.data.Parameters;
import ai.mindslab.engedu.intent.IntentManager;
import ai.mindslab.engedu.intent.IntentServiceType;
import ai.mindslab.engedu.intent.service.IntentFinderService;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Map;
import java.util.Scanner;

@Slf4j
@Service
public class PhonicsSttService_Old extends SttService {
	
	
	@Autowired
	private IntentManager intentManager;
	
	@Value("${engedu.phonics.rec.dir}")
	private String sttRecDir;

	@Value("${brain.dialog.tts.rec.dir}")
	private String ttsRecDir;

	@Value("${brain.stt.sampleRate}")
	private int sampleRate;
	
	@Value("${brain.stt.ip}")
	private String sttIp;
	
	@Value("${brain.stt.port}")
	private int sttPort;

	@Autowired
	private UserService userService;
	
	@Autowired
	private IntentFinderService intentFinderService;

	@Value("${client.record.domain}")
	private String domain;
	
	@Value("${engedu.phonics.meaningDic.dir}")
	private String meaningDicDir;
	
	@Value("${engedu.phonics.wordnetDic.dir}")
	private String wordnetDicDir;
	
	@Value("${engedu.socket.timeout}")
	private int timeout;

	@Override
	public void onSttResult(WebSocketSession session, Map resultMap, ByteArrayOutputStream byteArrayOutputStream) {

		Parameters parameters = new Parameters();
		try {

			String utter = String.valueOf(resultMap.get("utter"));
			SttWriteFile sttWriteFile = new SttWriteFile();
			if(getParameterMap(session) != null) {
				parameters = getParameterMap(session);
			} else {
				throw new IllegalStateException();
			}
			
			removeSttClient(session);
			
			SttFileResponseVO sttFileResponseVO = sttWriteFile.write(byteArrayOutputStream,utter,sttRecDir,domain,parameters);

			log.debug("parameters.getUserId() :"+parameters.getUserId());			// UserId		
			log.debug("parameters.getTargetLetter() :"+parameters.getTargetLetter());		// targetLetter
			log.debug("parameters.getCounter() :"+parameters.getCounter());		// targetLetter
			log.debug("parameters.getAnswerText() :"+parameters.getAnswerText());	// AnswerText
			log.debug("sttFileResponseVO.getFileFolderFileWavName() :"+sttFileResponseVO.getFileFolderFileWavName());	// wav file path /record/kaldi/testUserId_20181025113940_687.wav
			log.debug("meaningDicDir:"+ meaningDicDir );
			log.debug("wordnetDicDir:"+ wordnetDicDir );

//			8K test
			/*
			write8kWav(sttFileResponseVO.getFileFolderFileWavName());
			String kaldiSTTresult = kaldiSTTResult( wav8kFileNm );
			log.debug("kaldiSTTresult :>>>>>>>>>>"+kaldiSTTresult+"<<<<<<<<<<");
			
			String wav8kFileNm =   sttFileResponseVO.getFileFolderFileWavName().replaceAll(".wav", "_8000.wav"); 
			String kaldiSTPresult = kaldiSTPResult( wav8kFileNm );
			*/
			
			String kaldiSTP = kaldiSTPResult16(sttFileResponseVO.getFileFolderFileWavName(), parameters.getUserId(), parameters.getAnswerText());
			log.debug("kaldiSTP :>>>>>>>>>>"+kaldiSTP+"<<<<<<<<<<");
			
			Response response;
			Result result = new Result();
			String resultId ="";
			
			String targetLetters[] = parameters.getTargetLetter().split(",");
			String word = parameters.getAnswerText();
			
			String meaningLst = "";
			String phonemes =  "";
			String phonemes_count = ""; 
			String stresses =  "";
			String syllables =  "";
			String ARPA =  "";
			String IPA =  "";
			String wordList =  "";
			String imageOutFile ="";

// ==================================================================================================================================
// POM.xml 에서 일부 라이브러리 삭제에 의한 에러 방지
// ==================================================================================================================================
//			testWord testWord = new testWord();
//			Map<String, String> returnMap =testWord.calculate(parameters.getAnswerText(), targetLetters, meaningDicDir, wordnetDicDir);
			
//			meaningLst = returnMap.get("meaningLst");
//			phonemes = returnMap.get("phonemes");
//			phonemes_count = returnMap.get("phonemes_count");
//			stresses = returnMap.get("stresses");
//			syllables = returnMap.get("syllables");
//			ARPA = returnMap.get("ARPA");
//			IPA = returnMap.get("IPA");
//			wordList = returnMap.get("wordList");
// ------------------------------------------------------------------------------------------------------------------------------------
			log.debug("meaningLst:"+ meaningLst );
			log.debug("phonemes:"+ phonemes );
			log.debug("phonemes_count:"+ phonemes_count );
			log.debug("stresses:"+ stresses );
			log.debug("syllables:"+ syllables );
			log.debug("ARPA:"+ ARPA );
			log.debug("IPA:"+ IPA );
//		    	log.debug("wordList"+ wordList );
		
		
			imageOutFile = sttFileResponseVO.getFileFolderFileWavName().replaceAll(".wav", ".png");
			log.debug("imageOutFile:"+ imageOutFile );

// ==================================================================================================================================
// POM.xml 에서 일부 라이브러리 삭제에 의한 에러 방지
// ==================================================================================================================================
//			testAudioWaveformCreator awc = new testAudioWaveformCreator(sttFileResponseVO.getFileFolderFileWavName(), imageOutFile);
//			awc.createAudioInputStream();
// ------------------------------------------------------------------------------------------------------------------------------------
			result.setRecordUrl(sttFileResponseVO.getFileFolderFileWavName());	// recordPath :   /record/kaldi/testUserId_20181025113940_687.wav
			result.setPhonicsStpResult(kaldiSTP);	// KALDI_STP : P EH N
			result.setPhonicsDicResult(phonemes);	// 	KALDI_DIC :P EH N
			result.setWavDiagramUrl(imageOutFile);
			result.setAnswerText(parameters.getAnswerText()); 
			result.setUserText(utter);
			
			response =  new Response(Integer.toString(IRestCodes.ERR_CODE_SUCCESS), IRestCodes.ERR_MSG_SUCCESS, IRestCodes.RESULT_TYPE_PHONICS_STT,resultId,result);
			
			ResponseLogResolver.insertResponseLog(response, parameters.getUserId(), null);
			
			ObjectMapper mapper = new ObjectMapper();
			String jsonString = mapper.writeValueAsString(response);
			log.info("jsonString: {} ",jsonString);
			
			session.sendMessage(new TextMessage(jsonString));

		} catch (IllegalStateException illegalStateException) {

			illegalStateException.printStackTrace();

			try {
				Response response =  new Response(
						Integer.toString(IRestCodes.ERR_CODE_WEB_SOCKET_ILLEGAL_STATE_ERROR)
						, IRestCodes.ERR_MSG_WEB_SOCKET_ILLEGAL_STATE_ERROR
						, IRestCodes.RESULT_TYPE_PHONICS_STT
						, null
						, null
				);
				ResponseLogResolver.insertResponseLog(response, parameters.getUserId(), null);

				ObjectMapper mapper = new ObjectMapper();
				String jsonString = mapper.writeValueAsString(response);
				log.info("jsonString: {} ",jsonString);

			} catch (Exception e) {
				e.printStackTrace();
			}

			
		} catch (Exception e1) {

			e1.printStackTrace();

			try {

				Response response =  new Response(
						Integer.toString(IRestCodes.ERR_CODE_FAILURE)
						, IRestCodes.ERR_MSG_FAILURE
						, IRestCodes.RESULT_TYPE_PHONICS_STT
						, null
						, null
				);
				ResponseLogResolver.insertResponseLog(response, parameters.getUserId(), ExceptionUtils.getStackTrace(e1));

				ObjectMapper mapper = new ObjectMapper();
				String jsonString = mapper.writeValueAsString(response);
				log.info("jsonString: {} ",jsonString);

				session.sendMessage(new TextMessage(jsonString));

			} catch (Exception e) {
				e.printStackTrace();
			}

			
		}
	}
	
	
	private String kaldiSTPResult16(String fileNm, String userId, String answerText) {
		
		
		
		String result="";

//		String wavFileNm = fileNm.substring(fileNm.lastIndexOf("/")+1);
		
		log.debug("kaldiSTPResult16 fileNm:"+fileNm);
//		log.debug("kaldiSTPResult16 wavFileNm:"+wavFileNm);
		log.debug("kaldiSTPResult16 userId:"+userId);
		log.debug("kaldiSTPResult16 answerText:"+answerText);
		
		
        // /home/minds/green/kaldi-dnn-ali-gop/egs/aspire/s5/getPhonemes.sh /record/phonics/2018/10/30/RU3MrOs0hTIfXtP_20181030165146_931.wav pen
        String[] cmd={"/home/minds/green/kaldi-dnn-ali-gop/egs/librispeech/phonics/getPhonemes.sh",userId, fileNm,answerText};

        Process process;
        StringBuffer sb = new StringBuffer();
        try {
            process = Runtime.getRuntime().exec(cmd);
            InputStream is = process.getInputStream();
            Scanner  sc = new Scanner(is);
            String tmp="";
            while(sc.hasNextLine()){
                tmp = sc.nextLine();
                
                /*System.out.println(tmp);
                tmp = tmp.trim();
                if (tmp.indexOf('_') > -1) {
                    tmp = tmp.substring(0,tmp.indexOf('_'));
                }
                sb.append(tmp).append('-');*/
            }
            
            // tmp return string : EGG EH G
            tmp = tmp.substring(tmp.indexOf(" ")+1); // 위에서 원 단어인 EGG를 제거하기 위해 
            sb.append(tmp);
            
            sc.close();
            is.close();
            process.waitFor();

            String s1 = sb.toString();

        //    s1 = s1.replaceAll("sil\\-", "");
         //   s1 = s1.substring(0, s1.length()-1);
        //    System.out.println(s1);
            result=s1;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
	        
	        return result;
	}
	
	
	private String kaldiSTPResult(String fileNm) {
		
		String result="";

		String wavFileNm = fileNm.substring(fileNm.lastIndexOf("/")+1);
		String txtFileNm = wavFileNm.replaceAll(".wav", "_STP.txt");
		
        // /home/minds/green/kaldi-dnn-ali-gop/egs/aspire/s5/getPhonemes.sh hello.wav hello.txt
        String[] cmd={"/home/minds/green/kaldi-dnn-ali-gop/egs/aspire/s5/getPhonemes.sh",
        		wavFileNm, txtFileNm};

        Process process;
        StringBuffer sb = new StringBuffer();
        try {
            process = Runtime.getRuntime().exec(cmd);
            InputStream is = process.getInputStream();
            Scanner  sc = new Scanner(is);
            String tmp;
            while(sc.hasNextLine()){
                tmp = sc.nextLine();
                tmp = tmp.trim();
                if (tmp.indexOf('_') > -1) {
                    tmp = tmp.substring(0,tmp.indexOf('_'));
                }
                sb.append(tmp).append('-');
            }
            sc.close();
            is.close();
            process.waitFor();

            String s1 = sb.toString();

            s1 = s1.replaceAll("sil\\-", "");
            s1 = s1.substring(0, s1.length()-1);
        //    System.out.println(s1);
            result=s1;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
	        
	        return result;
	}
	
	private String kaldiSTTResult(String fileNm) {

		StringBuffer strbuf = new StringBuffer();
		
		String wavFileNm = fileNm.substring(fileNm.lastIndexOf("/")+1);
		String txtFileNm = wavFileNm.replaceAll(".wav", "_STT.txt");

	    // /home/minds/green/kaldi-dnn-ali-gop/egs/aspire/s5/getWord.sh hello.wav hello.txt
	    String[] cmd={"/home/minds/green/kaldi-dnn-ali-gop/egs/aspire/s5/getWord.sh",wavFileNm, txtFileNm};
	
	    Process process;
	    try {
	        process = Runtime.getRuntime().exec(cmd);
	        InputStream is = process.getInputStream();
	        Scanner  sc = new Scanner(is);
	        while(sc.hasNextLine()) {
	          //  System.out.println(sc.nextLine());
	            strbuf.append(sc.nextLine());
	        }
	        sc.close();
	        is.close();
	        process.waitFor();
	
	
	    } catch (Exception e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
	    }
	    
	    return strbuf.toString();
		
	}
	
	
	public void write8kWav(String source) {
		
		String target= source.replaceAll(".wav", "_8000.wav");

        Runtime rt = Runtime.getRuntime();
        Process pc = null;

		try {
			//외부 프로세스 실행
			
			
			 pc = rt.exec("sox -q  "+source+" "+target+"  rate -L -s 8000");
			log.debug("sox -q  "+source+" "+target+"  rate -L -s 8000 start");
            pc.waitFor();
            pc.destroy();
            log.debug("sox -q  "+source+" "+target+"  rate -L -s 8000 end");

		} catch (Exception e) {
            log.error("Exception : " + e.getMessage());
			e.printStackTrace();
		}

	}
	
	

	@Override
	public void openSession(WebSocketSession session, Map<String, Object> params) {
		log.debug("open PhonicsStt Session: {}" , session.getId());

		String userId = (String) params.get("userId");
		intentFinderService.updateServiceType(IntentServiceType.PHONICS,userId);


		//find model
		String model = SttModelResolver.getModel(session, params);
		
		
		String lang  = (String)params.get("language");
		
		log.debug("open PhonicsSttService model: {} lang:{} sampleRate: {}" , model,lang,Integer.toString(sampleRate));
		log.debug("open PhonicsSttService socket timeout: {} " , Integer.toString(timeout));
		
		
		CommonSttClient sttClient = new CommonSttClient(sttIp, sttPort, lang, model, Integer.toString(sampleRate), this,timeout, (String) params.get("answerText"));
		sttClient.init(session);
		
		putClient(session, sttClient);
		putParameterMap(session, params);
		
	}
	
	public void removeSttClient(WebSocketSession session) {
		try {
			CommonSttClient client = getClient(session);
			if(client != null) {
				getClient(session).shutdown();
				removeClient(session);
				log.info("close PhonicsStt Session: {}" , session.getId());				
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		removeParameterMap(session);
	}

	
	@Override
	public void onClose(WebSocketSession session) {
		removeSttClient(session);
		/*try {
			getClient(session).shutdown();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		removeClient(session);
		removeParameterMap(session);
		log.info("close SimpleStt Session: {}" , session.getId());*/
	}

	@Override
	public void onMessage(WebSocketSession session, String base64Audio, boolean last) {
		CommonSttClient client = getClient(session);
		if(client != null) {
			Decoder decoder = Base64.getDecoder();
			byte[] decodedByte = decoder.decode(base64Audio.split(",")[1]);
			client.sendData(decodedByte);
		}
	}

	@Override
	public void onMessage(WebSocketSession session, ByteBuffer buffer, boolean last) {
		CommonSttClient client = getClient(session);
		if(client != null) {
			byte[] decodedByte = buffer.array();
			client.sendData(decodedByte);
		}
	}

}
