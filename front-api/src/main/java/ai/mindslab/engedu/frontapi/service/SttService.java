package ai.mindslab.engedu.frontapi.service;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

import ai.mindslab.engedu.common.codes.IExtensionCodes;
import ai.mindslab.engedu.common.codes.IMarkCodes;
import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.common.utils.DateUtil;
import ai.mindslab.engedu.common.utils.FileCreateUtil;
import ai.mindslab.engedu.frontapi.client.CommonCnnSttClient;
import ai.mindslab.engedu.frontapi.controller.data.Response;
import ai.mindslab.engedu.frontapi.controller.data.SttFileResponseVO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import ai.mindslab.engedu.frontapi.client.CommonSttClient;
import ai.mindslab.engedu.frontapi.service.data.Parameters;
import lombok.extern.slf4j.Slf4j;

/**
 * @author kirk
 * @since 2018. 08. 03
 */
@Slf4j
abstract class SttService implements WebSocketCallbackInterface {

	@Value("${epd.msg.send}")
	private boolean epdUse;

	private HashMap<WebSocketSession, CommonCnnSttClient> clientsCnn = new HashMap<WebSocketSession, CommonCnnSttClient>();
	private HashMap<WebSocketSession, CommonSttClient> clients = new HashMap<WebSocketSession, CommonSttClient>();
	private HashMap<WebSocketSession, Parameters> clientParameterMap = new HashMap<WebSocketSession, Parameters>();

	protected CommonSttClient getClient(WebSocketSession session) {
		return clients.get(session);
	}

	protected CommonSttClient putClient(WebSocketSession session, CommonSttClient sttClient) {
		return clients.put(session, sttClient);
	}

	protected CommonCnnSttClient getCnnClient(WebSocketSession session) {
		return clientsCnn.get(session);
	}

	protected CommonCnnSttClient putCnnClient(WebSocketSession session, CommonCnnSttClient sttClient) {
		return clientsCnn.put(session, sttClient);
	}

	protected void removeClient(WebSocketSession session) {
		clients.replace(session, null);
		clients.remove(session);

		clientsCnn.replace(session, null);
		clientsCnn.remove(session);
	}

	protected Parameters getParameterMap(WebSocketSession session) {
		return clientParameterMap.get(session);
	}

	protected Parameters putParameterMap(WebSocketSession session, Map<String, Object> map) {
		Parameters parm = new Parameters();
		parm.setV((String)map.get("v"));
		parm.setBiz((String)map.get("biz"));
		parm.setChannel((String)map.get("channel"));
		parm.setLanguage((String)map.get("language"));
		parm.setService((String)map.get("service"));
		parm.setUserId((String)map.get("userId"));
		parm.setLectureId((String)map.get("lectureId"));
		parm.setChapterId((String)map.get("chapterId"));
		parm.setContentId((String)map.get("contentId"));
		parm.setSequence((String)map.get("sequence"));
		parm.setAnswerText((String)map.get("answerText"));
		parm.setRecordYn((String)map.get("recordYn"));
		parm.setTargetLetter((String)map.get("targetLetter"));
		parm.setCounter((String)map.get("counter"));
		parm.setCheckSymbol((String)map.get("checkSymbol"));
		
		return clientParameterMap.put(session, parm);
	}

	protected void removeParameterMap(WebSocketSession session) {
		clientParameterMap.replace(session, null);
		clientParameterMap.remove(session);
	}

	protected void sendEpdMessage(WebSocketSession session) throws IOException {
		if(epdUse) {
			Response response = new Response(Integer.toString(IRestCodes.ERR_CODE_EPD), IRestCodes.ERR_MSG_EPD, "", "", null);
			ObjectMapper mapper = new ObjectMapper();
			String jsonString = mapper.writeValueAsString(response);
			log.info("jsonString: {} ", jsonString);
			session.sendMessage(new TextMessage(jsonString));
		}
	}
	// 새로운 세션 생성
	public abstract void openSession(WebSocketSession session, Map<String, Object> params);

	public abstract void onClose(WebSocketSession session);

	public abstract void onMessage(WebSocketSession session, String base64Audio, boolean last);
	public abstract void onMessage(WebSocketSession session, ByteBuffer buffer, boolean last);

}
