package ai.mindslab.engedu.frontapi.service;

import ai.mindslab.engedu.common.codes.IRestCodes;
import ai.mindslab.engedu.common.data.DialogVO;
import ai.mindslab.engedu.common.service.UserService;
import ai.mindslab.engedu.frontapi.client.CommonSttClient;
import ai.mindslab.engedu.frontapi.common.ResponseLogResolver;
import ai.mindslab.engedu.frontapi.common.SttModelResolver;
import ai.mindslab.engedu.frontapi.common.SttWriteFile;
import ai.mindslab.engedu.frontapi.controller.data.Response;
import ai.mindslab.engedu.frontapi.controller.data.Result;
import ai.mindslab.engedu.frontapi.controller.data.SttFileResponseVO;
import ai.mindslab.engedu.frontapi.service.data.Parameters;
import ai.mindslab.engedu.intent.IntentServiceType;
import ai.mindslab.engedu.intent.service.IntentFinderService;

import ai.mindslab.engedu.intent.vo.ExtInfo;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Map;

@Slf4j
@Service
public class TutorialSttService extends SttService {
	
	@Value("${brain.tutorial.stt.rec.dir}")
	private String recDir;
	
	@Value("${brain.stt.sampleRate}")
	private int sampleRate;
	
	@Value("${brain.stt.ip}")
	private String sttIp;
	
	@Value("${brain.stt.port}")
	private int sttPort;

	@Value("${client.record.domain}")
	private String domain;
	
	@Value("${engedu.socket.timeout}")
	private int timeout;
	
	@Autowired
	private IntentFinderService intentFinderService;
	
	@Autowired
	private UserService userService;
	
	@Override
	public void onSttResult(WebSocketSession session, Map resultMap, ByteArrayOutputStream byteArrayOutputStream) {

		Parameters parameters = new Parameters();
		try {
			// send EPD message
			sendEpdMessage(session);

//			String utter = String.valueOf(resultMap.get("utter")).replaceAll("번", ",").replaceAll("[^0-9,]", "");
			String utter = String.valueOf(resultMap.get("utter")).replaceAll("정답은", "").trim();

			SttWriteFile sttWriteFile = new SttWriteFile();
			if(getParameterMap(session) != null) {
				parameters = getParameterMap(session);
			} else {
				throw new IllegalStateException();
			}
			
			removeSttClient(session);
			
			SttFileResponseVO sttFileResponseVO = sttWriteFile.write(byteArrayOutputStream,utter,recDir,domain,parameters);

			Result result = new Result();
			
			Response response = new Response();
			response.setResCode(Integer.toString(IRestCodes.ERR_CODE_SUCCESS));
			response.setResMsg(IRestCodes.ERR_MSG_SUCCESS);
			response.setResultType(IRestCodes.RESULT_TYPE_TUTORIAL_STT);
			
			response.setResult(result);
			
			result.setUserText(utter);
			result.setRecordUrl(sttFileResponseVO.getRecordUrl());

			ExtInfo extInfo = new ExtInfo();
			extInfo.setCurrentDomain(IntentServiceType.ServiceEnum.ST0039.getDomainName());
			result.setExtInfo(extInfo);

			insertDialog(sttFileResponseVO); 
			ResponseLogResolver.insertResponseLog(response, parameters.getUserId(), null);

			ObjectMapper mapper = new ObjectMapper();
			String jsonString = mapper.writeValueAsString(response);
			log.info("jsonString: {} ",jsonString);
			
			session.sendMessage(new TextMessage(jsonString));

		} catch (IllegalStateException illegalStateException) {

			illegalStateException.printStackTrace();

			try {
				Response response =  new Response(
						Integer.toString(IRestCodes.ERR_CODE_WEB_SOCKET_ILLEGAL_STATE_ERROR)
						, IRestCodes.ERR_MSG_WEB_SOCKET_ILLEGAL_STATE_ERROR
						, IRestCodes.RESULT_TYPE_TUTORIAL_STT
						, null
						, null
				);
				ResponseLogResolver.insertResponseLog(response, parameters.getUserId(), null);

				ObjectMapper mapper = new ObjectMapper();
				String jsonString = mapper.writeValueAsString(response);
				log.info("jsonString: {} ",jsonString);

			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e1) {

			e1.printStackTrace();

			try {

				Response response = new Response(
						Integer.toString(IRestCodes.ERR_CODE_FAILURE)
						, IRestCodes.ERR_MSG_FAILURE
						, IRestCodes.RESULT_TYPE_TUTORIAL_STT
						, null
						, null
				);
				ResponseLogResolver.insertResponseLog(response, parameters.getUserId(), ExceptionUtils.getStackTrace(e1));

				ObjectMapper mapper = new ObjectMapper();
				String jsonString = mapper.writeValueAsString(response);
				log.info("jsonString: {} ", jsonString);

				session.sendMessage(new TextMessage(jsonString));

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private void insertDialog(SttFileResponseVO sttFileResponseVO) {
		log.info("sttFileResponseVO.toString():::::"+sttFileResponseVO.toString());
		
        DialogVO dialogVO = new DialogVO();
        dialogVO.setUserId(sttFileResponseVO.getUserId());
        dialogVO.setFilePath(sttFileResponseVO.getFilePath());
        dialogVO.setSttFileTextName(sttFileResponseVO.getFileTextName());
        dialogVO.setSttFileWaveName(sttFileResponseVO.getFileWavName());
        dialogVO.setCreatorId(sttFileResponseVO.getUserId());

        
        dialogVO.setTtsFileMp3Name("");

        dialogVO.setUserText(sttFileResponseVO.getUtter());

        dialogVO.setAnswerText(sttFileResponseVO.getUtter());

		userService.insertUserDialog(dialogVO);
	}

	@Override
	public void openSession(WebSocketSession session, Map<String, Object> params) {
		log.debug("open TutorialStt Session: {}" , session.getId()); 
		
		String userId = (String) params.get("userId");
		intentFinderService.updateServiceType(IntentServiceType.TUTORIAL, userId);
		
		//find model
		String model = SttModelResolver.getModel(session, params);
		
		
		String lang  = (String)params.get("language");
		
		log.debug("open TutorialStt model: {} lang:{} sampleRate: {}" , model,lang,Integer.toString(sampleRate));
		log.debug("open TutorialStt socket timeout: {} " , Integer.toString(timeout));
		
		CommonSttClient sttClient = new CommonSttClient(sttIp, sttPort, lang, model, Integer.toString(sampleRate), this,timeout, (String) params.get("answerText"));
		sttClient.init(session);
		
		putClient(session, sttClient);
		putParameterMap(session, params);
		
	}

	public void removeSttClient(WebSocketSession session) {
		try {
			CommonSttClient client = getClient(session);
			if(client != null) {
				getClient(session).shutdown();
				removeClient(session);
				log.info("close TutorialStt Session: {}" , session.getId());				
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		removeParameterMap(session);
	}
	
	@Override
	public void onClose(WebSocketSession session) {
		removeSttClient(session);
	}

	@Override
	public void onMessage(WebSocketSession session, String base64Audio, boolean last) {
		CommonSttClient client = getClient(session);
		if(client != null) {
			Decoder decoder = Base64.getDecoder();
			byte[] decodedByte = decoder.decode(base64Audio.split(",")[1]);
			client.sendData(decodedByte);
		}
	}

	@Override
	public void onMessage(WebSocketSession session, ByteBuffer buffer, boolean last) {
		CommonSttClient client = getClient(session);
		if(client != null) {
			byte[] decodedByte = buffer.array();
			client.sendData(decodedByte);
		}
	}
}
