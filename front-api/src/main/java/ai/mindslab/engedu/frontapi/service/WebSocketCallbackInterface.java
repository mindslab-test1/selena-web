package ai.mindslab.engedu.frontapi.service;

import java.io.ByteArrayOutputStream;
import java.util.Map;

import javax.websocket.Session;

import org.springframework.web.socket.WebSocketSession;

import ai.mindslab.engedu.common.exceptions.EngEduException;

public interface WebSocketCallbackInterface {
	
	public void onSttResult(WebSocketSession session,Map resultMap, ByteArrayOutputStream byteArryOutputStream) throws EngEduException ;
	public void removeSttClient(WebSocketSession session) ;

}
