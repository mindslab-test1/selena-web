/*License (MIT)

Copyright Â© 2013 Matt Diamond

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of 
the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
DEALINGS IN THE SOFTWARE.
*/

(function(window){

  var WORKER_PATH = 'js/recorderjs/recorderWorker.js';

  var Recorder = function(source, cfg){
    var config = cfg || {};
    var bufferLen = config.bufferLen || 16384;
    this.context = source.context;
    if(!this.context.createScriptProcessor){
       this.node = this.context.createJavaScriptNode(bufferLen, 2, 2);
    } else {
       this.node = this.context.createScriptProcessor(bufferLen, 2, 2);
    }
    
    function handleMessageFromWorker(e) {

      switch(e.data.command) {
          case 'connected':
              console.log('onmessage connected');
              break;
          case 'stt_result':
              $("#sttresult").val(jsonObj.userText);
              //$("#sttresult").val(e.data.message);
              console.log('onmessage stt_result');

              stop();
              $("#record").removeClass('recording');
              recording = false;
              break;
          case 'request':
              var $logArea = $('#logArea');
              var jsonObj = JSON.parse(e.data.message);

              $("#sttresult").val(jsonObj.userText);

              $logArea.val($logArea.val() +'\nREQUEST:'+ e.data.message+'\n');
              $logArea.val($logArea.val() +'\n사용자 : '+ jsonObj.userText + '\n');
              break;
          
          case 'timeOut':
        	  
        	  stop();
              $("#record").removeClass('recording');
              recording = false;
              break;
              
          case 'response':
              var $logArea = $('#logArea');
              var jsonObj = JSON.parse(e.data.message);
              var recordUrl = jsonObj.result.ttsUrl;
              if(e.data.type == "SF"){
            	  console.log('jsonObj.userText::'+jsonObj.result.userText);
            	  console.log('jsonObj.freeTalkResult::'+jsonObj.result.freeTalkResult);
            	  $("#sttresult").val(jsonObj.result.userText);
            	  $("#freetalkresult").val(jsonObj.result.freeTalkResult);
            	  
            	  $logArea.val($logArea.val() +'\nRESPONSE:'+ e.data.message+'\n');
            	  $logArea.val($logArea.val() +'\n디봇 : '+ jsonObj.result.ttsText + '\n');
            	  $logArea.val($logArea.val() +'\n==============================================\n');
            	  $logArea.scrollTop($logArea.prop('scrollHeight'));

              } else {
            	  switch(e.data.type) {

                  case 'P' :
                      $('#phonicsResult').val(jsonObj.result.phonicsResult);
                      $('#phonicsUserPron').val(jsonObj.result.phonicsUserPron);
                      break;
            	  case 'E':
            		  $('#pronounceScore').val(jsonObj.result.pronounceScore);
            		  $('#grammarScore').val(jsonObj.result.grammarScore);
            		  recordUrl = jsonObj.result.recordUrl;
                      break;
                  case 'U':
                      $('#grammarScore').val(jsonObj.result.averageScore);    // 임시로 사용
                      break;
                      
                  case 'F':
            		  $('#freetalkresult').val(jsonObj.result.freeTalkResult);
            		  break;
            		  
            		 
                  case 'KALDI':
                	  
                	  $("#recordUrl").val(jsonObj.result.recordUrl);
					  $("#KALDI_STP").val(jsonObj.result.phonicsStpResult);
					  $("#KALDI_DIC").val(jsonObj.result.phonicsDicResult);
					  $("#WAVE_DIAGRAM").attr("src",jsonObj.result.wavDiagramUrl);
					  break;
					  
                  case 'KALDI_LETTER':
                	  
                	  $("#recordUrl").val(jsonObj.result.recordUrl);
					  //$("#AnswerText").val(jsonObj.result.letterStlResult);
					  $("#UserText").val(jsonObj.result.userText);
					  $("#WAVE_DIAGRAM").attr("src",jsonObj.result.wavDiagramUrl);
					  break;

            	  }

            	  $logArea.val($logArea.val() +'\nRESPONSE:'+ e.data.message+'\n');
            	  $logArea.val($logArea.val() +'\n디봇 : '+ jsonObj.result.ttsText + '\n');
            	  $logArea.val($logArea.val() +'\n==============================================\n');
            	  $logArea.scrollTop($logArea.prop('scrollHeight'));
            	  
            	  
            	  var audio = $("#player");
                  $("#recordUrl").attr("src",recordUrl);
            	  /****************/
            	  audio[0].pause();
            	  audio[0].load();
            	  
            	  audio[0].oncanplaythrough = audio[0].play();
            	  
              }
              

              stop();
              $("#record").removeClass('recording');
              recording = false;
              break;
    	};	
    }
    
    var worker = new Worker(config.workerPath || WORKER_PATH);
    
    worker.addEventListener('message', handleMessageFromWorker);
    
    worker.postMessage({
      command: 'init',
      config: {
        sampleRate: this.context.sampleRate
      }
    });
    var recording = false,
      currCallback;

    this.node.onaudioprocess = function(e){
      if (!recording) return;
      worker.postMessage({
        command: 'record',
        buffer: [
          e.inputBuffer.getChannelData(0),
          e.inputBuffer.getChannelData(1)
        ]
      });
    }

    this.configure = function(cfg){
      for (var prop in cfg){
        if (cfg.hasOwnProperty(prop)){
          config[prop] = cfg[prop];
        }
      }
    }

    this.record = function(){
    	this.context.resume();
	    worker.postMessage({
	        command: 'openWS',
	        config: {
                sampleRate: this.context.sampleRate,
                lang: $("#LANG").val(),
                type: $("#TYPE").val(),
                userId: $("#userId").val(),
                answerText: $("#answerText").val(),
                letterAnswerText : $("#AnswerText").val(),
                biz: $("#biz").val(),
                bookId: $("#bookId").val(),
                chapterId: $("#chapterId").val(),
                questionId: $("#questionId").val(),
                wssUrl : $("#wssUrl").val(),
                targetLetter : $("#targetLetter").val(),
                counter : $("#counter").val(),
                checkSymbol: $("#checkSymbol").val()
	        }
	      });    	
	      recording = true;
    }

    this.stop = function(){
    	this.context.suspend();
        worker.postMessage({
            command: 'closeWS',
            config: {
              sampleRate: this.context.sampleRate
            }
          });    	
      recording = false;
    }

    this.clear = function(){
      worker.postMessage({ command: 'clear' });
    }

    this.getBuffers = function(cb) {
      currCallback = cb || config.callback;
      worker.postMessage({ command: 'getBuffers' })
    }

    this.exportWAV = function(cb, type){
      currCallback = cb || config.callback;
      type = type || config.type || 'audio/wav';
      if (!currCallback) throw new Error('Callback not set');
      worker.postMessage({
        command: 'exportWAV',
        type: type
      });
    }

    this.exportMonoWAV = function(cb, type){
      currCallback = cb || config.callback;
      type = type || config.type || 'audio/wav';
      if (!currCallback) throw new Error('Callback not set');
      worker.postMessage({
        command: 'exportMonoWAV',
        type: type
      });
    }

    worker.onmessage = function(e){

//      var blob = e.data;
//      currCallback(blob);
    }

    source.connect(this.node);
    this.node.connect(this.context.destination);   // if the script node is not connected to an output the "onaudioprocess" event is not triggered in chrome.
  };

  Recorder.setupDownload = function(blob, filename){
//    var url = (window.URL || window.webkitURL).createObjectURL(blob);
//    var link = document.getElementById("save");
//    link.href = url;
//    link.download = filename || 'output.wav';
  }

  window.Recorder = Recorder;

})(window);
